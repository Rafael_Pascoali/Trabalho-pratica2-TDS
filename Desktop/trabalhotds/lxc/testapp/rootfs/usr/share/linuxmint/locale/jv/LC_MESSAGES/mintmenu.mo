��    E      D  a   l      �  	   �     �                    )  K   6  %   �  -   �     �  "   �  F        U     ^     t     �     �     �     �  	   �     �     �  	   �                    +  -   @     n     u     �     �     �     �     �     �     �     �     �     �     �     	     	     (	     /	  1   ;	  6   m	     �	     �	  $   �	     �	     �	     �	     
     5
     E
     [
     l
     �
  '   �
     �
     �
     �
     �
     �
        	          �  %     �  	   �     �     �     �  
   	  L     +   a  9   �     �  '   �  K     	   X     b     {     �     �  
   �     �     �     �     �  
             (     :     L  5   a     �      �     �     �     �     �                          (  
   =     H     V     ]  	   o     y  3   �  9   �     �     �  (        .     6     L     j     �  !   �     �     �     �  +        2     I     d     k     t     {     �     �     3                  ?   D   B   ,             "      )   '             #           ;                 @                   +         %   C   !   .       8      /          7   9           :                 <   5   0       6   E   *           1   =   (                         4           2      $   A                              	      >      
       &   -    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Enable Internet search Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: zaenal arifin <www.pilihankedua@gmail.com>
PO-Revision-Date: 2017-07-02 02:38+0000
Last-Translator: anandiamy <Unknown>
Language-Team: Javanese <jv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <durung diset> Sekitaran Menu Lanjut MATE Sedoyo Kabeh aplikasine Terapan'ne Nelusuri kabeh gludegkan lan pengendali disk lan map akses seko komputer iki Nelusuri lan masang cemepake prangkat lunak Nelusuri pratondo-buku lan sakgludegkan lokasine jaringan Nelusuri brekas sing keguwak Nelusuri panggonane perkakas nok destop Klik kanggo nyetel tombol akselerator anyar kanggo mbukak lan nutup menu.   Kompiuter Susunane sistem sampeyan Pusate Ngontroli Rak iso nginisial plagin Rak iso momot plagin Tetampilan empôk'e tetampil Ngubah sajian Ngosongi ekrak Ngaktifake panelusuran Internet Senengan'e Map Omah ngLebokno sekatan ngLebokno ruangan ngInstall paket '%s' Nginstal, ngGuwak, lan ngeapgred paket prangkat lunak Ngluncurake ngLuncurake ketiko aku log mlebu Ngunci Sekat Log metu utowo skakel pengguno Log metu Sajian Acuan'e sajian Nami Nami: Jejaringkerjo Mbukak map pribadimu Kekarep'an Manajer Paket Rintis Pilih akselerator Panggonan Acuane Pencet Backspace kanggo mbusak keybinding sing ana. Tekan Escape utawa klik maneh kanggo mbatalake operasi.   Mbedal Ngulangi plagin Elingi kategori terakhir utawa telusuran ngGuwak ngGuwak seko favorite Mbutuhno sandi kanggo mbukake nggoleki paket kanggo install Mileh sakwiji map Nampilno sedoyo aplikasi-aplikasi Tampil'no tombôl ikon Nampilno kelompok'e ikon Tampilno ing kesenenganku Ngademke, mbaleni nguripno, nundo, tah turu Manajer Prangkat lunak Nami swap lan nami jenerik Sistim Treminal Têma: Ekrak Mreteli ngGanggo garis printah 