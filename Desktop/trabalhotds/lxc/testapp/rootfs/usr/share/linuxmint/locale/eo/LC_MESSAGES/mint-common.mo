��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  #     #   )     M     i     �  =   �  	   �     �  6   �       )   )     S      Z  \   {     �                                       	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-06-21 19:19+0000
Last-Translator: Piet Coppens <Unknown>
Language-Team: Esperanto <eo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s da plia diskospaco estos uzitaj. %s da diskospaco estos malokupitaj. %s estos elŝutitaj entute. Aldonaj ŝanĝoj postulatas Okazis eraro Ne eblas forigi pakon %s, ĉar ĝi postulatas de aliaj pakoj. Flatpakoj Instali Pako %s estas deviga programoparto de la sekvaj pakoj: Pakoj forigotaj Bonvolu rigardi suban liston de ŝanĝoj. Forigi La sekvaj pakoj estos forigitaj: Ĉi tiu menuero ne rilatas al iu ajn pako. Ĉu vi ĉiaokaze deziras ĝin forigi el la menuo? Promocii 