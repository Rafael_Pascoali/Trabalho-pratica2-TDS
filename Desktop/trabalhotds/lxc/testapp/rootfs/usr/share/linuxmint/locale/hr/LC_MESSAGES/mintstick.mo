��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  (   �  2   �     �     �               0     9     G     ^  #   {  C   �     �  @  �     9  
   L     W     n     q     }     �  
   �     �  
   �  	   �     �  5   �  "        6  1   9     k     |  )     )   �     �     �  1      
   2     =     R  
   h     s     �     �     �  
   �     �  ,   �  ?   �  -   +  $   Y     ~  A   �  %   �  2        4  .   Q  <   �  /   �  1   �  (        H  I   d     �     �     �     �     �     �            ,   '     T     d  )   m     �     �  	   �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-26 17:40+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Greška se dogodila pri kopiranju slike. Greška se dogodila pri stvaranju particije na %s. Pojavila se greška Dogodila se greška. Greška ovjere. Izračunavanje… Provjeri Suma provjere Datoteke sume provjere Suma provjere se ne podudara Odaberite naziv vašeg USB uređaja Preuzmite ISO sliku ponovno. Njezina suma provjere se  ne podudara. Sve izgleda valjano! FAT32 
  + Kompatibilan svugdje.
  -  Ne može rukovati datotekama većim od 4GB.

exFAT
  + Kompatibilan gotovo svugdje.
  + Može rukovati datotekama većim od 4GB.
  -  Nije kompatibilan kao FAT32.

NTFS 
  + Kompatibilan s Windowsima.
  -  Nije kompatibilan s Mac-om i većinom hardverskih uređaja.
  -  Povremeni problemi s kompatibilnost na Linuxu (NTFS je vlasnički i napravljen obrnutim inženjeringom).

EXT4 
  + Moderan, stabilan, brz, dnevnički.
  + Podržava dozvole Linux datoteka.
  -  Nije kompatibilan s Windowsima, Mac-om i većinom hardverskih uređaja.
 Datotečni sustav: Formatiraj Formatiraj USB uređaj GB GPG potpisi GPG potpisana datoteka GPG potpisana datoteka: Idi natrag ISO potvrda ISO slika: ISO slike ISO: Ako vjerujete potpisu, možete vjerovati i ISO slici. Provjera cjelovitosti je neuspjela KB Ključ nije pronađen na poslužitelju ključeva. Lokalne datoteke MB Napravi USB uređaj za pokretanje sustava Napravi USB uređaj za pokretanje sustava Više informacija ID uređaja nije pronađen Nema dovoljno slobodnog prostora na USB uređaju. SHA256 sum SHA256 datoteka sume SHA256 datoteka sume: SHA256sum: Odaberi sliku Odaberi USB uređaj Odaberi sliku Potpisao: %s Veličina: TB SHA256 suma provjere ISO slike je nevaljana. SHA256 datoteka sume provjere ne sadrži sumu za ovu ISO sliku. SHA256 datoteka sume provjere nije potpisana. USB uređaj je uspješno formatiran. Suma provjere je nevaljana Suma provjere je ispravna ali istovjetnost sume nije provjerenea. GPG datoteka se ne može preovjeriti. GPG datoteka se ne može preuzeti. Provjerite URL. Slika je uspješno zapisana. Datoteka sume provjere se ne može provjeriti. Datoteka sume provjere se ne može preuzeti. Provjerite URL. Ova ISO slika je potvrđena pouzdanim potpisom. Ova ISO slika je potvrđena nepouzdanim potpisom. Ova ISO slika izgleda kao Windows slika. Ovo je službena ISO slika. Ovo će uništiti sve podatke na USB uređaju, sigurno želite nastaviti? URL-ovi USB zaspisnik slika USB uređaj Formatiranje USB uređaja USB uređaj: Nepoznat potpis Nepouzdan potpis Provjeri Provjeri vjerodostojnost i cjelovitost slike Naziv uređaja: Uređaj: Windows slike zahtijevaju posebnu obradu. Zapiši bajta nepoznato 