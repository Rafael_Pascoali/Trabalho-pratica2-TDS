��          �   %   �      0  w   1     �     �     �     �     �     �             ^   9     �     �     �     �     �     �  	          ,        G     N     U     r  �  �  �   d     �               %     C     [     c     x  e   �     �           1     7     L     e     |     �  -   �     �     �     �     �                                                   	                                  
                                          <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Language Language Settings Language settings Language support Languages None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Some language packs are missing Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-07-05 22:10+0000
Last-Translator: Indrit Bashkimi <indrit.bashkimi@gmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>Shënim: Instalimi ose përditësimi i paketave të gjuhës mund të shkaktojë instalimin e ndonjë gjuhe shtesë</small></b> Shto një gjuhë të re Shto një gjuhë të re Shto... Aplikoja të gjithë sistemit E instaluar plotësisht Instalo Instalo / hiq gjuhë Instalo / hiq gjuhë... Instalo ndonjë paketë gjuhe që mungon, skedarë të përkthimeve, fjalorë për gjuhën e zgjedhur Instalo paketat e gjuhës Instaloni gjuhën e përzgjedhur Gjuha Parametrat e Gjuhës Rregullimet për gjuhën Mbështetja gjuhësore Gjuhët Asnjë Numrat, valuta, adresat, njësitë matëse... Krahina Hiq Hiq gjuhën e zgjedhur Mungojnë disa paketa gjuhe 