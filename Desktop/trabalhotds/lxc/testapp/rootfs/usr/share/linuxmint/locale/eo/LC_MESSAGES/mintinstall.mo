��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  D     C   Q  &   �  %   �  $   �          
  	                  /  5   M  
   �     �  m   �            A   &  "   h     �  	   �     �     �     �  	   �     �     �     �     �  T   
  =   _     �     �     �     �     �  	   �     �  	   �  B      	   C     M     S     Z  
   y     �     �     �  [   �     )     7     C     K     O     T  5   [  /   �  
   �     �     �     �  	   �     �            ?   '  	   g     q     y     �  7   �  0   �  <   �     ;     Z     u     �     �     �  :   �  @   �     %     ;  	   A     K         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-07-25 14:21+0000
Last-Translator: Piet Coppens <Unknown>
Language-Team: Esperanto <eo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s por elŝuti, %(localSize)s da diskospaco liberigita %(downloadSize)s por elŝuti, %(localSize)s da diskospaco postulata %(localSize)s da diskospaco liberigita %(localSize)s da diskospaco postulata %d tasko ruliĝas %d taskoj ruliĝas 3D Pri Utilaĵoj Ĉiuj Ĉiuj aplikaĵoj Ĉiuj operacioj kompletigitaj Okazis eraro dum provo aldoni la flatpakan deponejon. Tabulludoj Ne eblas instali Ne eblas trakti ĉi tiun dosieron, dum estas aktivaj operacioj.
Bonvolu provi denove, post kiam ili finiĝis. Ne eblas forigi Babilado Klaku <a href='%s'>ĉi tie</a> por aldoni vian propran prijuĝon. Nun prilaborado de la sekvaj pakoj Detaloj Desegnado Elektaĵoj de la redaktisto Edukado Elektroniko Retpoŝto Imitiloj Fundamentoj Dosiera komunigo Unua-persono Flatpaka subteno nun ne disponeblas. Provu instali flatpakon kaj gir1.2-flatpak-1.0. Flatpaka subteno nun ne disponeblas. Provu instali flatpakon. Tiparoj Ludoj Grafiko Instali Instali novajn aplikaĵojn Instalite Instalitaj aplikaĵoj Instalado Instalado de ĉi tiu pako eble neripareble damaĝus vian sistemon. Interreto Ĝavo Lanĉi Limigi serĉon al nuna listado Matematiko Plurmediaj kodekoj Plurmediaj kodekoj por KDE Neniu kongrua pako trovita Ne estas pakoj por montri.
Eble ĉi tio indikas problemon - provu aktualigi la kaŝmemoron. Ne disponeble Neinstalite Oficejo PHP Pako Fotado Bonvolu pacienci. Ĉi tio eble prenos iom da tempo... Bonvolu uzi apt-get por instali ĉi tiun pakon. Programado Eldonado Python Realtempa strategio Aktualigi Aktualigi la liston de pakoj Forigi Forigado Forigo de ĉi tiu pako eble neripareble damaĝus vian sistemon. Prijuĝoj Skanado Scienco Scienco kaj edukado Serĉi en priskribo de pakoj (eĉ pli malrapida serĉo) Serĉi en resumo de pakoj (pli malrapida serĉo) Traserĉado de programaraj deponejoj. Bonvolu iomete atendi. Montri instalitajn aplikaĵojn Simulado kaj kurkonkursado Programarmastrumilo Sono Sono kaj video Sistem-iloj La flatpaka deponejo, kiun vi provas aldoni, jam ekzistas. Estas nun aktivaj operacioj.
Ĉu vi certas, ke vi volas forlasi? Vico-bazita strategio Video Montriloj Reto 