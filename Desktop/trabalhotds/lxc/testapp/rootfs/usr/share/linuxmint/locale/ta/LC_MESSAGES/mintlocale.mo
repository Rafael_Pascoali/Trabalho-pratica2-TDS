��          �   %   �      0  w   1     �     �     �     �     �     �               (     F     ]     {     �     �     �  	   �     �     �     �     �     �       �  %  2  �  <     8   P     �  Y   �  =   �  (   1     Z  :   p  @   �  D   �  M   1	     	  .   �	  .   �	     �	     
  O   
  !   m
     �
     �
  M   �
  Z                                                       
                            	                                                <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Install Install / Remove Languages Install / Remove Languages... Install language packs Install the selected language Language Language Settings Language settings Language support Languages No locale defined None Region Remove Remove the selected language Some language packs are missing Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-08-02 09:05+0000
Last-Translator: Kamala Kannan <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>குறிப்பு: மொழி தொகுப்புகளை நிறுவுதல் அல்லது மேம்படுத்துதல் கூடுதல் மொழிகளின் நிறுவலை தூண்ட முடியும்</small></b> ஒரு புதிய மொழியை சேர் புதிய மொழியை சேர்க்க சேர்... கணினி-முழுமைக்கும் செயற்படுத்து முழுமையாக நிறுவப்பட்ட உள்ளீட்டு முறை நிறுவுக மொழிகளை நிறுவ / நீக்க மொழிகளை நிறுவு / நீக்கு... மொழி தொகுப்புகளை நிறுவ தேர்ந்தெடுத்த மொழியை நிறுவு மொழி மொழி அமைப்புகள் மொழி அமைப்புகள் மொழி சேவை மொழிகள் மொழி வரையறுக்கப்பட்டவில்லை எதுவுமில்லை பகுதி அகற்று தேர்ந்தெடுத்த மொழியை நீக்கு சில மொழி தொகுப்புகள் காணவில்லை 