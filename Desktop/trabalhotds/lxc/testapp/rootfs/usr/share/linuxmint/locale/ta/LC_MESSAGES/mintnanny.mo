��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  [   �  �   V  V     >   b  8   �  =  �  C     >   \  @   �  �   �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-03-27 21:59+0000
Last-Translator: Sudhakaran Pooriyaalar <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ஒரு தகுந்தா வலைய நிர்வாகப் பெயர் தேர்ந்தெடுக்கப்பட்ட வலைய நிர்வாகப் பெயர்களுக்கு அணுகல்-ஐ முடக்கு முடக்கப்பட்ட வலைய நிர்வாகங்கள் வலைய நிர்வாக முடப்பான் வலைய நிர்வாகப் பெயர் வலைய நிர்வாகப் பெயர்கள் ஒரு ஆங்கிலயெழுத்தில் அல்லது ஒரு எண்ணில் தொடங்கி முடிய வேண்டும், மேலும் ஆங்கிலயெழுத்துகள், எண்கள், முற்றுப்புள்ளிகள் மற்றும் இணைப்புக்கோடுகள் ஆகியவையே இப்பெயர்கள் கொண்டிருக்கப் பெறும். எடுத்துக்காட்டு: my.number1domain.com தகுந்தா வலைய நிர்வாகம் பெற்றோரின் கட்டுப்பாடு தயவுச்செய்து நீங்கள் முடக்க வேண்டிய வலைய நிர்வாகப் பெயரைத் தட்டு அச்சு இடுக 