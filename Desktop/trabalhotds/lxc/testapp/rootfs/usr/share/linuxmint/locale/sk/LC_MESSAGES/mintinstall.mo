��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  C   e  ?   �  -   �  ,     $   D  :   i     �  
   �     �     �     �     �     �  >        B     N     U  q   n     �     �  1   �  !   -     O     [     i     p     �     �     �  
   �     �     �     �     �  j   �  V   M     �     �  +   �     �     �     �          !     1     L  ]   Z     �     �     �  -   �  
   �       2        Q     Y  p   x  /   �          %     7     C     Z     ^  
   e  ;   p  3   �     �     �     �               %     C     O     [  Z   j     �  
   �     �     �  F   �  E   9  .     $   �     �  
   �     �  0        7     <     I     \  C   r  G   �     �          1     B  a   V     �     �     �     �     �  "   �     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-03 15:00+0000
Last-Translator: menom <Unknown>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s sa stiahne, %(localSize)s bude uvoľnené na disku %(downloadSize)s sa stiahne, %(localSize)s potrebných na disku %(localSize)s diskového priestoru uvoľnené %(localSize)s vyžaduje diskového priestoru %d Recenzia %d Recenzie %d Recenzií Prebieha %d úloha Prebiehajú %d úlohy Prebieha %d úloh 3D O programe Príslušenstvo Pridať Všetko Všetky aplikácie Všetky operácie sú hotové Pri pokuse o pridanie flatpakového repozitára nastala chyba. Stolné hry Vetva: Nemôžem nainštalovať Tento súbor nie je možné spracovať, kým prebiehajú operácie.
Prosím, skúste to znova po ich dokončení. Nemôžem odstrániť Chat Klikni <a href='%s'>sem</a> na pridanie recenzie. Pracuje sa na týchto balíčkoch Podrobnosti Dokumentácia Kresba Odporúčané programy Vzdelávanie Elektronika E-mail Emulátory Editory Zdieľanie súborov 3D hry Flatpak (%s) Podpora pre Flatpak momentálne nie je k dispozícii. Skúste nainštalovať flatpak a gir1.2-flatpak-1.0. Podpora pre Flatpak momentálne nie je k dispozícii. Nainštalujte, prosím, Flatpak. Písma Hry Generuje sa vyrovnávacia pamäť, čakajte Grafika Domovská stránka Inštalovať Inštalovať nové aplikácie Nainštalovaný Nainštalované aplikácie Inštaluje sa Nainštalovanie tohto balíka by mohlo spôsobiť nenapraviteľné škody vo vašom systéme. Internet Java Spustiť Obmedziť vyhľadávanie na aktuálnu listinu Matematika Multimediálne kodeky Multimediálne kodeky pre pracovné prostredie KDE Názov: Nenašiel sa žiaden balíček Nenašli sa žiadne balíky.
Je možné, že sa vyskytol problém - skúste aktualizovať vyrovnávaciu pamäť. Nie sú k dispozícii žiadne snímky obrazovky Nedostupné Nenainštalované Kancelária Voliteľné komponenty PHP Balík Fotografia Prosím buďte trpezliví, môže to trvať nejaký čas... Prosím použite apt na inštaláciu tohto balíka. Programovanie Publikovanie Python Real-time stratégie Aktualizovať Aktualizovať zoznam balíkov Diaľkové: Odstrániť Odstraňuje sa Odstránenie tohto balíka by mohlo spôsobiť nenapraviteľné škody vo vašom systéme. Recenzie Skenovanie Veda Veda a vzdelávanie Hľadať v celých popisoch balíčkov (veľmi pomalé vyhľadávanie) Hľadať v stručných popisoch balíčkov (pomalšie vyhľadávanie) Hľadám repozitáre, zdroje softvéru, moment Zobraziť nainštalované aplikácie Simulácie a preteky Veľkosť: Správca softvéru Niečo sa pokazilo. Kliknutím to skúste znova. Zvuk Zvuk a video Systémový balík Systémové nástroje Flatpakový repozitár, ktorý sa pokúšate pridať, už existuje. Práve beží niekoľko operácií.
Ste si istý, že chcete skončiť? Toto je systémový balík Tento balík je Flatpak Skúste to znova Ťahové stratégie Nedá sa komunikovať so servermi. Skontrolujte svoje internetové pripojenie a skúste to znova. Nedostupné Verzia: Video Prehliadače Web Správca balíkov vášho systému 