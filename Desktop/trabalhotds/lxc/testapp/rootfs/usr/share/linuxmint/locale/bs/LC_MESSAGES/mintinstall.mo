��    -      �  =   �      �  ?   �  B   !  !   d  $   �     �     �     �     �     �     �     �  	   �     �  	   �     �                         %  	   >     H     Q     _     m     t     |  +   �     �  
   �     �     �     �     �     �  3     *   @     k     �     �     �     �     �     �    �  L   �  I   *	  .   t	  +   �	     �	  
   �	     �	     �	     �	     �	     �	     
     
  	   
     #
     5
     =
     B
  
   J
     U
     p
     |
     �
     �
     �
     �
     �
  :   �
                    9  	   @  
   J     U  1   i  .   �     �     �     �               )     5                $              !      (         -   "   ,              #                                        +                          &   	      
                *                    %      '           )          %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-07-11 14:04+0000
Last-Translator: Almir Zulic <zalmir@yahoo.com>
Language-Team: Bosnian <bs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s za preuzeti, %(localSize)s prostora na disku je oslobođeno %(downloadSize)s za preuzeti, %(localSize)s prostora na disku je potrebno %(localSize)s prostora na disku je oslobođeno %(localSize)s prostora na disku je potrebno 3D O programu Alati Igre na ploči Chat Detalji Crtanje Obrazovanje Email Emulatori Razmjena datoteka Fontovi Igre Grafika Instaliraj Instaliraj nove aplikacije Instalirano Internet Nije dostupno Nije instalirano Uredski programi Paket Fotografija Molimo koristite apt-get da bi ste instalirali ovaj paket. Programiranje Izdavaštvo Strategije u realnom vremenu Ukloni Recenzije Skeniranje Nauka i obrazovanje Pretraži po opisu paketa (još sporija pretraga) Pretraži po rezimeu paketa (sporija pretraga) Simulacije i utrke Upravljanje aplikacijama Zvuk i video Sistemski alati Turn-based strategije Preglednici Web 