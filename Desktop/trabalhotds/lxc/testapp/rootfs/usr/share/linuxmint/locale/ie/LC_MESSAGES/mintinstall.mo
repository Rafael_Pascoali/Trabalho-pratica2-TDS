��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  D     F   [  &   �  (   �  $   �                    *     /     B  8   ^     �     �  b   �           5  :   :     u  	   �     �  
   �  	   �     �     �  
   �  
   �     �     �  Y     D   ^     �     �     �  	   �     �  	   �     �        J        W     `     e  !   l  
   �     �     �  "   �  Q   �     @     N     [     b     f  
   n  ,   y  -   �     �     �     �     �  	             9  	   A  F   K     �  
   �     �     �  9   �  3     2   5      h     �     �     �     �     �  1   �  7        D     Y     _     g         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-12-25 09:04+0000
Last-Translator: Silvara <Unknown>
Language-Team: Interlingue <ie@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ie
 %(downloadSize)s a descargar, %(localSize)s de disc-spacie a liberar %(downloadSize)s a descargar, %(localSize)s plu de disc-spacie besonat %(localSize)s de disc-spacie a liberar %(localSize)s plu de disc-spacie besonat %d tache es activ %d taches es activ 3D Pri Accessories Omni Omni applicationes Omni operationes es complet Un error evenit penante adjunter li repositoria Flatpak. Ludes de table Ne successat installar Ne posse processar ti-ci file quande hay operationes ancor activ.
Ples repenar pos lor completion. Ne successat remover Chat Fa un clic <a href='%s'>ci</a> por adjunter vor recension. Processante li sequent paccages Detallies Dessine Lu max bon Education Electronica E-post Emulatores Essentiale Partition de files Prim person Suporte de flatpak ne es bentost disponibil. Pena installar flatpak e gir1.2-flatpak-1.0. Suporte de flatpak ne es bentost disponibil. Pena installar flatpak. Fondes Ludes Grafica Installar Installar nov applicationes Installat Installat applicationes Installante Installation de ti-ci paccage posse dar ínreparabil damage a vor sistema. Internet Java Lansar Limitar li sercha al actual liste Matematica Codecs de multimedia Codecs de multimedia por KDE Null correspondent paccages trovat Null paccages a monstrar.
To posse indicar un problema - pena refriscar li cache. Índisponibil Ne installat Oficie PHP Paccage Fotografie Patientie. To va postular alquant témpor... Ples usar apt-get por installar ti-ci paccage Programmation Publication Python Strategies de real tempore Refriscar Refriscar li liste de paccages Remover Removente Remotion de ti-ci paccage posse dar ínreparabil damage a vor sistema. Recensiones Scannation Scientie Scientie e education Serchar in descritiones de paccages (sercha mem plu lent) Serchar in summariums de paccages (sercha plu lent) Serchante repositorias de programmas, ples atender Monstrar installat applicationes Simulatores e curses Gerente de programmas Son Son e video Utensiles del sistema Li repositoria Flatpack que vu adjunte ja existe. Quelc operationes es ancor activ.
Esque vu vole surtir? Strategies de tornes Video Visores Web 