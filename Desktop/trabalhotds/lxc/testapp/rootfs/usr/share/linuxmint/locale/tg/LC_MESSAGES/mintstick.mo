��          �   %   �      0  )   1  2   [     �     �      �     �     �     �       "   $     G     T     g  )   w  #   �  N   �       	   %     /  
   C     N     \     b  �  j  h   3  f   �  &     ,   *  D   W     �  ,   �  A   �  A   %  4   g  (   �  !   �  7   �  I   	  L   i	  �   �	  (   h
     �
  -   �
     �
     �
     �
                     	   
                                                                                                            An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: Victor Ibragimov <victor.ibragimov@gmail.com>
PO-Revision-Date: 2021-04-14 17:19+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <tg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: tg
 Ҳангоми нусхабардории тасвири диск хатогӣ ба вуҷуд омад. Ҳангоми эҷодкунии қисми диск дар %s хатогӣ ба вуҷуд омад. Хатогӣ ба вуҷуд омад. Хатои санҷиши ҳаққоният Номеро барои драйви USB интихоб намоед Формат кардан Формат кардани драйви USB Таҳия кардани драйви боршавандаи USB Таҳия кардани драйви боршавандаи USB Драйви USB фазои кофӣ надорад. Интихоби тасвири диск Интихоби драйви USB Тасвири дискеро интихоб кунед Драйви USB бо муваффақият формат шудааст. Тасвири диск бо муваффақият сабт шудааст. Ин амал ҳамаи иттилоотро дар драйви USB хароб мекунад. Шумо мутмаин ҳастед, ки мехоҳед идома диҳед? Сабти тасвири диски USB Драйви USB Форматкунандаи драйви USB Драйви USB: Барчаспи ҳаҷм: Сабт кардан номаълум 