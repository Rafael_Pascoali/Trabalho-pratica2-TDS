��          �      \      �      �     �          2     R  =   d  	   �     �     �  5   �     �  0   
     ;  	   A     K     R  '   _  _   �     �  �  �  )   p     �     �  &   �     �  O        \     n       6   �     �  0   �          #     4     C  -   Z  \   �     �                                  	                                                     
            %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: mint-common
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-12-06 14:02+0000
Last-Translator: KAMI <kami911@gmail.com>
Language-Team: Hungarian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: hu
 %s lemezterület lesz használatba véve. %s lemezterület szabadul fel. Összesen %s lesz letöltve. További változtatások szükségesek Hiba történt Nem távolítható el a(z) „%s” csomag, mert más csomagok függenek tőle. Visszafejlesztés Flatpak csomagok Telepítés A(z) %s csomag függősége a következő csomagoknak: Eltávolítandó csomagok Nézze meg a változtatások alábbi listáját. Teljes eltávolítás Újratelepítés Eltávolítás Frissítés kihagyása A következő csomagok lesznek eltávolítva: Ez a menüpont semmilyen csomaghoz nem kapcsolódik. Mindenképp eltávolítsuk a menüből? Frissítés 