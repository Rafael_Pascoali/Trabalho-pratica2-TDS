��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  m     g   �  G   �  <   2  "   o     �     �     �     �     �  8   �  X   "  !   {     �  �   �     �     �  _   �  D   '     l  
   }     �  
   �     �     �     �     �     �       �   )  x   �     1     ?     O     \  %   c     �  #   �     �  x   �     D     S     \  @   e     �  '   �  4   �  '     �   :     �       
             "  
   +  @   6  P   w     �     �     �      �       0   +     \     k  �   z     �               #  ^   <  P   �  B   �  .   /  #   ^     �     �     �  $   �  j   �  m   K     �  
   �     �     �         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-06-26 11:14+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Persian <fa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ‏%(downloadSize)s برای بارگیری، %(localSize)s از فضای دیسک اشغال خواهد شد ‏%(downloadSize)s برای بارگیری، به %(localSize)s از فضای دیسک نیاز است ‏%(localSize)s از فضای دیسک را اشغال کرده است ‏به %(localSize)s از فضای دیسک نیاز است %d وظیفه در حال اجرا سه‌بعدی درباره لوازم ضروری همه تمام برنامه‌ها تمامی فرایندها به اتمام رسیدند خطایی هنگام تلاش برای افزودن مخزن فلت‌پک رخ داد. بازی‌های تخته‌ای نمی‌توان نصب کرد تا وقتی که فرایندهای فعالی وجد دارند، نمی‌توان این پرونده را پردازش کرد.
لطفاً بعد از این که آن‌ها تمام شدند، دوباره سعی کنید. نمی‌توان برداشت گپ برای افزودن بازبینی خود، <a href='%s'>این‌جا</a> کلیک کنید. هم‌اکنون در حال کار روی بسته‌های زیر جزئیات ‌ طراحی انتخاب سردبیران آموزش الکترونیک رایانامه شبیه‌سازها ملزومات هم‌رسانی پرونده اول‌شخص پشتیبانی از فلت‌پک هم‌اکنون موجود نیست. سعی کنید flatpak و gir1.2-flatpak-1.0 را نصب کنید. پشتیبانی از فلت‌پک هم‌اکنون موجود نیست. سعی کنید flatpak را نصب کنید. قلم‌ها بازی‌ها گرافیک نصب نصب برنامه‌های جدید نصب شده برنامه‌های نصب شده نصب کردن نصب این بسته می‌تواند آسیب‌های تعمیرناپذیری به سامانه‌تان بزند. اینترنت جاوا اجرا محدود کردن جست‌وجو به سیاههٔ کنونی ریاضیات کدک‌های چندرسانه‌ای کدک‌های چندرسانه‌ای برای KDE بستهٔ مرتبطی یافت نشد بسته‌ای برای نمایش وجود ندارد.
شاید این یک مشکل را نشان می‌دهد - سعی کنید انباره را تازه‌سازی کنید. موجود نیست نصب نشده اداری PHP بسته عکاسی لطفاً صبور باشید. کمی زمان می‌برد… لطفاً از apt-get برای نصب این بسته استفاده کنید. برنامه‌نویسی نشر پایتون استراتژی بی‌درنگ تازه‌سازی تازه‌سازی سیاههٔ بسته‌ها برداشتن برداشتن برداشتن این بسته می‌تواند آسیب‌های تعمیرناپذیری به سامانه‌تان بزند. بازبینی‌ها پویش علمی علمی و آموزشی جست‌وجو در توضیحات بسته‌ها (باز هم جست‌وجوی کندتر) جست‌وجو در خلاصهٔ بسته‌ها (جست‌وجوی کندتر) جست‌وجوی مخازن نرم‌افزاری، یک لحظه نمایش برنامه‌های نصب شده شبیه‌سازی و مسابقه مدیر نرم‌افزار صدا صدا و ویدیو ابزارهای سامانه‌ای مخزن فلت‌پکی که قصد افزودنش را دارید، هم‌اکنون وجود دارد. هم‌اکنون چند فرایند فعالند.
مطمئنید که می‌خواهید ترک کنید؟ استراتژی نوبتی ویدیو نمایشگرها وب 