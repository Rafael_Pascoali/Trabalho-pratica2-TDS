��          t      �              %   0     V     f     u  q   �     �             -   1  q  _  !   �  (   �          4  	   J  �   T  !   �            6   ,                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: LMDE
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-01-22 06:50+0000
Last-Translator: Butterfly <Unknown>
Language-Team: Linux Mint Türkiye
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s geçerli bir alan adı değil. Seçilen alan adlarına erişimi engelle Engellenen alan adları Alan Adı Engelleyici Alan adı Alan adları bir harf ya da sayı ile başlayıp bitmelidir, ve sadece harf, sayılar, noktalar ve tire içerebilir. Türkçe harfler içeremez. Örnek: benim.1sayilialanadim.com Geçersiz Alan Adı Ebeveyn Denetimi Lütfen engellemek istediğiniz alan adını yazınız 