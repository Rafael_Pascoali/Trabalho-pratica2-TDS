��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  @   �  }   5  F   �  (   �     #  D  =  (   �  (   �  =   �  �                          
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-07-31 15:00+0000
Last-Translator: Pratyay Mondal <Unknown>
Language-Team: Bengali <bn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s কোনও বৈধ ডোমেন নাম নয়। নির্বাচিত ডোমেন নামগুলিতে প্রবেশ নিষিদ্ধ করুন বাঁধাপ্রাপ্ত ডোমেইনসমূহ ডোমেইন ব্লকার ডোমেন নাম ডোমেন নামগুলি অক্ষর বা সংখ্যার সাথে শুরু এবং শেষ হওয়া আবশ্যক, এবং এর মাঝে কেবল অক্ষর, সংখ্যা, বিন্দু এবং হাইফেন থাকতে পারে। উদাহরণ: my.number1domain.com অকার্যকর ডোমেন প্যারেন্টাল কন্ট্রোল আপনি যে ডোমেন নামটি ব্লক করতে চান, তা দয়া করে টাইপ করুন 