��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  $   �  ,        J     Z     h  �   {  "   �     "     ;  9   P                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-07-01 14:24+0000
Last-Translator: Silvio Celeste <Unknown>
Language-Team: Neapolitan <nap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nunn'è nu nomme 'e dominio buono Blocc l'access 'e nomm 'e dominio selezionat Domini bloccati Blocco domini Nomme d''o dominio I nommi 'e Dominio anna accummencià e fernì cu na lettera o nu nummero e ponn contenè sulament lettere, nummeri, punt e trattin. Pe dicere: mio.dominio-numero1.com Dominio ca nunn'è buono Cuntroll 'e famiglia Agg pacienz, scrivi 'o nomme d''o dominio ca vuò bloccà 