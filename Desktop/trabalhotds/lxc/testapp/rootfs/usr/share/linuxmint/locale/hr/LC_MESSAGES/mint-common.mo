��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  -   *  +   X     �     �  7   �  #   �  )     +   D  !   p  $   �     �  B   �     	     (	     0	  
   ?	  (   J	     s	      �	     �	     �	     �	     �	  $   �	  `   
     n
  	   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-30 16:31+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s više diskovnog prostora će biti zauzeto. %s diskovnog prostora će biti oslobođeno. %s biti će ukupno preuzeto. Dodatne promjene su potrebne Dodatni softver biti će vraćen na prijašnju inačicu Dodatni softver biti će instaliran Dodatni softver biti će potpuno uklonjen Dodatni softver biti će ponovno instaliran Dodatni softver biti će uklonjen Dodatni softver biti će nadograđen Greška se dogodila Nemoguće je ukloniti paket %s jer ga upotrebljavaju drugi paketi. Vrati prijašnju inačicu Flatpak Flatpak paketi Instaliraj Paket %s je zavisnost sljedećih paketa: Paketi za uklanjanje Pogledajte zapis promjena ispod. Potpuno ukloni Ponovno instaliraj Ukloni Preskoči nadogradnju Sljedeći paketi će biti uklonjeni: Ova stavka izbornika nije pridružena nijednom paketu. Želite li ju ipak ukloniti iz izbornika? Nadopuna će biti preskočena Nadogradi 