Ţ          ä   %   Ź      @  )   A  2   k          ą      Ç    č     ő     ü          )  "   A     d     q       )     #   ž  N   â     1  	   B     L  
   `     k     y           T     T   i     ž     Ű  .   ű    *	     6  $   F  "   k  "     3   ą     ĺ     ü       ;   .  0   j  m        	  	   $     .  
   F     Q     j     w                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-05-22 02:15+0000
Last-Translator: samson <Unknown>
Language-Team: Amharic <am@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ááľáá áŽá á ááľá¨á áá­ áĽááłá áľáá°áľ á°ááĽáŻá áľáá°áľ á°ááĽáŻá á­áááá­ á ááá á­ áá­ áĽááłá %s. áľáá°áľ á°ááĽáŻá á¨áá¨áááŤ áľáá°áľ áľá á­áá¨áĄ á áĽá­áľá USB Stick FAT32 
  + ááá áŚáł á°áľáá áá
  -  ááá­ áá á¨ 4ááŁ á áá­ áá­áá˝ ááŤá á á­á˝áá

exFAT
  + ááá áŚáł á°áľáá áá
  + á¨ 4ááŁ á áá­ áá­áá˝ ááŤá á­á˝áá
  -  á°áľáá á á­á°áá áĽáá° FAT32.

NTFS 
  + á¨ Windows áá­ á°áľáá áá.
  -  á°áľáá á á­á°áá á¨ Mac áĽá á¨ á á­áŤáł á¨ á ááŤáŤ á á­áá˝ áá­
  -  á ááłááľ áá á¨ Linux áá­ á á­áľáá (NTFS is proprietary and reverse engineered).

EXT4 
  + áááá: á¨ á°á¨áá: ááŁá: journalized.
  + á¨ ááá­áľ á¨ áá­á áááśá˝ á­á°ááá
  -  á°áľáá á á­á°áá á¨ Windows, Mac áĽá á¨ á á­áŤáł á áŤáá˝ áá­
 á ááŤá¨áĽ á¨ USB stick Format ááľá¨ááŤ bootable USB stick ááá áŞáŤ bootable USB stick ááá áŞáŤ á  USB stick áá­ á á áŁáś áŚáł á¨áá ááľá á­áá¨áĄ á¨ USB stick á­áá¨áĄ ááľá á­áá¨áĄ á¨ USB stick á áááŁ á°áłá­áś formatted ááá ááľá á áááŁ á°áłá­áś á°á˝áá á  USB stick áá­ áŤáá áłáł á áá á­á áááĄ á áĽá­ááĽ ááá á á­áááá? USB ááľá ááťááŤ USB Stick á¨ USB Stick Formatter USB stick: á¨ áá á á°á¨á: ááťááŤ áŤááłáá 