��    D      <  a   \      �  	   �     �     �                 K   &  %   r  -   �     �  "   �  F   �     E     N     d     s     �     �     �  	   �     �  	   �     �     �     �       -        G     N     c     o     �     �     �     �     �     �     �     �     �     �     �     	     	  1   	  6   F	     }	     �	  $   �	     �	     �	     �	     �	     
     
     4
     E
     Y
  '   n
     �
     �
     �
     �
     �
     �
  	   �
     �
  �  �
     �     �     �     �     �     �  [   �  +   U  3   �      �  #   �  S   �  
   N     Y     r  $   �  "   �  
   �     �     �     �  
   	          %     :     L  2   f     �     �     �  &   �     �     
          &     +     1     6     S     \     o     t     �     �  D   �  >   �     #     )  &   ?     f     n      �     �     �     �     �           0  (   K     t  &   �     �     �     �     �     �     �     2                   >   C   A   +             !      (   &             "           :                 ?                   *         $   B       -       7      .          6   8           9                 ;   4   /       5   D   )           0   <   '                        3           1      #   @                              	      =      
       %   ,    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-03-17 13:02+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Galician <gl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <sen estabelecer> Sobre Nenú avanzado de MATE Todo Todos os aplicativos Aplicativos Examinar todos os ficheiros locais e remotos e cartafoles accesíbeis desde este computador Examinar e instalar o software dispoñíbel Examinar as redes locais e localizacións marcadas Examinar os ficheiros eliminados Examinar os elementos do escritorio Prema para estabelecer un novo acelerador de teclado para abrir e pechar o menú.   Computador Configurar o seu sistema Centro de control Non foi posibel iniciaar o engadido: Non foi posibel cargar o engadido: Escritorio Tema de escritorio Editar o menú Baleirar o lixo Preferidos Cartafol persoal Inserir un separador Inserir un espazo Instalar o paquete «%s» Instalar, retirar e anovar os paquetes de software Iniciar Iniciar cando inicie sesión Bloquear a pantalla Saír da sesión ou cambiar de usuario Saír da sesión Menú Preferencias do menú Nome Nome: Rede Abrir o seu cartafol persoal Opcións Xestor de paquetes Ruta Escolla un acelerador Lugares Preferencias Prema «Espazo atrás» para limpar a actual combinación de teclas. Prema «Escape»ou prema de novo para cancelar a operación.   Saír Recargar os engadidos Lembrar a última categoría  ou busca Retirar Retirar de preferidos O desbloqueo require contrasinal Busca de paquetes a instalar Seleccionar un cartafol Amosar todos os aplicativos Amosar a icona no botón Amosar as iconas das categorías Amosar nos meus preferidos Apagar, reiniciar, suspender ou hibernar Xestor de software Intercambiar o nome e o nome xenérico Sistema Terminal Tema: Lixo Desinstalar Usar a liña de ordes 