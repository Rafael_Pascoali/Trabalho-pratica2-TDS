��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  C   _  ?   �  4   �  !     P   :     �  3   �     �     �     �     �     �       (   	  )   2     \     x     �  ,        A     ^     {     �     �     �  %   �     �     �     �     �  u   �     G     P     V  5   \     �     �  	   �  .   �  #   �  +        ;  *   V     �     �     �     �     �     �     �  $   �  +   !      M     n     �  7   �     �     �     �                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2010-10-26 14:38+0000
Last-Translator: Yngve Spjeld-Landro <l10n@landro.net>
Language-Team: Norwegian Nynorsk <nn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s finst ikkje i innstillingsfila funnen under %(2)s eller %(3)s %(percentage)s av %(number)d filer - lastar opp til %(service)s %(percentage)s av 1 fil - lastar opp til %(service)s %(size_so_far)s av %(total_size)s %(size_so_far)s av %(total_size)s - %(time_remaining)s står att (%(speed)s/sek) %s-eigenskapar <b>Skriv namnet til den nye opplastingstenesta:</b> Om B Avbryt Avbryta opplastinga? Kontroller sambandet Lukk Klarte ikkje å finna tilgjengeleg plass Klarte ikkje å lagre inntillingsendringa Definer opplastingstenester Opplastingskatalog. <TIMESTAMP> er bytt ut med det noverande tidsstempelet i samsvar med formatet som er i bruk. Standardval: . Vil du avbryte opplastinga? Drag &amp: slepp her for å laste opp til %s Fila er for stor for tenesta Fila er for stor for tenesta Fila vart lasta opp. GB GiB Vert: Standard vertsnamn eller IP-adresse:  kB KiB MB MiB Standardassord: SCP-tilkoplingar er utan, FTP-tilkoblingar nyttar tom streng, SFTP-tilkoplingar nyttar ~/.ssh-nøklar Passord: Stig: Port: Fjernport, standard er 21 for FTP, 22 for SFTP og SCP Køyr i bakgrunnen Tenestenamn: Tenester: Lasta opp %(number)d filer til «%(service)s» Lasta opp 1 fil til «%(service)s» Opplastinga til '%(service)s' vart avbroten Denne tenesta krev passord Tidsstempelformat (strftime). Standardval: Tidsstempel: Skriv: Nettadresse (URL): Ukjend teneste: %s Opplastingsrettleiar Opplastingsrettleiar… Opplastingstenester Klarte ikkje å laste opp til '%s':  Lastar opp %(number)d filer til %(service)s Lastar opp 1 fil til %(service)s Lastar opp fila… Brukar: Brukarnamn, standard er å bruke ditt lokale brukernamn _Fil _Hjelp tilkopla utan feil 