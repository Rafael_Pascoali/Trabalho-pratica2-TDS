��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  0   +  )   \  &   �     �  .   �  *   �  ,   '  .   T  +   �  0   �     �  8   �     *	     3	     ;	     D	  0   J	     {	  %   �	     �	     �	     �	     �	  *   �	  m   
  #   t
  	   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-05 10:12+0000
Last-Translator: Rhoslyn Prys <rprys@posteo.net>
Language-Team: Welsh <cy@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Bydd %s mwy o le ar ddisg yn cael ei ddefnyddio. Bydd %s o le ar ddisg yn cael ei ryddhau. Bydd %s yn cael ei lwytho i lawr i gyd Mae angen newidiadau ychwanegol Bydd meddalwedd ychwanegol yn cael ei israddio Bydd meddalwedd ychwanegol yn cael ei osod Bydd meddalwedd ychwanegol yn cael ei ddileu Bydd meddalwedd ychwanegol yn cael ei ail osod Bydd meddalwedd ychwanegol yn cael ei dynnu Bydd meddalwedd ychwanegol yn cael ei ddiweddaru Digwyddodd gwall Methu tynnu pecyn %s am fod ei angen ar becynnau eraill. Israddio Flatpak Flatpaks Gosod Mae'r pecynnau canlynol yn dibynnu ar becyn %s : Pecynnau i'w tynnu Edrychwch ar y rhestr newidiadau isod Tynnu Ailosod Tynnu Hepgor y diweddaru Bydd y pecynnau canlynol yn cael eu tynnu: Nid yw'r eitem dewislen yma wedi ei gysylltu ag unrhyw becyn. Hoffech chi ei dynnu o'r ddewislen beth bynnag? Bydd diweddariadau'n cael eu hepgor Diweddaru 