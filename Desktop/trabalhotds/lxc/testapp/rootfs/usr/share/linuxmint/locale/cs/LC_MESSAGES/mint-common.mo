��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  3   -  *   a     �  $   �  3   �  #   �  %   !  -   G  $   u  #   �     �  E   �     	     +	     3	     <	  9   I	     �	  .   �	  	   �	     �	     �	     �	  )   �	  l   )
     �
     �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 19:18+0000
Last-Translator: Marek Hladík <mhladik@seznam.cz>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s dalšího prostoru na úložišti bude využito. %s prostoru na úložišti bude uvolněno. Celkem bude staženo %s Jsou požadovány dodatečné změny Dodatečný software bude vrácen na starší verzi Bude nainstalován další software Dodatečný software bude vyčištěn Dodatečný software bude znovu nainstalován Dodatečný software bude odstraněn Dodatečný software bude povýšen Došlo k chybě Nelze odstranit balíček %s, protože ho vyžadují jiné balíčky. Vrátit starší verzi Flatpak Flatpaky Nainstalovat Balíček %s je závislostí následujících balíčků: Balíčky k odebrání Prohlédněte si níže uvedený seznam změn. Vyčistit Přeinstalovat Odebrat Vynechat povýšení Následující balíčky budou odebrány: Tato položka nabídky nesouvisí s žádným softwarovým balíčkem. Chcete ji i tak odstranit z nabídky? Aktualizace budou přeskočeny Přejít na novější verzi 