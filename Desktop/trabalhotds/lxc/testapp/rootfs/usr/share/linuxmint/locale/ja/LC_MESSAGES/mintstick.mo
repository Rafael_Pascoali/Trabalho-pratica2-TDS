��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  ?   �  N   �  !   	     5	  (   H	  l  q	     �  +   �  /     &   M  1   t     �     �     �  :   �  3   6  e   j     �     �  "   �          -     G     T                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: https://launchpad.net/~linuxmint-translation-team-japanese
PO-Revision-Date: 2020-12-21 20:00+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Japanese <https://launchpad.net/~linuxmint-translation-team-japanese>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ja
 イメージのコピー中にエラーが発生しました。 %s 上へのパーティション作成時にエラーが発生しました。 エラーが発生しました。 認証エラー。 USB メモリの名前を選択します FAT32 
  + どの環境でも互換性があります。
  -  4GB を超えるファイルは扱えません。

exFAT
  + ほとんどの環境で互換性があります。
  + 4GB 以上のファイルも扱えます。
  -  FAT32 よりも互換性は劣ります。

NTFS 
  + Windows 環境との互換性があります。
  -  Mac およびほとんどのハードウェア機器との互換性がありません。
  -  Linux 環境でも互換性の問題が生じるかもしれません (NTFS の仕様はプロプライエタリで、リバースエンジニアリングされたものです)。

EXT4 
  + 近代的で、安定しており、速く、信頼性があります。
  + Linux のファイルパーミッションをサポートしています。
  -  Windows, Mac およびほとんどのハードウェア機器との互換性がありません。
 フォーマット USB メモリをフォーマットします 起動可能な USB メモリを作成します 起動可能な USB メモリの作成 USB メモリの空き領域が足りません。 イメージの選択 USB メモリを選択します イメージを選択します USB メモリのフォーマットに成功しました。 イメージの書き込みに成功しました。 この操作により USB メモリのデータが全て削除されます。よろしいですか？ USB イメージライター USB メモリ USB メモリフォーマッター USB メモリ: ボリュームラベル: 書き込み 不明 