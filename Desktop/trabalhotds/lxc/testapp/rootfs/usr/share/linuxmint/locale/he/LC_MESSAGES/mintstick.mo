Þ    O        k         ¸  )   ¹  2   ã          (     ;     Q     `     f     o     ~        :   ±     ì         
     
     #
     6
     9
     H
     X
     i
     q
  
   
  
   
     
  1   
     Ï
     æ
     é
                    .     F     W  "   j  
             ©  
   »     Æ     Ó     æ     ö          
  -     >   ;  #   z  )        È  I   à  "   *  4   M  #     #   ¦  5   Ê  2      5   3  $   i       N   ­     ü       	          
   0     ;     M     a  2   h          ©  *   ±     Ü     â     è  ¢  ð  3     +   Ç     ó     	           7  
   P     [  !   q  #     *   ·  s   â     V    r     ù  
              <     C     R     h                    ¯     È  <   Í  /   
     :  /   A     q       -     -   À     î        =        \     n  )         ³     Ô     è            	   3     =  K   D  j     =   û  .   9  #   h  S     6   à  ]     #   u  I     p   ã  N   T  W   £  X   û  ,   T  E        Ç     Ô     ê      ü          0     K  
   f  B   q     ´     Ç  R   Ï  
   "     -     6     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-29 05:02+0000
Last-Translator: Avi Markovitz <avi.markovitz@gmail.com>
Language-Team: Hebrew <he@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ×××¨×¢× ×©×××× ××¢×ª ××¢×ª×§×ª ×××××ª. ×××¨×¢× ×©×××× ××¢×ª ××¦××¦×ª %s. ×××¨×¢× ×©×××× ×××¨×¢× ×©××××. ×©××××ª ×××××ª. ××ª××¦×¢ ×××©××... ××××§× ×¡××× ×××§××¨×ª ×§×××¥ ×¡××××Ö¾×××§××¨×ª ×¡××× ×××§××¨×ª ×× ×ª××× × × ×××××¨ ×©× ××××¡× ×× ××× × × ××××¨×× ××ª ××ª ×ª××× ×ªÖ¾×××£ ×Ö¾ISO ×©××. ×¡××××Ö¾××××§××¨×ª ×©×× ××× × ×ª×××. ×××× × ×¨×× ×ª×§××! FAT32 
  + ×ª××× ××××.
  - ×× ×ª××× ××§××¦×× ×××××× ×Ö¾4 ××´×.

exFAT
  + ×ª××× ×××¢× ××××.
  + ×ª××× ××§××¦×× ×××××× ×Ö¾4 ××´×.
  - ×× ×ª××× ××××× ××× ×Ö¾FAT32.

NTFS 
  + ×ª××× ××××× ×××¡.
  -  ×× ×ª××× ×××§ ×××¨×× ××ª×§× × ×××××¨×.
  -  ××¢×××ª ×ª×××××ª ××§×¨×××ª ×¢× ××× ××§×¡ (NTFS ××× ×§× ××× × ×××× ××¡ ×××××¨).

EXT4 
  + ×××©× ×, ××¦××, ××××¨, ×¢× ×××× ×¤×¢××××××ª.
  + ×ª××× ×××¨×©×××ª ×§××¦× ××× ××§×¡.
  -  ×× ×ª××× ××××× ×××¡,â ××§ ×××¨×× ××ª×§× × ×××××¨×.
 ××¢×¨××ª ×§××¦××: ×ª×× ××ª ×ª×× ××ª ×××¡× × ××× USB ××´× ××ª×××ª GPG ×§×××¥ ××ª×× GPG ×§×××¥ ××ª×× GPG: ×××¨× ×××××ª ISO ×ª××× ×ªÖ¾×××£ ISO: ×ª××× ××ªÖ¾×××£ ISO ISO: ×× ×××ª××× ××××× ×, × ××ª× ××××× ×Ö¾ISO. ××××§×ª ×××××××ª  (Integrity) ××©×× ×§×´× ×× × ××¦× ××¤×ª× ××©×¨×ª ×××¤×ª×××ª. ×§××¦×× ××§××××× ××´× ××¦××¨×ª ×××¡× × ××× ××¨Ö¾××ª××× ××¦××¨×ª ×××¡× × ××× ××¨Ö¾××ª××× ××××¢ × ××¡×£ ×× × ××¦× ×××× ××¨× ××× ×× × ×¤× ×××¡×× ×××× ××××¡× ×× ×××. ×¡×××× SHA256 ×¡×××× ×§×××¥ SHA256 ×§×××¥ ×¡××××Ö¾×××§××¨×ª SHA256: ×¡××××Ö¾×××§××¨×ª SHA256: ××××¨×ª ××××ª ××××¨×ª ×××¡× × ××× ××××¨×ª ××××ª ××ª×× ×¢×Ö¾×××: %s ××××: ××´× ×¡××××Ö¾××××§××¨×ª SHA256 ×©× ×ª××× ×ªÖ¾×××£ ×Ö¾ISO ×©×××. ×§×××¥ ×¡××××Ö¾××××§××¨×ª SHA256 ××× × ×××× ×¡×××××× ××ª××× ×ªÖ¾×××£ ISO ××. ×§×××¥ ×¡××××Ö¾××××§××¨×ª SHA256 ××× × ××ª××. ××××¡× ×× ××× ×ª××× ×ª ×××¦×××. ×¡××××Ö¾××××§××¨×ª ×ª×§×× ×¡××××Ö¾××××§××¨×ª ×ª×§×× ×× ×××× ××ª ××¡×××× ×× ××××ª×. ×× × ××ª× ××× ×××××§ ××ª ×§×××¥ ×Ö¾gpg. ×× × ××ª× ××× ××××¨×× ××ª ×§×××¥ ×Ö¾gpg. × × ×××××§ ××ª ××¢× ×Ö¾URL. ×××××ª × ××ª×× ×××¦×××. ×× × ××ª× ××× ×××××§ ××ª ×§×××¥ ×¡××××Ö¾××××§××¨×ª. ×× × ××ª× ××× ××××¨×× ××ª ×§×××¥ ×¡××××Ö¾××××§××¨×ª. × × ×××××§ ××ª ××¢× ×Ö¾URL. ×ª××× ×ªÖ¾×××£ ISO ×× ××××ª× ××××¦×¢××ª ××ª××× ××××× ×. ×ª××× ×ªÖ¾×××£ ISO ×× ××××ª× ××××¦×¢××ª ××ª××× ×××ª× ××××× ×. ×ª××× ×ªÖ¾×××£ ×× × ×¨××ª ××× ×ª××× ×ªÖ¾×××£ ×©× ××¢×¨××ª ××× ×××¡. ×××× ×ª××× ×ªÖ¾×××£ ISO ×¨××©×××ª ×× ×× ×ª×× ×× ××××¡× ×× ××× ×××©×××, ××××©××? ××¢× × URL ××××ª ×××¡× USB ×××¡× × ××× ××ª×× ×ª ×××¡× × ××× USB ×××¡× × ×××: ××ª××× ×× ××××¢× ××ª××× ×× ×××× × ×××××ª ×××××ª ××××× ××ª ×××××××ª ×©× ×ª××× ×ªÖ¾××××£ ×ª××××ª ××¨×: ××¨×: ×ª××× ×ªÖ¾×××£ ×©×  ××¢×¨××ª ××× ×××¡ ×××¨×©×ª ×¢×××× ×××××. ××ª××× ××ª×× ×× ××××¢ 