��    *      l  ;   �      �  ?   �  B   �  !   ,  $   N     s     v     |     �     �     �     �     �  	   �     �     �     �     �     �     �  	   �                    $     ,     8  
   D     O     b     i     q     z  3   �  *   �     �               &     3     G     O  �  S  i   E  c   �  9   	  3   M	     �	      �	     �	     �	     �	     �	  
   �	     �	     
  '   ,
     T
     c
     p
     
  3   �
     �
     �
     �
        
   	          '     >  *   U     �     �     �     �  O   �  U   3  $   �     �     �  #   �  %        1     F            !                              '                         *   &                $       )                                "       %                 	          #                 (   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: Victor Ibragimov <victor.ibragimov@gmail.com>
PO-Revision-Date: 2014-01-14 23:01+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <tg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: tg
 %(downloadSize)s боргирӣ мешавад, %(localSize)s фазои диск холӣ шудааст %(downloadSize)s боргирӣ мешавад, %(localSize)s фазои диск лозим аст %(localSize)s фазои диск холӣ шудааст %(localSize)s фазои диск лозим аст 3D Дар бораи барнома Лавозимот Бозиҳои рӯимизӣ Чат Тафсилот Нақша Почтаи электронӣ Тақлидгар Мубодилакунии файлҳо Шрифтҳо Бозиҳо Графика Насб кардан Насб кардани барномаҳои нав Насбшуда Интернет Насбнашуда Офис Баста Суратгирӣ Барномасозӣ Интишордиҳӣ Стратегияи вақти воқеӣ Тоза кардан Тақризҳо Дар ҳоли ҷустуҷӯ Илм ва маълумот Ҷустуҷӯ дар тавсифи бастаҳо (ҷустуҷӯи суст) Ҷустуҷӯ дар хулосаи бастаҳо (ҷустуҷӯи сусттар) Тақлид ва мошинронӣ Мудири нармафзор Садо ва видео Абзорҳои системавӣ Стратегияи бозгаштӣ Бинандагон Веб 