��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  -   �  7   �     �          +     D  	   R     \  !   r  %   �  #   �  G   �     &  q  9     �  	   �     �     �  
   �     �     
  
   "     -     E     Q     _  0   d  #   �     �  -   �     �     �  !   �  !         B  $   T  ,   y     �     �     �  	   �     �               2     B     K  .   N  B   }  ,   �  .   �  $     Z   A  $   �  F   �  &     )   /  K   Y  <   �  F   �  &   )     P  Y   p     �     �     �     �               0  	   C  3   M     �     �  =   �     �     �     �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-09 20:19+0000
Last-Translator: Toni Estevez <toni.estevez@gmail.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Se ha producido un error al copiar la imagen. Se ha producido un error al crear una partición en %s. Se ha producido un error Se ha producido un error. Error de autenticación. Calculando... Verificar Suma de comprobación Archivos de suma de comprobación Discrepancia de suma de comprobación Elija un nombre para la memoria USB Descargue de nuevo la imagen ISO. Su suma de comprobación no coincide. ¡Todo se ve bien! FAT32 
  + Es compatible con todos los sistemas.
  -  No admite archivos mayores de 4 GB.

exFAT
  + Es compatible con casi todos los sistemas.
  + Admite archivos mayores de 4 GB.
  -  No es tan compatible como FAT32.

NTFS 
  + Compatible con Windows.
  -  No es compatible con Mac ni con numerosos dispositivos de hardware.
  -  Problemas ocasionales de compatibilidad con Linux (es privativo y requiere ingeniería inversa).

EXT4 
  + Moderno, estable, rápido y transaccional.
  + Es compatible con los permisos de archivo de Linux.
  -  No es compatible con Windows, con Mac ni con numerosos dispositivos de hardware.
 Sistema de archivos: Formatear Formatear una memoria USB GB Firmas GPG Archivo firmado con GPG Archivo firmado con GPG Retroceder Verificación de la ISO Imagen ISO: Imágenes ISO ISO: Si confía en la firma, puede confiar en la ISO. Verificación de integridad fallida KB Clave no encontrada en el servidor de claves. Archivos locales MB Crear una memoria USB de arranque Crear una memoria USB de arranque Más información No se ha encontrado un ID de volumen No hay suficiente espacio en la memoria USB. Suma SHA256 Archivo de sumas SHA256 Archivo de sumas SHA256 SHA256sum Seleccione la imagen Seleccione una memoria USB Seleccione una imagen Firmado por: %s Tamaño: TB La suma SHA256 de la imagen ISO es incorrecta. El archivo de sumas SHA256 no contiene sumas para esta imagen ISO. El archivo de sumas SHA256 no está firmado. Se ha formateado correctamente la memoria USB. La suma de comprobación es correcta La suma de comprobación es correcta, pero no se ha verificado la autenticidad de la suma. No se pudo comprobar el archivo gpg. No se ha podido descargar el archivo gpg. Compruebe la dirección URL. Se ha grabado correctamente la imagen. No se pudo comprobar el archivo de sumas. No se ha podido descargar el archivo de sumas. Compruebe la dirección URL. Esta imagen ISO está verificada por una firma de confianza. Esta imagen ISO está verificada por una firma que no es de confianza. Esta ISO parece una imagen de Windows. Esta es una imagen ISO oficial. Esta acción destruirá todos los datos de la memoria USB. ¿Seguro que quiere continuar? URLs Grabador de imágenes USB Memoria USB Formateador de memorias USB Memoria USB: Firma desconocida Firma no confiable Verificar Verificar la autenticidad e integridad de la imagen Etiqueta de volumen: Volumen: Las imágenes de Windows requieren un procesamiento especial. Grabar bytes desconocido 