��          t      �              %   0     V     f     u  q   �     �             -   1  �  _      �          =     T     j  �   x     �          +  (   =                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-26 21:57+0000
Last-Translator: Asier Iturralde Sarasola <Unknown>
Language-Team: Basque <eu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ez da baliozko domeinu-izena. Hautatutako domeinuak blokeatu Blokeatutako domeinuak Domeinu Blokeatzailea Domeinu-izena Domeinu-izenek letra edo digitu batekin hasi eta bukatu behar dute eta letra, digitu, puntu eta marratxoak soilik eduki ditzazkete. Adibidea: nire.domeinua.eus Domeinu baliogabea Gurasoen Kontrola Idatzi blokeatu nahi duzun domeinu-izena 