��    
      l      �       �      �   %        6     F     U  q   a     �     �        �    *   �  2   �     �            �   0     �     �     �                      
                   	    %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-10-03 15:36+0000
Last-Translator: amm <Unknown>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: 
 %s no est unu nùmene de domìniu vàlidu. Bloca s'atzessu a nùmenes de domìniu dislindados Domìnios blocados Blocadore de domìnios Nùmene de domìniu Sos nùmenes de domìnios depent cumintzare e acabbare cun una lìtera o nùmeru, e podent cuntènnere isceti lìteras, nùmeros, puntos e linieddas. Esèmpiu: my.number1domain.com Domìniu non vàlidu Controllu de genitores 