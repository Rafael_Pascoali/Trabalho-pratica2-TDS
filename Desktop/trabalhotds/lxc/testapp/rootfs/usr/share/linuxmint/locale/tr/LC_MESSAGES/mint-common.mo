��          �      ,      �      �     �     �     �     
  =        Z     c  5   k     �  0   �  	   �     �  '   �  _   "     �  q  �      �          <     U     r  K   �     �     �  9   �     #  =   ;     y     �      �  p   �     "                                       	                      
                 %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Reinstall Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: LMDE
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-12-12 07:17+0000
Last-Translator: Butterfly <Unknown>
Language-Team: Linux Mint Türkiye
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ek disk alanı kullanılacak. %s disk alanı boşaltılacak. Toplamda %s indirilecek. Ek değişiklikler gerekiyor Bir hata oluştu %s paketi, diğer paketlerin gerekliliği olduğu için  kaldırılamıyor. Flatpak uygulamaları Kur %s paketi, aşağıdaki paketlerin bağımlılığıdır: Kaldırılacak paketler Lütfen aşağıdaki değişikliklerin listesini inceleyiniz. Yeniden yükle Kaldır Aşağıdaki paketler silinecek: Bu menü öğesi herhangi bir paketle bağlantılı değildir. Buna rağmen menüden kaldırmak istiyor musunuz? Sürüm Yükselt 