��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  F   :  =   �  *   �  3   �  "     m   A     �     �  J   �  4   +  :   `     �  ;   �  �   �     y                                       	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-08-07 18:49+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <(nothing)>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: sr
 %s јоп простора диска биће искоришћено. %s простора диска биће ослобођено. %s биће укупно преузето. Додатне промене су потребне Грешка се догодила Не могу да уклоним пакет „%s“ јер га захтевају други пакети. Флатпак пакети Инсталирај Пакет „%s“ је зависност следећих пакета: Пакети који ће бити уклоњени Погледајте листу промена испод. Уклони Следећи пакети ће бити уклоњени: Ова ставка менија није повезана ни са једним пакетом. Да ли желите да је уклоните? Надогради 