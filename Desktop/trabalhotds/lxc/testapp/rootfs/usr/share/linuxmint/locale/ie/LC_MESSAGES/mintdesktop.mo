��    V      �     |      x  
   y     �  2   �     �     �     �     �  �        �  P   �     1	  9   A	     {	  �   �	     ;
     D
     U
     e
     m
     ~
     �
  -   �
     �
     �
  >   �
     1  
   6     A  
   G  X   R  ^   �  Z   
  X   e  	   �     �  
   �     �     �     �               "     1     :     Q     d     u     �     �     �     �     �     �     �     �      �  -        6     S  0   i     �     �     �     �     �  	   �  5   �     -     6     P     V     b     ~     �     �  2   �     �  .   �     +     1     E     U     c     r     �  �  �  	   *     4  E   9          �  +   �     �  �   �     �  S   �     +  @   @     �  �   �  
   3     >     P     a     i     �     �  7   �     �  "   �  ;   
     F     J     _  
   f  T   q  Z   �  V   !  T   x  
   �     �  
   �     �     �               (     6     J     S     i     |     �     �     �     �     �     �     �     �     �  *     ,   0      ]     ~  )   �     �     �     �            	   *  G   4     |     �  
   �     �  /   �     �  !        3  @   H  	   �  1   �     �     �     �     �     �       I   #     '      /   T   H                                        (   C   %   3       +      2   U       G                  P   I   .           N   1   B   V   5   	   J       ;   8   "   6   M   *   Q   $              &      K   F              L                  7   ,       R                    E       A                 !       )   
   O      D                                9         >               @          -   #      <   S           =   ?      0                   :       4    Advantages Auto Automatically maximize nearly screen sized windows Awesome Buttons labels: Buttons layout (CSD windows): Buttons layout: Click on the help button for more information about window managers.
Use the 'wm-recovery' command to switch back to the default window manager.
Use the 'wm-detect' command to check which window manager is running. Compiz Compiz is installed by default in Linux Mint. Note: It is not available in LMDE. Compiz settings Compositing (shadows, animations, effects, GPU rendering) Compton Compton is a compositing manager. It can be used in combination with windows managers such as Marco, Metacity or Xfwm4. In Xfce, using Xfwm4+Compton is known to reduce screen tearing. Computer Configure Compiz Configure Xfwm4 Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Double (HiDPI) Fine-tune desktop settings Here's a description of some of the window managers available. Home Icon size: Icons Icons only If Marco is not installed already, you can install it with <cmd>apt install marco</cmd>. If Metacity is not installed already, you can install it with <cmd>apt install metacity</cmd>. If Mutter is not installed already, you can install it with <cmd>apt install mutter</cmd>. If Xfwm4 is not installed already, you can install it with <cmd>apt install xfwm4</cmd>. Interface Large Linux Mint Mac style (Left) Marco Marco + Compositing Marco + Compton Marco + Picom Marco settings Metacity Metacity + Compositing Metacity + Compton Metacity + Picom Metacity settings Mounted Volumes Mutter Mutter settings Network Normal Openbox Openbox + Compton Openbox + Picom Overview of some window managers Place new windows in the center of the screen Pros and cons of compositing Reset Compiz settings Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only The Windows section lets you choose a window manager. Toolbars Traditional style (Right) Trash Tweak Xfwm4 Use system font in titlebar User interface scaling Welcome to Desktop Settings. Window Manager Window events (moving, resizing, focusing windows) Windows Windows borders, title bars and window buttons Xfwm4 Xfwm4 + Compositing Xfwm4 + Compton Xfwm4 + Picom Xfwm4 settings root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-13 13:14+0000
Last-Translator: Silvara <Unknown>
Language-Team: Interlingue <ie@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Avantages Auto Maximisar automaticmen fenestres quel have dimensiones simil al ecran Awesome Etiquettes de butones: Arangeament de butones (fenestres con CSD): Arangeament de butones: Far un clic sur li buton Auxilie por reciver information pri gerentes de fenestres.
Usa li comande 'wm-recovery' por reverter al predefinit gerente de fenestres.
Usa li comande 'wm-detect' por controlar que gerente de fenestres es activ. Compiz Compiz es predefinitmen installat in Linux Mint. Note: It ne es disponibil in LMDE. Parametres de Compiz Composition (ombres, animationes, effectes, presentation de GPU) Compton Compton es un gerente de composition. It posse esser usat con gerentes de fenestres Marco, Metacity o Xfwm4. Li usage de Xfwm4+Compton in Xfce posse reducter li tearing. Computator Configurar Compiz Configurar Xfwm4 Pupitre Parametres del Pupitre Icones del pupitre Desavantages Ne monstar li contenete de fenestres quande on move les Duplic (HiDPI) Configurar li ambientie de pupitre Vi un descrition de alcun disponibil gerentes de fenestres. Hem Dimension de icones: Icones Sol icones Si Marco ne es ja installat, on posse installar it med <cmd>apt install marco</cmd>. Si Metacity ne es ja installat, on posse installar it per <cmd>apt install metacity</cmd>. Si Mutter ne es ja installat, on posse installar it med <cmd>apt install mutter</cmd>. Si Xfwm4 ne es ja installat, on posse installar it med <cmd>apt install xfwm4</cmd>. Interfacie Grand Linux Mint Stil de Mac (a levul) Marco Marco + compostion Marco + Compton Marco + Picom Parametres de Marco Metacity Metacity + compostion Metacity + Compton Metacity + Picom Parametres de Metacity Montet volumes Mutter Parametres de Mutter Rete Normal Openbox Openbox + Compton Openbox + Picom Comparation de alcun gerentes de fenestres Plazzar nov fenestres in li centre del ecran Pluses e minuses del composition Reverter parametres de Compiz Selecte elementes visibil sur li Pupitre: Monstrar icones sur butones Monstrar icones in menús Micri Textu sub elementes Textu apu elementes Sol textu Li section «Fenestres» permisse selection de un gerente de fenestres. Barras de instrumentarium Traditional stil (a dextri) Paper-corb Adjustar Xfwm4 Usar li fonde del sistema por li panel de titul Scale del interfacie Benvenit a Parametres del Pupitre Gerente de fenestres Evenimentes de fenestres (movement, redimension, cambio de foco) Fenestres Bordes de fenestres, barras de titul e li butones Xfwm4 Xfwm4 + compostion Xfwm4 + Compton Xfwm4 + Picom Parametres de Xfwm4 root@linuxmint.com Launchpad Contributions:
  Silvara https://launchpad.net/~mistresssilvara 