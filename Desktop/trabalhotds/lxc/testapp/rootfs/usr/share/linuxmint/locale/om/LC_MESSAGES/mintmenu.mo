��    C      4  Y   L      �  	   �     �     �     �     �  K   �  %   /  -   U     �  "   �  F   �               !     0     K     a     i  	   w     �  	   �     �     �     �     �  -   �                     ,     C     J     O     `     e     k     s     �     �     �     �     �     �  1   �  6   	     :	     ?	  $   N	     s	     z	     �	     �	     �	     �	     �	     
     
  '   +
     S
     d
     
     �
     �
     �
  	   �
     �
  �  �
     O     ]     d     j     {  %   �     �  -   �     �  )     D   @     �     �     �     �     �     �                0  
   K     V     e     y     �  4   �     �     �     �          '     ,     4     G     M     S     Z     p     y     �     �     �  	   �  (   �  2   �               .     H     P     m     �     �     �     �     �  %   �          9  !   K     m     s     �  
   �     �     �     )      -   &       (          ?   =                           %         .                0             !   *       ,       B   :   2          +              #                >   $   <   3      
   '          @              C              9       7      	   A   1   ;      5      8                  "   6         4   /        <not set> About All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-09-07 14:06+0000
Last-Translator: gudeta <gudbog@gmail.com>
Language-Team: Oromo <om@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Hin qindoofne Waayee Hunda Dhimbaastu hunda Dhimbaaftota Baxxee gahinsa kompiyutara kana jiran Mosajjii jiran barbaadii ijaari Iddoowwan cimdaa qe'ee fi torbarruu sakatta'i fayilii haqaman sakkata'i Maalimaa minjaala'aa irra jiran sakatta'i Qaxxaamurtoota haara hojii baafata cufuuf banuurra oolchuuf caqasi   Kompiyutara Sirna kee qindeessi Wiirtuu to'annaa Ittisuuqa kaasuu hindandeenye Ittisuuqa naqachuu hindandeenye Minjaala'aa(Desktooppii) Dhamsa minjaala'aa Baafata gulaali Baattoo gataa qulqulleessi Jaalatamaa Ukaankaa manaa Gargareessaa galchi Iddoduwa galchi %s dhimbaaftuu ijaari Ijaarsa, buqqaasuu fi olkaasa mosaajjii qarqabaalee, Kaasisi Yaroo ani seenu kaasisi Argiitii qulfeessi Bahi yookin fayyadamaa jijjiiri Bahi Baafata Fedhiilee baafataa Maqaa Maqaa Cimdaa Ukaanka matayyaa bani Filmaata Mosaggii qarqabaa Bakkumsa Qaxxaamura kaasi Iddowwan Fedhiilee Keybinding jiran haquuf Backspace dhiibi Dalaga dhiisuf Escape dhiibi yookin lama cuqaasi   Cufi Ittisuuqa lamnaqadhu Akaaku soqa darbe yaada'u Buqqisi Jaallamtoota keessaa buqqisi Banuuf iggitni barbaachisa qarqaba ijaartu soqi Ukaanka filadhu Dhimbaasota hunda agarsiisi Saajoo qabduu agarsiisi Sajoo akaakuu agarsiisi Jaallamtoota koo keessatti agarsiisii Cufi, Lamkaasi, ykn riphi Mosaggii taliigaa Maqaafi maqaa idilaa wal-jijjiiri Sirna Buufata/terminal Dhamsa: Bakkagataa Buqqaasi Xurree ajajaa fayyadami 