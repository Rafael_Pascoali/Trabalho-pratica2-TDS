��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  !     (   :     c     s     �  �   �          :     P  *   f                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-10-03 20:31+0000
Last-Translator: Cédric VALMARY (Tot en òc) <cvalmary@yahoo.fr>
Language-Team: Occitan (post 1500) <oc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s es pas un nom de domeni valid. Blocar l'accès a d'unes noms de domenis Domenis blocats Blocador de Domeni Nom de domeni Los noms de domeni devon començar e finir per de letras o de chifras, e conténer sonque de letras, de chifras, de punts e de jonhents. Exemple : mon.nom2domeni.com Nom de domeni invalid Contraròtle parental Picatz lo nom de domeni que volètz blocar 