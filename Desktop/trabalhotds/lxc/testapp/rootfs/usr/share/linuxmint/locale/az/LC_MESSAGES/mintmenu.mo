��    E      D  a   l      �  	   �     �                    )  K   6  %   �  -   �     �  "   �  F        U     ^     t     �     �     �     �  	   �     �     �  	   �                    +  -   @     n     u     �     �     �     �     �     �     �     �     �     �     �     	     	     (	     /	  1   ;	  6   m	     �	     �	  $   �	     �	     �	     �	     
     5
     E
     [
     l
     �
  '   �
     �
     �
     �
     �
     �
        	          �  %     �     �     �     �     �       L     )   b  C   �     �  1   �  d      
   �     �     �     �     �     �               2     F     d     p     |     �  #   �  9   �     �            )   .     X     h     p     �     �     �     �  
   �     �  
   �     �            >     @   ]     �     �  +   �     �     �  '     .   -     \     l     �     �      �  5   �          -     J     R     [  	   d     n  !   s     3                  ?   D   B   ,             "      )   '             #           ;                 @                   +         %   C   !   .       8      /          7   9           :                 <   5   0       6   E   *           1   =   (                         4           2      $   A                              	      >      
       &   -    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Enable Internet search Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-08-09 19:41+0000
Last-Translator: Arif <mdyos@inbox.ru>
Language-Team: Azerbaijani <az@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <qurulmayıb> Bilgi Qabaqcıl "MATE" Seçməsi Hamısı Bütün uyğulamalar Uyğulamalar Bu bilgisayardan əlçatan bütün yerli, uzaq disklərə, qovluqlara gözat Əlçatan öndüzümə gözat, quraşdır Bitik aralığı edilmiş, yerli tor işi yerləşimlərinə gözat Silinən düzümlərə gözat İş qoyacağında yerləşən ögələrə gözat Seçmə açılması, bağlanması üçün yeni bir sürətləndirici düymə qurmaq üçün tıkla   Bilgisayar Öz qurmacanızı qurun Yönətmə Ortası Qoşma başladıla bilmədi Qoşma yüklәnilə bilmәdi: İş qoyacağı İş qoyacağı örtüyü Seçməni düzəlt Çöplüyü boşalt Arator axtarışını işlət Sevimlilər Ev Qovluğu Ayırıcı yeridin Ara yeridin '%s' qablaşdırmasını quraşdır Öndüzüm qablaşdırmalarını quraşdır, sil, yenilə Başlat Mən girdikdə başlat Göstəricini Açarla Oturumdan çıx ya da işlədəni dəyiş Oturumdan çıx Seçmə Seçmə üstünlükləri Ad Adı: Tor işi Kişisəl qovluğunuzu açın Seçimlər Qablaşdırma Yönətməni Yerləşim Sürətləndiricini götür Yerlər Üstünlüklər Var olan düymə-bağlamanı silmək üçün Geri sili basın. İşdən vazkeçmək üçün "Esc"i bas ya da bir daha tıkla.   Çıx Qoşmaları yenidәn yüklә Son bölümü ya da axtarışı yadda saxla Sil Sevimlilərdən sil Açmaq üçün keçid sözü gərəkdir Quraşdırmaq üçün qablaşdırmaları axtar Bir qovluq seç Tətbiqetmələri göstər Düymə ikonunu göstər Bölüm ikonlarını göstər Mənim sevimlilərimdə göstər Söndür, yenidən başlat, gözlət ya da yuxuya get Öndüzüm Yönətməni Dəyişmə adı, toplam adı Qurmaca Terminal Örtük: Çöplük Sök Göstəriş cızığını işlət 