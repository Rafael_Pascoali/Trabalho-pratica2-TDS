��          �   %   �      0  )   1  2   [     �     �      �     �     �     �       "   $     G     T     g  )   w  #   �  N   �       	   %     /  
   C     N     \     b  �  j  ;     H   G     �     �  '   �     �     �     	     %  !   A     c     y     �  ,   �  %   �  R   �     P     d     m     �     �     �  
   �                	   
                                                                                                            An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-03-09 11:48+0000
Last-Translator: Quentin PAGÈS <Unknown>
Language-Team: Occitan (post 1500) <oc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Una error s'es produita al moment de la còpia de l'imatge. Una error s'es produita al moment de la creacion d'una particion sus %s. Una error s'es produita. Error d'autentificacion. Causissètz un nom per vòstra clau USB Formatar Formatar una clau USB Crear una clau USB bootabla Crear una clau USB bootabla Pas pro d'espaci sus la clau USB. Causissètz un imatge Seleccionatz una clau USB Seleccionatz un imatge La clau USB es estada formatada amb succès. L'imatge es estat escrit amb succès. Totas las donadas presentas sus la clau USB seràn suprimidas. Volètz contunhar ? Creator de clau USB Clau USB Formatador de clau USB Clau USB : Nom de volum : Escriure desconegut 