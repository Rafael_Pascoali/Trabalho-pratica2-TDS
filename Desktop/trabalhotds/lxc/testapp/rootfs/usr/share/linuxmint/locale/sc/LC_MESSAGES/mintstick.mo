��          �      l      �  )   �  2        >     Q      g    �     �  "   �     �     �     �  )   �  #        =  	   N     X  
   l     w     �     �  �  �  "     2   9     l     �  .   �  e  �     5
  2   =
     p
     �
     �
     �
  #   �
     �
               >     O     i  
   p               	                                                                       
            An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-10-27 19:06+0000
Last-Translator: amm <Unknown>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: 
 Faddina in sa copia de s'imàgine. Faddina in sa creatzione de una partitzione in %s. B'at àpidu una faddina. Faddina de autenticatzione Sèbera unu nùmene pro su dispositivu USB tuo FAT32 
  + Cumpatìbile cun totu is sistemas.
  - Non podet manigiare archìvios prus mannos de 4GB.

exFAT
  + Cumpatìbile cun belle totu is sistemas.
  + Podet manigiare archìvios prus mannos de 4GB.
  - Prus pagu cumpatìbile de FAT32.

NTFS 
  + Cumpatìbile cun Windows.
  - No est cumpatìbile cun Mac e sa prus parte de dispositivos.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Cumpatìbile cun permissos de archìviu de Linux.
  - No est cumpatìbile cun Windows, Mac e sa prus parte de dispositivos.
 Formatu Non b'at ispàtziu bastante in su dispositivu USB. Sèbera imàgine Sèbera unu dispositivu USB Sèbera un'imàgine Dispositivu USB formatadu. Iscritura de s'imàgine cumpletada. Iscritore de imàgines de USB Dispositivu USB Formatadore de dispositivos USB Dispositivu USB: Eticheta de su volùmene: Iscrie disconnotu 