��          �      \      �  ;   �  ,     %   :     `  
   }     �     �     �     �     �     �     �     �     �          )     5     =     L  �  b  u   �  C   s  L   �  E        J     W     t     �     �  .   �  '   �           	  $   !  2   F     y     �  "   �  ,   �                       	      
                                                                      A large panel with grouped windows and a small system tray. A small panel and a traditional window list. Add all the missing multimedia codecs Choose your favorite layout. Contribute Desktop Colors Documentation First Steps Help Install Multimedia Codecs Introduction to Linux Mint Modern Panel Layout Release Notes Show this dialog at startup Traditional Welcome Welcome Screen Welcome to Linux Mint Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-04-27 14:42+0000
Last-Translator: MohammadSaleh Kamyab <Unknown>
Language-Team: Persian <fa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:34+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 تابلویی بزرگ با پنجره‌های گروه‌بندی شده و یک سینی سامانهٔ کوچک. تابلویی کوچک و سیاههٔ پنجره‌ای سنتی. افزودن تمامی کدک‌های چندرسانه‌ای ناپیدا چیدمان مورد علاقه‌تان را انتخاب کنید. مشارکت رنگ‌های میزکار مستندات گام‌های نخست راهنما نصب کدک‌های چندرسانه‌ای آشنایی با لینوکس مینت نوین چیدمان تابلو یادداشت‌های انتشار نمایش این محاوره هنگام شروع سنتی خوش‌آمدید صفحهٔ خوش‌آمدگویی به لینوکس مینت خوش آمدید 