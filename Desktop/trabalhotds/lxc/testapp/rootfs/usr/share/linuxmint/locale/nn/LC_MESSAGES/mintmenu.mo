��    R      �  m   <      �  	   �     �                    )     D  K   Q  %   �  -   �     �  "        )     5  F   A     �     �     �     �     �     �     �  	   �     	  	   	     	     %	     <	  
   H	     S	     d	     q	  -   �	     �	     �	     �	     �	     �	     �	     
     
     
     $
     5
     :
     @
     H
     b
     j
     z
     
     �
     �
  1   �
  6   �
               #     *     @     \     {     �     �     �     �     �     �  -        0     G  '   U     }     �     �     �     �     �     �  	   �     �  �  �     �     �     �     �     �     �     �  R   �  2   F  5   y     �  &   �  	   �  
   �  C     
   I     T     h  '   w  '   �  
   �     �     �     �  
   �     
  #     
   5     @     M     a     s  +   �  
   �     �     �     �     �     �            	         *     <     A     G     P     m     q     �     �     �     �  6   �  7   �                >     D     [     y  	   �     �     �     �     �     �  
     %        4     I  ,   Z     �  !   �     �     �     �     �  
   �     �     �        *         #       1      9   )   ?                	         !   <       4              
                  &   "           E      D   0   6                G   O   P   N             C   '   K   R              B   $   J      :       A          7   %       H       L   @      F                     .   /       ,          +       5   -          3   8      =      Q       >   I   (      ;   2       M        <not set> About Advanced MATE Menu All All applications Applet button in the panel Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Button icon Button text Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Folder: General applet options Home Folder Icon sizes Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Keyboard shortcut Launch Launch when I log in Layout Lock Screen Log out or switch user Logout Menu Menu button Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show applications comments Show button icon Show category icons Show in my favorites Show places Show recently used documents and applications Show search bar on top Show tooltips Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal The size of the icons Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-07-13 16:25+0000
Last-Translator: Makenna Makesh <Unknown>
Language-Team: Norwegian Nynorsk <nn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <ikkje valt> Om Avansert MATE-meny Alt Alle programma Miniprogramknapp i panelet Program Bla gjennom alle lokale og eksterne platelager og mapper som denne maskina kan nå Sjå gjennom og installer tilgjengeleg programvare Bla gjennom nettverkslokasjonane i bokmerka og lokalt Sjå gjennom sletta filer Sjå gjennom elementa på skrivebordet Knappikon Knapptekst Trykk for å velja ny hurtigtast for opning og lukking av menyen.   Datamaskin Konfigurer systemet Kontrollsenter Klarte ikkje å starte programtillegget Klarte ikkje å laste programtillegget: Skrivebord Skrivebordstema Endre menyen Tøm papirkorga Favorittar Mappe: Generelle miniprogram innstillingar Heimemappe Ikon sorleik Set inn skiljeteikn Set inn mellomrom Installer pakken '%s' Installer, fjern og oppgrader programpakkar Hurtigtast Start Start når eg loggar inn Formgjeving Lås skjermen Logg ut eller bytt brukar Logg ut Meny Menyknapp Menyinnstillingar Namn Namn: Nettverk Opna den personlege mappa di Val Pakkehandsamar Stig Vel hurtigtast Stader Innstillingar Trykk rettetast for å fjerna eksisterande hurtigtast. Trykk ESC eller trykk igjen for å avbryta handlinga.   Avslutt Last programtillegga på nytt Fjern Fjern frå favorittane Krev passord for å låse opp Søk etter installasjonspakkar Vel mappe Vis all programma Vis programkommentarar Vis knappeikon Vis kategori-ikon Vis i favorittane mine Vis stader Vis nylege brukte dokument og program Vis søkjefelt øvst Vis hjelpebobler Avslutning, omstart, kvile- eller dvalemodus Programvarehandsamar Bytt mellom namn og generisk namn System Terminal Ikon storleik Tema: Papirkorga Avinstaller Bruk kommandolinja 