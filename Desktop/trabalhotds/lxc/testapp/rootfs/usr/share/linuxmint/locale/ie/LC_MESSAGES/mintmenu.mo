��    s      �  �   L      �	  	   �	     �	     �	     �	     �	      
     
  
   
      
     -
  %   E
     k
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
               '     /     @     H  	   V  	   `     j     z     �  	   �     �  
   �     �     �  
   �     �     �     �            
        $     5     B  -   W     �     �     �     �     �     �     �     �     �            
             /     4     :     B     T     n     v     �     �     �     �  1   �     �     �     �  $        +     2     H     \     c     s     �     �     �     �     �     �     �          -     <     L     b     }     �  $   �     �     �  -   �          -     >     E  )   N     x     �     �     �  	   �     �  '   �     �  �  �     �     �     �     �     �     �     �     �     �       +   #     O     e     t  
   �  
   �     �     �     �     �     �            
   /     :     N     V  
   i     t     �     �     �     �     �     �                '     7     @     V     ^     e     q     }     �     �  6   �  	   �                    5     A  %   Q     w     �     �     �     �     �     �     �     �     �       
   +     6     J     O     f     k  8   x     �     �     �     �     �     �          ,     4     J     f     ~     �     �     �     �      �          '     @     T  $   p     �     �  .   �     �       *        H     e     {     �  ,   �     �     �     �  
   �     �     �  )   
     4               2   J       E   #           V   .   k   )       p       >             7      &          5               ?   T       ;   b       R   +   /                   =      c   $   ,      W       @   9   "   \          *                     j   :   _   F   H   K   '   ]   1   h       A   ^   Z   U                        -   X   !         L   `       O   3      S      n   M       I   	       
      (      P      B   m   Y   o          f       i   C   8   6       D   0      Q   d   r   s       <          e   [      4      G   a          N                      q               %   g       l    <not set> About Add to desktop Add to panel Advanced MATE Menu All All applications Appearance Applications Applications categories Browse and install available software Browse deleted files Button icon Button text Categories Computer Configure your system Control Center Couldn't load plugin: Custom Place Custom height Custom places Custom theme selection Default Delete from menu Desktop Desktop theme Documents Edit menu Edit properties Empty trash Enable Internet search Favorites Find Hardware Find Ideas Find Software Find Tutorials Find Users Folder: GTK Bookmarks General Height Home Folder Icon sizes Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Items Keyboard shortcut Launch Launch when I log in Layout Lock Screen Log out or switch user Logout Lookup %s in Dictionary Menu Menu button Menu icons Menu preferences Name Name: Network Number of columns Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Quit Recently used Reload plugins Remember the last category or search Remove Remove from favorites Run with NVIDIA GPU Search Search Computer Search Computer for %s Search Dictionary Search DuckDuckGo Search DuckDuckGo for %s Search Wikipedia Search Wikipedia for %s Search command Search for packages to install Search options Section layout Select a folder Show all applications Show applications comments Show button icon Show category icons Show favorites when the menu is open Show in my favorites Show places Show recently used documents and applications Show system management Software Manager System Terminal The file or location could not be opened. The size of the icons Theme Theme: Trash Uninstall Use the command line You can add your own places in the menu milliseconds Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-06-05 19:09+0000
Last-Translator: Silvara <Unknown>
Language-Team: Interlingue <ie@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <ne assignat> Pri Adjunter al Pupitre Adjunter al panel Menú de MATE avansat Omni Adjunter applicationes Aspecte Applicationes Categories de applicationes Inspecter e installar disponibil programmas Navigar removet files Icone de buton Textu del buton Categories Computator Configurar vor sistema Centre de control Ne successat cargar un plugin. Personal loc Personalisat altore Personal locs Selection de un personal tema Predefinit Remover ex li menú Pupitre Tema del ambientie Documentes Modificar li menú Modificar li proprietás Vacuar li Paper-corb Permisser sercha in li internet Li preferet Trovar aparatura Trovar idées Trovar programmas Trovar instructiones Trovar usatores Fólder: Marca-págines de GTK General Altore Hem fólder Dimensiones Inserter un separator Inserter interspacie Installar li paccage «%s» Installar, remover e actualisar paccages de programmas Elementes Rapid-taste Lansar Lansar al inicie del session Arangeament Serrar li ecran Cluder li session o cambiar li usator Cluder li session Serchar %s in li dictionarium Menú Buton del menú Icones de menú Preferenties del menú Nómine Nómine: Rete Númere de columnes Aperter vor fólder personal Parametres Gerente de paccages Rute Selecte un rapid-taste Locs Preferenties Presse li retroclave por vacuar li selectet rapid-taste. Surtir Recentmen usat Recargar li plugines Memorar li ultim categorie Remover Remover ex li preferet Lansar con GPU NVIDIA Serchar Serchar li computator Serchar %s in li computator Serchar li dictionarium Serchar med DuckDuckGo Serchar %s med DuckDuckGo Serchar li Wikipedia Serchar %s in li Wikipedia Comande de sercha Serchar por installabil paccages Parametres de sercha Arangeament de sectiones Selecter un fólder Monstrar omni applicationes Monstar li comentas de applicationes Monstrar li icone Monstrar icones de categories Monstrar li preferet quande on aperte li menú Monstrar in mi preferet Monstrar li locs Monstrar recent documentes e applicationes Monstrar gerentie de sistema Gerente de programmas Sistema Terminal Ne successat aperter li file o localisation. Dimension de icones Tema Tema: Paper-corb Desinstallar Usar li linea de comandes On posse adjunter su propri locs al menú millisecondes 