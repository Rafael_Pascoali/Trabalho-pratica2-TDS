��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  -   (  +   V     �     �  "   �  �  �  	   �
     �
  !   �
  !     )   #     M     d     z  +   �  (   �  Z   �     A     [     i     �     �     �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-10-19 18:38+0000
Last-Translator: Guillermo Molleda <gmolleda@us.es>
Language-Team: Esperanto <eo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Okazis eraro dum kopiado de la ekzakta kopio. Okazis eraro dum kreado de subdisko sur %s. Okazis eraro. Aŭtentiga eraro. Elektu nomon por via usb-bastoneto FAT32 
  + Kongruas ĉie.
  - Ne traktas dosierojn de pli ol 4GB.

exFAT
  + Kongruas preskaŭ ĉie.
  + Traktas dosierojn de pli ol 4GB.
  - Ne same kongrua kiel FAT32.

NTFS 
  + Kongrua kun Vindozo.
  - Ne kongrua kun Mac kaj plej multe da aparatoj.
  - Fojfoje kongruaj problemoj kun Linukso (NTFS estas proprieta kaj retroprojektita).

EXT4 
  + Moderna, stabila, rapida, ĵurnaligita.
  + Subtenas atingopermesojn de Linuksaj dosieroj.
  - Ne kongrua kun Vindozo, Mac kaj plej multe da aparatoj.
 Strukturi Strukturi usb-bastoneton Fari praŝargeblan usb-bastoneton Fari praŝargeblan usb-bastoneton Ne sufiĉe da spaco sur la usb-bastoneto. Elekti ekzaktan kopion Elekti usb-bastoneton Elekti ekzaktan kopion La usb-bastoneto estis sukcese strukturita. La ekzakta kopio estis sukcese skribita. Ĉi tio detruos ĉiujn datumojn sur la usb-bastoneto, ĉu vi certas, ke vi volas daŭrigi? Usb-ekzakta-kopi-skribilo Usb-bastoneto Strukturilo de usb-bastoneto Usb-bastoneto: Datumportila etikedo: Skribi nekonata 