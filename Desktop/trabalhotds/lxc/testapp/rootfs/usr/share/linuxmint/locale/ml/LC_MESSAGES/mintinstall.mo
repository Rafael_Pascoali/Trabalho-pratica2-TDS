��    +      t  ;   �      �  ?   �  B   �  !   <  $   ^     �     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �     �  	              )     7     >     F     R  
   ^     i     |     �     �     �  3   �  *   �     	          0     @     M     a     i  �  m  �   '  �   �  Q   b	  N   �	     
     
  !   /
     Q
     q
  *   �
     �
  $   �
     �
  *     +   1     ]     v     �  1   �  l   �  :   J  !   �  7   �     �     �       $   '  '   L  =   t  (   �  $   �        I     �   c  �     O   �  7   (  :   `  4   �  C   �  $        9            "         !                    (                          +   '                %       *                                #       &                	          $                 )   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-10-25 12:26+0000
Last-Translator: praveenp <Unknown>
Language-Team: Malayalam <ml@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s ഡൗൺലോഡ് ചെയ്യാനുണ്ട്, %(localSize)s സംഭരണസ്ഥലം മോചിപ്പിച്ചു %(downloadSize)s ഡൗൺലോഡ് ചെയ്യാനുണ്ട്, %(localSize)s സംഭരണസ്ഥലം ആവശ്യമുണ്ട് %(localSize)s സംഭരണസ്ഥലം മോചിപ്പിച്ചു %(localSize)s സംഭരണസ്ഥലം ആവശ്യമുണ്ട് ത്രിമാനം വിവരണം ഉപകരണങ്ങള്‍ ബോർഡ് കളികൾ ചാറ്റ് വിശദവിവരങ്ങള്‍ ചിത്രരചന വിദ്യാഭ്യാസം ഇമെയില്‍ എമുലേറ്ററുകള്‍ ഫയല്‍ പങ്കിടല്‍ ഫോണ്ടുകൾ കളികള്‍ ഗ്രാഫിക്സ് ഇൻസ്റ്റോൾ ചെയ്യുക പുതിയ ആപ്ലിക്കേഷനുകൾ ഇൻസ്റ്റോൾ ചെയ്യുക ഇന്‍സ്റ്റോള്‍ ചെയ്തു ഇന്റർനെറ്റ് ഇൻസ്റ്റോൾ ചെയ്തില്ല ഓഫീസ് പാക്കേജ് ഛായാഗ്രഹണം പ്രോഗ്രാമിങ് പ്രസിദ്ധീകരണം തത്സമയ തന്ത്രരൂപീകരണം നീക്കം ചെയ്യുക നിരൂപണങ്ങള്‍ സ്കാനിങ് ശാസ്ത്രവും വിദ്യാഭ്യാസവും പാക്കേജുകളുടെ വിവരണങ്ങളില്‍ തിരയുക (വളരെ മന്ദഗതിയിലുള്ള തിരയല്‍) പാക്കേജുകളുടെ സംക്ഷിപ്ത വിവരണങ്ങളില്‍ തിരയുക (മന്ദഗതിയിലുള്ള തിരയല്‍) സിമുലേഷനുകളും വേഗതാമത്സരവും സോഫ്റ്റ്‌വേർ മാനേജർ ശബ്ദവും ചലച്ചിത്രവും സിസ്റ്റം ഉപകരണങ്ങൾ ഊഴമനുസരിച്ചുള്ള തന്ത്രം കാഴ്ചക്കാര്‍ വെബ് 