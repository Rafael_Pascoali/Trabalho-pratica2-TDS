��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  :   {  H   �     �          6  
   P  	   [     e     n       #   �  A   �       G       `     l     u     �  	   �     �     �     �     �     �     �     �  -   �           =  +   @     l     x     {     �     �     �  +   �               1  
   G     R     g          �     �     �  2   �  I   �  *   0  .   [     �  H   �  $   �  3     )   D  ,   n  ;   �  ;   �  ?     &   S  $   z  Q   �     �     �  	          
   )     4     F     \  4   e     �     �  ;   �     �     �           K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-03 17:48+0000
Last-Translator: Fs00 <Unknown>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Si è verificato un errore durante la copia dell'immagine. Si è verificato un errore durante la creazione di una partizione su %s. Si è verificato un errore Si è verificato un errore. Errore di autenticazione. Calcolo... Controlla Checksum File di checksum Checksum non corrispondente Scegli un nome per la tua penna USB Scarica di nuovo l'immagine ISO. Il suo checksum non corrisponde. Sembra tutto corretto! FAT32 
  + Compatibile ovunque.
  -  Non può gestire file più grandi di 4GB.

exFAT
  + Compatibile quasi ovunque.
  + Può gestire file più grandi di 4GB.
  -  Non così compatibile come FAT32.

NTFS 
  + Compatibile con Windows.
  -  Non compatibile con Mac e la maggior parte dei dispositivi hardware.
  -  Occasionali problemi di compatibilità con Linux (NTFS è proprietario e reverse engineered).

EXT4 
  + Moderno, stabile, veloce, journalized.
  + Supporta i permessi sui file di Linux.
  -  Non compatibile con Windows, Mac e la maggior parte dei dispositivi hardware.
 Filesystem: Formatta Formatta una penna USB GB Firme GPG File firmato GPG File firmato GPG: Indietro Verifica ISO Immagine ISO: immagini ISO ISO: Se ti fidi della firma puoi fidarti dell'ISO. Controllo integrità fallito KB Chiave non trovata sul server delle chiavi. File locali MB Crea una penna USB avviabile Crea penna USB avviabile Maggiori informazioni Nessun ID volume trovato Non c'è abbastanza spazio sulla penna USB. Somma SHA256 File delle somme SHA256 File di somma SHA256: SHA256sum: Seleziona l'immagine Seleziona una penna USB Seleziona un'immagine Firmato da: %s Dimensioni: TB La somma SHA256 dell'immagine ISO non è corretta. Il file delle somme SHA256 non contiene le somme per questa immagine ISO. Il file delle somme SHA256 non è firmato. La penna USB è stata formattata con successo. Il checksum è corretto Il checksum è corretto ma l'autenticità della somma non è verificata. Impossibile controllare il file gpg. Impossibile scaricare il file gpg. Controlla l'URL. L'immagine è stata scritta con successo. Impossibile controllare il file delle somme. Impossibile scaricare il file delle somme. Controlla l'URL. Questa immagine ISO è verificata da una firma attendibile. Questa immagine ISO è verificata da una firma non attendibile. Questa ISO sembra un'immagine Windows. Questa è un'immagine ISO ufficiale. Tutti i dati nella penna USB verranno cancellati, sei sicuro di voler continuare? URL Scrivi immagine su USB Penna USB Formatta penna USB Penna USB: Firma sconosciuta Firma non attendibile Verifica Verifica l'autenticità e l'integrità dell'immagine Etichetta del volume: Volume: Le immagini di Windows richiedono un'elaborazione speciale. Scrivi byte sconosciuto 