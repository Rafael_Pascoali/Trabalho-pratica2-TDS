��    A      $  Y   ,      �  
   �     �     �     �     �  P   �          ,     4     =     N     V     g     u  -   �     �  >   �       
          
   !  \   ,  X   �  ^   �  \   A  X   �  	   �     	  
   	     	     #	     )	     9	     H	     Q	     d	     v	     �	     �	     �	  2   �	      �	     �	     
  0   /
     `
     v
     �
     �
     �
  	   �
  5   �
     �
     �
               8     U     d  `   l     �     �     �     �  �  	     �     �     �     �     �  Z   �     6     K  
   S     ^  
   r     }     �     �  )   �  #   �  ?   �     <     I     W  
   \  d   g  _   �  e   ,  c   �  _   �     V     b  
   g     r     �     �     �     �     �     �     �     �            -     &   M  $   t  "   �  (   �     �     �               )     D  :   P     �     �  
   �  $   �  &   �            B   '     j     p     �  �   �     >   5      3         '   !       /      ?              .          %   A                    -   6         =                                
         *          0       7   	   $   #       <   2       8      &                     (   +             9      "       4      )   :          ;              ,   1   @                  Advantages Awesome Buttons labels: Buttons layout: Compiz Compiz is installed by default in Linux Mint. Note: It is not available in LMDE. Compiz settings Compton Computer Configure Compiz Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Fine-tune desktop settings Here's a description of some of the window managers available. Home Icon size: Icons Icons only If Compton is not installed already, you can install it with <cmd>apt install compton</cmd>. If Marco is not installed already, you can install it with <cmd>apt install marco</cmd>. If Metacity is not installed already, you can install it with <cmd>apt install metacity</cmd>. If Openbox is not installed already, you can install it with <cmd>apt install openbox</cmd>. If Xfwm4 is not installed already, you can install it with <cmd>apt install xfwm4</cmd>. Interface Large Linux Mint Mac style (Left) Marco Marco + Compton Marco settings Metacity Metacity + Compton Metacity settings Mounted Volumes Network Openbox Openbox + Compton Openbox is a fast and light-weight window manager. Overview of some window managers Pros and cons of compositing Reset Compiz settings Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only The Windows section lets you choose a window manager. Toolbars Traditional style (Right) Trash Use system font in titlebar Welcome to Desktop Settings. Window Manager Windows Within the operating system, the <gui>window manager</gui> is the part which is responsible for: Xfwm4 Xfwm4 + Compton root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-07-13 16:52+0000
Last-Translator: Makenna Makesh <Unknown>
Language-Team: Norwegian Nynorsk <nn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Fordelar Storarta Knappemerke: Knappedrakt: Compiz Compiz er installert som standard i Linux Mint. Merk: Compiz er ikkje tilgjengeleg i LMDE. Compiz-innstillingar Compton Datamaskin Compiz-instillingar Skrivebord Skrivebordsinnstillingar Skrivebordsikon Ulemper Gøym innhaldet når vindauget blir drege Finjuster skrivebordsinnstillingane Her er ei skildring av nokre tilgjengelege vindaugehandsamarar. Heimeområde Ikonstorleik: Ikon Berre ikon Om Compton ikkje er installert frå før, kan du installere det med <cmd> apt install compton</cmd>. Om Marco ikkje er installert frå før, kan du installere det med <cmd>apt install marco</cmd>. Om Metacity ikkje er installert frå før, kan du installere det med <cmd>apt install metacity</cmd>. Om Openbox ikkje er installert frå før, kan du installere det med <cmd>apt install openbox</cmd>. Om Xfwm4 ikkje er installert frå før, kan du installere det med <cmd>apt install xfwm4</cmd>. Grensesnitt Stor Linux Mint Mac-stil (venstre) Marco Marco + Kompton Marco-innstillingar Metacity Metacity + Compton Metacity-innstillingar Monterte område/einingar Nettverk Openbox Openbox + Compton Openbox er ein rask og lett vindaugehandsamar Oversyn over nokre vindaugehandsamarar Fordelar og ulemper med samansetjing Tilbakestill Compiz-innstillingane Vel elementa du vil ha på skrivebordet: Vis ikon på knappane Vis ikon i menyane Liten Tekst under elementa Tekst ved sida av elementa Berre tekst Vindaugeinnstillingane lar deg velje ein vindaugehandsamar Verktøylinjer Tradisjonell stil (høgre) Papirkorga Bruk systemskrifttypen i tittellinja Velkommen til Skrivebordsinnstillingar Vindaugehandsamar Vindauge I operativsystemet er det <gui>window manager</gui> som handterer: Xfwm4 Xfwm4  + Compton root@linuxmint.com Launchpad Contributions:
  Jess https://launchpad.net/~vindefjasen
  Makenna Makesh https://launchpad.net/~makennamaliah
  Yngve Spjeld-Landro https://launchpad.net/~yslandro
  helgemol https://launchpad.net/~helgemol 