��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  S   �  ;   �  0         I  N   j     �  :   �                         (     C  %   J  *   p  #   �  u   �     5  /   U      �  5   �     �     �     �  
   �  *        2     5     9     <  �   @     �     �     �  =   �          -     C  4   S  )   �  /   �  %   �  (     	   1     ;     A     M     j          �     �  *   �     �          (  2   5     h     n  #   w                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-01-06 19:59+0000
Last-Translator: Mark Kwidzińsczi <Unknown>
Language-Team: Kashubian <csb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s nie je nastôwiony w kònfigùracjowim lopkù nalazłim pòd %(2)s abò %(3)s %(percentage)s %(number)d lopków - Wladënk do %(service)s %(percentage)s 1 lopka - Wladënk do %(service)s %(size_so_far)s z %(total_size)s %(size_so_far)s z %(total_size)s -òstało %(time_remaining)s (%(speed)s/sek.) %s Swòjiznë <b>Proszã wpisac miono nowi ùsłëżnotë sélaniô:</b> Ò B Òprzestań Òprzestac wladënk? Sprôwdzë sparłãczenié Zamkni Ni mòże sprôwdzëc wòlnegò placu Ni mòże zapisac zmianë kònfigùracëji Definiujë ùsłëżnotë sélaniô Katalog do sélaniô. <TIMESTAMP> je zastãpiony z aktualnym datownikã, zgódno z wëbrónym fòrmatã. Domëslno: . Chcesz òprzestac nen wladënk? Przecygni ë ùpùszczë tuwò bë sélac do %s Lopk je wikszy jakno wòlny plac Lopk je wikszô jakno zezwôlony przez ùsłëżnotã Lopk ùdało wësłóny. GB GiB Kòmpùtr: Miono serwera abò adresë IP, domëslny:  KB KiB MB MiB Parola, domëslno: sparłãcznié SCP bez paolë, pùstô wôrtnota dlô sparłãczenia FTP, klucze ~/.ssh dlô spałãczeniô SFTP Parola: Stegna: Pòrt: Daleczi pòrt, domëslny to 21 dlô FTP, 22 dlô SFTP ë SCIP Zrëszë w spódkù Miono ùsłëżnotë: Ùsłëżnotë: Ùdałi wladënk %(number)d lopków do '%(service)s' Ùdałi wladënk 1 lopka do '%(service)s' Wladënk do '%(service)s' òstôł òprzestóny Do ti ùsłëżnotë je nót parolë. Fòrmat datownika (strftime). Domëslno: Datownik: Ôrt: Adresa URL: Nieznónô ùsłëżnota: %s Menadżera sélaniô Menadżera sélaniô... Ùsłëżnotë sélaniô Fela wladënkù do '%s':  Wladënk %(number)d lopków do %(service)s Wladënk 1 lopka do %(service)s Sélanié lopka... Brëkòwnik: Miono brëkòwnika, domëslno takô jakno môlowô _Lopk _Pòmòc ùdałé domëslné sparłãczenié 