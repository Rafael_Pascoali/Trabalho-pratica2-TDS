��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  !   #  .   E     t     �     �  
  �  	   �
     �
     �
     �
          5     E     Z      j     �  H   �     �               /     >     O     V                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-04-23 16:34+0000
Last-Translator: H.G. <Unknown>
Language-Team: Chinese (Traditional) <zh_TW@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 複製映像檔時發生錯誤。 在 %s 建立磁碟分割區時發生錯誤。 發生錯誤。 驗證錯誤。 命名您的 USB 隨身碟 FAT32 
  + 相容性最佳。
  -  無法處理大於 4GB 的檔案。

exFAT
  + 幾乎每個平臺都可以相容。
  + 無法處理大於 4GB 的檔案。
  -  相容性比 FAT32 稍差。

NTFS 
  + 相容於 Windows。
  -  不相容 Mac 與大多數硬體裝置。
  -  在 Linux 下偶有相容性問題 (因 NTFS 是專有格式，遂以逆向工程完成)。

EXT4 
  + 現代化、穩定、快速、具日誌功能。
  + 支援 Linux 檔案權限。
  -  不相容於 Windows、Mac 與大多數硬體裝置。
 格式化 格式化 USB 隨身碟 製作可開機 USB 隨身碟 製作可開機 USB 隨身碟 USB 隨身碟空間不足。 選擇映像檔 選擇 USB 隨身碟 選擇映像檔 成功格式化 USB 隨身碟。 成功寫入映像檔。 這會銷毀 USB 隨身碟裡的所有資料，您是否確定要繼續? USB 映像寫入工具 USB 隨身碟 USB 隨身碟格式化程式 USB 隨身碟: 儲存區標籤: 寫入 未知 