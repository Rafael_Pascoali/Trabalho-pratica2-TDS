��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  4   �  A   +  #   m  #   �     �  �   �      �  &   �       H   0                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-07-19 05:50+0000
Last-Translator: myname <rezonans.89@mail.ru>
Language-Team: Tatar <tt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s дөрес булмаган домен исеме Сайланган доменнарга керүне бикләү Бикләнгән доменнар Доменнар бикләгече Домен исеме Домен исеме хәрефтән яисә саннан башланырга һәм тәмамланырга тиеш. Доменда хәрефләр, саннар, нокталар һәм дефислар гына кулланыла алалар. Үрнәк: my.number1domain.com Дөрес булмаган домен Ата-ана контроле Бикләргә теләгән домен исемен кертегез 