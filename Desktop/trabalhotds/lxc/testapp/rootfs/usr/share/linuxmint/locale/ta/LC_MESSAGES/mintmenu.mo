��    D      <  a   \      �  	   �     �     �                 K   &  %   r  -   �     �  "   �  F   �     E     N     d     s     �     �     �  	   �     �  	   �     �     �     �       -        G     N     c     o     �     �     �     �     �     �     �     �     �     �     �     	     	  1   	  6   F	     }	     �	  $   �	     �	     �	     �	     �	     
     
     4
     E
     Y
  '   n
     �
     �
     �
     �
     �
     �
  	   �
     �
  �  �
  2   �     �  E   �     (  7   D  !   |  �   �  i   �  �     S   �  o   �  �   d     C  J   S  7   �  W   �  Q   .     �  0   �  +   �  7   �  $   +     P  7   m  4   �  9   �  �        �  Y   �  .     Q   J     �     �  4   �     �            c   4  $   �  .   �     �  2   �     ,  $   B  �   g  �        �  D     b   P     �  O   �  >     b   U  G   �  V      M   W  A   �  G   �  �   /  4   �  d        j     �     �  +   �  (   �  D        2                   >   C   A   +             !      (   &             "           :                 ?                   *         $   B       -       7      .          6   8           9                 ;   4   /       5   D   )           0   <   '                        3           1      #   @                              	      =      
       %   ,    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-03 14:39+0000
Last-Translator: Kamala Kannan <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <அமைக்கப்படவில்லை> இதைப்பற்றி மேம்படுத்தப்பட்ட MATE பட்டி அனைத்தும் அனைத்து பயன்பாடுகள் பயன்பாடுகள் இந்த கணினியால் அணுக முடிந்த அனைத்து உள்ளமை மற்றும் தொலை வட்டுகள் மற்றும் கோப்புறைகளில் உலாவு உலாவி கிடைக்கும் மென்பொருளை நிறுவவும் புத்தகக்குறியிட்ட மற்றும் உள்ளமை பிணைய இடங்களில் உலாவு நீக்கப்பட்ட கோப்புகளில் உலாவு பணிமேடையின் மீதுள்ள உருப்படிகளில் உலாவு பட்டியைத் திறந்து மூடுவதற்கு ஒரு புதிய முடுக்கு விசையை அமைக்க இங்கே சொடுக்கவும்.   கணினி உங்கள் கணினியைக் கட்டமைக்க கட்டுப்பாட்டு மையம் செருகுநிரலை துவக்க முடியவில்லை: செருகுநிரலை ஏற்ற முடியவில்லை: பணிமேடை பணிமேடையலங்காரம் திருத்தப் பட்டி குப்பையை காலியாக்கு பிடித்தவைகள் இல்ல அடைவு பிரிப்பானைச் செருகு இடைவெளியைச் செருகு '%s' தொகுப்பை நிறுவவும் மென்பொருள் தொகுப்புகளை நிறுவ, நீக்க மற்றும் மேம்படுத்த துவக்கு நான் புகுபதியும்போது துவக்கவும் திரையைப் பூட்டுக விடுபதிகை அல்லது பயனர் மாற்று விடுபதிகை பட்டி பட்டி விருப்பங்கள் பெயர் பெயர்: பிணையம் உங்கள் தனிப்பட்ட அடைவைத் திறக்கவும் விருப்பங்கள் தொகுப்பு மேலாளர் பாதை ஒரு முடுக்கியை எடு இடங்கள் விருப்பங்கள் தற்போதுள்ள விசைப்பிணைப்பை அழிக்க பின் நகர்வு விசையை அழுத்தவும். செயல்பாட்டை ரத்து செய்ய விடுபடு விசையை அழுத்தவும் அல்லது மீண்டும் சொடுக்கவும்.   வெளியேறு செருகுநிரல்களை மீளேற்று. கடந்த வகை அல்லது தேடலை நினைவில் கொள் நீக்கு பிடித்தவைகளிலிருந்து அகற்று திறக்க கடவுச்சொல் தேவை நிறுவுவதற்கு தொகுப்புகளைத் தேடவும் ஒரு கோப்புறையை தேர்ந்தெடு அனைத்து பயன்பாடுகளையும் காட்டு பொத்தான் சின்னத்தைக் காட்டு வகை சின்னங்களைக் காட்டு என் பிடித்தவைகளில் காண்பி பணிநிறுத்தம், மறுதொடக்கம், இடைநிறுத்தம் அல்லது துயில் கொள் மென்பொருள் மேலாளர் பெயர் மற்றும் பொதுவான பெயரை இடமாற்று அமைப்பு முனையம் அலங்காரம்: குப்பைத் தொட்டி நிறுவலை நீக்கு கட்டளை வரியை பயன்படுத்து 