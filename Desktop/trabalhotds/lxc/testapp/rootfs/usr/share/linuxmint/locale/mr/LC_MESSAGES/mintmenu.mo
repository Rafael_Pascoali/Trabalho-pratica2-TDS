��    ,      |  ;   �      �     �     �     �     �     �  "        )     2     A     \     r     z  	   �     �  -   �     �     �     �     �     �     �     �          !     1     6     =     I     N     ]     d     z     �     �     �     �     �     �                    %  	   +  �  5     �     �  (   �     "  B   >  ?   �     �  +   �  D   �  @   B	     �	  2   �	     �	  (   �	  �   
  /   �
     �
  4   �
  	             +  B   H     �  %   �  	   �     �  '   �     	  :        X  g   r  W   �  "   2  8   U  -   �  T   �  :     `   L     �     �  
   �     �     �     (   #   )                                                 
                                 *                    '       !          %   	   ,   +      $   &                                  "              About All All applications Applications Browse deleted files Browse items placed on the desktop Computer Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Empty trash Favorites Home Folder Install, remove and upgrade software packages Lock Screen Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Places Preferences Quit Reload plugins Remove Remove from favorites Requires password to unlock Select a folder Show all applications Show button icon Show in my favorites Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-10 15:50+0000
Last-Translator: Vikram Kulkarni <kul.vikram@gmail.com>
Language-Team: Marathi <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 च्या विषयी सर्व सर्व कार्यक्रम कार्यक्रम काढून टाकलेल्या फाईल पहा डेस्कटॉप वरील वस्तू पहा संगणक नियंत्रण केंद्र प्लगीन सुरु नाही करता आले. प्लगीन लोड नाही करता आले डेस्कटॉप कचरापेटी रिकामी कर पसंतीचे गृह निर्देशिका कार्यक्रमाचे संकुल स्थापित कर, काढून टाक किंवा उन्नत कर स्क्रीन सीलबंद कर सूची सूची प्राधान्यक्रम नाव नावः संगणक जाळे खाजगी निर्देशिका खुली कर विकल्प संकुल प्रबंधक पाथ ठिकाणे प्राधान्यक्रम बंद करा प्लगीन पुन्हा लोड करा. काढून टाक 'माझ्या आवडीचे' या रकान्यातून काढून टाक उघडण्यासाठी पासवर्ड ची आवश्यकता फोल्डर निवडा सर्व कार्यक्रम दाखवा बटन चे चिन्ह दाखव 'माझ्या आवडीचे' या रकान्यात दाखव सॉफ्टवेयर व्यवस्थापक नाव आणि सामान्य नाव यांची अदलाबदल कर प्रणाली टर्मिनल थीम: कचरापेटी काढून टाक 