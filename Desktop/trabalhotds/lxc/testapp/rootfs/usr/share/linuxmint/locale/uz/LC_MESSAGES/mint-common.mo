��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  %   2     X     w  *   �  :   �  ,   �  )   "  2   L  .     *   �     �  X   �     D	     \	     d	     l	  *   x	     �	  6   �	     �	     �	     
  !   
  "   A
  j   d
  $   �
  	   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 19:32+0000
Last-Translator: Umidjon Almasov <Unknown>
Language-Team: Uzbek <uz@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: uz
 %s koʻproq disk maydoni ishlatiladi. %s disk maydoni boʻshatiladi. %s jami yuklab olinadi. Qo‘shimcha o‘zgarishlar talab qilinadi Qo‘shimcha dasturiy ta'minotning versiyasi pasaytiriladi Qo‘shimcha dasturiy ta'minot o‘rnatiladi Qo‘shimcha dasturiy ta'minot tozalanadi Qo‘shimcha dasturiy ta'minot qayta o‘rnatiladi Qo‘shimcha dasturiy ta'minot olib tashlanadi Qo‘shimcha dasturiy ta'minot yangilanadi Xatolik yuz berdi %s paketini olib tashlab boʻlmaydi, chunki bu boshqa paketlar tomonidan talab qilinadi. Versiyasini pasaytirish Flatpak Flatpak O‘rnatish %s paketi quyidagi paketlarga bogʻliqdir: Olib tashlanadigan paketlar Quyidagi o‘zgarishlar ro‘yxatini ko‘rib chiqing. Tozalash Qayta o‘rnatish Olib tashlash Yangilanishni o‘tkazib yuborish Quyidagi paketlar olib tashlanadi: Menyudagi ushbu element xech qanday dastur bilan bog‘lanmagan. Uni menyudan olib tashlashni xohlaysizmi? Yangilanishlar o‘tkazib yuboriladi Yangilash 