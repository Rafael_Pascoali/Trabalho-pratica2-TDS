��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  K   �  `   E  0   �  3   �       S  '  .   {  -   �  0   �  ]   	                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-22 15:25+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Thai <th@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ไม่ใช่ชื่อโดเมนที่ใช้ได้ บล็อกการเข้าถึงชื่อโดเมนที่เลือก โดเมนที่บล็อกไว้ โปรแกรมบล็อกโดเมน ชื่อโดเมน ชื่อโดเมนต้องขึ้นต้นและลงท้ายด้วยตัวอักษร หรือตัวเลขเท่านั้น  และป้อนได้เฉพาะตัวอักษร ตัวเลข จุด และยัติภังค์เท่านั้น ตัวอย่าง: my.number1domain.com โดเมนไม่ถูกต้อง ส่วนของผู้ปกครอง โปรดพิมพ์ชื่อโดเมนที่คุณจะบล็อก 