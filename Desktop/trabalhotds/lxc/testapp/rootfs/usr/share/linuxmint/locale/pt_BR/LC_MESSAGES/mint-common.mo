��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  -   E  &   s     �  )   �  $   �  "     1   *  &   \  !   �  #   �     �  J   �     $	     5	     =	     F	  6   O	     �	  0   �	     �	  
   �	     �	     �	  &   
  \   (
  "   �
  	   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-06 21:44+0000
Last-Translator: Gilberto vagner <vagner.unix@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s de espaço em disco adicional será usado. %s de espaço em disco será liberado. %s irá ser baixado no total. Alterações adicionais são necessárias O software adicional será rebaixado Software adicional será instalado O software adicional será eliminado por completo O software adicional será reinstalado Software adicional será removido Software adicional será atualizado Ocorreu um erro Não é possível remover o pacote %s, pois é exigido por outros pacotes. Rebaixar versão Flatpak Flatpaks Instalar O pacote %s é uma dependência dos seguintes pacotes: Pacotes a serem removidos Por favor verifique a lista de mudanças abaixo. Eliminar Reinstalar Remover Pular atualização Os seguintes pacotes serão removidos: Esse item do menu não está associado a nenhum pacote. Você quer mesmo removê-lo do menu? As atualizações serão ignoradas Atualizar 