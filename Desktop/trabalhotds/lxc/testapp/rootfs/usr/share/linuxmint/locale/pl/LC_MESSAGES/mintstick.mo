��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  ,   �  3   �     �     �     	     "     0     9     H     ^  '   }  @   �     �          �     �     �     �     �     �     �     �     �  
   �  
          ,     -   C     q  )   t     �     �     �     �     �  &      !   '     I     U     e     v     �     �     �     �     �     �  +   �  0   �  #   0  ,   T     �  U   �      �  1     "   J      m  1   �  2   �  8   �  0   ,     ]  h   z  
   �  !   �               8     E     U  
   g  2   r     �     �  3   �     �     �          K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-26 17:23+0000
Last-Translator: Marek Adamski <Unknown>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Wystąpił błąd podczas kopiowania obrazu. Wystąpił błąd podczas tworzenia partycji na %s. Wystąpił błąd Wystąpił błąd. Błąd uwierzytelniania. Obliczanie... Sprawdź Suma kontrolna Pliki sum kontrolnych Suma kontrolna się nie zgadza Wybierz nazwę dla swojego nośnika USB Pobierz ponownie obraz ISO. Jego suma kontrolna się nie zgadza. Wszystko wygląda dobrze! FAT32 
  + Kompatybilny ze wszystkim.
  - Nie może obsłużyć plików większych niż 4GB.

exFAT
  + Kompatybilny z prawie wszystkim.
  + Może obsługiwać pliki większe niż 4GB.
  - Nie tak kompatybilny jak FAT32.

NTFS 
  + Kompatybilny z systemami Windows.
  - Niekompatybilny z Macami i większością urządzeń.
  - Zdarzają się problemy z kompatybilnością na Linuksie (NTFS jest zastrzeżony i został odtworzony za pomocą inżynierii wstecznej).

EXT4
  + Nowoczesny, stabilny, szybki, udokumentowany.
  + Wspiera Linuksowy system uprawnień.
  - Niekompatybilny z systemami Windows, Mac oraz większością urządzeń.
 System plików: Formatuj Formatuj nośnik USB KB Podpisy GPG Podpisany plik GPG Podpisany plik GPG: Wstecz Weryfikacja ISO Obraz ISO: Obrazy ISO ISO: Jeśli ufasz podpisowi, możesz zaufać ISO. Sprawdzenie integralności nie powiodło się KB Nie znaleziono klucza na serwerze kluczy. Lokalne pliki MB Utwórz rozruchowy nośnik USB Utwórz rozruchowy nośnik USB Więcej informacji Nie znaleziono identyfikatora woluminu Za mało miejsca na nośniku USB. Suma SHA256 Plik sum SHA256 Plik sum SHA256: Suma SHA256: Wybierz obraz Wybierz nośnik USB Wybierz obraz Podpisany przez: %s Rozmiar: TB Suma SHA256 obrazu ISO jest nieprawidłowa. Plik sum SHA256 nie zawiera sum tego obrazu ISO. Plik sum SHA256 nie jest podpisany. Nośnik USB został pomyślnie sformatowany. Suma kontrolna jest prawidłowa Suma kontrolna jest prawidłowa, ale autentyczność sumy nie została zweryfikowana. Nie można sprawdzić pliku GPG. Nie można pobrać pliku GPG. Sprawdź adres URL. Obraz został pomyślnie zapisany. Nie można sprawdzić pliku sum. Nie można pobrać pliku sum. Sprawdź adres URL. Ten obraz ISO jest weryfikowany zaufanym podpisem. Ten obraz ISO jest weryfikowany przez niezaufany podpis. Ten plik ISO wygląda jak obraz systemu Windows. To jest oficjalny obraz ISO. To działanie wymaże wszystkie dane znajdujące się na nośniku USB. Czy na pewno chcesz kontynuować? Adresy URL Zapisywanie obrazów płyt na USB Nośnik USB Formatowanie nośników USB Nośnik USB: Nieznany podpis Niezaufany podpis Zweryfikuj Zweryfikuj autentyczność i integralność obrazu Etykieta wolumenu: Wolumin: Obrazy Windows wymagają specjalnego przetwarzania. Zapisz bajty nieznany 