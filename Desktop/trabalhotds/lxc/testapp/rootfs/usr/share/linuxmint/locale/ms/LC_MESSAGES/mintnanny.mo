��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �  -        H     ]     o  �   {          "     3  +   C                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-18 06:47+0000
Last-Translator: Fazwan Fazil <takaizu91@gmail.com>
Language-Team: Malay <ms@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s bukan nama domain yang sah Halang laluan kepada nama domain yang dipilih Domain yang dihalang Penghalang domain Nama Domain Nama domain mesti dimulakan atau diakhiri dengan satu abjad atau digit, dan hanya boleh mengandungi abjad, digit, titik dan tanda hubung. Contoh: my.number1domain.com Domain tidak sah Kawalan penjaga Sila taip nama domain yang anda ingin sekat 