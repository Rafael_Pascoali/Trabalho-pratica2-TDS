��          �   %   �      0  )   1  2   [     �     �      �     �     �     �       "   $     G     T     g  )   w  #   �  N   �       	   %     /  
   C     N     \     b  �  j  d     k   r  .   �  2     V   @     �  0   �  =   �  =     R   W  %   �  3   �  ,   	  |   1	  X   �	  �   
  9   �
     �
  O   �
     L  )   `     �     �                	   
                                                                                                            An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-01-11 10:29+0000
Last-Translator: Vikram Kulkarni <kul.vikram@gmail.com>
Language-Team: Marathi <mr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 प्रतिमा कॉपी करताना एक त्रुटी आली आहे. %s वर विभाजन तयार करताना एक त्रुटी आली आहे. एक त्रुटी आली आहे. प्रमाणिकरण त्रुटी. आपल्या USB स्टिकसाठी नाव निवडा करा फॉरमॅट USB स्टिक फॉरमॅट करा बूटेबल USB स्टिक तयार करा बूटेबल USB स्टिक तयार करा USB स्टिक वर पुरेशी जागा नाही आहे. प्रतिमा निवडा USB स्टिकची निवडा करा प्रतिमा निवड करा USB स्टिक यशस्वीरित्या स्वरुपित करण्यात आली आहे. प्रतिमा यशस्वीरित्या लिहिले आहे. या USB  स्टिकवरील सर्व डाटा नष्ट होईल, आपण पुढे जाऊ इच्छित आहात का ? USB वर प्रतिमा लिहीनारा USB स्टिक USB स्टिक फॉरमॅट करणारी प्रणाली USB स्टिक व्हॉल्यूम लेबल: लिहा अपरिचित 