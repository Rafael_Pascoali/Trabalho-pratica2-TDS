��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  %   1  "   W  $   z  *   �  *   �  )   �  (     -   H  &   v  *   �     �  N   �     2	     >	     F	     O	  5   \	     �	  :   �	     �	     �	  	   �	     
      $
  V   E
  %   �
     �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 18:15+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s mehr Speicherplatz wird verwendet. %s Speicherplatz wird freigegeben. %s werden insgesamt heruntergeladen. Zusätzliche Änderungen sind erforderlich Zusätzliche Programme werden herabgestuft Zusätzliche Programme werden installiert Zusätzliche Programme werden gesäubert Zusätzliche Programme werden neu installiert Zusätzliche Programme werden entfernt Zusätzliche Programme werden aktualisiert Ein Fehler ist aufgetreten Paket %s kann nicht entfernt werden, da es von anderen Paketen benötigt wird. Herabstufen Flatpak Flatpaks Installieren Das Paket %s ist von den folgenden Paketen abhängig: Zu entfernende Pakete Bitte beachten Sie das unten stehende Änderungsprotokoll. Säubern Neu installieren Entfernen Aktualisierung überspringen Folgende Pakete werden entfernt: Dieser Menüpunkt ist keinem Paket zugeordnet. Wollen Sie ihn aus dem Menü entfernen? Aktualisierungen werden übersprungen Aktualisieren 