��    D      <  a   \      �  	   �     �     �                 K   &  %   r  -   �     �  "   �  F   �     E     N     d     s     �     �     �  	   �     �  	   �     �     �     �       -        G     N     c     o     �     �     �     �     �     �     �     �     �     �     �     	     	  1   	  6   F	     }	     �	  $   �	     �	     �	     �	     �	     
     
     4
     E
     Y
  '   n
     �
     �
     �
     �
     �
     �
  	   �
     �
  �  �
     �     �     �  
   �     �  	   �  `     ,   d  :   �     �  '   �  ^     
   q     |     �     �  /   �     �     �               ,     ;     P     _     q  /   �     �     �     �     �     �     �                    $     +     J     S     h     o     �     �  M   �  I   �     2     6  (   L     u     z  &   �  $   �     �     �          (     C  &   _     �  '   �     �  	   �     �     �  	   �     �     2                   >   C   A   +             !      (   &             "           :                 ?                   *         $   B       -       7      .          6   8           9                 ;   4   /       5   D   )           0   <   '                        3           1      #   @                              	      =      
       %   ,    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-04-16 16:46+0000
Last-Translator: Indrit Bashkimi <indrit.bashkimi@gmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <Nuk është caktuar> Rreth Menu MATE e avancuar Të Gjitha Të gjithë programet Programet Shfleton të gjithë disqet dhe kartelat lokale dhe në distancë të arritshme nga ky kompjuter Shfleto dhe instalo programet e disponueshme Shfleton pozicionet e rrjetit lokal dhe në libërshënues Shfletoni skedarët e fshirë Shfletoni temat e vendosura në desktop Klikoni për të vendosur një çelës të ri përshpejtues për hapjen dhe mbylljen e menu.   Kompjuteri Konfiguroni sistemin tuaj Qendra e Kontrollit Nuk mund të nisja plugin Nuk është e mundur të ngarkohet prapashtesa: Desktopi Tema e desktopit Ndrysho menunë Zbraze koshin Të Preferuara Kartela e Shtëpisë Vendos ndarës Vendos hapësirë Instalo paketën '%s' Instalo, hiq dhe aktualizo paketat e programeve Nise Nise kur unë hyj Blloko Ekranin Dil ose ndrysho përdoruesin Dil Menu Preferencat e menusë Emri Emri: Rrjeti Hapni kartelën tuaj personale Opsionet Menaxheri i Paketave Shtegu Pick një përshpejtues Vendet Preferencat Shtypni Backspace për të pastruar pulsantët e rrugëshkurtimit ekzistuese. Shtypni arratisjes ose klikoni përsëri për të anulluar operacionin.   Dil Ringarko prapashtesat Kujto kategorinë ose kërkimin e fundit Hiqe Hiqe nga të favorizuarat kërkon fjalëkalim për tu zhbllokuar Kërko për paketa për ti instaluar Zgjidhe një Dosje Shfaqi të gjitha programet Shfaq ikonën e butonave Shfaq ikonat e kategorisë Shfaqe në favoritet e mija Fike, rindize, pezulloje apo hibernoje Menaxhuesi i Programeve Ndrysho emrin me emrin e përgjithshëm Sistemi Terminali Tema: Koshi Çinstalo Përdor rreshtin e komandave 