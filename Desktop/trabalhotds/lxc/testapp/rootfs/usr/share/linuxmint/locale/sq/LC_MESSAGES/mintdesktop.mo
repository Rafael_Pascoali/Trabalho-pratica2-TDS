��    7      �  I   �      �  
   �     �     �     �  �   �     �     �     �     �     �     �  -   �     �  >        R  
   W     b  
   h  \   s  X   �  ^   )  \   �  X   �  	   >     H  
   N     Y     j     p     �     �     �     �     �     �  2   �      	  0   "	     S	     i	     }	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     
     
     "
     5
  �  H
     �     �          $  �   +     �  
   �       $        1     D  A   R     �  R   �               %     ,  a   :  ]   �  c   �  a   ^  ]   �  
        )  
   1     <     N     T     d     m     �     �     �     �  @   �  .   �  4   $     Y     r     �     �     �     �     �     �     �  2   �     %     6     ?     E     U  �   h         "   .           (             %             	              &   !          )   '   +   3   4   /   2   #                     *                 6      ,      1             
                               5                      -            0         7   $    Advantages Buttons labels: Buttons layout: Compiz Compiz is a very impressive window manager, very configurable and full of amazing effects and animations. It is however the least reliable and the least stable. Compton Computer Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Fine-tune desktop settings Here's a description of some of the window managers available. Home Icon size: Icons Icons only If Compton is not installed already, you can install it with <cmd>apt install compton</cmd>. If Marco is not installed already, you can install it with <cmd>apt install marco</cmd>. If Metacity is not installed already, you can install it with <cmd>apt install metacity</cmd>. If Openbox is not installed already, you can install it with <cmd>apt install openbox</cmd>. If Xfwm4 is not installed already, you can install it with <cmd>apt install xfwm4</cmd>. Interface Large Linux Mint Mac style (Left) Marco Marco + Compton Metacity Metacity + Compton Mounted Volumes Network Openbox Openbox + Compton Openbox is a fast and light-weight window manager. Overview of some window managers Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Window Manager Windows Xfwm4 Xfwm4 + Compton root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-07-21 13:14+0000
Last-Translator: Indrit Bashkimi <indrit.bashkimi@gmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Përfitimet Etiketat e butonave: Dalja e butonave: Compiz Compiz është një menaxher dritaresh mbresëlënës, shumë i konfigurueshëm dhe me plot efekte dhe animacione mahnitëse. Megjithatë, është më pak i besueshëm dhe më pak i qëndrueshëm. Compton Kompjuteri Desktopi Rregullimet e Hapësirës së Punës Ikonat e desktopit Disavantazhet Mos e shfaq përmbajtjen e dritareve ndërkohë që i zvarrit ato Rregullo parametrat e desktopit Këtu është një përshkrim i disa menaxherëve të dritareve të disponueshëm. Shtëpia Madhësia e ikonës: Ikonat Vetëm ikonat Nëse Compton nuk është akoma i instaluar, mund ta instaloni me <cmd>apt install compton</cmd>. Nëse Marco nuk është akoma i instaluar, mund ta instaloni me <cmd>apt install marco</cmd>. Nëse Metacity nuk është akoma i instaluar, mund ta instaloni me <cmd>apt install metacity</cmd>. Nëse Openbox nuk është akoma i instaluar, mund ta instaloni me <cmd>apt install openbox</cmd>. Nëse Xfwm4 nuk është akoma i instaluar, mund ta instaloni me <cmd>apt install xfwm4</cmd>. Ndërfaqja E madhe Linux Mint Stil Mac (Majtas) Marco Marco + Compton Metacity Metacity + Compton Volumet e Montuara Rrjeti Openbox Openbox + Compton Openbox është një menaxher dritaresh i shpejtë dhe i lehtë. Përmbledhje e disa menaxherëve të dritareve Zgjidh temat që dëshiron të shohësh në desktop: Shfaq ikonat tek butonat Shfaq ikonat në menu E vogël Teksti poshtë temave Teksti midis temave Vetëm teksti Panele Stil tradicional (Djathtas) Koshi Përdor gërmat e sistemit në shiritin e titullit Menaxheri Window Dritaret Xfwm4 Xfwm4 + Compton root@linuxmint.com Launchpad Contributions:
  Indrit Bashkimi https://launchpad.net/~indritbsh
  Vilson Gjeci https://launchpad.net/~vilsongjeci
  mzs.112000 https://launchpad.net/~mzs-112000 