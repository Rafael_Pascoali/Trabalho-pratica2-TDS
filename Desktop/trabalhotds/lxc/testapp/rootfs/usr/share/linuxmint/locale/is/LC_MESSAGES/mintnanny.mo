��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     ,  !   K  
   m  	   x     �  �   �          "     .  2   >                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-28 11:58+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: is
 %s er ekki gilt nafn á léni. Hindra aðgang að völdum lénum Læst lén Lénalás Heiti léns Heiti léna verða að byrja og enda á staf eða tölu, og mega einungis innihalda bókstafi, tölustafi, bandstrik eða punkta. Dæmi: netfang.is Ógilt lén Foreldrastjórn Skráðu inn heiti lénsins sem þú vilt útiloka 