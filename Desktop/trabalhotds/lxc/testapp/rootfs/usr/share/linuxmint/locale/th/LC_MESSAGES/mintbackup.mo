��            )   �      �  )   �  4   �  )         *     6     B     J     Y     f     z  $   �  !   �     �     �     �  "        8     @  
   G  
   R  
   ]     h     {     �     �  g   �  B   7  &   z  )   �  �  �  W   e  z   �  K   8  1   �  ?   �  *   �  !   !	  0   C	  6   t	  -   �	  N   �	  Q   (
  '   z
  Q   �
  6   �
  Q   +  3   }     �     �  "   �  $   �  *   "  E   M  C   �  <   �  �     �     W   �  a   �                                                        
                                                                       	          %s is not located in your home directory. An error occurred while opening the backup file: %s. An error occurred while reading the file. Backing up: Backup Tool Backups Calculating... Deselect all Exclude directories Exclude files Make a backup of your home directory No packages need to be installed. Personal data Please choose a backup file. Please choose a directory. Please select packages to install. Refresh Remove Restore... Restoring: Select all Software selection The backup was aborted. The following errors occurred: The restoration was aborted. This backup file is either too old or it was created with a different tool. Please extract it manually. You do not have the permission to write in the selected directory. Your files were successfully restored. Your files were successfully saved in %s. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-01-16 10:08+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Thai <th@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ไม่อยู่ในไดเรกทอรีบ้านของคุณ เกิดข้อผิดพลาดขณะเปิดไฟล์การสำรองข้อมูล: %s. เกิดข้อผิดพลาดขณะอ่านแฟ้ม กำลังสำรองข้อมูล: เครื่องมือสำรองข้อมูล การสำรองข้อมูล กำลังคำนวณ... เลิกเลือกทั้งหมด ทำการยกเว้นตำแหน่ง ทำการยกเว้นแฟ้ม สำรองข้อมูลในตำแหน่งของคุณ ไม่มีแพคเกจที่จะต้องติดตั้ง ข้อมูลส่วนตัว โปรดเลือกไฟล์การสำรองข้อมูล โปรดเลือกไดเรกทอรี โปรดเลือกแพคเกจที่จะติดตั้ง เรียกใหม่อีกครั้ง เอาออก กู้คืน... กำลังกู้คืน: เลือกทั้งหมด เลือกซอฟต์แวร์ การสำรองข้อมูลถูกยกเลิก เกิดข้อผิดพลาดต่อไปนี้: การเรียกคืนถูกยกเลิก ไฟล์การสำรองข้อมูลนี้เก่าเกินไปหรือถูกสร้างด้วยเครื่องมืออื่น โปรดแยกไฟล์นี้ด้วยตนเอง คุณไม่มีสิทธิ์ในการเขียนในไดเรกทอรีที่เลือก แฟ้มของคุณถูกกู้คืนสำเร็จแล้ว ได้บันทึกไฟล์ของคุณใน %s สำเร็จแล้ว 