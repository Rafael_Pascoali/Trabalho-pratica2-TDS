��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  *   +  2   V     �     �  #   �  :  �  
        #  )   :  )   d  1   �     �     �     �  #   �        Y   =     �     �     �     �     �     �  	   �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-05-25 16:06+0000
Last-Translator: Saša Marjanović <Unknown>
Language-Team: Serbian Latin <sr@latin@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Došlo je do greške pri kopiranju kopije. Greška se dogodila pri stvaranju particije na %s. Dogodila se greška. Greška u autentifikaciji. Izaberite naziv za Vaš USB uređaj FAT32 
  + Kompatibilan svuda.
  - Ne može upravljati datotekama većim od 4GB.

exFAT
  + Kompatibilan gotovo svuda.
  + Može upravljati datotekama većim od 4GB.
  - Nije kompatibilan sa FAT32.

NTFS 
  + Kompatibilan s Windows-om.
  - Nije kompatibilan s Mac-om i većinom hardverskih uređaja.
  - Povremeni problemi s kompatibilnost na Linuxu (NTFS je vlasnički i napravljen obrnutim inženjeringom).

EXT4 
  + Moderan, stabilan, brz, dnevnički.
  + Podržava dozvole Linux datoteka.
  - Nije kompatibilan s Windowsima, Mac-om i većinom hardverskih uređaja.
 Formatiraj Formatiraj USB uređaj Napravi USB uređaj za pokretanje sistema Napravi USB uređaj za pokretanje sistema Nema dovoljno slobodnog prostora na USB uređaju. Izaberite kopiju Izaberi USB uređaj Izaberite jednu kopiju USB uređaj je uspešno formatiran. Kopija je uspešno zapisana. Ovo će uništiti sve podatke na USB uređaju, da li ste sigurni da želite da nastavite? USB pisatelj kopija USB uređaj Formatiranje USB uređaja USB uređaj: Oznaka diska: Upiši nepoznato 