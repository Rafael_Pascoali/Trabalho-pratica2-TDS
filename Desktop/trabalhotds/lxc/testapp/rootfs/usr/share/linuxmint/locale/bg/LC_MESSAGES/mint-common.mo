��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  N     N   e  +   �  A   �     "  �   A     �     �  @   �  &   '  T   N     �  C   �  �   �     �                                       	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-20 20:44+0000
Last-Translator: Nikolay Trifonov <trifonov88@gmail.com>
Language-Team: Bulgarian <bg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Ще бъде използвано %s дисково пространство. Ще бъде освободено %s дисково пространство. ще бъдат свалени общо %s. Необходими са допълнителни промени Получи се грешка Пакетът %s не може да бъде премахнат тъй като други пакети зависят от него. Flatpaks Инсталирай Следните пакети зависят от пакет %s: Пакети за премахване Моля, разгледайте списъка с промените по-долу. Премахни Следните пакети ще бъдат премахнати: Тези елементи от менюто не са асоциирани с никой пакет. Искате ли да ги премахнете? Актуализирай 