��    +      t  ;   �      �  ?   �  B   �  !   <  $   ^     �     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �     �  	              )     7     >     F     R  
   ^     i     |     �     �     �  3   �  *   �     	          0     @     M     a     i  �  m  c   *  p   �  ;   �  >   ;	     z	     �	     �	     �	     �	     �	     �	     
     
     +
     A
     [
     h
     x
     �
  H   �
     �
          '     G     T     d     �     �  5   �     �       -     /   ?  m   o  |   �  5   Z  +   �  #   �     �  /   �     *     :            "         !                    (                          +   '                %       *                                #       &                	          $                 )   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-03-13 23:41+0000
Last-Translator: Navdeep Singh <Unknown>
Language-Team: Punjabi <pa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ਡਾਉਨਲੋਡ ਲਈ %(downloadSize)s, %(localSize)s ਡਿਸਕ ਸਪੇਸ ਖਾਲੀ ਡਾਉਨਲੋਡ ਲਈ %(downloadSize)s, %(localSize)s ਡਿਸਕ ਸਪੇਸ ਚਾਹੀਦੀ ਹੈ %(localSize)s ਡਿਸਕ ਥਾਂ ਖਾਲੀ ਹੋਈ %(localSize)s ਡਿਸਕ ਥਾਂ ਚਾਹੀਦੀ ਹੈ 3ਡੀ ਇਸ ਬਾਰੇ ਸਹੂਲਤਾਂ ਬੋਰਡ ਖੇਡਾਂ ਗੱਲਾਂਬਾਤਾਂ ਵੇਰਵਾ ਡਰਾਇੰਗ ਸਿੱਖਿਆ ਈਮੇਲ ਐਮੁਲੇਟਰ ਫਾਇਲ ਸਾਂਝ ਫੋਂਟ ਖੇਡਾਂ ਗਰਾਫਿਕਸ ਇੰਸਟਾਲ ਕਰੋ ਨਵੀਆਂ ਐਪਲੀਕੇਸ਼ਣ ਇੰਸਟਾਲ ਕਰੋ ਇੰਸਟਾਲ ਹੈ ਇੰਟਰਨੈੱਟ ਇੰਸਟਾਲ ਨਹੀਂ ਦਫਤਰ ਪੈਕੇਜ ਫੋਟੋਗਰਾਫ਼ੀ ਪਰੋਗਰਾਮਿੰਗ ਪਬਲਿਸ਼ਿੰਗ ਵਾਸਤਵਿਕ-ਸਮਾਂ ਰਣਨੀਤਿ ਹਟਾਓ ਰੀਵਿਊ ਸਕੈਨ ਕੀਤਾ ਰਿਹਾ ਹੈ ਵਿਗਿਆਨ ਅਤੇ ਵਿੱਦਿਆ ਪੈਕੇਜਾਂ ਦੇ ਵੇਰਵੇ ਵਿੱਚ ਭਾਲ (ਹੋਰ ਵੀ ਹੋਲੀ ਭਾਲ) ਪੈਕੇਜ ਸੰਖੇਪ ਜਾਣਕਾਰੀ ਵਿੱਚ ਖੋਜ (ਖੋਜ ਹੌਲੀ ਹੁੰਦੀ ਹੈ) ਸਿਮੁਲੇਸ਼ਨ ਅਤੇ ਰੇਸਿੰਗ ਸਾਫਟਵੇਅਰ ਮੈਨੇਜਰ ਸਾਊਡ ਤੇ ਵਿਡੀਓ ਸਿਸਟਮ ਟੂਲ ਚਾਲ-ਅਧਾਰਿਤ ਰਣਨੀਤਿ ਦਰਸ਼ਕ ਵੈੱਬ 