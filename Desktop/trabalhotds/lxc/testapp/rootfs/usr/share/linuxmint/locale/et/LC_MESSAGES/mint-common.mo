��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  #   	     -     I     c       >   �  	   �     �  1   �       *   $     O      W  Z   x     �                                       	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-06-23 06:16+0000
Last-Translator: mahfiaz <mahfiaz@gmail.com>
Language-Team: Estonian <et@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s kettaruumi võetakse kasutusele. %s kettaruumi vabastatakse. Kokku laaditakse alla %s. Vajalikud on lisamuudatused Tekkis viga Paketti %s ei saa eemaldada, sest seda vajavad teised paketid. Flatpakid Paigalda Pakett %s on vajalik järgnevate pakettide jaoks: Eemaldatavad paketid Palun vaata all olevat muudatuste loendit. Eemalda Eemaldatakse järgmised paketid: See menüükirje pole seotud ühegi paketiga. Kas soovid selle menüüst siiski eemaldada? Uuenda 