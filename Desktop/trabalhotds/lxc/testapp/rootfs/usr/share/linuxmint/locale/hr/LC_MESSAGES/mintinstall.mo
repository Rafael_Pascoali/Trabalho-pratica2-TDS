��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v    �  Q   �  V   �  0   I  5   z  &   �  >   �       
        $     -     3     7     F  A   ^     �     �     �  e   �     5     H  ?   Q  '   �     �     �     �     �     �     �       	             !     5     F  ]   S  H   �     �        ,        2  
   :  
   E     P     k     w     �  Q   �     �     �     �  $     
   '     2     H     e  &   l  ]   �     �  
             )     .     D     H     N  6   Z  /   �     �     �     �     �          
           2  
   9  O   D  	   �  
   �     �     �  6   �  4   �  9   3     m     �  
   �     �  8   �     �     �     
       9   &  ;   `     �     �     �     �  a   �  
   V  	   a     k     q     }     �     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-30 20:10+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s za preuzimanje, %(localSize)s zauzima na disku nakon instalacije %(downloadSize)s za preuzimanje, %(localSize)s je potrebno slobodnog prostora na disku %(localSize)s zauzima na disku nakon instalacije %(localSize)s je potrebno slobodnog prostora na disku %d recenzija %d recenzije %d recenzija %d zadatak pokrenut %d zadatka pokrenuta %d zadataka pokrenuto 3D O programu Pomagala Dodaj Sve Sve aplikacije Sve radnje su završene Dogodila se greška pri pokušaju dodavanja Flatpak repozitorija. Društvene igre Ogranak: Nemoguće instalirati Nemoguća obrada ove datoteke kada su radnje u tijeku.
Pokušajte kasnije, nakon što radnje završe. Nemoguće ukloniti Razgovor Kliknite <a href='%s'>ovdje</a> kako bi dodali svoju recenziju. Trenutno se radi na sljedećim paketima Pojedinosti Dokumentacija Crtanje Odabir distribucije Obrazovanje Elektronika E-pošta Emulatori Osnove Dijeljenje datoteka Uloga prvog lica Flatpak (%s) Flatpak podrška trenutno nije dostupna. Pokušajte instalirati flatpak i gir1.2-flatpak-1.0. Flatpak podrška trenutno nije dostupna. Pokušajte instalirati flatpak. Slova Igre Stvaranje predmemorije, pričekajte trenutak Grafika Naslovnica Instaliraj Instaliraj nove aplikacije Instalirano Instalirane aplikacije Instalacija Instaliranje ovog paketa može prouzrokovati nepopravljivu štetu vašem sustavu. Internet Java Pokreni Ograniči pretragu na trenutni popis Matematika Multimedijski kôdeki Multimedijski kôdeki za KDE Naziv: Nema pronađenih odgovarajućih paketa Nema paketa za prikazati.
To može ukazivati na problem - pokušajte osvježiti predmemoriju. Nema dostupne slike zaslona Nedostupno Nije instalirano Ured Neobavezne komponente PHP Paket Fotografija Budite strpljivi. Ovo može potrajati dulje vrijeme... Koristite 'apt-get' za instalaciju ovog paketa. Programiranje Izdavaštvo Python Strategije u stvarnome vremenu Osvježi Osvježi popis paketa Udaljeni ogranak: Ukloni Uklanjanje Uklanjanje ovog paketa može prouzrokovati nepopravljivu štetu vašem sustavu. Recenzija Skeniranje Znanost Znanost i obrazovanje Pretraži u opisu paketa (još sporije pretraživanje) Pretraži u sažetku paketa (sporije pretraživanje) Pretraživanje softverskih repozitorija, malo pričekajte Prikaži instalirane aplikacije Simulacije i utrke Veličina: Upravitelj softvera Nešto je pošlo po krivu. Kliknite za ponovni pokušaj. Zvuk Zvuk i video Paket sustava Alati sustava Flatpak repozitorij koji pokušavate dodati već postoji. Trenutno postoje aktivne radnje.
Sigurno želite zatvoriti? Ovo je paket sustava Ovo je Flatpak paket Pokušaj ponovno Strategije vremenskog poteza Nemoguća komunikacija s poslužiteljima. Provjerite svoj pristup internetu i pokušajte ponovno. Nedostupno Inačica: Video Preglednici Mreža Vaš upravitelj paketa sustava 