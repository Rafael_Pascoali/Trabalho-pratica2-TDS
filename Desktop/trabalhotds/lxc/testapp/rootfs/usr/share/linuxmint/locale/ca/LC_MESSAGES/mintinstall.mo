��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  J   Y  I   �      �          .  -   G     u     x  
   �     �     �     �  $   �  A   �          #     +  r   A     �     �  >   �  4   
     ?     G     U     \  	   w     �     �  	   �  
   �     �     �     �  Y   �  E   E     �     �      �     �     �  
   �     �     �          !  V   0     �     �     �  !   �     �     �     �     �  )     c   .     �     �     �  
   �     �     �     �  
   �  .     0   2     c     p     |     �  
   �     �     �  	   �  
   �  R   �  	   8     B     K     T  =   i  .   �  *   �  $        &     :     @  3   U     �     �     �     �  >   �  B   �     ?     \     v     �  e   �                    #     2      6     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-01 10:54+0000
Last-Translator: Isidro Pisa <isidro@utils.info>
Language-Team: Catalan <ca@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s per a la baixada, %(localSize)s d'espai de disc alliberat %(downloadSize)s per a la baixada, %(localSize)s d'espai de disc requerit %(localSize)s d'espai alliberat. %(localSize)s d'espai requerit %d Revisió %d Revisions %d tasca en execució %d tasques en execució 3D Quant a Accessoris Afegeix Tot Totes les aplicacions S'han completat totes les operacions S'ha produït un error en intentar afegir el dipòsit de Flatpak. Jocs de tauler Branca: No es pot instal·lar No es pot processar aquest fitxer mentre hi hagi operacions actives.
Torneu-ho a provar després que hagin acabat. No es pot eliminar Xat Feu clic <a href='%s'>aquí</a> per afegir la vostra ressenya. Actualment està treballant en els següents paquets Detalls Documentació Dibuix Recomanacions dels editors Educació Electrònica Correu electrònic Emuladors Essencials Compartició de fitxers Primera persona Flatpak (%s) El suport Flatpak no està disponible. Intenteu instal·lar flatpak i gir1.2-flatpak-1.0. El suport Flatpak no està disponible. Intenteu instal·lar flatpak . Lletres Jocs Generant memòria cau, un moment Gràfics Pàgina d'inici Instal·la Instal·leu aplicacions noves Instal·lat Aplicacions instal·lades Instal·lació La instal·lació d'aquest paquet podria causar un dany irreparable al vostre sistema. Internet Java Executa Limita la cerca al llistat actual Matemàtiques Còdecs multimèdia Còdecs multimèdia per a KDE Nom: No s'ha trobat cap paquet que coincideixi No hi ha cap paquet a mostrar.
Això pot indicar un problema: proveu d'actualitzar la memòria cau. No hi ha captures de pantalla No disponible No instal·lat Ofimàtica Components opcionals PHP Paquet Fotografia Sigueu pacient, això pot trigar una estona... Utilitzeu apt-get per instal·lar aquest paquet. Programació Publicació Python Estratègia en temps real Actualitza Actualitza la llista de paquets Remota: Suprimeix Supressió L'eliminació d'aquest paquet podria causar un dany irreparable al vostre sistema. Ressenyes Escaneig Ciència Ciència i educació Cerca a la descripció dels paquets (cerca encara més lenta) Cerca al resum dels paquets (cerca més lenta) Cercant repositoris de software, un moment Mostra les aplicacions instal·lades Simulació i curses Mida: Gestor de programari Quelcom ha fallat. Feu clic per tornar-ho a provar. So So i vídeo Paquet del sistema Eines del sistema Ja existeix el dipòsit de Flatpak que esteu intentant afegir. Actualment hi ha operacions actives.
Esteu segur que voleu sortir? Aquest paquet és de sistema Aquest paquet és Flatpak Torneu-ho a provar Estratègia per torns No es pot comunicar amb els servidors. Comproveu la vostra connexió a Internet i torneu-ho a provar. No disponible Versió: Vídeo Visualitzadors Web El gestor de paquets del sistema 