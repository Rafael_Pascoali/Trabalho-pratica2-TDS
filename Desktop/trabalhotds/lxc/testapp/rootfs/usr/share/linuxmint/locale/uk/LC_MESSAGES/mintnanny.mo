��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  ;      D   <  #   �  '   �     �  �   �  $   �     �  )     j   /                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-05-13 13:24+0000
Last-Translator: Mykola Tkach <chistomov@gmail.com>
Language-Team: Ukrainian <uk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s не є дозволеним доменним ім'ям. Блокувати доступ до вибраних доменів Заблоковані домени Блокувальник доменів Ім'я домену Доменні імена повинні починатися та закінчуватися літерою чи цифрою, і можуть містити тільки літери, цифри, крапки та дефіси. Приклад: my.number1domain.com Хибний домен Батьківський контроль Будь ласка, введіть ім'я домену, який потрібно заблокувати 