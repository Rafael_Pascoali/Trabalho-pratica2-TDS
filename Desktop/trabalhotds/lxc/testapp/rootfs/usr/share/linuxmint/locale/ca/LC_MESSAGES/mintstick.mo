��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  3   �  ;   �     �          -     D     Y     b     r      �  "   �  C   �       �  0     �     �     �               )     ;     N     [     n     z     �  4   �  &   �     �  ,   �          &  )   )  (   S     |  )   �  '   �     �     �               '     =     Z     q     �     �  .   �  C   �  *   �  +   '     S  X   s  &   �  4   �      (  +   I  9   u  G   �  C   �  )   ;  #   e  U   �     �     �     �     	     %     3     I     ]  1   f     �     �  <   �     �     �  
   �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-26 17:15+0000
Last-Translator: Isidro Pisa <isidro@utils.info>
Language-Team: Catalan <ca@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 S'ha produït un error mentre es copiava la imatge. S'ha produït un error mentre es creava una partició a %s. S’ha produït un error S'ha produït un error. Error d'autenticació. S'està calculant... Comprova Suma de control Fitxers de suma de comprovació La suma de control no coincideix Trieu un nom per a la memòria USB Baixeu la imatge ISO de nou. La seva suma de control no coincideix. Tot sembla estar bé! FAT32 
  + És compatible amb tots els sistemes.
  -  No pot gestionar els fitxers més grans de 4 GB..

exFAT
  + És compatible gairebé amb tots els sistemes.
  + Pot gestionar els fitxers més grans de 4 GB..
  -  No és tan compatible com FAT32.

NTFS 
  + És compatible amb Windows.
  -  No és compatible amb Mac ni tampoc amb la majoria dels dispositius de maquinari.
  -  Té problemes de compatibilitat ocasionals amb Linux (NTFS és propietari, a partir d'enginyeria inversa).

EXT4 
  + És modern, estable, ràpid i transaccional.
  + Admet els permisos de fitxers de Linux.
  -  No és compatible ni amb Windows ni amb Mac, ni amb la majoria dels dispositius de maquinari.
 Sistema de fitxers: Formata Formateu una memòria USB GB Signatures GPG Fitxer signat GPG Fitxer signat GPG: Torna enrere Verificació d'ISO Imatge ISO: Imatges ISO ISO: Si confieu en la signatura, podeu confiar en la ISO. La comprovació d'integritat ha fallat KB No s'ha trobat la clau al servidor de claus. Fitxers locals MB Creeu una memòria USB que pugui arrencar Crea una memòria USB que pugui arrencar Més informació No s'ha trobat cap identificador de volum No hi ha prou espai en la memòria USB. Suma SHA256 Fitxer de sumes SHA256 Fitxer de sumes SHA256: Suma SHA256: Seleccioneu la imatge Seleccioneu una memòria USB Seleccioneu una imatge Signat per: %s Mida: TB La suma SHA256 de la imatge ISO no coincideix. El fitxer de sumes SHA256 no conté sumes per a aquesta imatge ISO. El fitxer de sumes SHA256 no està signat. La memòria USB s'ha formatat correctament. La suma de control és correcta La suma de comprovació és correcta, però no s'ha verificat l'autenticitat de la suma. No s'ha pogut comprovar el fitxer gpg. No s'ha pogut baixar el fitxer gpg. Comproveu l'URL. La imatge s'ha escrit amb èxit. No s'ha pogut comprovar el fitxer de sumes. No s'ha pogut baixar el fitxer de sumes. Comproveu l'URL. Aquesta imatge ISO es verifica mitjançant una signatura de confiança. Aquesta imatge ISO es verifica mitjançant una signatura no fiable. Aquesta ISO sembla una imatge de Windows. Aquesta és una imatge ISO oficial. Això destruirà totes les dades en la memòria USB, esteu segur que voleu continuar? URLs Escriptor d'imatges USB Memòria USB Formatador de memòries USB Memòria USB: Signatura desconeguda Signatura no fiable Verifica Verifica l'autenticitat i integritat de la imatge Etiqueta del volum: Volum: Les imatges de Windows requereixen un processament especial. Escriu bytes desconegut 