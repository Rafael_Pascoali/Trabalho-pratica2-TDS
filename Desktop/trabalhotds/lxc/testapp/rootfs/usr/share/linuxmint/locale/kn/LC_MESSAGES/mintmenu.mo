��    '      T  5   �      `     a     g     k     |  K   �  -   �                 	   #     -  	   9     C     O     `     g     s     z          �     �     �     �     �     �     �     �     �     �     �     �          #     4     ;     D  	   J     T  �  i          3  +   F  -   r    �  �   �  	   F	  +   P	     |	  .   �	  4   �	  *    
  .   +
  4   Z
     �
  5   �
     �
     �
               %  c   8     �  7   �     �                /     K  >   j  H   �  M   �  7   @     x     �     �  U   �  2                 #          !                      &       %                         	       
          $                      '                                                        "    About All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse bookmarked and local network locations Computer Control Center Desktop Edit menu Empty trash Favorites Home Folder Insert separator Launch Lock Screen Logout Menu Name Name: Network Open your personal folder Options Package Manager Path Places Preferences Quit Remove Select a folder Show all applications Show in my favorites Software Manager System Terminal Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2010-06-15 14:38+0000
Last-Translator: ಸುಭಾಸ್ ಭಟ್(Subhas Bhat) <Unknown>
Language-Team: Kannada <kn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ಇದರ ಬಗ್ಗೆ ಎಲ್ಲವೂ ಎಲ್ಲ ಅನ್ವಯಿಕಗಳು ಅನ್ವಯಿಕಗಳು (applications) ಈ ಗಣಕಿದಿಂದ ನಿಲುಕಿಸಿಕೊಳ್ಳಬಹುದಾದಂತಹ ಸ್ಥಳೀಯ ಹಾಗು ದೂರಸ್ಥ ಡಿಸ್ಕುಗಳನ್ನು ಮತ್ತು ಕಡತಕೋಶಗಳನ್ನು ವೀಕ್ಷಿಸು ಬುಕ್‍ಮಾರ್ಕ್ ಮಾಡಲಾದ ಹಾಗು ಸ್ಥಳೀಯ ಜಾಲಬಂಧ ಸ್ಥಳಗಳನ್ನು ವೀಕ್ಷಿಸು ಗಣಕ ನಿಯಂತ್ರಣ ಕೇಂದ್ರ ಡೆಸ್ಕ್ ಟಾಪ್ ಪರಿವಿಡಿ ಸಂಪಾದಿಸು ಕಸದಬುಟ್ಟಿ ಖಾಲಿಮಾಡು ಅಚ್ಚುಮೆಚ್ಚಿನವು ನೆಲೆ (ಹೋಮ್) ಕಡತಕೋಶ ವಿಭಾಜಕವನ್ನು ಸೇರಿಸು ಆರಂಭಿಸು ತೆರೆಯನ್ನು ಲಾಕ್ ಮಾಡು ನಿರ್ಗಮಿಸು ಮೆನು ಹೆಸರು ಹೆಸರು: ಜಾಲಬಂಧ ನಿಮ್ಮ ವೈಯಕ್ತಿಕ ಕಡತಕೋಶವನ್ನು ತೆರೆಯಿರಿ ಆದ್ಯತೆಗಳು ಪ್ಯಾಕೇಜ್ ವ್ಯವಸ್ಥಾಪಕ ಮಾರ್ಗ ಸ್ಥಳಗಳು ಆಯ್ಕೆಗಳು ನಿರ್ಗಮಿಸು ತೆಗೆದುಹಾಕು ಒಂದು ಕಡತಕೋಶವನ್ನು ಆರಿಸಿ ಎಲ್ಲ ಅನ್ವಯಿಕಗಳನ್ನು ತೋರಿಸು. ನನ್ನ ನೆಚ್ಚಿನವುಗಳಲ್ಲಿ ತೋರಿಸು ತಂತ್ರಾಂಶ ವ್ಯವಸ್ಥಾಪಕ ವ್ಯವಸ್ಥೆ ಟರ್ಮಿನಲ್ ಕಸಬುಟ್ಟಿ ಅನುಸ್ಥಾಪಿಸಿದ್ದನ್ನು ತೆಗೆದುಹಾಕು ಆಜ್ಞಾ ಸಾಲನ್ನು ಬಳಸು 