��    <      �  S   �      (     )     /     3     D  K   Q  %   �  -   �     �  "        )     2     H     W     r     �     �  	   �     �  	   �     �     �     �     �  -   �     +     2     G     S     j     q     v     �     �     �     �     �     �     �     �     �     �     �     �     �          1     A     W     h     |  '   �     �     �     �     �     �     �  	   	     	  �  !	     �
     �
     �
     �
  �   �
  J   �  S   �  0   )  _   Z     �  9   �       K   '  <   s     �     �      �  $   	     .  %   =     c  $   �  -   �  l   �  
   C  ,   N  "   {  8   �     �     �  $   �            
      8   +  
   d      o     �  
   �     �     �  0   �     �  $     3   3     g  /   �  6   �  :   �  "   %  Z   H  "   �  .   �     �       	     
        (  5   A     8   :                                                    )               <      +      "                        6   .   #      $   0                    (                  	             7         !       1             -   '       5   4   9   *          3   2      ;      &   /          %      ,   
    About All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Places Preferences Quit Reload plugins Remove Remove from favorites Requires password to unlock Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2010-10-31 15:17+0000
Last-Translator: Dejan Noveski <Unknown>
Language-Team: Macedonian <mk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 За Сите Сите апликации Апликации Разгледај ги сите локални, далечни дискови и папки пристапливи од овој компјутер Листај и инсталирај расположлив софтвер Разгледај обележани и локални мрежни локации Листај избришани датотеки Разгледај предмети поставени на работната површина Компјутер Конфигурирање на Вашиот систем Контролен центар Не може да се иницијализира компонентата Не може да се вчита компонентата: Работна површина Десктоп тема Промени го менито Испразни ја корпата Омилени Домашен директориум Вметни сепаратор Вметни празно место Инсталирај го пакетот '%s' Инсталирајте, отстранете или надградете софтверски пакети Пушти Пушти кога ќе се логирам Заклучи го екранот Одјави се или промени корисник Одјавa Мени Параметри на менито Име Име: Мрежа Отворете ја Вашата лична папка Опции Менаџер на пакети Патека Места Преференци Напушти Превчитај ги компонентите Отстрани Отстрани од омилени Бара лозинка за отклучување Одберете папка Прикажи ги сите апликации Прикажи ја иконата на копчето Прикажи ги иконите за категории Прикажи во омилени Исклучи, рестартирај, суспендирај или хибернирај Менаџер на софтвер Смени име и генеричко име Систем Терминал Тема: Корпа Деинсталирај Користи ја командната линија 