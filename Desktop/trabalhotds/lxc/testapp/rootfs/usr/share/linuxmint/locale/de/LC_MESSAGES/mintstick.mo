��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  6   �  J   �          -     I     ^     t  
   |     �     �  '   �  O   �     $  L  9     �     �     �     �     �     �     �     �     �               (  D   -  &   r     �  /   �     �     �  !   �  !         "  "   8  (   [     �     �     �     �     �     �     �                 ,   "  D   O  *   �  +   �     �  P     .   W  V   �  )   �  0     X   8  H   �  N   �  ,   )  #   V  ^   z     �     �  	   �       
        "  !   6     X  =   `     �     �  7   �  	   �        	        K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-27 14:34+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Beim Kopieren des Abbildes ist ein Fehler aufgetreten. Während des Erstellens einer Partition auf %s ist ein Fehler aufgetreten. Ein Fehler ist aufgetreten Ein Fehler ist aufgetreten. Legitimierungsfehler Berechnung läuft … Prüfen Prüfsumme Prüfsummendateien Prüfsummenfehler Einen Namen für den USB-Stick wählen. Laden Sie das ISO-Abbild erneut herunter. Die Prüfsumme stimmt nicht überein. Alles sieht gut aus! FAT32 
  + Mit allen Geräten kompatibel.
  – Kann nicht mit Dateien größer als 4GB umgehen.

exFAT
  + Fast überall kompatibel.
  – Kann mit Dateien größer als 4GB umgehen.
  – Nicht so kompatibel wie FAT32.

NTFS 
  + Mit Windows kompatibel.
  – Nicht mit Mac und den meisten Geräten kompatibel.
  – Gelegentliche Kompatibilitätsprobleme mit Linux (NTFS ist proprietär und wurde nachkonstruiert)

EXT4 
  + Modern, stabil, schnell und journalisiert.
  + Unterstützt Linux-Dateizugriffsrechte.
  – Nicht mit Windows, Mac und den meisten anderen Geräten kompatibel.
 Dateisystem: Formatieren USB-Stick formatieren GB GPG-Signaturen GPG-signierte Datei GPG-signierte Datei: Zurückgehen ISO-Überprüfung ISO-Abbild: ISO-Abbilder ISO: Wenn Sie der Signatur vertrauen, können Sie auch der ISO vertrauen. Unversehrtheitsprüfung fehlgeschlagen KB Schlüssel nicht auf Schlüsselserver gefunden. Lokale Dateien MB Startfähigen USB-Stick erstellen Startfähigen USB-Stick erstellen Weitere Informationen Keine Datenträgerkennung gefunden Nicht genügend Platz auf dem USB-Stick. SHA256-Summe SHA256-Summendatei SHA256-Summendatei: SHA256-Summe: Abbild auswählen Einen USB-Stick auswählen Abbild auswählen Signiert von: %s Größe: TB Die SHA256-Summe des ISO-Abbilds ist falsch. Die SHA256-Summendatei enthält keine Summen für dieses ISO-Abbild. Die SHA256-Summendatei ist nicht signiert. Der USB-Stick wurde erfolgreich formatiert. Die Prüfsumme ist richtig Die Prüfsumme ist richtig, aber die Echtheit der Summe wurde nicht überprüft. Die GPG-Datei konnte nicht überprüft werden. Die GPG-Datei konnte nicht heruntergeladen werden. Bitte überprüfen Sie die Adresse. Das Abbild wurde erfolgreich geschrieben. Die Summendatei konnte nicht überprüft werden. Die Summendatei konnte nicht heruntergeladen werden. Bitte überprüfen Sie die Adresse. Dieses ISO-Abbild ist durch eine vertrauenswürdige Signatur bestätigt. Dieses ISO-Abbild ist durch eine nicht vertrauenswürdige Signatur bestätigt. Dieses ISO sieht wie ein Windows-Abbild aus. Das ist ein offizielles ISO-Abbild. Das wird alle Daten auf dem USB-Stick löschen. Sind Sie sicher, dass sie fortfahren möchten? Adressen USB-Abbilderstellung USB-Stick USB-Stick-Formatierer USB-Stick: Unbekannte Signatur Nicht vertrauenswürdige Signatur Prüfen Überprüfen Sie die Echtheit und Unversehrtheit des Abbildes Datenträgername: Datenträger: Windows-Abbilder erfordern eine spezielle Verarbeitung. Schreiben Bytes unbekannt 