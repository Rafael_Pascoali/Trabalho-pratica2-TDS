��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  (   �  0   �     �     �     	     !     .     7     J     g      �  <   �     �  �  �     �     �     �     �     �     �     �  
   	       
   *  
   5  
   @  9   K      �     �  )   �     �     �  ,   �  ,        B     T  $   r     �     �     �     �     �     �     	       	   %     /  *   2  >   ]  '   �  /   �     �  F        [  4   z     �  #   �  G   �  5   9  7   o  +   �     �  M   �  
   @  8   K     �  +   �     �     �     �     �  #        %     5  3   =     q     x  	        K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-18 10:33+0000
Last-Translator: Marek Hladík <mhladik@seznam.cz>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Při kopírování obrazu nastala chyba. Nastal problém při vytváření oddílu na %s. Došlo k chybě Vyskytla se chyba. Problém s ověřením. Výpočet… Ověřit Kontrolní součet Soubory kontrolního součtu Chyba kontrolního součtu Zvolte název pro USB klíčenku Stáhněte ISO obraz znovu. Kontrolní součet se neshoduje. Všechno je v pořádku! FAT32 
  + Kompatibilní všude.
  -  Neumí obsloužit soubory větší než 4GB.

exFAT
  + Kompatibilní téměř všude.
  + Umí obsloužit soubory větší než 4GB.
  -  Není tak kompatibilní jako FAT32.

NTFS 
  + Kompatibilní se systémem Windows.
  -  Nekompatibilní s počítači Apple a většinou jednoúčelových přístrojů.
  -  Občas problémy s kompatibilitou v systému Linux (NTFS je proprietární a podpora vznikla zpětným inženýrstvím).

EXT4 
  + Moderní, stabilní, rychlé a s žurnálem.
  + Podporuje linuxová souborová práva.
  -  Není kompatibilní se systémy Windows, macOS a většinou jednoúčelových přístrojů.
 Souborový systém: Formátovat Formátovat USB klíčenku GB GPG podpisy Soubor GPG podpisů Soubor GPG podpisů: Jít zpět Ověření ISO obrazu ISO obraz: Obrazy ISO ISO obraz: Pokud důvěřujete podpisu, můžete věřit ISO obrazu. Kontrola integrity se nezdařila kB Klíč nebyl nalezen na serveru klíčů. Lokální soubory MB Vytvořit spouštěcí (boot) USB klíčenku Vytvořit spouštěcí (boot) USB klíčenku Další informace Nenalezeno žádné ID svazku Nedostatek místa na USB klíčence. SHA256 součet Soubor SHA256 součtů Soubor SHA256 součtů: SHA256 součet: Vybrat obraz Vyberte USB klíčenku Zvolit obraz Podepsáno: %s Velikost: TB SHA256 součet ISO obrazu není správný. Soubor SHA256 součtů neobsahuje součty pro tento ISO obraz. Soubor součtů SHA256 není podepsán. USB klíčenka byla úspěšně zformátována. Kontrolní součet je správný Kontrolní součet je správný, ale pravost součtu nebyla ověřena. Soubor gpg nelze zkontrolovat. Soubor gpg nelze stáhnout. Zkontrolujte adresu URL. Obraz byl úspešně zapsán. Soubor součtů nelze zkontrolovat. Soubor s kontrolními součty nelze stáhnout. Zkontrolujte adresu URL. Tento ISO obraz je ověřen důvěryhodným podpisem. Tento ISO obraz je ověřen nedůvěryhodným podpisem. Tento ISO obraz vypadá jako obraz Windows. Toto je oficiální ISO obraz. Tato operace smaže všechna data na USB klíčence! Přejete si pokračovat? URL adresa Nástroj pro zápis obrazů systému na USB úložiště USB klíčenka Nástroj pro formátování USB úložišť USB klíčenka: Neznámý podpis Nedůvěryhodný podpis Ověřit Ověřte pravost a integritu obrazu Popisek svazku: Svazek: Obrazy Windows vyžadují speciální zpracování. Zapsat bajtů neznámý 