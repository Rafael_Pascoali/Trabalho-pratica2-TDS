��          �      |      �     �                    0     @     M     U     p     �     �     �     �     �  	   �     �  ,   �     ,     3     :     W  �  w     9     U  
   q     |     �     �     �     �  !   �  "     !   .     P     X     q  	   �     �  '   �     �     �      �  +   �                 	                       
                                                         Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Install Install / Remove Languages Install / Remove Languages... Install language packs Install the selected language Language Language Settings Language settings Languages None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Some language packs are missing Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-08-19 15:46+0000
Last-Translator: Alekĉjo <Unknown>
Language-Team: Franco-Provençal <frp@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Apondre una novèla lèngua Apondre una novèla lèngua Apondre… Aplicar a tot lo sistèmo Instalâ complètament Mètoda d’entrâ Instalar Instalar e recotar de lèngües Instalar e recotar de lengües… Instalar los paquèts de lèngües Instalar la lèngua sèlèccionâ Lèngua Configuracion de lèngua Configuracions de lèngua Lèngües Nyun Nombros, monèyes, adrèces, mesures… Rèġion Recotar Recotar la lèngua sèlèccionâ Cârcos paquèts de lèngües sont mancants 