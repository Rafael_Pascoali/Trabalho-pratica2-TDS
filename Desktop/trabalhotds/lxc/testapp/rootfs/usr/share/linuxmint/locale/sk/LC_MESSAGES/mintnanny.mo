��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  #   �  -   !     O     b     t  �   �          &     8  :   N                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-23 22:49+0000
Last-Translator: Dusan Kazik <prescott66@gmail.com>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nie je platným názvom domény. Blokovať prístup k vybraným názvom domén Blokované domény Blokovanie Domén Názov domény Názvy domén musia začínať a končiť písmenom alebo číslicou a môžu obsahovať iba písmená, číslice, bodky a pomlčky. Príkad: moja.cislo1domena.sk Neplatná doména Rodičovská Kontrola Prosím, zadajte názov domény, ktorú chcete zablokovať 