��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  3   �  M   .  '   |  %   �     �  �   �  "   �  (   �  "   �  P                          
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-08 09:24+0000
Last-Translator: Murat Käribay <d2vsd1@mail.ru>
Language-Team: Kazakh <kk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s атты домен жол берілмейді. Таңдалған домендерге жетімдігін бөгеттеу Бөгеттелген домендер Домендер бөгеттеуші Доменнің атауы Доменнің аты әріппен басталып аяқталуы тиіс немесе сандар мен тек әріптерден, сандар, нүкте мен дефистер. Мысалы: my.number1domain.com Жол берілмейтін домен Ата-аналық бақылау Құлыптап тастаған келген доменді енгізіңіз 