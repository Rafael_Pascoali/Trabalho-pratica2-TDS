��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  3   �  Y   #  !   }  @   �     �  �   �  "   �     �  #     i   ?                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-20 18:47+0000
Last-Translator: spacy01 <Unknown>
Language-Team: Bulgarian <bg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s не е валидно име на домейн. Спиране на достъпа до избраните имена на домейни Блокирани домейни Приложение за блокиране на домейни Име на домейн Имената на домейните трябва да започват и свършват с буква или цифра и могат да съдържат само букви, цифри, точки и тирета. Пример: my.number1domain.com Невалиден домейн Родителски контрол Моля напишете името на домейна, който искате да блокирате 