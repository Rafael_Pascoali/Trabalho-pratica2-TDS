��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  6   �  F   �          +     F     a  	   t     ~  &   �  2   �  "   �  Y        h  g  }     �     �                    .     A  	   W     a     x  
   �     �  X   �  )   �       .        L     \     _     ~     �     �  #   �     �     �       	   2     <     T     o     �  	   �     �  ;   �  M   �  #   /  ,   S  -   �  U   �  '     ;   ,  %   h  0   �  E   �  >     @   D  /   �  !   �  \   �     4     9     O     X     o     {     �  	   �  :   �     �  	     9        H     P     W     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-03 14:43+0000
Last-Translator: guwrt <guwrt77@gmail.com>
Language-Team: French (Canada) <fr_CA@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Une erreur s'est produite lors de la copie de l'image. Une erreur s'est produite lors de la création d'une partition sur %s. Une erreur s’est produite Une erreur s'est produite. Erreur d'authentification. Calcul en cours... Vérifiez Somme de contrôle Fichiers somme de contrôle (checksum) La somme de contrôle ne correspond pas (checksum) Choisir un nom pour votre clé USB Veuillez télécharger l'image ISO à nouveau si la somme de contrôle ne correspond pas. Tout semble correct! FAT32
   + Compatible avec tout.
   - Ne peut pas gérer les fichiers de plus de 4 Go.

exFAT
   + Compatible avec presque tout.
   + Peut gérer les fichiers de plus de 4 Go.
   - Pas aussi compatible que FAT32.

NTFS
   + Compatible avec Windows.
   - Non compatible avec Mac et la plupart des périphériques matériels.
   - Problèmes de compatibilité occasionnels avec Linux (NTFS est propriétaire et rétroconçu).

EXT4
   + Moderne, stable, rapide, journalisé.
   + Prend en charge les autorisations de fichiers Linux.
   - Non compatible avec Windows, Mac et la plupart des périphériques matériels.
 Système de fichiers : Formater Formater une clé USB Go Signatures GPG Fichier GPG signé Fichier GPG signé : Retourner Vérification de l'ISO Image ISO : Images ISO ISO : Si vous avez confiance en la signature, vous pouvez avoir confiance avec le fichier ISO. La vérification d'intégrité a échoué ko La clé n'a pas été trouvée sur le serveur. Fichiers locaux Mo Créer une clé USB amorçable Créer une clé USB amorçable Plus d'informations Aucun volume trouvé Pas assez d'espace sur la clé USB. Somme SHA256 Fichier de contrôle SHA256 Fichier de contrôle SHA256 : SHA256 : Sélectionner une image Sélectionner une clé USB Sélectionner une image Signé par : %s Taille : To La somme de contrôle SHA256 du fichier ISO est incorrecte. Le fichier de contrôle SHA256 ne contient pas la somme pour cette image ISO. Le fichier SHA256 n'est pas signé. La clé USB a été formattée avec succès. La somme de contrôle est correcte (checksum) La somme de contrôle est correcte mais son authentification n'a pas été vérifié. Le fichier GPG ne peut être vérifié. Le fichier GPGne peut être téléchargé. Vérifiez l'URL. L'image a été écrite avec succès. Le fichier de contrôle ne peut être vérifié. Le fichier de contrôle ne peut être téléchargé. Vérifiez l'URL. Cette image ISO est vérifiée par une signature de confiance. Cette image ISO est vérifiée par une signature sans confiance. Cette image ISO ressemble à une image Windows. Ceci est une image ISO officielle Toutes les données présentes sur la clé USB seront supprimées. Souhaitez-vous continuer? URLs Créateur de clé USB Clé USB Formatteur de clé USB Clé USB : Signature inconnue Signature sans confiance Vérifiez Vérifiez l'authenticité et l'intégrité de cette image. Étiquette du volume : Volume : Les images Windows requierent une méthode particulière. Écrire octets inconnu 