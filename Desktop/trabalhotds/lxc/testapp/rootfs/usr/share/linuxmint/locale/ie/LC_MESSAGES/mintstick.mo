��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  #     ,   =     j     |  $   �  !  �     �
     �
     �
       ,   7     d     v     �  *   �  !   �  :   �     &  	   ?     I  
   ]     h     }     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-05-15 01:50+0000
Last-Translator: Silvara <Unknown>
Language-Team: Interlingue <ie@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Un errore evenit copiante li image. Un errore evenit creante un partition sur %s Un errore evenit. Errore de autentification. Provide un nómine por vor clave USB FAT32 
  + Universalmen compatibil.
  -  Ne posse haver files plu grand quam 4Go.

exFAT
  + Presc universalmen compatibil.
  + Posse haver files plu grand quam 4Go.
  -  Ne es tam compatibil quam FAT32.

NTFS 
  + Compatibil con Windows.
  -  Ne es compatibil con Mac e majorité de apparates.
  -  Sporadic incidentes de compabilitá con Linux (NTFS es proprietari e retroconstructet).

EXT4 
  + Modern, stabil, rapid, jurnalat.
  + Supporta permissiones de files de Linux.
  -  Ne es compatibil con Windows, Mac e li majorité de apparates.
 Formatar Formatar un clave USB Crear un clave USB de inicie Crear un clave USB de inicie Manca li spacie disponibil sur li clave USB. Selecter un image Selecter un clave USB Selecter un image Li clave USB esset formatat successosimen. Li image sta scrit successosimen. Omni data sur ti clave USB va esser destructet; continuar? Scrition de images a USB Clave USB Formatar claves USB Clave USB: Etiquette de volume: Scrir ínconosset 