��             +         �     �     �     �     �     �       -        G     b  
   g     r  
   x  	   �     �     �     �     �  0   �     �                    .  	   @     J     S     m     s     �     �     �  �  �     O     ^     q  
   x     �     �  '   �  #   �     �                    %     .     4     M     _  >   f     �     �     �     �     �     �  
     "        6  "   ?     b     u    z                         
                                                                                                     	                  Buttons labels: Buttons layout: Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Mac style (Left) Mounted Volumes Network Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Window Manager Windows translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-05-19 19:53+0000
Last-Translator: Zigmārs Dzenis <Unknown>
Language-Team: Latvian <lv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Pogu norādes: Pogu izkārtojums: Dators Darbvirsma Darbvirsmas iestatījumi Darbvirsmas ikonas Nerādīt logu saturu, tos pārvietojot Pielāgot darbvirsmas iestatījumus Mājas Ikonu izmērs: Ikonas Tikai ikonas Saskarne Liels Mac stils (kreisā puse) Atmiņas ierīces Tīkls Izvēlieties elementus, kurus vēlaties redzēt uz darbvirsmas Rādīt ikonas uz pogām Rādīt ikonas izvēlnēs Mazs Teksts apakšā Teksts blakus Tikai teksts Rīkjoslas Tradicionālais stils (labā puse) Miskaste Lietot sistēmas fontu tituljoslā Logu Pārvaldnieks Logi Launchpad Contributions:
  Aleksejs Trufans https://launchpad.net/~missionars
  BismarX https://launchpad.net/~karlis-martinsons
  Janis Elmeris https://launchpad.net/~janis-elmeris
  Juris Daikteris https://launchpad.net/~juris-daikteris
  Klāvs Priedītis https://launchpad.net/~klavs-pr
  Reinis Zumbergs https://launchpad.net/~reinis-zumbergs
  Rihards Pfeifle https://launchpad.net/~spr1nt3r
  Tomass Pētersons https://launchpad.net/~tomass139-deactivatedaccount
  Zigmārs Dzenis https://launchpad.net/~zigmars 