��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  G   2  G   z  /   �  *   �  +        I     L     Q     ]     a     m  .   �     �     �  n   �     ?     P  9   _  -   �     �     �     �     �     �                         0  X   ?  ?   �  
   �     �     �     �     �               )  H   1     z     �     �  "   �     �     �     �  "   �          !     )     7     ?     C     K  7   S  0   �     �     �     �     �     �     �       	     E     	   `     j     s     y  /   �  *   �  -   �          ,     >     V     ]     q  4   �  D   �     �       	        "         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-06-13 12:37+0000
Last-Translator: Slimane Selyan AMIRI <selyan.kab@protonmail.com>
Language-Team: Kabyle <kab@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: kab
 %(downloadSize)s i usider, %(localSize)s n umkan deg udebsi yettuserreh %(downloadSize)s i usider, %(localSize)s n umkan deg udebṣi yettwasra %(localSize)s n umkan deg udebsi yettuserreḥ. %(localSize)s n umkan deg udebsi yettwasra %d n temhelt tetteddu %d n tmehlin tteddunt 3D Ɣef Isemmadanen Akk Akk isnasen Akk timehlin mmdent Teḍra-d tuccḍa di tmerna n usersay Flatpak Uraren igwnan d awezɣi asbeddi D awezɣi asekker n ufaylu-agi skud tteddunt temhelin.
Ttxil-k eɛreḍ tikkelt-nniḍen ticki fukent tmehlin. D awezɣi tukksa Adiwenni usrid Sit <a href='%s'>dagi</a>  iwakken ad ternuḍ tanayt ik. Timehlin ttedunt akka tura ɣef ikemmusen-agi Talqayt Asuneɣ Ifranen n imaẓragen Asegmi Taliktṛunit Imayl Amtellel Isεan azal Beṭṭu n ifuyla Amdan-Amezwaru Tallelt n Flatpak ulac-itt akka tura. Ɛreḍ sebded n flatpak akked gir1.2-flatpak-1.0. Tallelt n Flatpak ulac-itt akka tura. Ɛreḍ sebded n flatpak. Tisefsiyin Uraren Idlifen Sebded Sebded isnasen imaynuten Yebded Isnasen ibedden Asebded Asbeddi n ukemmus-agi ad d-yeglu s wuguren war aṣeggem i unagraw-inek. Internet Java Sker Sebded anadi ɣer tebdart tamirant Tusnakt Ikudaken igetmedia Ikudaken igetmedia i KDE Ulac akemmus inmeɣran akked unadi Ulac izegrar ad d-nesken. Ulac-it Ur yebdid ara Tanarit PHP Akemmus Tawlaft Ttxil-k ṣber. Ayagi izmer ad iṭṭef kra n wakud... Seqdec apt-get akken ad tesbeddeḍ akemmus-agi. Taneflit Asuffeɣ Python Tasuddest n wakud ilaw Smiren Smiren tadart n izegrar Kkes Asembiwel Tukksa n ukemmus-agi ad teglu s wuguren war aṣeggem i unagraw-inek. Iceggiren Tasleṭ Tusna Tussna akked uselmed Nadi deg uglam n ikemmusen (ula d anadi xfifen) Nadi deg ugzul n  ikemmusen (anadi xfifen) Anadi n isersayen n iseɣẓanen, arǧu ciṭ Sken isnasen ibedden taserwest d tazla Amsefrak n iseɣẓanen Imesli Imesli akked Uvidyu Ifecka n unagraw Asersay Flatpak i tebɣiḍ ad ternuḍ yella yakan. Llant tmehli i yetteddun akka tura.
D tidet tebɣiḍ ad teffɣeḍ? Tastratijit s nuba Tavidyut Imeskanen Web 