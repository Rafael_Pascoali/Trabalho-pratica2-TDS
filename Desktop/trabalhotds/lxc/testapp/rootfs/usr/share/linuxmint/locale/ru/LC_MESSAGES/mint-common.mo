��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  H   C  R   �  -   �  B     =   P  D   �  N   �  K   "	  ;   n	  ?   �	     �	  u   

     �
     �
     �
     �
  D   �
  6   +  Y   b     �     �     �  )     :   2  �   m  2   �     -                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-13 16:02+0000
Last-Translator: Артём Котлубай <artemkotlubai@yandex.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Будет занято дискового пространства: %s. Будет освобождено дискового пространства: %s. Всего будет загружено: %s. Требуются дополнительные изменения Дополнительное ПО будет понижено Дополнительное ПО будет установлено. Дополнительное ПО будет полностью удалено Дополнительное ПО будет переустановлено Дополнительное ПО будет удалено Дополнительное ПО будет обновлено Произошла ошибка Невозможно удалить пакет %s, так как он требуется другим пакетам. Понизить версию «Плоский пакет» Пакеты flatpak Установить От пакета %s зависят следующие пакеты: Пакеты, которые будут удалены Пожалуйста, посмотрите на список изменений ниже. Полного удаления Переустановить Удалить Пропустить обновление Следующие пакеты будут удалены: Данный пункт меню не связан ни с одним пакетом. Всё равно удалить его из меню? Обновления будут пропущены Обновить 