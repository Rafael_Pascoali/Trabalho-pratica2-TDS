��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  $     $   C     h     w  '   �  u  �  
   2     =  $   W  $   |  #   �     �     �     �  *   
  %   5  Q   [     �     �  $   �     �       	     	   %                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-05-07 12:56+0000
Last-Translator: Moo <hazap@hotmail.com>
Language-Team: Lithuanian <lt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Kopijuojant atvaizdį įvyko klaida. Kuriant %s skaidinį, įvyko klaida. Įvyko klaida. Tapatybės nustatymo klaida. Pasirinkite USB atmintinės pavadinimą FAT32 
  + Suderinamas visur.
  -  Neapdoroja didesnius nei 4GB failus.

exFAT
  + Suderinamas beveik visur.
  + Gali apdoroti didesnius nei 4GB failus.
  -  Ne toks suderinamas kaip FAT32.

NTFS 
  + Suderinamas su Windows.
  -  Nesuderinamas su Mac ir daugeliu aparatinės įrangos įrenginių.
  -  Kartais pasitaikančios suderinimo su Linux problemos (NTFS yra nuosavybinis formatas ir yra atgaminamas naudojant apgrąžos inžineriją).

EXT4 
  + Šiuolaikiškas, stabilus, greitas, rašomas į žurnalą.
  + Palaiko Linux failų leidimus.
  -  Nesuderinamas su Windows, Mac ir daugeliu aparatinės įrangos įrenginių.
 Formatuoti Formatuoti USB atmintinę Sukurti paleidžiamą USB atmintinę Sukurti paleidžiamą USB atmintinę USB atmintinėje neužtenka vietos. Pasirinkti atvaizdį Pasirinkite USB atmintinę Pasirinkti atvaizdį USB atmintinė buvo sėkmingai formatuota. Atvaizdis buvo sėkmingai įrašytas. Tai sunaikins visus USB atmintinėje esančius duomenis, ar tikrai norite tęsti? USB atvaizdžių rašyklė USB atmintinė USB atmintinės formatavimo įrankis USB atmintinė: Tomo etiketė: Įrašyti nežinoma 