��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �  6   R  2   �  +   �  -   �          $  
   -  &   8  P   _  
   �  .   �  �   �  
   k                            	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-02-27 21:48+0000
Last-Translator: Ibrahim Saed <ibraheem5000@gmail.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ar_EG
 سيتم استخدام %s من مساحة القرص. سيتم تحرير %s من مساحة القرص. سيتم تنزيل %s في المُجمل. يتطلب ذلك برمجيات إضافية حدث خطأ Flatpaks تثبيت الحزم المراد إزالتها برجاء إلقاء نظرة على قائمة  التغييرات أدناه. إزالة سيتم إزالة الحزم التالية: عنصر القائمة هذا غير مرتبط بأي حزمة. هل تريد إزالته من القائمة بأي حال؟ ترقية 