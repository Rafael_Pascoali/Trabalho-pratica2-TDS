��    -      �  =   �      �  ?   �  B   !  !   d  $   �     �     �     �     �     �     �     �  	   �     �  	   �     �                         %  	   >     H     Q     _     m     t     |  +   �     �  
   �     �     �     �     �     �  3     *   @     k     �     �     �     �     �     �  �  �  o   �  k   �  :   g	  6   �	     �	     �	     
     
     >
     N
     j
  !   }
     �
  !   �
  "   �
     �
               6  N   I  ,   �     �     �  )   �     '     4     J  �   i  !   �       ;   4     p     �     �  2   �  {   �  y   m  5   �  :     &   X       >   �     �     �                $              !      (         -   "   ,              #                                        +                          &   	      
                *                    %      '           )          %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-05-12 19:32+0000
Last-Translator: M A Al Masud <Unknown>
Language-Team: Bengali <bn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s ডাউনলোড হবে, %(localSize)s জায়গা খালি হয়েছে %(downloadSize)s ডাউনলোড হবে, %(localSize)s জায়গা প্রয়োজন %(localSize)s জায়গা খালি হয়েছে %(localSize)s জায়গা প্রয়োজন থ্রিডি পরিচিতি আনুষঙ্গিক বোর্ড গেমস চ্যাট বিস্তারিত থ্রিডি শিক্ষাবিষয়ক ইমেইল ইমুলেটরসমূহ ফাইল শেয়ারিং ফন্টসমূহ খেলা গ্রাফিক্স ইনস্টল নতুন অ্যাপলিকেশন ইনস্টল করুন ইনস্টল করা হয়েছে ইন্টারনেট অনুপস্থিত ইন্সটল করা হয়নি অফিস প্যাকেজ ফটোগ্রাফি অনুগ্রহ পূর্বক apt-get ব্যবহার করে প্যাকেজটি ইন্সটল করুন। প্রোগ্রামিং প্রকাশনা রিয়েল-টাইম স্ট্রাটেজি অপসারণ পর্যালোচনা স্ক্যানিং বিজ্ঞান এবং শিক্ষা প্যাকেজসমূহের বিবরণের অনুসন্ধান করুন (even slower search) প্যাকেজসমূহের সারমর্মে অনুসন্ধান করুন (slower search) সিমুলেশন এন্ড রেসিং সফটওয়্যার ব্যবস্থাপক শব্দ এবং ভিডিও সিস্টেম টুল টার্ন-বেইসড স্ট্রাটেজি দর্শক ওয়েব 