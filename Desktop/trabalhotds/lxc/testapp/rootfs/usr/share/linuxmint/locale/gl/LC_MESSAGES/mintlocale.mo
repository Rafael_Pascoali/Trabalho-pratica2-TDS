��            )   �      �  ,   �  w   �     6     I     \     c     u     �  �   �     ;     C     ^  ^   |     �     �               +     =  	   N     X     j  ,   o     �     �     �     �     �  �  �  -   �  �   �     g     ~  
   �     �     �     �  �   �     �	     �	     �	  ^   �	     3
     O
     n
     u
     �
     �
     �
     �
     �
  1   �
                 "   ;     ^                                            	                                               
                                               %d language installed %d languages installed <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Some language packs are missing System locale Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-10-19 17:38+0000
Last-Translator: Alberto Gómez <Unknown>
Language-Team: Galician <gl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %d linguaxe instalada %d linguaxes instaladas <b><small>Nota: a instalación ou actualización de paquetes de idioma pode iniciar a instalación de idiomas adicionais</small></b> Engadir un novo idioma Engadir un novo idioma Engadir... Aplicar a todo o sistema Totalmente instalado Método de entrada Os métodos de entrada utilízanse para escribir símbolos e caracteres ausentes no teclado. Son útiles para escribir en chinés, xaponés, coreano, tailandés, vietnamita ... Instalar Instalar / Retirar idiomas Instalar / Retirar idiomas... Instalar paquetes de idioma, ficheiros de traducións e dicionarios para o idioma seleccionado Instalar paquetes de idioma Instalar o idioma seleccionado Idioma Axustes de idioma Axustes do idioma Compatibilidade de idioma Idiomas Non definiu ningún idioma Ningún Números, moeda, enderezos, unidades de medida... Zona Retirar Retirar o idioma seleccionado Faltan algúns paquetes de idiomas Idioma do sistema 