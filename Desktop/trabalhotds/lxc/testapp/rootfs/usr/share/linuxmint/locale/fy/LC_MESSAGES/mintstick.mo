��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  3   �  ;        H     e     �     �     �     �     �  "   �          6     O  &   j     �  =   �     �  	   �       
         +     7     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-12 13:51+0000
Last-Translator: David Metzlar <metzlar94@live.nl>
Language-Team: Frisian <fy@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 In flater is optreden by it kopiearjen fan de image Der wie in flater mei it oanmeitsjen fan in partysje op %s. In flater hat sich foar dien Flater by it autentiseren. Kies in namme foar jo USB Formattearje USB stick formatteare Opstartbare USB stick meitsje Opstartbare USB stick meitsje Net genôch romte op de USB stick. Image bestân selektearje Selektearje in USB stick Selektear in Image bestân Formataasje fan de USB stick is slagge Image oanmeitsjen is slagge Dit sil alle data op de USB ferwyderje, binne jo der wis fan? USB Image skriuwer USB stick USB Stick formattearder USB stick: Skiif label Skriuwe 