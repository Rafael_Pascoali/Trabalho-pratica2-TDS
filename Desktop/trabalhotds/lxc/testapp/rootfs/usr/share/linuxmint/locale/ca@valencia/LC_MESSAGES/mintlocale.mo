��    !      $  /   ,      �  w   �     a     t     �     �     �     �     �     �  �   �     r     z     �  ^   �          )     G     P     b     t  	   �     �     �  ,   �     �     �     �     �          1     ?     G  �  K  �   %     �     �  	   �     �     �     	     	     $	  �   6	     �	  $   �	  !   
  j   :
     �
      �
     �
     �
               +  -   3     a  (   e     �     �     �     �     �  "   �  
                       	                                              
                                                  !                                                <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Cancel Fully installed Help Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Welcome XIM Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-02-15 19:09+0000
Last-Translator: Juan Hidalgo-Saavedra <Unknown>
Language-Team: Catalan (Valencian) <ca@valencia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>Nota: la instal·lació o actualització de paquets d'idioma pot llançar la instal·lació d'idiomes addicionals</small></b> Afegir un nou idioma Afegir un nou idioma Afegir... Aplicar a tot el sistema Cancel·lar Instal·lat per complet Ajuda Mètode d'entrada Els mètodes d'entrada s'utilitzen per a escriure símbols i caràcters absents en el teclat. Són útils per a escriure en xinés, japonès, coreà, tailandès, vietnamita ... Instal·lar Instal·lar / desinstal·lar idiomes Instal·lar o eliminar idiomes… Instal·la els paquets d'idioma, arxius de traduccions i diccionaris que falten per a l'idioma seleccionat Instal·lar paquets d'idioma Instal·lar l'idioma seleccionat Llengua Configuració d'idioma Configuració d'idioma Suport de llengua Idiomes No s'ha especificat la configuració regional Cap Números, moneda, direccions, mesures... Regió Suprimir Suprimir l'idioma seleccionat Xinés simplificat Falten alguns paquets d'idiomes Configuració regional del sistema Benvinguts XIM 