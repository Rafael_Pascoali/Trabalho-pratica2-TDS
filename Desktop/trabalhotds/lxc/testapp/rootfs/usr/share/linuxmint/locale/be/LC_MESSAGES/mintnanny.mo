��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  8   �  F   1  !   x  '   �     �  �   �     �     �  %   �  T   �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-11 14:05+0000
Last-Translator: meequz <meequz@gmail.com>
Language-Team: Belarusian <be@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s - не выглядае як назва дамена. Блакаванне доступу да абраных даменаў Заблакаваць дамен Блакавальнік даменаў Назва дамена Назвы даменаў маюць пачынацца з літары або лічбы, і могуць змяшчаць толькі літары, лічбы, кропкі і злучкі. Узор: my.number1domain.com Хібны дамен Бацькоўскі кантроль Увядзіце назву дамена, які хочаце заблакаваць 