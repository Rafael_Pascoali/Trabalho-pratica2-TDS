��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  <   Z  E   �  :   �  "     Q   ;     �  G   �     �     �     �     �     
     $  #   +  )   O     y  �   �  ,   8  0   e  8   �  /   �     �            
   #  &   .     U     X     \     _  �   c                 8        U     g     x  B   �  3   �  .   �  2   *  +   ]     �     �     �     �     �     �     �  +     0   1  %   b     �     �  5   �     �     �  %   �                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2010-10-12 14:29+0000
Last-Translator: Reuben Potts <Unknown>
Language-Team: Manx <gv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : (n == 2 ? 1 : 2);
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Cha nel %(1)s currit er 'sy coadan config fo  %(2)s ny %(3)s %(percentage)s jeh coadanyn %(number)d - laadey heose dys %(service)s %(percentage)s jeh 1 coadan - Laadey heose dys %(service)s %(size_so_far)s jeh %(total_size)s %(size_so_far)s jeh %(total_size)s - %(time_remaining)s faagailit (%(speed)s/sec) %s Reihghyn <b>Cur stiagh yn ennym my sailt son yn shirveeish laadey heose noa:</b> Mychione B Cur ass Cur laadey heose ass? Jeeagh trooid cochiangley Dooney Cha noddym geddyn reaymys ry-geddyn Cha doddym sauail ny caghlaaghyn reihghyn Reih yn shirveeish laadey heose Ynnyd dy laadey heose dys. Ta'n cowewydertraa 'sy traa t'ayn currit ayns ynnyd jeh'n <COWREYDERTRAA>, goll er yn aght cowreyder traa currit. Liorish cadjin: . Vel oo gearree cur yn laadey heose shoh ass? Tayrn &amp; Lhiggey ayns shoh dy laadey heose %s Ta'n coadan ny smoo ny yn reamys ry-geddyn yn shirveeish Ta'n coadan ny smoo ny yn ard-sym yn shirveeish Coadan laadit heose dy kiart GB GiB Cuirreyder Ennym cuirreyder ny enmys IP, cadjin:  KB KiB MB MiB Fockle follit, liorish cadjn: fockle follit-ny sloo cochiangley SCP, cochiangley FTP cochiangley FTP orgheryn  ~/.ssh jannoo ymmydit jeh son cochiangleyn SFTP Focklefollit: Raad: Purt: Purt foddee, ta'n cadjin 21 son FTP, 22  son SFTP as SCP Roie 'sy cooylrey Enym shirveeish: Shirveeish: Va ny coadanyn  %(number)d laadit heose dy kiart dys '%(service)s' Va 1 coadan laadit heose dy kiart dys '%(service)s' Va'n laadey heose dys '%(service)s' currit ass Ta feym ec yn shirveeish shoh er ny fockle follit. Aght cowreydertraa (traastr).Liorish cadjin Cowreydertraa: sorçh: URL: Shirveeish gyn fys: %s Reireyder laadey heose Reireyder laadey heose... Shirveeish laadey heose Cha ren laadey heose dys  '%s' gobbraghey:  Laadey heose coadanyn %(number)d dys %(service)s Laadey heose 1 coadan dys %(service)s Laadey heose yn coadan Ymmydeyr Ennym ymmydeyr, cadjin dys yn ymmydeyr ynnydagh ayd's _Coadan _Cooney Cochiangley currit er jerrey dy kiart 