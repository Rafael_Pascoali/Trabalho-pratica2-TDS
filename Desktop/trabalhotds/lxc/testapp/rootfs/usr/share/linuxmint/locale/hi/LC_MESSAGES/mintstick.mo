��    &      L  5   |      P  )   Q  2   {     �     �     �     �      �         '     4     ;     N     Q     Y     \     _     y     �  "   �     �     �     �     �     �  )   �  #   (  N   L     �     �  	   �     �  
   �     �     �     �            �    F   �	  I   �	     A
     ^
  1   ~
     �
  .   �
  !   �
  �    "   �  5   �          
     $     '  <   *  <   g  "   �  Y   �     !  "   >     a     ~     �  [   �  L   �  �   7     �  /   �       B   .     q  +   �     �  $   �     �                       "                                 $      %                 !                                
                                    #          	              &            An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Check Choose a name for your USB Stick Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick GB Go back KB MB Make a bootable USB stick Make bootable USB stick More information Not enough space on the USB stick. Select Image Select a USB stick Select an image Size: TB The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Verify Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-10 14:03+0000
Last-Translator: Devansh Mahajan <Unknown>
Language-Team: Hindi <hi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: hi
 फ़ाइल कॉपी करते समय त्रुटि। %s पर विभाजन करते समय त्रुटि। त्रुटि हुई त्रुटि हुई। पुष्टिकरण त्रुटि। जाँचें USB के लिए नाम चुनें सबकुछ ठीक है! FAT32 
  + सब जगह संगत।
  - 4GB से बड़ी फ़ाइलें उपयोग करने में असमर्थ।

exFAT
  + लगभग सब जगह संगत।
  + 4GB से बड़ी फ़ाइलें उपयोग करने में समर्थ।
  - FAT32 जितना संगत नहीं।

NTFS 
  + विंडोज के साथ संगत।
  - एप्पल के मैक एवं ज़्यादातर हार्डवेयर के साथ असंगत।
  - लिनक्स के साथ कभी-कभी संगतता समस्याएँ (NTFS अमुक्त है व रिवर्स-इंजीनियर किया गया है)।

EXT4 
  + आधुनिक, स्थिर, तेज़, नियमित लॉग-फ़ाइल बनता है।
  + लिनक्स फ़ाइल अनुमतियों का समर्थन करता है।
  -  विंडोज, एप्पल के मैक एवं ज़्यादातर हार्डवेयर के साथ असंगत।
 फॉर्मेट करें यूएसबी फॉर्मेट करें GB पीछे जाऐं KB MB बूट योग्य यूएसबी बनाएँ बूट योग्य यूएसबी बनाएँ अधिक जानकारी यूएसबी पर पर्याप्त स्पेस नहीं है। फ़ाइल चुनें यूएसबी चुनें फ़ाइल चुनें आकार TB यूएसबी सफलतापूर्वक फॉर्मेट हो गई। फ़ाइल सफलतापूर्वक राइट की गई। इससे यूएसबी का सारा डाटा नष्ट हो जाएगा, क्या आप जारी रखना चाहते हैं? URLs यूएसबी फ़ाइल राइटर यूएसबी यूएसबी फॉर्मेट हेतु साधन यूएसबी : अज्ञात सिग्नेचर जाँचें वॉल्यूम लेबल : राइट करें अज्ञात 