��    -      �  =   �      �     �     �  P   �     I     Y     a     j     {     �     �  >   �     �  
   �     �  
   �  \     X   _  ^   �  \     X   t  	   �     �  
   �     �     �     �                  2   (      [     |  0   �     �     �     �  	   �     �                /     7     =     P  �  c     
     %
  X   ,
     �
     �
  	   �
     �
  
   �
     �
     �
  I        R     U     k     x  }   �  y        �  }     y   �  
   �       
             !     :     C  
   _     j  :   r  1   �  %   �  6     &   <  "   c     �     �     �  0   �     �     �     �     �  H                   '          	           &   $                             #                "         ,                                             
   )      -                         +          !       *   (   %       Buttons layout: Compiz Compiz is installed by default in Linux Mint. Note: It is not available in LMDE. Compiz settings Compton Computer Configure Compiz Desktop Desktop Settings Desktop icons Here's a description of some of the window managers available. Home Icon size: Icons Icons only If Compton is not installed already, you can install it with <cmd>apt install compton</cmd>. If Marco is not installed already, you can install it with <cmd>apt install marco</cmd>. If Metacity is not installed already, you can install it with <cmd>apt install metacity</cmd>. If Openbox is not installed already, you can install it with <cmd>apt install openbox</cmd>. If Xfwm4 is not installed already, you can install it with <cmd>apt install xfwm4</cmd>. Interface Large Linux Mint Marco Marco settings Metacity Metacity settings Network Openbox Openbox is a fast and light-weight window manager. Overview of some window managers Reset Compiz settings Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text only Trash Welcome to Desktop Settings. Window Manager Windows Xfwm4 root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-12-25 18:07+0000
Last-Translator: Nicat Məmmədov <n1c4t97@gmail.com>
Language-Team: Azerbaijani <az@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Düymәlәrin düzülüşü: Compiz Compiz, Linux Mint-də əvvəlcədən quraşdırılıb. Qeyd: Bu LMDE-də mövcud deyil. Compiz tənzimləmələri Compton Kompüter Compiz-i quraşdır Masaüstü Masaüstü Tәnzimlәmәlәri Masaüstü piktoqramları Burada bəzi pəncərə idarəçiləri haqqında açıqlamalar mövcuddu. Ev Piktoqram ölçüsü: Piktoqramlar Yalnız piktoqramlar Əgər Compton quraşdırılmayıbsa, siz bunu <cmd>apt install compton</cmd> əmrinin köməyi ilə quraşdıra bilərsiniz. Əgər Marco quraşdırılmayıbsa, siz bunu <cmd>apt install marco</cmd> əmrinin köməyi ilə quraşdıra bilərsiniz. Əgər Metacity quraşdırılmayıbsa, siz bunu <cmd>apt install metacity</cmd> əmrinin köməyi ilə quraşdıra bilərsiniz. Əgər Openbox quraşdırılmayıbsa, siz bunu <cmd>apt install openbox</cmd> əmrinin köməyi ilə quraşdıra bilərsiniz. Əgər Xfwm4 quraşdırılmayıbsa, siz bunu <cmd>apt install xfwm4</cmd> əmrinin köməyi ilə quraşdıra bilərsiniz. İnterfeys Böyük Linux Mint Marco Marco tənzimləmələri Metacity Metacity tənzimləmələri Şəbəkə Openbox Openbox, sürətli və yüngül pəncərə idarəçisidir. Bəzi pəncərə idarəçilərinə ümumi baxış Compiz tənzimləmələrini sıfırla Masaüstündə görmәk istәdiyiniz şeylәri seçin: Düymәlәrdә piktoqramları göstәr Menyularda piktoqramları göstәr Kiçik Yalnız mәtn Zibil Masaüstü Tәnzimlәmәlәrinə xoş gəldiniz. Pəncərə İdarəçisi Pəncərələr Xfwm4 root@linuxmint.com Launchpad Contributions:
  Nicat Məmmədov https://launchpad.net/~nicat 