��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �     m     �     �     �     �     �     �     �  !        -     4  K   P     �                            	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-04-24 16:01+0000
Last-Translator: Will Peix <Unknown>
Language-Team: Chinese (Traditional) <zh_TW@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 會使用到 %s 的容量。 %s 的容量會被清除掉。 總共會下載 %s 需要進行其他更改 有錯誤發生 Flatpaks 安裝 要移除的軟體包 請查看下方的變更清單。 移除 將移除下列軟體包： 此選項並未與任何軟體包關聯。您是否要從選單中移除？ 更新 