��    *      l  ;   �      �  )   �  2   �               +     A     P     V      _    �     �     �     �  
   �     �     �     �     �     �     �  "     
   &     1     >     Q     a     g  )   j  #   �  N   �            	        '  
   ;     F     M     [     c     i     o  �  w  0   
  >   >
     }
     �
     �
     �
  	   �
     �
      �
  &    	   /     9     O     R     _     d     g     j     �     �  %   �  
   �     �     �       
   %     0  2   3  /   f  K   �     �     �  	   �     	  
   !  	   ,     6  	   M     W     _  	   e                            &   *                                           
   $           (                          "             )       	          %                               #            !       '    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick GB ISO images ISO: KB MB Make a bootable USB stick Make bootable USB stick More information Not enough space on the USB stick. SHA256sum: Select Image Select a USB stick Select an image Size: TB The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Verify Volume label: Volume: Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-10 02:25+0000
Last-Translator: karm <melo@carmu.com>
Language-Team: Interlingua <ia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Un error occurreva durante le copia del imagine. Un error occurreva durante le creation de un partition sur %s. Un error occurreva Un error occurreva. Error de authentication. Calculante... Controlar Controlo de summa Elige un nomine pro tu clave USB FAT32 
  + Compatibile ubique.
  -  Non  pote gerer files major de 4GB.

exFAT
  + Compatibile quasi ubique.
  + Pote gerer files major de 4GB.
  -  Non assi compatibile como FAT32.

NTFS 
  + Compatibile con Windows.
  -  Non compatibile con Mac e plure dispositivos hardware.
  -  Occasional problemas de compatibilitate con Linux (NTFS es proprietari e reverse ingenierisate).

EXT4 
  + Moderne, stabile, rapide, journalisate.
  + Supporta le permissiones del files de Linux.
  -  Non compatibile con Windows, Mac  e plure dispositivos hardware.
 Formattar Formatar un clave USB GB Imagines ISO ISO: kB MB Crear un clave USB bootabile Render bootabile un clave USB Altere informationes Spatio insufficiente in le clave USB. SHA256sum: Seliger un imagine Seliger a clave USB Seliger un imagine Dimension: TB Le drive USB ha essite formatate con bon successo. Le imagine ha essite scribite con bon successo. Isto destruera tote le datos sur le clave USB, desira tu vermente proceder? URLs Scriptor de imagine USB Clave USB Formatator de clave USB Clave USB: Verificar Etiquetta de volumine: Volumine: Scriber bytes incognite 