��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  '     *   =     h  +   }     �  �   �     <     \     o  ?   �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-01-03 14:12+0000
Last-Translator: Sitonir de Oliveira <sitonir@outlook.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s não é um nome de domínio válido. Bloqueia o acesso a domínios selecionados Domínios bloqueados Bloqueador de domínios (controle dos pais) Nome do domínio Os nomes de domínio devem começar e terminar com uma letra ou um dígito, e só podem conter letras, números, pontos e hifens. Exemplo: meu.dominionumero1.com Domínio inválido Controle dos Pais Por favor, informe o nome do domínio que você deseja bloquear 