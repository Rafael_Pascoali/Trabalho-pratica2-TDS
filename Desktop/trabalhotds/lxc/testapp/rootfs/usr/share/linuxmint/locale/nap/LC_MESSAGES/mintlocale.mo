��    $      <  5   \      0  ,   1  w   ^     �     �     �               %  �   2     �     �     �  ^        {     �     �     �     �     �     �     �  	   �            ,        L     S     Z     w     �     �     �     �     �  
   �  �  �  M   �  n   �     b	     w	     �	  !   �	     �	     �	  �   �	  	   �
     �
     �
  Z   �
          9  
   X     c     k     r     �     �     �     �     �  $   �     �               !  '   5     ]     q     x     �  
   �                                               	   $                  "         #                                      !                                                          
       %d language installed %d languages installed <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Telugu Thai Traditional Chinese Vietnamese Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-08-21 18:07+0000
Last-Translator: Silvio Celeste <Unknown>
Language-Team: Neapolitan <nap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %d maniera 'e chiacchiarià installata %d maniere 'e chiacchiarià installate <b><small> Statt accort: Si installi o aggiorni i pacchetti d''a lengua putiss installa ati lengue</small></b> Miett na lengua nova Miett na lengua nova Miett... Miett Tutt Tuorn Tuorn 'o Sistema Installat san san Metodo d'immissione I metodi d'immissione servono pe scrivere simboli e caratteri ca nun ce stann ngopp a tastiera. So utili pe scrivere in Cinese, Giapponese, Koreano, Thailandese e Vietnamita... Installà Miett/ Liev Lengue Miett / Liev Lengue... Installa tutt i pacchetti d''a lingua, 'e traduzioni e i dizionari p''a lengua selezionata Install pacchetti 'e lengua Installa 'a lengua selezionata Giapponese Koreano Lengua Settagg d''a Lengua Settaggi d''a lengua Supporto d''a lengua Lengue Nisciuna lengua scelta Nient Nummeri, sordi, indirizzi, misure... Regione Liev Liev 'a lengua selezionata Cinese Semplificato Certi pacchett d''a lingua nun ce stann Lengua d''o sistema Telugu Thailandese Cinese Tradizionale Vietnamita 