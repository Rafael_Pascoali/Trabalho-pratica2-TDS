��    8      �  O   �      �  ?   �  B     !   \  $   ~     �     �     �     �     �     �     �     �  	   �     �     �  	   �                         %     -  	   F  
   P     [     d     i     p     ~     �     �     �     �  +   �     �  
   �     �     �                          )     1  3   G  *   {     �     �     �     �     �     �          
       �    G   �	  Q   "
     t
  ,   �
     �
  !   �
     �
               /     M     `     t     �  '   �     �  #   �               "     1  4   >     s     �     �     �     �     �     �     �     �  
   �     
  ]        {  !   �     �  .   �     �     �            
   *     5  o   T  j   �  :   /  -   j  
   �     �     �  )   �  
             5                1         )   2      7           6   -                   (              &         "   /                                   
       3                     5      '   8       !   *      #                    .       ,       $             %   	   4       0   +                    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories All Board games Chat Details Drawing Education Electronics Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Installing Internet Java Launch Not available Not installed Office PHP Package Photography Please use apt-get to install this package. Programming Publishing Python Real-time strategy Remove Removing Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound Sound and video System tools Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-08-05 08:49+0000
Last-Translator: Murat Käribay <d2vsd1@mail.ru>
Language-Team: Kazakh <kk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s жүктеледі, %(localSize)s босатылады %(downloadSize)s жүктеледі, %(localSize)s диск қажет етеді %(localSize)s диск бос %(localSize)s диск қажет етеді 3D Бағдарлама туралы Қосымша құралдар Барлығы Үстел ойындары Әңгімелесу (чат) Толығырақ Сурет салу Оқу-ағарту Электроника Электронды пошта (Email) Эмуляторлар Файлдарды жариялау Қаріптер Ойындар Графика Орнату Жаңа бағдарламаларды орнату Орнатылған Орнатылуда Интернет Java Ашу Қол жетімді емес Орнатылмаған Офис PHP Пакет Фотосурет Өтініш, мына пакетті орнату үшін apt-get-ті қолданыңыз Бағдарламалау Жариялаулар жасау Python Нақты уақыт стратегиялар Жою Өшірілуде Шолулар Сканерлеу Ғылым Ғылым және Білім Пакеттердің сипаттамасы бойынша іздеу (одан да баяу іздейді) Пакеттердің қысқа сипаттамасы бойыша іздеу (баяу іздейді) Симуляторлар және автожарыстар Бағдарламалар менеджері Дыбыс Аудио және видео Жүйелік құралдар Қадамдық стратегиялар Бейне Қарау құралдары Веб 