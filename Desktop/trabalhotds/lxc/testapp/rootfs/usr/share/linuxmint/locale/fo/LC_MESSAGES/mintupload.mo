��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  I   I  >   �  4   �  !     O   )     y  O   �     �     �     �     �     �  	          '   -     U  �   u  !     -   /  3   ]  8   �     �     �     �     �  $   �               #     &  v   *     �     �     �  <   �     �     	  
     ;   #  2   _  .   �  #   �  0   �          &     ,     1     F     [     s  #   �  *   �      �     �  	     A        Y     `     h                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2010-10-21 09:57+0000
Last-Translator: Gunleif Joensen <Unknown>
Language-Team: Faroese <fo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s er ikki sett í samansetinga fíluni, funnin undir %(2)s ella %(3)s %(percentage)s av %(number)d fílur - Uppsendi til %(service)s %(percentage)s av 1 fílu - Uppsendi til %(service)s %(size_so_far)s av %(total_size)s %(size_so_far)s av %(total_size)s - %(time_remaining)s er eftir (%(speed)s/sec) %s Eginleikar <b>Vinarliga skriva inn  eitt navn fyri tí nýggju uppsendinga tænastuna:</b> Um B Ógilda Ógilda uppsending? Kanna samband Lat aftur Kundi ikki fáa tøkt pláss Kundi ikki goyma broyttu samansetingina Skilmarka uppsendinga tænastur Fíluskrá at uppløða til. <TIMESTAMP> verður sett í staðin fyri núverandi tíðarstempil, eftirfylgjandi śetta tíðastemplinum. Sjálvsett er: Vil tú avbróta hesa uppsending? Drag &amp; slepp her fyri at senda upp til %s Fílan er størri enn tøka plássið á tænastuni Fílan er størri enn mest loyvda stødd hjá tænastuni Fílu uppsending væleydnað. GB GiB Vertur: Vertsnavn ella IP adressa, forsett:  KB KiB MB MiB Sjálvsett atlát : atlátsleysar SCP sambinding, tómastrong FTP sambinding, ~/.ssh lyklar nýttir til SFTP sambingar Atlát: Leið: Portur: Fjarsett portur, 21 er forsett fyri FTP, 22 fyri SFTP og SCP Koyr í bakgrundini Tænastu navn: Tænastur: Upsending av %(number)d fílum til '%(service)s' eydnaðist Uppsending av 1 fílu til '%(service)s' eydnaðist Uppsendingin til '%(service)s' varð ógildað Hendan tænastan krevur eitt atlit. Tíðarstempla snið (strftime). Við forsettum: Tíðarstempul: Slag: URL: Ókeynd tænasta: %s Uppsendinga Leiðari Uppsendinga leiðari... Uppsendinga tænastur Uppsending til '%s' miseydnaðist:  Uppsendi %(number)d fílur til %(service)s Uppsendi 1 fílu til %(service)s Uppsendi fíluna... Brúkari: Brúkaranavn, ferð til títt forsetta staðarbundna brúkaranavn _Fíla _Hjálp sambinding væleydnað tryggja 