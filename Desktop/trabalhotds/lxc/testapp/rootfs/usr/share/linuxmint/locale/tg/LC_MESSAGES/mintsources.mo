��          �   %   �      `     a     u     z  :   �     �     �     �     
          )     -     2     :     A     E  8   J     �  
   �     �     �     �     �     �  /   �  6   '     ^  �  p  4   9     n  (   w  y   �       ,   '  H   T     �  C   �  
   �  
   �     
          )     -  �   6  J   �     	  A   #	  &   e	  %   �	     �	     �	  e   �	  k   4
  #   �
                                  	                                                                                   
       Backported packages Base Cannot add PPA: '%s'. Configure the sources for installable software and updates Country Edit the URL of the PPA Edit the URL of the repository Enabled Fix MergeList problems Key Main Mirrors Open.. PPA PPAs Please enter the name of the repository you want to add: Purge residual configuration Repository Restore the default settings Select a mirror Software Sources Sources Speed The problem was fixed. Please reload the cache. There is no more residual configuration on the system. Unstable packages Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: Victor Ibragimov <victor.ibragimov@gmail.com>
PO-Revision-Date: 2014-01-16 14:28+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <tg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: tg
 Бастаҳои нармафзори бойгонӣ Асос PPA илова намешавад: '%s'. Танзим кардани манбаҳо барои нармафзор ва навсозиҳои насбшаванда Кишвар Таҳрир крдани URL барои PPA Таҳрир крдани URL барои анбори нармафзор Фаъолшуда Ҳал кардани мушкилиҳои муттаҳидсозӣ Калид Асосӣ Оинаҳо Кушодан... PPA PPA-ҳо Лутфан, номи анбори нармафзореро, ки мехоҳед илова кунед, ворид намоед: Тоза кардани танзимоти боқимондаи изофӣ Анбори нармафзор Барқарор кардани танзимоти пешфарз Интихоб кардани оина Манбаъҳои нармафзор Манбаҳо Суръат Мушкилӣ ҳал шудааст. Лутфан, зерҳофизаро навсозӣ кунед. Ягон танзимоти боқимондаи изофӣ дар система вуҷуд надорад Бастаҳои ноустувор 