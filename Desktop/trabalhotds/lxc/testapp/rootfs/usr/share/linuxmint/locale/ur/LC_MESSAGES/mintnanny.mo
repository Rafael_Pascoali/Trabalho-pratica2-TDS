��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  /   �  E        d          �  �   �     �     �     �  q   �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-05-28 13:37+0000
Last-Translator: Umar Hammad <Unknown>
Language-Team: Urdu <ur@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ناموزوں ڈومیں کا نام ہے۔ منتخب شدہ ڈومین ناموں تک رسائی روک دیں بلاک شدہ ڈومین ڈومین بلاکر ڈومین نام ڈومین ناموں کو ایک حرف یا ایک عدد کے ساتھ شروع اور ختم کرنا لازمی ہے، اور اس میں صرف حروف، ہندسے، نقطے اور ڈیش (-)، شامل ہو سکتے ہیں۔ مثال:  my.number1domain.com ناموزوں ڈومین آبائی کنٹرول براہ مہربانی جس ڈومین کو آپ بلاک کرنا چاہتے ہیں اس کا نام لکھیں 