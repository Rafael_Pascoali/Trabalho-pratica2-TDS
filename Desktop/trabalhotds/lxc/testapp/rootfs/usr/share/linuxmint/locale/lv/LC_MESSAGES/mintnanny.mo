��    
      l      �       �      �   %        6     F     U     a          �  -   �  �  �  !   ^  &   �     �     �     �     �            <   $            
          	                       %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-08-16 15:16+0000
Last-Translator: tuxmaniack <Unknown>
Language-Team: Latvian <lv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nav derīgs domēna nosaukums. Bloķēt pieeju izvēlētiem domēniem Bloķētie domēni: Domēnu bloķētājs Domēna nosaukums Piemērs: mans.numur1domens.lv Nederīgs domēns Vecāku kontrole Lūdzu, ievadiet domēna nosaukumu, kuru vēlaties bloķēt. 