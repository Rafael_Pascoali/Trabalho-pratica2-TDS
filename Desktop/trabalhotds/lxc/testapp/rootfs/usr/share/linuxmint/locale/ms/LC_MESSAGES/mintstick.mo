��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  #     .   6     e     z     �  0  �     �
     �
     �
       %   :  
   `     k     z  #   �     �  M   �          )     2  	   E     O     \     b                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-09-30 02:36+0000
Last-Translator: abuyop <Unknown>
Language-Team: Malay <ms@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Ralat berlaku semasa menyalin imej. Ralat berlaku semasa mencipta sekatan pada %s. Ralat telah berlaku. Ralat Pengesahihan. Pilih nama untuk Pena USB anda FAT32 
  + Serasi pada mana-mana peranti.
  -  Tidak dapat mengendalikan fail lebih besar daripada 4GB.

exFAT
  + Serasi hampir pada mana-mana peranti.
  + Boleh mengendalikan fail lebih besar daripada 4GB.
  -  Tidak sepenuhnya serasi seperti FAT32.

NTFS 
  + Serasi dengan Windows.
  -  Tidak serasi dengan Mac dan kebanyakan peranti.
  -  Ada masalah keserasian dengan Linux (NTFS bersifat proprietari).

EXT4 
  + Modern, stabil, pantas, terjurnal.
  + Sokong keizinan fail Linux.
  -  Tidak serasi dengan Windows, Mac dan kebanyakan peranti perkakasan.
 Format Format pemacu pena USB Buat pemacu pena USB boleh but Buat pemacu pena USB boleh but Ruang dalam pena USB tidak mencukupi. Pilih Imej Pilih pena USB Pilih satu imej Pena USB telah berjaya diformatkan. Imej telah berjaya ditulis. Ia akan musnahkan semua data dalam pena USB, anda pasti hendak meneruskannya? Penulis Imej USB Pena USB Pemformat Pena USB Pena USB: Label volum: Tulis tidak diketahui 