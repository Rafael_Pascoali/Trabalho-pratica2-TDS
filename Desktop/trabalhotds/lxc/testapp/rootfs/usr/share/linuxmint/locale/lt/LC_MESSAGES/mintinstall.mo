��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  C   M  D   �  &   �  !   �  A        a     d     i     q     w     �  2   �     �     �  j   �     e  	   {  R   �  %   �     �  	   
       
   .     9     E     Q  	   ^     h     {  c   �  M   �     <  	   E     O     W     `     x     �  
   �  I   �  
   �     �     �  #     
   '     2      G  %   h  t   �       
     	        $     (     0  2   <  )   o     �     �     �     �     �  #   �  
     	     K     
   f     q     }     �  4   �  1   �  ?        B     ]     x     �     �     �  3   �  =   �     ,     J     R     _         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-05-16 10:34+0000
Last-Translator: Moo <hazap@hotmail.com>
Language-Team: Lithuanian <lt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s atsiųsti, %(localSize)s atlaisvintos vietos diske %(downloadSize)s atsiųsti, %(localSize)s reikalaujamos vietos diske Atlaisvinta %(localSize)s disko vietos Reikia %(localSize)s disko vietos Vykdoma %d užduotis Vykdomos %d užduotys Vykdoma %d užduočių 3D Apie Priedai Visos Visos programos Visos operacijos užbaigtos Bandant pridėti Flatpak saugyklą, įvyko klaida. Stalo žaidimai Nepavyksta įdiegti Nepavyksta apdoroti šį failą, kol yra aktyvių operacijų.
Joms užbaigus darbą, bandykite dar kartą. Nepavyksta pašalinti Pokalbiai Spustelėkite <a href='%s'>čia</a>, norėdami pridėti savo asmeninę apžvalgą. Šiuo metu dirbama su šiais paketais Informacija Piešimas Redaktoriaus pasirinkimai Švietimas Elektronika El. paštas Emuliatoriai Reikmenys Failų bendrinimas Pirmojo asmens Šiuo metu Flatpak palaikymas yra neprieinamas. Pabandykite įdiegti flatpak ir gir1.2-flatpak-1.0. Šiuo metu Flatpak palaikymas yra neprieinamas. Pabandykite įdiegti flatpak. Šriftai Žaidimai Grafika Įdiegti Diegti naujas programas Įdiegta Įdiegtos programos Įdiegiama Šio paketo įdiegimas gali padaryti nepataisomą žalą jūsų sistemai. Internetas Java Paleisti Riboti paiešką iki esamo sąrašo Matematika Multimedijos kodekai Multimedijos kodekai, skirti KDE Nerasta jokių atitinkančių paketų Nėra jokių paketų, kuriuos rodyti.
Tai gali reikšti, kad yra problemų - pabandykite įkelti podėlį iš naujo. Neprieinama Neįdiegta Raštinė PHP Paketas Fotografija Būkite kantrūs. Tai gali šiek tiek užtrukti... Šio paketo įdiegimui naudokite apt-get. Programavimas Leidyba Python Realaus laiko strategija Įkelti iš naujo Iš naujo įkelti paketų sąrašą Pašalinti Šalinama Šio paketo pašalinimas gali padaryti nepataisomą žalą jūsų sistemai. Apžvalgos Nuskaitymas Mokslas Švietimas ir mokslas Ieškoti paketų aprašuose (dar lėtesnė paieška) Ieškoti paketų santraukose (lėtesnė paieška) Minutėlę, atliekama paieška programinės įrangos saugyklose Rodyti įdiegtas programas Simuliacijos ir lenktynės Programų tvarkytuvė Garsas Garsas ir vaizdas Sistemos įrankiai Flatpak saugykla, kurią bandote pridėti, jau yra. Šiuo metu yra aktyvių operacijų.
Ar tikrai norite išeiti? Ėjimais pagrįsta strategija Vaizdas Žiūryklės Žiniatinklis 