��          t      �              %   0     V     f     u  q   �     �             -   1  �  _      �  '        B     T  
   e  |   p     �            /   -                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-08 15:20+0000
Last-Translator: Emil Våge <Unknown>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s er ikke et gyldig domenenavn. Blokker tilgangen til valgte domenenavn Blokkerte domener Domene-blokkerer Domenenavn Domenenavn må starte og slutte med en bokstav eller et tall og kan bare inneholde bokstaver, tall, punktum og bindestreker. Eksempel: mitt.nummer1domene.no Ugyldig domene Foreldrekontroll Vennligst skriv inn domenenavnet du vil bokkere 