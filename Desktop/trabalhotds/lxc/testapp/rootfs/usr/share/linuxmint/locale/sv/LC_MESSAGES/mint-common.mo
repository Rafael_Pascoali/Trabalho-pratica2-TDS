��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  (   :  $   c     �     �  *   �  *   �  %     ,   @  '   m  *   �     �  @   �  
   	     	     $	  
   2	  +   =	     i	  %   �	     �	     �	     �	     �	  $   �	  ^   
  %   e
  
   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 20:10+0000
Last-Translator: Jan-Olof Svensson <jan-olof.svensson@abc.se>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s mer diskutrymme kommer att användas. %s diskutrymme kommer att frigöras. %s kommer att hämtas totalt. Ytterligare ändringar krävs Ytterligare program kommer att nedgraderas Ytterligare program kommer att installeras Ytterligare program kommer att rensas Ytterligare program kommer att ominstalleras Ytterligare program kommer att tas bort Ytterligare program kommer att uppgraderas Ett fel uppstod Kan inte ta bort paketet %s eftersom det behövs av andra paket. Nedgradera Flatpak Flatpak-paket Installera Följande paket är beroende av paketet %s: Paket som kommer att tas bort Ta en titt på ändringslistan nedan. Rensa Ominstallera Ta bort Hoppa över uppgradering Följande paket kommer att tas bort: Det här menyvalet är inte kopplat till något paket. Vill du ta bort det från menyn ändå? Uppdateringar kommer att hoppas över Uppgradera 