��          �   %   �      @  )   A  4   k     �     �     �     �     �     �  $   �  !        8     U  "   p     �     �  
   �     �     �     �  g     \   i  B   �  &   	  )   0  �  Z  *     =   0     n      �     �     �     �     �  ,   �     
  $   (     M  ,   h     �     �     �     �     �     �  {   	  i   �	  <   �	     )
     H
                                                                                
                                   	           %s is not located in your home directory. An error occurred while opening the backup file: %s. Backing up: Backup Tool Backups Calculating... Exclude directories Exclude files Make a backup of your home directory No packages need to be installed. Please choose a backup file. Please choose a directory. Please select packages to install. Refresh Remove Restoring: The backup was aborted. The following errors occurred: The restoration was aborted. This backup file is either too old or it was created with a different tool. Please extract it manually. Warning: The meta file could not be saved. This backup will not be accepted for restoration. You do not have the permission to write in the selected directory. Your files were successfully restored. Your files were successfully saved in %s. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-11-27 12:47+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s er ikke plassert i hjemmekatalogen din. Det oppstod en feil under åpning av sikkerhetskopifilen: %s. Sikkerhetskopierer: Verktøy for sikkerhetskopiering Sikkerhetskopier Beregner … Utelat mapper Utelat filer Lag en sikkerhetskopi av hjemmeområdet ditt Ingen pakker må installeres. Vennligst velg en sikkerhetskopifil. Vennligst velg en katalog. Vennligst velg pakkene som skal installeres. Oppdater Fjern Gjenoppretter Sikkerhetskopien ble avbrutt. Følgende feil oppstod: Gjenopprettingen ble avbrutt. Denne sikkerhetskopifilen er enten for gammel eller den ble opprettet med et annet verktøy. Vennligst pakk ut det manuelt. Advarsel: Meta-filen kunne ikke lagres. Denne sikkerhetskopien vil ikke bli akseptert for gjenoppretting. Du har ikke tillatelse til å skrive i den valgte katalogen. Filene dine ble gjenopprettet. Filene dine ble lagret i %s. 