��          �      \      �  )   �  2   �     .     A      W     x          �     �  "   �     �  )   �  #   $  N   H     �  	   �     �  
   �     �  �  �  )   �  2   �     �     �           3     :     M     g  "        �  )   �  #   �  N        R  	   c     m  
   �     �                             	                                                       
               An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select a USB stick The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-08-07 21:48+0000
Last-Translator: mzs.112000 <mzs.112000@gmail.com>
Language-Team: English (Australia) <en_AU@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select a USB stick The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: 