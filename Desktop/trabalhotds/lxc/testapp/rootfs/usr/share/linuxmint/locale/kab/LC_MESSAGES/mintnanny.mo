��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  $     5   )     _     q     �  �   �     "     ?     W  :   i                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-11-13 19:35+0000
Last-Translator: Belkacem Mohammed <belkacem77@gmail.com>
Language-Team: Kabyle <kab@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s mačči d isem n taɣult ameɣtu. Sewḥel anekcum ɣer yesmawen n taɣult yettwafernen Tiɣula iweḥlen Amsewḥal n taɣult Isem n taɣult Yessefk ismawen n taɣult ad bdun neɣ ad faken q usekkil neɣ azwil, udiɣ ad yegber kan isekkilen, izwilen, tineqqiḍin neɣ ijerriḍen. Amedya: my.number1domain.com Taɣult mai d tameɣtut Asenqed n imawlan Ma ulac aɣilif sekcem taɣult i tebqiḍ ad tesweḥleḍ 