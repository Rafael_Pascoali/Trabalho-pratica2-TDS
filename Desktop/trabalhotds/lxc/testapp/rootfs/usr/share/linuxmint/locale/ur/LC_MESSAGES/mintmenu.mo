��    E      D  a   l      �  	   �     �                    )  K   6  %   �  -   �     �  "   �  F        U     ^     t     �     �     �     �  	   �     �     �  	   �                    +  -   @     n     u     �     �     �     �     �     �     �     �     �     �     �     	     	     (	     /	  1   ;	  6   m	     �	     �	  $   �	     �	     �	     �	     
     5
     E
     [
     l
     �
  '   �
     �
     �
     �
     �
     �
        	          �  %     �  
   �     �     �           '  �   =  Q   �  S   +  5     :   �  �   �     z  !   �     �  /   �  /   �     #  #   3  #   W     {  .   �     �     �     �     �        `   4     �  <   �     �  /   �     *  
   8     C     b     i     q  )        �     �     �     �  
   �     �  Q     a   `     �  (   �  o   �     k      x  6   �  <   �       /   ,     \  '   {  -   �  `   �     2  R   N     �  
   �     �     �     �  +   �     3                  ?   D   B   ,             "      )   '             #           ;                 @                   +         %   C   !   .       8      /          7   9           :                 <   5   0       6   E   *           1   =   (                         4           2      $   A                              	      >      
       &   -    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Enable Internet search Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-12-11 11:04+0000
Last-Translator: Waqar Ahmed <waqar.17a@gmail.com>
Language-Team: Urdu <ur@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <غیر طے شدہ> متعلق اعلیٰ میٹ فہرست تمام تمام ایپلی کیشنیں ایپلی کیشنز براؤز کریں تمام لوکل اور ریموٹ ڈسکوں اور فولڈروں کو، جو اس کمپیوٹر سےقابلِ رسائی ہیں۔ دستیاب کردہ سافٹ وئیر پرکھیں اور انسٹال کریں بک مارک شدہ اور لوکل نیٹ ورک مقامات براؤز کریں حزف کردہ فائلوں کو براؤز کریں ڈیسک ٹاپ پہ پڑی چیزیں براؤز کریں فہرست کھولنے اور بند کرنے کے واسطے نیا ایکسلر یٹر سیٹ کرنے کے لیے کلک کریں۔   کمپیوٹر سسٹم کی تشکیل کریں انتظامی مرکز پلگ ان شروع کرنے میں ناکام یہ پلگ ان لوڈ نہیں ہو پایا: ڈیسک ٹاپ ڈیسک ٹاپ کی شکل(theme): فہرست کی تدوین کریں ردی خالی کریں انٹرنیٹ سے تلاش فعال کریں پسندیدہ گھر فولڈر سیپ ریٹر ڈالیں سپیس ڈالیں پیکج %s کو نصب کریں اپنے سافٹ وئیر انسٹال کریں، حزف کریں اور اپ گریڈ کریں شروع شروع کر دیں جیسے ہی میں لاگ ان ہوں اسکرین مقفل کریں لاگ آوٹ کریں یا صارف بدلیں لاگ آؤٹ فہرست فہرست کی ترجیحات نام نام: نیٹ ورک اپنا ذاتی فولڈر کھولیں اختیارات پیکج منتظم پاتھ ایکسلر یٹر چنیں جگہیں ترجیحات پچھلی کی بائنڈنگ کو مٹانے کے لیے BACKSPACE دبائیں اس عمل کو ختم کرنے کے لیے ESC دبائیں یا دوبارہ کلک کریں   بند کریں پلگ ان دوبارہ لوڈ کریں آخری استعمال کیا جانے والا زمرہ یا آخری کی گئی  تلاش یاد رکھیں ہٹائیں پسندیدہ سے ہٹائیں کھولنے کے لیے پاس ورڈ درکار ہے پیکج انسٹال کرنے کے لیے تلاش کریں فولڈر منتخب کریں تمام ایپلی کیشنیں دکھائیں بٹن آئکن دکھائیں زمروں کے آئکن دکھائیں میرے پسندیدہ میں دکھائیں کمپیوٹر بند کریں، ری سٹارٹ کریں، معطل کریں یا سلا دیں سوفٹویئر منتظم نام اور عام نام (generic name)کا آپس میں مبادلہ کریں سسٹم ٹرمنل شکل(theme): ردی ان انسٹال کمانڈ لائن استعمال کریں 