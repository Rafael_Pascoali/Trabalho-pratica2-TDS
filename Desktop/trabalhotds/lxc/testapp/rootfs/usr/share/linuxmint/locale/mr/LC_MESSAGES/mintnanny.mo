��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  4   5  _   j  %   �  "   �       D  3  (   x     �  1   �  m   �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-09 05:21+0000
Last-Translator: Vikram Kulkarni <kul.vikram@gmail.com>
Language-Team: Marathi <mr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
X-Poedit-Country: INDIA
X-Poedit-Language: Marathi
 %s वैध डोमेन नाव नाही. निवडक डोमेन वरील प्रवेश अवरुद्ध करा अवरुद्ध डोमेन डोमेन अवरोधक डोमेनचे नाव डोमेन नावे सुरू ⁠⁠⁠किंवा समाप्त केवळ अक्षरे किंवा अंक सह ⁠⁠⁠होणे आवश्यक आहे, आणि केवळ अक्षरे, अंक, ठिपके आणि हायफन असू शकतात उदाहरण: my.number1domain.com अवैध डोमेन पालकांचे नियंत्रण आपण अवरोधित करू इच्छित डोमेन नाव टाइप करा 