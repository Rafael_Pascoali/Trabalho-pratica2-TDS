��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �  '        8     I     Y  w   f     �     �       .   !                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-10-14 14:14+0000
Last-Translator: Eilidh Martin <Unknown>
Language-Team: Scots <sco@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s isnae a valed domaine name Block access tae selectit domaine names Blockit domaines Domaine Blocker Domaine name Domaine names hae tae stairt an' end wi' a letter or a nummer, an' can only contain letters, nummers, dots an' hyphens. Exaimple: my.nummer1domaine.com Invaled Domaine Parental Reinin-in Please type the domaine name ye want tae block 