��          �      �        �   	  P   �     �  >        C  
   H     S  
   Y  X   d  ^   �  X        u  �   {  �         �     �  	   �     �     �     �  �   �     �  �  �  ]  /  �   �	     %
  q   ?
     �
     �
     �
     �
  �     �   �  �   $     �  &  �  +  �  M        \     e     y     �     �  &  �  s   �           	                                           
                                                  Compiz is a very impressive window manager, very configurable and full of amazing effects and animations. It is however the least reliable and the least stable. Compiz is installed by default in Linux Mint. Note: It is not available in LMDE. Computer Here's a description of some of the window managers available. Home Icon size: Icons Icons only If Marco is not installed already, you can install it with <cmd>apt install marco</cmd>. If Metacity is not installed already, you can install it with <cmd>apt install metacity</cmd>. If Xfwm4 is not installed already, you can install it with <cmd>apt install xfwm4</cmd>. Large Marco is the default window manager for the MATE desktop environment. It is very stable and very reliable. It can run with or without compositing. Metacity was the default window manager for the GNOME 2 desktop environment. It is very stable and very reliable. It can run with or without compositing. Overview of some window managers Small Text only Toolbars Trash Windows Xfwm4 is the default window manager for the Xfce desktop environment. It is very stable and very reliable. It can run with or without compositing. translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-08-02 09:13+0000
Last-Translator: Smax3 <msendyuk@hotmail.com>
Language-Team: Yiddish <yi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Compiz איז זײער אַן אימפּאָזאַנטער פֿענצטער־מענעדזשער, ער איז זײער קאָנפֿיגורירבאַר און פֿול מיט אומבאַגרײַפֿלעכע עפֿעקטן און אַנימאַציעס. מערניט, איז ער דער אונפֿאַרלאָזלעכסטער און דער אונסטאַבילסטער. Compiz איז אינסטאַלירט אין Linux Mint דורך דער פֿעליקײט. אָנמערקונג: ער איז ניט בנימצא אין LMDE קאָמפּיוטער‬ ס'איז דאָ אַ באַשרײַבונג פֿון עטלעכע בנימצא פֿענצטער־מענעדזשערס. היים גרייס פֿון בילדל: בילדלעך בלױז ביבלדלעך ווען Macro איז ניט נאָך אינסטאַלירט, איר קענט אים אינסטאַלירן מיט <cmd>apt install marco</cmd>. ווען Metacity איז ניט נאָך אינסטאַלירט, איר קענט אים אינסטאַלירן מיט <cmd>apt install metacity</cmd>. ווען Xfwm4 איז ניט נאָך אינסטאַלירט, איר קענט אים אינסטאַלירן מיט <cmd>apt install xfwm4</cmd>. גרױס Macro איז דער פֿענצטער־מענעדזשער פֿאַר דער MATE עקראַנאָרט־סבֿיבֿה דורך דער פֿעליקײט. ער איז זײער סטאַביל און פֿאַרלאָזלעך. ער קען פֿונקציאָנירן מיט אָדער אָן קאָמפּאָזיטונג. Metacity איז דער פֿענצטער־מענעדזשער פֿאַר דער GNOME2 עקראַנאָרט־סבֿיבֿה דורך דער פֿעליקײט. ער איז זײער סטאַביל און פֿאַרלאָזלעך. ער קען פֿונקציאָנירן מיט אָדער אָן קאָמפּאָזיטונג. איבערבליק פֿון עטלעכע פֿענצטער־מענעדזשערס קלײן בלױז טעקסט מכשיר־װירעס אָפּפאַל פֿענסטערס Xfwm4 איז דער פֿענצטער־מענעדזשער פֿאַר דער Xfce עקראַנאָרט־סבֿיבֿה דורך דער פֿעליקײט. ער איז זײער סטאַביל און פֿאַרלאָזלעך. ער קען פֿונקציאָנירן מיט אָדער אָן קאָמפּאָזיטונג. Launchpad Contributions:
  Imyirtseshem https://launchpad.net/~imyirtseshem
  Smax3 https://launchpad.net/~msendyuk 