��    c      4  �   L      p  ?   q  B   �  !   �  $   	     ;	      P	     q	     t	     z	     �	     �	     �	     �	  5   �	     �	     �	     
  _   
     p
     ~
  3   �
  +   �
     �
     �
     �
  	               	     
   (     3     @     M  Z   Z  C   �     �     �                 	   /     9  
   P  F   [     �     �     �     �     �     �     �     	       L   *     w     �     �     �     �     �     �  -   �  +   �     %  
   1     <     C     V     ^     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �     �     �     �     �  6     E   <     �     �  	   �     �     �     �     �     �     �  �  �  B   �  ?   '  $   g  !   �     �  /   �     �  	   �  
                       -  :   H     �     �     �  V   �     �       D     1   Y     �     �     �     �  
   �     �     �  
   �     �     �       �     k   �     	       	        )     -     E     M  	   a  W   k  	   �     �     �  )   �  	          $   )     N     U  _   q  !   �     �  
   
                    $  4   6  7   k     �  
   �     �     �     �     �     �  	     \        m     z     �     �  .   �  0   �  5   �     .     L     c     i          �     �     �  5   �  E   �     ,     D     ]     m     �     �     �     �     �                5   /          =           X   $   ,   K   0   -   a   <   %       U                      T   *   1       [          B      8   I       @           .   &                         O   D               \   P   ^   !   N   Q   ?         >         E   H                    A   7   
   C           "   4   Y          ;       	   L      b                     :       R          ]      S      c   Z      G       9   )       F   `   +   (   #             V   J   6   W   3   M       2              '   _             %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unavailable Version: Video Viewers Web Project-Id-Version: Mint Install
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-12-12 07:12+0000
Last-Translator: Butterfly <Unknown>
Language-Team: Linux Mint Türkiye Çeviri Tayfası <hsngrms@ovi.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
X-Poedit-Country: TURKEY
Language: 
 %(downloadSize)s indirilecek, %(localSize)s disk alanı boşalacak %(downloadSize)s indirilecek, %(localSize)s disk alanı gerekli %(localSize)s disk alanı boşalacak %(localSize)s disk alanı gerekli %d İncelene %d İnceleme %d görev çalışıyor %d görev çalışıyor 3D Hakkında Donatılar Ekle Tümü Tüm Uygulamalar Tüm işlemler tamamlandı Flatpak deposunu eklemeye çalışırken bir hata oluştu. Masa oyunları Dal: Kurulamıyor Bu dosya aktif işlemler varken işlenemiyor.
Lütfen bittikten sonra yeniden deneyin. Kaldırılamıyor Sohbet Kendi incelemenizi eklemek için <a href='%s'>buraya</a> tıklayın. Şu anda aşağıdaki paketlerde çalışılıyor Detaylar Çizim Editörün Seçtikleri Eğitim Elektronik E-posta Öykünücüler Gerekliler Dosya paylaşımı Birinci-sahış Flatpak (%s) Flatpak desteği şu an kullanılabilir değildir. Kullanabilmek için flatpak ve gir1.2-flatpak-1.0 paketlerini yüklemeyi deneyin. Flatpak desteği şu an kullanılabilir değildir. Kullanabilmek için flatpak paketini yüklemeyi deneyin. Yazı Tipleri Oyunlar Grafikler Kur Yeni uygulamalar yükle Kuruldu Yüklü Uygulamalar Kuruluyor Bu paketin kurulması sisteminizde onarılması mümkün olmayan hasara neden olabilir. İnternet Java Başlat Aramayı mevcut listeleme için sınırla Matematik Çokluortam Çözücüleri KDE için Çokluortam Çözücüleri İsim: Eşleşen paket bulunamadı Gösterilecek paket yok.
Bu bir sorun olduğunu gösterebilir - önbelleği yenilemeyi deneyin. Ekran görüntüsü mevcut değil Kullanılabilir değil Kurulmadı Ofis PHP Paket Fotoğrafçılık Lütfen sabırlı olunuz. Bu biraz zaman alabilir... Bu paketi yüklemek için lütfen apt-get kullanınız. Programlama Yayımlama Python Gerçek zamanlı strateji Yenile Paketlerin listesini yenile Kaldır Siliniyor Bu paketin kaldırılması sisteminizde onarılması mümkün olmayan hasara neden olabilir. İncelemeler Tarama Bilim Bilim ve Eğitim Paketlerin tanımında ara (daha yavaş arama) Paketlerin özet bilgilerinde ara (yavaş arama) Yazılım depoları araştırılıyor, biraz bekleyin Yüklü uygulamaları göster Simülasyon ve yarış Boyut Yazılım Yöneticisi Ses Ses ve Video Sistem Paketi Sistem Araçları Eklemek çalıştığınız Flatpak deposu zaten var. Şu anda etkin işlemler mevcut.
Çıkmak istediğinize emin misiniz? Bu bir sistem paketidir Bu bir Flatpak paketidir Yeniden deneyin Turn-based strateji Kullanılabilir değil Sürüm: Video Görüntüleyiciler Web 