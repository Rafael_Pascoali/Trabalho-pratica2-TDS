��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �  %   {     �     �     �     �     �     �       !        A     H  F   g     �                            	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-02-27 14:14+0000
Last-Translator: AlephAlpha <alephalpha911@gmail.com>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 将消耗 %s 的额外磁盘空间。 将释放 %s 的磁盘空间。 共需下载 %s。 需要额外的更改 出现错误 Flatpak 安装 将移除下列软件包 请查看下面的修改列表。 移除 下列软件包将被移除： 该菜单项未关联任何包。您是否想将其从菜单中移除? 升级 