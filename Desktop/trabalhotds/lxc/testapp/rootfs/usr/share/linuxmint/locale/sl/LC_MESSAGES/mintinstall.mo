��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J    N  F   P  K   �  -   �  +     W   =     �  
   �     �     �     �     �  ?   �          &  k   ;     �     �  5   �  1   �     ,     8     @     S     b     n  
   w     �     �     �  [   �  E        J     Q     V     ^     f     |     �     �  R   �     �                  
   6     A      [     |  V   �     �     �                      4   #  ,   X     �     �     �     �     �     �     �     �  S   �     N     T     d     l  4   �  3   �  6   �     &     F     Z     y     ~     �  4   �  N   �     !     4     :     J         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-07-14 06:16+0000
Last-Translator: Martin Srebotnjak <miles@filmsi.net>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s za prenos, %(localSize)s prostega diskovnega prostora %(downloadSize)s za prenos, %(localSize)s diskovnega prostora potrebovanega %(localSize)s sproščenega prostora na disku %(localSize)s zahtevanega prostora na disku %d opravil je v teku %d opravilo je v teku %d opravili sta v teku %d opravila so v teku 3D O programu Pripomočki Vse Vsi programi Vsa opravila so končana Pri poskusu dodajanja skladišča Flatpak je prišlo do napake. Namizne igre Namestitev ni možna Te datoteke ni mogoče obdelati, ker je več aktivnih operacij.
Poskusite znova kasneje, ko se zaključijo. Ni mogoče odstraniti Klepet Kliknite <a href='%s'>sem</a>, da dodate svojo oceno. Trenutno izvajanje operacij na naslednjih paketih Podrobnosti Risanje Izbira uredništva Izobraževanje Elektronika E-pošta Emulatorji Osnove Souporaba datotek V prvi osebi Podpora za Flatpak je trenutno na voljo. Poskusite namestiti flatpak in gir1.2-flatpak-1.0. Podpora za Flatpak trenutno ni na voljo. Poskusite namestiti flatpak. Pisave Igre Grafika Namesti Namesti nove programe Nameščeno Nameščene aplikacije Nameščanje Z namestitvijo tega paketa lahko povzročite nepopravljivo škodo svojemu sistemu. Internet Java Zaženi Omeji iskanje na trenuten seznam Matimatika Večpredstavnostni kodeki Večpredstavnostni kodeki za KDE Ni zadetkov med paketi Ni paketov za prikaz.
To lahko predstavlja težavo: poskusite osvežiti predpomnilnik. Ni na voljo Ni nameščeno Pisarna PHP Paket Fotografija Bodite potrpežljivi. To lahko traja nekaj časa ... Uporabite apt-get za namestitev tega paketa. Programiranje Založništvo Python Strategije v realnem času Osveži Osveži seznam paketov Odstrani Odstranjevanje Z odstranitvijo tega paketa lahko povzročite nepopravljivo škodo svojemu sistemu. Ocene Optično branje Znanost Znanost in izobraževanje Iskanje po opisih paketov (še počasnejše iskanje) Iskanje po povzetkih paketov (počasnejše iskanje) Iskanje po skladiščih programka, počakajte trenutek Prikaži nameščene aplikacije Simulacije in dirke Upravljalnik programske opreme Zvok Zvok in video Sistemska orodja Skladišče Flatpak, ki želite dodati, že obstaja. V teku so aktivne operacije.
Ali ste prepričani, da želite končati z delom? Potezne strategije Video Pregledovalniki Splet 