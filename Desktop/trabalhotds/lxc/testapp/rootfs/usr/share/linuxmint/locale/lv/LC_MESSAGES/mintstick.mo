��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  $   �  .   �     !     4  &   O  	   v     �  "   �     �  4   �          %     A  ,   V  $   �  R   �     �          '     E     U  
   g     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-12-08 23:13+0000
Last-Translator: Arvis Lācis <Unknown>
Language-Team: Latvian <lv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Gadījās kļūda, kopējot attēlu. Gadījās kļūda, veidojot nodalījumu uz %s. Gadījās kļūda. Autentifikācijas kļūda: Izvēlieties USB zibatmiņas nosaukumu Formatēt Formatēt USB zibatmiņu Izveidot palaižamu USB zibatmiņu Izveidot palaižamu zibatmiņu USB zibatmiņā nav pietiekami daudz brīvas vietas. Izvēlieties attēlu Izvēlieties USB zibatmiņu Izvēlieties attēlu USB zibatmiņa tika veiksmīgi noformatēta. Attēls tika veiksmīgi ierakstīts. Šī darbība iznīcinās visus datus uz USB zibatmiņas, vai vēlaties turpināt? USB zibatmiņas rakstītājs USB zibatmiņa USB zibatmiņas formatētājs USB zibatmiņa: Sējuma etiķete: Ierakstīt 