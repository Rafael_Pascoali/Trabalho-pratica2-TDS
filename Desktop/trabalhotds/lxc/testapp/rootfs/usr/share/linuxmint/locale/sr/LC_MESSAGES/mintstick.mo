��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  U   Q  e   �  "   	  ,   0	  ;   ]	  �  �	     l  (   �  1   �  -   �  @   
     K  "   e     �  0   �  .   �  �     2   �     �  %   �     �     
     %     .                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-28 22:16+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <(nothing)>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: sr
 Дошло је до грешке приликом умножавања одраза. Дошло је до грешке приликом прављења партиције на „%s“. Дошло је до грешке. Грешка аутентификације. Изаберите назив за свој УСБ диск FAT32 
  + Компатибилан свуда.
  - Не може руковати датотекама већим од 4ГБ.

exFAT
  + Компатибилан готово свуда.
  + Може руковати датотекама већим од 4ГБ.
  - Није тако компатибилно са FAT32.

NTFS 
  + Компатибилан са Виндоузом.
  - Није компатибилан са Меком и већином хардверских уређаја.
  - Повремени проблеми са компатибилношћу са Линуксом (NTFS је власнички и направљен обрнутим инжењерингом).

EXT4 
  + Модеран, стабилан, брз, дневнички.
  + Подржава дозволе Линукс датотека.
  - Није компатибилан са Виндоузом, Меком и већином хардверских уређаја.
 Форматирај Форматирајте УСБ диск Направите подизни УСБ диск Направи подизни УСБ диск Нема довољно простора на УСБ диску. Изабери одраз Изаберите УСБ диск Изаберите одраз УСБ је успешно форматиран. Одраз је успешно записан. Ово ће уништити све податке на УСБ диску, да ли сигурно желите да наставите? Уписивач одраза на УСБ диск УСБ диск Форматер за УСБ диск УСБ диск: Назив уређаја: Пиши непознато 