��    %      D  5   l      @     A     P  .   m     �     �     �     �  :   �     !     )     A     `     h  $   �     �     �     �    �     �     �     �     �     �     �  8   �     .  
   K     V     s     �     �     �  /   �  6   �     	       �        �     �  /   �     +	     ;	     C	     _	  D   y	     �	     �	  $   �	  	   
  #   
  -   /
     ]
     x
     }
    �
     �     �     �  	   �     �     �  B   �          ,     @     Y     q     �     �  3   �  -   �     �                                  
                      %                         	      $                   !                             #                                "                  More info: %s Adding PPAs is not supported Adding private PPAs is not supported currently Backported packages Base CD-ROM (Installation Disc) Cannot add PPA: '%s'. Configure the sources for installable software and updates Country Edit the URL of the PPA Edit the URL of the repository Enabled Error: must run as root Error: need a repository as argument Fix MergeList problems GB/s Key Linux Mint uses Romeo to publish packages which are not tested. Once these packages are tested, they are then moved to the official repositories. Unless you are participating in beta-testing, you should not enable this repository. Are you sure you want to enable Romeo? MB/s Main Mirrors Open.. PPA PPAs Please enter the name of the repository you want to add: Purge residual configuration Repository Restore the default settings Select a mirror Software Sources Sources Speed The problem was fixed. Please reload the cache. There is no more residual configuration on the system. Unstable packages kB/s Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-07-27 22:23+0000
Last-Translator: Kenan Dervišević <6nm0uygdd@mozmail.com>
Language-Team: Bosnian <bs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
  Više informacija: %s Dodavanje PPA nije podržano Dodavanje privatnih PPA trenutno nije podržano Backport paketi Osnovno CD-ROM (Instalacijski disk) Ne mogu dodati PPA: '%s'. Podesite izvore za programe koje je moguće instalirati i ažurirati Država Izmjenite URL od PPA: Izmjenite URL od skladišta softvera Omogućen Greška: mora se pokrenuti kao root Greška: kao argument je potreban repozitorij Popravi MergeList probleme GB/s Ključ Linux Mint koristio Romeo za objavu paketa koji nisu testirani. Kada ovi paketi budu testirani, oni će biti pomjereni na oficijelne repozitorije. Ukoliko ne učestvujete u beta testiranju, ne bi trebali omogućiti ovaj repozitorij. Da li ste sigurni da želite omogućiti Romeo? MB/s Glavni Mirror serveri Otvori... PPA PPAs Molimo vas upišite naziv skladišta softvera koje želite dodati: Očisti zaostalu konfiguraciju Skladište softvera Vrati na zadane postavke Odaberite mirror-server Izvori softvera Izvorni kod Brzina Problem je otklonjen. Molimo ponovo učitajte keš. Nema više zaostalih konfiguracija na sistemu Nestabilni paketi kB/s 