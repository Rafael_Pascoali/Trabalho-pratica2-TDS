��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  0   0  /   a     �     �  *   �  J  �     6     =     Y     u  #   �     �     �     �  +   �       Y   .     �     �     �     �     �     �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-02-27 04:17+0000
Last-Translator: Arief Setiadi Wibowo <q_thrynx@yahoo.com>
Language-Team: Indonesian <id@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Kesalahan terjadi ketika sedang menyalin berkas. Terjadi masalah ketika membuat partisi pada %s. Terjadi sebuah kesalahan. Masalah Autentikasi. Pilih sebuah nama untuk perangkat USB Anda FAT32 
  + Kompatibel di manapun.
  -  Tidak dapat menangani berkas yang lebih dari 4GB.

exFAT
  + Kompatibel hampir di manapun.
  + Mampu menangani berkas lebih dari  4GB.
  -  Tidak se-kompatibel FAT32.

NTFS 
  + Kompatibel dengan Windows.
  -  Tidak kompatibel dengan Mac dan hampir seluruh piranti perangkat keras.
  -  Kadang bermasalah kompatibel dengan Linux (NTFS adalah proprietary dan telah direkayasa balik).

EXT4 
  + Modern, stabil, cepat, terjurnal.
  + Mendukung  Linux file permissions.
  -  Tidak kompatibel dengan Windows, Mac hampir semua piranti perangkat keras.
 Format Format sebuah perangkat USB Membuat sebuah bootable USB Buat sebuah bootable USB Tak cukup ruang pada perangkat USB. Pilih Citra Pilih sebuah perangkat USB Pilih sebuah citra Perangkat USB sudah diformat dengan sukses. Citra sukses ditulis. Ini akan menghapus semua data pada perangkat USB, apakah kamu yakin ingin melanjutkannya? Penulis Citra USB Perangkat USB Pemormat Perangkat USB Perangkat USB: Label volume: Tulis tidak dikenal 