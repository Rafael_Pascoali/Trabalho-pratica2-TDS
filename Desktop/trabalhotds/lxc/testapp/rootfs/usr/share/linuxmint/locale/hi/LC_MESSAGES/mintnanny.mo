��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  C   �  i   .  %   �  "   �     �    �  (   �  "   (  .   K  c   z                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-03-31 13:51+0000
Last-Translator: Panwar <Unknown>
Language-Team: Hindi <hi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s मान्य डोमेन नाम नहीं है। चयनित डोमेन नाम हेतु अभिगम अवरुद्ध करें अवरुद्ध डोमेन डोमेन अवरोधक डोमेन नाम डोमेन नाम का आरंभ व समाप्ति अक्षर या अंक से एवं इसमें केवल अक्षर, अंक, बिंदु व हायफ़न ही प्रयुक्त हो। उदाहरण: my.number1domain.com अमान्य डोमेन अभिभावक नियंत्रण अवरुद्ध करने हेतु डोमेन नाम दर्ज करें 