��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �  3        O     g  	   v  }   �      �          -  *   >                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-10-28 14:03+0000
Last-Translator: Rhoslyn Prys <Unknown>
Language-Team: Welsh <cy@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: cy
 Nid yw %s yn enw parth dilys. Rhwystro mynediad i'r enwau parth sydd wedi'u dewis Parthau wedi'u rhwystro Rhwystro Parth Enw parth Rhaid i enw parth gychwyn a gorffen gyda llythyren neu rif a gall gynnwys dim ond llythrennau, rhifau, dotiau a chyplysnodau. Enghraifft: fy.mharthrhif1.cymru Parth Annilys Rheoli gan Rieni Teipiwch yr enw parth rydych am ei rwystro 