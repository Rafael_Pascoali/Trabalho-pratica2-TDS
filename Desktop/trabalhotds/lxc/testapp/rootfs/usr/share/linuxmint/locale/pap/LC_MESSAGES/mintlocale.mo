��          �   %   �      p  w   q     �     �               (     8  �   E     �     �       ^   /     �     �     �     �     �     �  	               ,   "     O     V     ]     z     �  �  �  u   k     �     �  	             3     I  �   X     		     	     *	  c   F	     �	     �	     �	     �	      
     
  	   &
     0
     B
  &   G
     n
     u
     z
  *   �
     �
                                           	                                  
                                              <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Some language packs are missing System locale Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-24 21:05+0000
Last-Translator: Frank <f.boye@hotmail.nl>
Language-Team: Papiamento <pap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>Note: Instalando of ora bo ke upgrade un pakete di idioma por kausa e instalashon di mas idioma</small></b> Añadi un Idioma Nobo Añadi un idioma nobo Añadi... Aplik'e na henter e sistema Kompletamente instala Metodo di yena Metodonan di yena ta wòrdu usa pa skibi simbolonan i karakternan kua no ta presente riba e keyboard. Nan ta práktiko ora di skibi Chines, Hapones, Koreano, Thai, Viatnames... Instala Instala / Kita Idiomanan Instala / Kita Idiomanan... Instala kualke pakete di idioma, dokumento pa tradukshon, dikshonarionan ku falta pa e idioma skohi Intala paketenan di idioma Instala e idioma selekta Idioma Konfigurashonnan di Idioma Opshonnan pa e idioma Idioma sostené Idiomanan Niun luga definí Niun Numbernan, valuta, adrèsnan, midí... Region Kita Kita e idioma selektá Algun pakete di idioma no por wòrdu haña Luga di e sistema 