��    O      �  k         �     �     �     �     �     �  )        <     B     V     j     o     �     �     �     �  !   �  :   �       	         *     E     N     S     k     s     �     �     �     �     �     �     �     �     �     �      	  (   	     1	     4	     =	     S	     X	     _	     p	     t	     y	  ^   �	     �	  +   �	     %
     ,
  
   D
     O
     j
     }
  
   �
     �
     �
     �
     �
     �
     �
  :   �
  V   6  R   �     �  U   �     S     _     q  	   y  '   �  *   �     �     �               #  �  @     �     �     �  !        *  1   =     o     x     �     �     �     �     �  #   �  
   	  1     4   F     {     �     �  	   �  	   �     �     �  "   �           #     2     7  	   @     J     \     b     t     y     �  /   �     �     �     �     �  
   �     �                 U        i  2   �     �     �     �     �     �          +     <     O     b     z  	   �  	   �  ?   �  O   �  E   <     �  M   �     �     �                    8  !   V     x  !   �     �     �     /                 	   &   4         7   H           J       3               6   9       A      *   8   '   =   ,      0       "      )             L      $         2           B   C   D          #   E   
   @   1   G                            M   .   O   :   <       !       %      ;      ?   I   >   (          F   +                                   5                              -   K   N     More info: %s Add Add missing keys Adding PPAs is not supported Additional Repositories All missing keys were successfully added. Apply Authentication Keys Backported packages Base CD-ROM (Installation Disc) Cancel Cancelling... Cannot add PPA: '%s'. Clear Click OK to update your APT cache Configure the sources for installable software and updates Country Downgrade Downgrade foreign packages Download Edit Edit the URL of the PPA Enabled Failed to download the PPA: %s. Fix MergeList problems Foreign packages GB/s Import Install Installed version Key Local Repository MB/s Main Mirrors No supported PPA of this name was found. OK Obsolete Official Repositories Open Open.. Optional Sources PPA PPAs Package Please enter the fingerprint of the public key you want to download from keyserver.ubuntu.com: Please reload the cache. Press Enter to continue or Ctrl+C to cancel Remove Remove foreign packages Repository Repository already exists. Repository version Restore the default settings Select All Select Mirror Select a mirror Software Sources Source code repositories Sources Speed The name of the PPA you entered isn't formatted correctly. The packages below are installed on your computer but not present in the repositories: The version of the following packages doesn't match the one from the repositories: This PPA does not support %s This PPA provides the following packages. Please select the ones you want to install. Unreachable Unstable packages Warning Worldwide You are about to add the following PPA: You are about to remove the following PPA: Your configuration changed already installed failed to remove PPA: '%s' kB/s version %s already installed Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-01-04 21:15+0000
Last-Translator: Silvara <Unknown>
Language-Team: Interlingue <ie@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
  Plu information: %s Adjunter Adjunter mancant claves Adjuntion de PPAs es ínsupportat Extra repositorias Omni mancant claves esset adjuntet successosimen. Applicar Claves de autentification Retroportat paccages Basal CD-ROM (Disco de installation) Anullar Interruptente... Ne successat adjunter un PPA: '%s'. Deselecter Fa un clic sur OK por actualisar vor cache de APT Configurar li fontes de programmas e actualisamentes País Desactualisar Desactualisar foren paccages Descargar Modificar Modificar li URL del PPA Activ Ne successat descargar li PPA: %s. Correcter problemas de MergeList Foren paccages Go/s Importar Installar Installat version Clave Local repositoria Mo/s Primari Spegules Null supportat PPA con ti nómine esset trovat. OK Obsolet Oficial repositorias Aperter Aperter... Orígines facultativ PPA PPAs Paccage Ples provider li fingre-print del public clave por descargar de keyserver.ubuntu.com: Ples actualisar li cache. Tippa Enter por continuar o Ctrl+C por interrupter Remover Remover foren paccages Repositoria Li repositoria ja existe. Version del repositoria Restituer li predefinit Selecter omnicos Selecter un spegul Selecter un spegul Orígines de programmas Repositorias de code de fonte Orígines Rapiditá Li nómine del PPA quel vu ha providet ne have un just formate. Li paccages ad infra es installat sur vor computator ma manca del repositorias: Li version del sequent paccages ne corresponde a ti del repositorias: Ti PPA ne supporta %s Ti PPA provide li sequent paccages. Ples selecter tis quel vu vole installar. Ínatingibil Ínstabil paccages Avise Li tot munde Vu va adjunter li sequent PPA: Vu va remover li sequent PPA: Vor configuration esset modificat ja es installat ne successat remover un PPA: '%s' ko/s li version %s ja es installat 