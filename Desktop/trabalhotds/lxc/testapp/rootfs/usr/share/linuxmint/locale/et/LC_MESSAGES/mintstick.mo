��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  "   �  .   �     �     �     �       	             ,     A     W  <   q     �  �  �     �     �     �     �     �     �               #     5     B     P  <   U     �     �  !   �     �     �     �          '     5      N     o     |     �  
   �     �     �     �     �     �     �  5      A   6  *   x  '   �     �  E   �      (  6   I      �  $   �  :   �  >     C   @  +   �     �  W   �     $     +     B     K  	   `     j     {  	   �  4   �     �  
   �  4   �     $     ,     2     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-31 16:15+0000
Last-Translator: mahfiaz <mahfiaz@gmail.com>
Language-Team: Estonian <et@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Tõmmise kopeerimisel ilmnes viga. Partitsiooni loomisel asukohta %s tekkis viga. Esines viga Tekkis viga. Autentimise viga. Arvutamine... Kontrolli Kontrollsumma Kontrollsumma failid Kontrollsumma on vale Nime valimine USB pulgale Laadi ISO tõmmis uuesti talla. Selle kontrollsumma on vale. Kõik paistab korras! FAT32 
  + Töötab igal pool.
  -  Failid ei saa olla suuremad kui 4GB.

exFAT
  + Töötab peaaegu igal pool.
  + Failid võivad olla suuremad kui 4GB.
  -  Pole siiski nii ühilduv kui FAT32.

NTFS 
  + Töötab Windowsis.
  -  Ei tööta Macis ja enamikus seadmetes.
  -  Vahel esineb probleeme ka Linuxis (NTFS on omanduslik ja tagurpidi avastatud).

EXT4 
  + Kaasaegne, stabiilne, kiire, žurnaaliv.
  + Toetab Linuxi failiõiguseid.
  -  Ei ühildu Windowsi, Maci ja enamiku seadmetega.
 Failisüsteem: Vorminda USB pulga vormindamine GB GPG allkirjad GPG allkirjafail GPG allkirjafail: Mine tagasi ISO kontrollimine ISO tõmmis: ISO tõmmised ISO: Kui sa usaldad allkirja, siis võid usaldada seda ISO faili. Terviklikkuse kontroll nurjus KB Võtit ei leitud võtmeserverist. Kohalikud failid MB Alglaaditava USB pulga loomine Alglaaditava USB pulga loomine Rohkem teavet Andmemahu ID-d ei leitud USB pulgal pole piisavalt ruumi. SHA256 summa SHA256 summade fail SHA256 summade fail: SHA256sum: Tõmmise valimine USB pulga valimine Vali tõmmis Allkirjastanud: %s Suurus: TB Selle ISO tõmmise SHA256 kontrollsumma ei ole õige. SHA256 summade fail ei sisalda selle ISO tõmmise kontrollsummat. SHA256 summade fail ei ole allkirjastatud. USB pulga vormindamine lõppes edukalt. Kontrollsumma on õige Kontrollsumma on õige, kuid kontrollsumma autentsus pole tõendatud. GPG faili ei saanud kontrollida. GPG faili ei õnnestunud alla laadida. Kontrolli URLi. Tõmmise kirjutamine oli edukas. Summade faili ei saanud kontrollida. Summade faili ei õnnestunud alla laadida. Kontrolli URLi. See ISO tõmmis on kontrollitud usaldusväärse allkirja abil. See ISO tõmmis on kontrollitud mitteusaldusväärse allkirja abil. See ISO näeb välja nagu Windowsi tõmmis. See on ametlik ISO tõmmis. Seda tehes kustutatakse kõik USB-l olevad andmed. Kas oled kindel, et soovid jätkata? URL-id USB tõmmise kirjutaja USB pulk USB pulga vormindaja USB pulk: Tundmatu allkiri Ebausaldusväärne allkiri Kontrolli Kontrolli selle tõmmise autentsust ja terviklikkust Ketta nimetus: Andmemaht: Windowsi tõmmistele on vajalik erinev töötlemine. Kirjuta baiti tundmatu 