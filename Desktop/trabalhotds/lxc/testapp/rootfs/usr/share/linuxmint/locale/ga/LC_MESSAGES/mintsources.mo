��          �   %   �      P     Q     V  :   l     �     �     �     �     �          	                    !  8   &     _  
   |     �     �     �     �     �  /   �  6        :  �  L  
   �  -     R   5     �     �      �  
   �     �     �     �  
   
  	             #  E   (  %   n  	   �  &   �     �     �     �     �  8   �  1   2     d                                                                                                            
          	       Base Cannot add PPA: '%s'. Configure the sources for installable software and updates Country Edit the URL of the PPA Edit the URL of the repository Enabled Fix MergeList problems Key Main Mirrors Open.. PPA PPAs Please enter the name of the repository you want to add: Purge residual configuration Repository Restore the default settings Select a mirror Software Sources Sources Speed The problem was fixed. Please reload the cache. There is no more residual configuration on the system. Unstable packages Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-05-02 19:24+0000
Last-Translator: Gearóid  Ó Maelearcaidh <gearoid@omaelearcaidh.com>
Language-Team: Irish <ga@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Bun chlár Ní féidir PPA: '%s' a chur leis an gcóras. Cumraigh na foinsí chun teacht ar bhogearraí gur féidir suiteáil is nuashonrú Tír Cuir URL den PPA in eagar Cuir in eagar URL na taisclainne Cumasaithe Réitigh fadhbanna le MergeList Eochair Príomh chlár Scátháin Oscail... PPA PPAs Iontráil ainm na taisclainne gur mhaith leat a chur leis an gcóras: Glan amach an chumraíocht iarmharach Taisclann Aischuir na socruithe réamhshocraithe Roghnaigh scáthán Foinsí Bogearraí Foinsí Luas Réitíodh an fhadhb. Athluchtaigh an tasice le d'thoil. Níl breis socruithe réamhshocraithe sa chóras. pacáistí éagobhsaí 