��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  '   �  *         K     `     x  �   �          )     <  2   N                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-04-14 13:50+0000
Last-Translator: Sérgio Marques <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s não é um nome de domínio válido. Bloquear acesso aos domínios selecionados Domínios bloqueados Bloqueador de domínios Nome do domínio Os nomes de domínios devem começar e acabar com uma letra ou dígito e apenas podem conter letras, dígitos, pontos e hífenes. Exemplo: my.number1domain.com Domínio inválido Controlo parental Introduza o nome do domínio que pretende bloquear 