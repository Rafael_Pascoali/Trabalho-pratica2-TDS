��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �       i   �  %   	  5   ,	  g   b	    �	  "   �  F   
  M   Q  @   �  e   �  2   F  I   y  ?   �  x     I   |  �   �  #   �     �  /   �     �           &     @                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-08-04 16:10+0000
Last-Translator: Pratyay Mondal <Unknown>
Language-Team: Bengali <bn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ছবিটির অনুলিপি করার সময় একটি ত্রুটি হয়ে গেছে। %s এ বিভক্ত করার সময় একটি ত্রুটি হয়েছে। ত্রুটি হয়েছে। অনুমতি যনিত ত্রুটি। আপনার USB স্টিকের জন্য একটি নাম চয়ন করুন FAT32
   + সর্বত্র সামঞ্জস্যপূর্ণ।
   - 4GB-র চেয়ে বড় ফাইলগুলি পরিচালনা করতে পারে না।

exFAT
   + প্রায় সর্বত্রই সামঞ্জস্যপূর্ণ।
   + 4GB-র চেয়ে বড় ফাইলগুলি পরিচালনা করতে পারে।
   - FAT32 এর মতো উপযুক্ত নয়।

NTFS
   - Windows সাথে সামঞ্জস্যপূর্ণ।
   - Mac এবং বেশিরভাগ হার্ডওয়্যার ডিভাইসের সাথে উপযুক্ত নয়।
   - Linux সাথে মাঝে মধ্যে সামঞ্জস্যতা সমস্যা (NTFS মালিকানাযুক্ত এবং বিপরীত ইঞ্জিনিয়ার্ড)।

EXT4
   + আধুনিক, স্থিতিশীল, দ্রুত, সাংবাদিকিত।
   + Linux ফাইল পার্মিশন্স সমর্থন দেয়।
   - Windows, Mac এবং বেশিরভাগ হার্ডওয়্যার ডিভাইসের সাথে উপযুক্ত নয়।
 ফরম্যাট করুন একটি USB স্টিক ফর্ম্যাট করুন একটি বুটেবল USB স্টিক তৈরি করুন বুটেবল USB স্টিক তৈরি করুন USB স্টিকটিতে পর্যাপ্ত যায়গা খালি নেই। ইমেজ নির্বাচন করুন একটি USB স্টিক নির্বাচন করুন। একটি ইমেজ নির্বাচন করুন USB স্টিকটির সকল তথ্য সফলভাবে মুছেফেলা হয়েছে। ইমেজটি সফল ভাবে লেখা হয়েছে। এটি USB স্টিকের সমস্ত ডেটা ধংস করে দিবে, আপনি কি নিশ্চিত যে এই কাজটি করতে চান? USB ইমেজ রাইটার USB স্টিক USB স্টিক ফরম্যাটার USB স্টিক ভলিউম লেবেল: রাইট করুন অপরিচিত 