��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  !   �  2        M     b     w  q   �     �       	   !  /   +                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-02-25 08:17+0000
Last-Translator: mahfiaz <mahfiaz@gmail.com>
Language-Team: Estonian <et@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s  ei ole korrektne domeeninimi. Juurdepääsu blokeerimine valitud domeeninimedele Blokeeritud domeenid Domeenide blokeerija Doomeninimi Domeeninimed peavad algama tähe või numbriga, võivad sisaldada ainult tähti, numbreid, punkte ja sidekriipse. Näide: minu.number1domeen.ee Vigane domeen Lapselukk Palun trüki domeeninimi, mida tahad blokeerida 