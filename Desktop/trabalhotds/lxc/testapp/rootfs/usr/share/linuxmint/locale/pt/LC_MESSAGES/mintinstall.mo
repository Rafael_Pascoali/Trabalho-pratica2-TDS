��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  J   e  G   �  +   �  )   $     N  &   g     �     �     �  	   �     �     �     �  ;   �     %     8     >  {   Y     �     �  ;   �  )   0     Z     c     r     z  
   �     �     �  
   �  
   �     �     �     �  i   �  R   ^     �     �  #   �  	   �     �            	   )     3  
   H  D   S     �     �     �  "   �     �     �  $   �       !   "  W   D  &   �     �     �     �     �            
     &     *   A     l     |     �     �  
   �     �     �     �  	   �  C   �  	   /     9     I     R  4   h  &   �  1   �     �          -     6  .   J     y     }     �     �  @   �  E   �     :     W     q     �  h   �                         .     2     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-02 14:21+0000
Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 A transferir %(downloadSize)s, %(localSize)s de espaço libertado no disco A transferir %(downloadSize)s, requer %(localSize)s de espaço em disco Lbertados %(localSize)s de espaço em disco Requer %(localSize)s  de espaço em disco %d análise %d análises %d tarefa em curso %d tarefas em curso 3D Acerca Acessórios Adicionar Tudo Todas as aplicações Todas as operações terminadas Ocorreu um erro ao tentar adicionar o repositório Flatpak. Jogos de tabuleiro Ramo: Não é possível instalar Não é possível processar este ficheiro se existirem operações ativas.
Por favor, tente novamente assim que terminarem. Não é possível remover Chat Clique <a href='%s'>aqui</a> para adicionar a sua análise. Estamos a trabalhar nos seguintes pacotes Detalhes Documentação Desenho Escolhas do editor Educação Eletrónica E-mail Emuladores Essenciais Partilha de ficheiros Primeira pessoa Flatpak (%s) Atualmente, o suporte do Flatpak não está disponível. Tente instalar 'flatpak' e 'gir1.2-flatpak-1.0'. Atualmente, o suporte do Flatpak não está disponível. Tente instalar 'flatpak'. Tipos de letra Jogos A gerar a cache. Aguarde um momento Gráficos Página inicial Instalar Instalar novas aplicações Instalado Programas instalados A instalar Instalar este pacote pode causar danos irreparáveis ao seu sistema. Internet Java Iniciar Limitar pesquisa à listagem atual Matemática Codificadores multimédia Codificadores multimédia para o KDE Nome: Não existem pacotes coincidentes Não há pacotes para mostrar.
Podem existir problemas. Experimente recarregar a cache. Não há imagens de ecrã disponíveis Indisponível Não instalado Produtividade Componentes opcionais PHP Pacote Fotografia Aguarde um pouco. Isto pode demorar... Utilize apt-get para instalar este pacote. Desenvolvimento Publicação Python Estratégia em tempo real Recarregar Recarregar lista de pacotes Remoto: Remover A remover Remover este pacote pode causar danos irreparáveis ao seu sistema. Análises Digitalização Ciência Ciência e educação Procurar na descrição do pacote (ainda mais lento) Procurar no resumo dos pacotes (lento) A pesquisar nos repositórios, aguarde um momento Mostrar aplicações instaladas Simulação e corridas Tamanho: Gestor de programas Algo correu mal. Clique para tentar novamente. Som Som e vídeo Pacote do sistema Ferramentas do sistema O repositório Flatpak que está  a tentar adicionar já existe. Ainda existem operações em curso.
Tem a certeza de que deseja sair? Isto é um pacote do sistema Este pacote é um Flatpak Tente novamente Estratégia por turnos Não foi possível comunicar com os servidores. Verifique a sua ligação à Internet e tente novamente. Indisponível Versão: Vídeo Visualizadores Web O gestor de pacotes do sistema 