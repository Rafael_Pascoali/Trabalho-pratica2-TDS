��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  #   �  2   �          3     I     c     r     �     �  )   �     �             4     %   S  q   y     �  	         
  
   *     5     B     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-10-06 01:38+0000
Last-Translator: Saki <Unknown>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Có lỗi xảy ra khi chép ảnh. Có lỗi xảy ra khi tạo phân vùng trên %s. Có lỗi xảy ra. Lỗi Chứng thực. Chọn tên cho thanh USB Định dạng Định dạng đĩa USB Tạo đĩa USB khởi động Tạo đĩa USB khởi động Không đủ không gian trên thanh USB. Chọn Ảnh Chọn thanh USB Chọn ảnh Thanh USB đã được định dạng thành công. Ảnh đã được ghi thành công. Thao tác này sẽ huỷ mọi dữ liệu trên thanh USB, bạn có chắc chắn muốn tiến hành không ? Trình Ghi Ảnh USB Thanh USB Trình Định dạng Thanh USB Thanh USB: Nhãn đĩa: Ghi 