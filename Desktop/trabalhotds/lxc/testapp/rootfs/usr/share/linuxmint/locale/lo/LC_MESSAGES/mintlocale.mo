��          �      |      �  w   �     i     |     �     �     �  �   �     \     d          �     �     �     �  	   �     �     �     �          
     *  �  8  �   �  '   �  '   �       ?   +  <   k  ?  �     �  B   �  B   A	     �	  !   �	  !   �	  3   �	     	
  <   
     S
     c
     y
  $   �
  0   �
                 
                                                                      	          <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install the selected language Language Language Settings Language settings Language support Languages No locale defined None Region Remove Some language packs are missing System locale Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-07-30 14:01+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Lao <lo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>ໝາຍເຫດ: ຕິດຕັ້ງ ຫລື ລຶບພາສາ ອາດຈະມີບັນຫາກັບການຕິດຕັ້ງພາສາຫຼາຍໆພາສາ</small></b> ເພີ່ມພາສາໃຫມ່ ເພີ່ມພາສາໃຫມ່ ເພີ່ມ... ໄດ້ຕິດຕັ້ງຢ່າງເຕັມຕົວ ທາງເລືອກການໃສ່ຂໍ້ມູນ ທາງເລືອກການໃສ່ຂໍ້ມູນ ໃຊ້ໃນການຂຽນສັນຍາລັກ ແລະຕົວໜັງສື ທີ່ບໍ່ມີໃນຄີບອດ. ໃຊ້ຂຽນພາສາຈີນ, ຍີ່ປຸ່ນ, ເກົາຫລີ, ໄທ, ຫວຽດນາມ... ຕິດຕັ້ງ ຕິດຕັ້ງຫຼືລຶບພາສາອອກໄປ ຕິດຕັ້ງພາສາໄດ້ເລືອກໄວ້ ພາສາ ຕັ້ງຄ່າພາສາ ຕັ້ງຄ່າພາສາ ການສະໜັບສະໜູນພາສາ ພາສາ ບໍໄດ້ຕັ້ງຄ່າບ່ອນຕັ້ງ ບໍ່ມີ ພາກພື້ນ ຍ້າຍອອກ ບາງພາສາຫາຍໄປ ບ່ອນຕັ້ງຂອງລະບົບ 