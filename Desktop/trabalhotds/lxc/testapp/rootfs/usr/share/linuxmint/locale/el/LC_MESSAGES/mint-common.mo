��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  =   &  ;   d  (   �  6   �  A      A   B  A   �  V   �  E   	  A   c	      �	  �   �	     R
     g
     o
     x
  a   �
  -   �
  n        �     �     �  )   �  =   �  �   ;  9        F                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-01 09:04+0000
Last-Translator: Vasilis Kosmidis <Unknown>
Language-Team: Greek <el@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s χώρου δίσκου θα χρησιμοποιηθεί. %s χώρου δίσκου θα απελευθερωθεί. %s θα ληφθούν συνολικά. Απαιτούνται επιπλέον αλλαγές Πρόσθετο λογισμικό θα υποβαθμιστεί Θα εγκατασταθεί πρόσθετο λογισμικό Πρόσθετο λογισμικό θα εκκαθαριστεί Θα γίνει επανεγκατάσταση πρόσθετου λογισμικού Θα απεγκατασταθεί πρόσθετο λογισμικό Πρόσθετο λογισμικό θα αναβαθμιστεί Συνέβη ένα σφάλμα Δεν είναι δυνατή η κατάργηση του πακέτου %s καθώς απαιτείται από άλλα πακέτα. Υποβάθμιση Flatpak Flatpaks Εγκατάσταση Το πακέτο %s είναι μια εξάρτηση των ακόλουθων πακέτων: Πακέτα που θα αφαιρεθούν Παρακαλώ ρίξτε μια ματιά στην λίστα με τις αλλαγές παρακάτω. Εκκαθάριση Επανεγκατάσταση Αφαίρεση Παράλειψη αναβάθμισης Θα αφαιρεθούν τα παρακάτω πακέτα: Αυτό το στοιχείο μενού δεν είναι συσχετισμένο με κανένα πακέτο. Θέλετε παρ' όλα αυτά να το αφαιρέσετε από το μενού; Οι ενημερώσεις θα παραλειφθούν Αναβάθμιση 