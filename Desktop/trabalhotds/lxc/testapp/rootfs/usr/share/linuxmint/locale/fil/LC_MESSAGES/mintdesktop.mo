��    (      \  5   �      p     q     �     �     �     �     �  -   �     �     
  
          
      \   +  \   �  	   �     �  
   �                !     (     0  2   8  0   k     �     �     �     �     �  	   �     �               "     >     [     j     r     �  �  �     .     J  	   d     n  !   |     �  8   �  <   �     .	     6	     D	     M	  f   ]	  f   �	     +
     9
  
   @
     K
     a
     z
     �
     �
  ?   �
  ;   �
  $         2     S     Z     u     �     �     �     �  +   �  :   �     1  
   J     U  r   h     #   (   !                                          "         
              '                        	                              %      $                         &               Buttons labels: Buttons layout: Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only If Compton is not installed already, you can install it with <cmd>apt install compton</cmd>. If Openbox is not installed already, you can install it with <cmd>apt install openbox</cmd>. Interface Large Linux Mint Mac style (Left) Mounted Volumes Mutter Network Openbox Openbox is a fast and light-weight window manager. Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Welcome to Desktop Settings. Window Manager Windows root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-09 04:12+0000
Last-Translator: Miguel Angelo <Unknown>
Language-Team: Filipino <fil@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Label para sa mga pindutan: Kaayusan ng mga pindutan: Kompyuter Pook ng Likha Mga setting para sa pook ng likha Mga icon sa pook ng likha Huwag ipakita ang laman ng window habang idina-drag sila I-ayon sa pinakatamang ayos ang mga setting ng pook ng likha Tahanan Laki ng icon: Mga Icon Mga icon lamang Kung hindi pa na-install ang Compton, maaari mo siyang gawin gamit ang <cmd>apt install compton</cmd>. Kung hindi pa na-install ang Openbox, maaari mo siyang gawin gamit ang <cmd>apt install compton</cmd>. Ang interface Malaki Linux Mint Style ng Mac (Kaliwa) Naka-mount na mga volume Mutter Network Openbox Ang Openbox ay window manager na mabilis at magaan sa kompyuter Piliin ang mga bagay na gusto mong makita sa pook ng likha: Ipakita ang mga icon sa mga pindutan Ipakita ang mga icon sa mga menu Maliit Sulat sa ibaba ng mga item Sulat sa tabi ng mga item Teksto lamang Toolbars Nakasanayang style (Kanan) Buntunan Gamitin ang font ng system para sa titlebar Maligayang pagdating sa mga setting para sa pook ng likha. Tagapangasiwa ng Bintana Mga Window root@linuxmint.com Launchpad Contributions:
  Miguel Angelo https://launchpad.net/~mangelus
  Ueliton https://launchpad.net/~ueli-ton 