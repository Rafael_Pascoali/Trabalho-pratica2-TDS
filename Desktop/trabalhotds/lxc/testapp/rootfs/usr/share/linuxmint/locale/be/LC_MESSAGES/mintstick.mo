��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  O   �  W   �     ,     L  ,   k     �     �     �  /   �  3   	  9   =  q   w  #   �  ]        k     �  )   �     �     �     �        
   &     1     L     ^     q  t   v  @   �     ,  5   1     g     �  :   �  :   �        ;     >   S     �     �     �  
   �     �  #   �           <     M     Y  ?   ^  a   �  1      A   2  4   t  {   �  4   %  W   Z  1   �  9   �  s     U   �  Y   �  C   B  +   �  k   �        "   $      G   +   Z      �      �   !   �      �   T   �      D!     Y!  T   p!     �!     �!     �!     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-06 18:39+0000
Last-Translator: Anton Hryb <Unknown>
Language-Team: Belarusian <be@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Узнікла памылка падчас капіявання вобраза. Узнікла памылка падчас стварэння раздзела на %s. Адбылася памылка Узнікла памылка. Памылка аўтэнтыфікацыі. Падлік... Праверка Кантрольная сума Файлы з кантрольнай сумай Кантрольная сума не супадае Задайце назву вашай USB-прыладзе Паўторна спампуйце вобраз ISO. Яго кантрольная сума не супадае. Усё выглядае добра! FAT32 
  + Сумяшчальна ўсюды.
  -  Немагчыма апрацоўваць файлы, большыя за 4GB.

exFAT
  + Сумяшчальна амаль ўсюды.
  + Можа апрацоўваць файлы, большыя за  4GB.
  -  Сумяшчальна менш за FAT32.

NTFS 
  + Сумяшчальна з Windows.
  -  Несумяшчальна з Mac і большасцю апаратных прылад.
  -  Выпадковыя праблемы з сумяшчальнасцю з Linux (NTFS прапрыетарны і са зваротнай распрацоўкай).

EXT4 
  + Сучасны, стабільны, хуткі, узаконены.
  + Падтрымка дазволаў файлаў Linux.
  - Не сумяшчальны з Windows, Mac і большасцю апаратных прылад.
 Файлавая сістэма: Фарматаваць Фарматаваць USB-прыладу ГБ Подпісы GPG Файл падпісаны GPG Файл падпісаны GPG: Назад Верыфікацыя ISO Вобраз ISO: Вобразы ISO ISO: Калі вы давяраеце гэтаму подпісу, то можаце давяраць вобразу ISO. Праверка цэласнасці не атрымалася. КБ Ключ не знойдзены на серверы. Лакальныя файлы МБ Зрабіць запускальную USB-прыладу Зрабіць запускальную USB-прыладу Падрабязней Не знойдзены ідэнтыфікатар тома На USB-прыладзе недастаткова месца. Сума SHA256 Файл сумы SHA256 Файл сумы SHA256: SHA256sum: Выбраць вобраз Выберыце USB-прыладу Выбраць вобраз Подпіс: %s Памер: ТБ У вобраза ISO няправільная сума SHA256. Файл сумы SHA256 не ўтрымлівае сум для гэтага вобраза ISO. Файл сумы SHA256 не падпісаны. USB-прылада паспяхова адфарматавана. Кантрольная сума правільная Кантрольная сума правільная, але сапраўднасць сумы не пацверджана. Не ўдалося праверыць файл gpg. Не ўдалося спампаваць файл gpg. Праверце URL адрас. Вобраз паспяхова запісаны. Не ўдалося праверыць файл сумы. Не ўдалося спампаваць файл кантрольнай сумы. Праверце URL адрас. Гэты вобраз ISO пацверджаны давераным подпісам. Гэты вобраз ISO пацверджаны недавераным подпісам. Гэты вобраз ISO падобны на вобраз Windows. Гэта афіцыйны вобраз ISO. Гэта знішчыць усе даныя на UBS-прыладзе. Жадаеце працягнуць? URLы Запіс вобраза на USB USB-прылада Фарматаванне USB-прылады USB-прылада: Невядомы подпіс Недавераны подпіс Спраўдзіць Спраўдзіць сапраўднасць і цэласнасць вобраза Ярлык тома: Дыскавы том: Вобразы Windows патрабуюць адмысловай апрацоўкі. Запісаць байтаў невядома 