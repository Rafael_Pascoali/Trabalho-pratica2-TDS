��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  "   /  %   R     x  $   �  1   �  "   �        &   1     X  (   w     �  8   �     �     	  
   	  	   	  /   %	     U	      l	     �	     �	     �	     �	      �	  S   �	     8
     T
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-11 18:54+0000
Last-Translator: Asier Iturralde Sarasola <Unknown>
Language-Team: Basque <eu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Diskoan %s gehiago erabiliko dira. Diskoan %s gehiago egongo dira libre. %s deskargatuko dira guztira. Aldaketa gehigarriak egin behar dira Software gehigarria aurreko bertsiora itzuliko da Software gehigarria instalatuko da Software gehigarria garbituko da Software gehigarria berrinstalatuko da Software gehigarria kenduko da Software gehigarria bertsio-berrituko da Errore bat gertatu da Ezin da %s paketea kendu, beste paketeek behar dutelako. Itzuli aurreko bertsiora Flatpak Flatpak-ak Instalatu %s paketea ondorengo paketeen mendekotasuna da: Kenduko diren paketeak Ikusi aldaketen zerrenda azpian. Garbitu Berrinstalatu Kendu Saltatu bertsio-berritzea Ondorengo paketeak kenduko dira: Menuko elementu hau ez dago pakete bati lotuta. Menutik ezabatu nahi duzu dena den? Eguneraketak saltatuko dira Bertsio-berritu 