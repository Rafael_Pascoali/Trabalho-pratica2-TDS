��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �  (   u      �     �  "   �                       &   ;     b  !   h  d   �  
   �                            	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-08 07:45+0000
Last-Translator: Arief Setiadi Wibowo <q_thrynx@yahoo.com>
Language-Team: Indonesian <id@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s lebih banyak ruang disk akan dipakai. %s ruang cakram akan dibebaskan. %s akan diunduh secara total. Perubahan tambahan yang diperlukan Terjadi galat Flatpak Pasang Paket-paket yang akan dihapus Harap lihat daftar perubahan di bawah. Hapus Paket-paket berikut akan dihapus: Item menu ini tidak berhubungan dengan paket manapun. Apakah anda ingin menghapusnya saja dari menu? Tingkatkan 