��    "      ,  /   <      �  )   �  2   #     V     h     {     �     �     �     �     �     �  
   �     �                 "   %     H     V  -   Y  #   �  )   �     �  I   �  "   7  #   Z  2   ~  5   �     �               ,     2  �  :  L   �  D        b     {     �     �  $   �  0   �  $   	     8	     =	     T	  *   i	     �	     �	  9   �	  :   �	     
     *
  4   .
  5   c
  =   �
  "   �
  a   �
  ;   \  8   �  U   �  Y   '  /   �     �  !   �     �     �              
                    !                                                                             "                 	                              An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Checksum files Checksum mismatch Everything looks good! GB GPG signatures ISO images Integrity check failed KB MB No volume ID found Not enough space on the USB stick. Signed by: %s TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The image was successfully written. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This is an official ISO image. Unknown signature Untrusted signature bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-13 18:46+0000
Last-Translator: Vondrosh Tenkač <Unknown>
Language-Team: Rusyn <rue@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Стала ся хыба при копірованьови образика. При учинені оддїла на %s стала ся хыба. Хыба ся стала Стала ся хыба. Хыба входа Учисленя... Файлы фіналної сумы Несовпаданя фіналної сумы Вшытко иде за шором! ҐБ GPG сіґнатуры ISO образикы Хыба перевіркы цїлости КБ МБ Ідентіфікатор тома не найденый Не є доста міста на USB-зберачови. Подписано: %s TБ Сума SHA256 ISO образика недобра. Сумы файлув SHA256 не подписані. USB-зберач одформатованый успішно. Фінална сума добра Фінална сума добра, айбо ї правдивость не перевірена. Gpg файл не може быти перевіреный. Образик быв успішно записаный. Сись ISO образик досвідченый бізувным подписом. Сись ISO образик досвідченый небізувным подписом. Исе офіціалный ISO образик. Незнамый подпис Небізувный подпис байтув незнамо 