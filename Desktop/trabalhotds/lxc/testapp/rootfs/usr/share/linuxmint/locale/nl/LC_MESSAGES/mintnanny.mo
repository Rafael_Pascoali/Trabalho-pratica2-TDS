��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �  (        6     L  
   ^  �   i  #   �          ,  1   ?                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-05-13 10:34+0000
Last-Translator: Pjotr12345 <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s is geen geldige domeinnaam. Blokkeer toegang tot gekozen domeinnamen Geblokkeerde domeinen Domeinblokkeerder Domeinnaam Domeinnamen moeten beginnen en eindigen met een letter of een cijfer, en kunnen uitsluitend letters, cijfers, punten en koppeltekens bevatten. Bijvoorbeeld: mijn.nummer1domein.nl Ongeldig domein Ouderlijk toezicht Voer a.u.b. de domeinnaam in die u wilt blokkeren 