��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  (   �  3   �     �               /  	   >     H     Z     z  )   �  C   �       g       �  
   �     �     �     �     �     �     �               '     3  /   8  "   h     �  &   �     �     �  "   �  "   �          #  &   C     j     x     �     �     �     �     �     �            &     ;   8  *   t  '   �     �  N   �  &   2  7   Y     �  )   �  G   �  :   "  B   ]  &   �     �  Z   �     =     C     U     a     }     �     �  	   �  0   �     �  	     4        C     J  	   O     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-11 19:09+0000
Last-Translator: Asier Iturralde Sarasola <Unknown>
Language-Team: Basque <eu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Errore bat gertatu da irudia kopiatzean. Errore bat gertatu da %s(e)n partizio bat sortzean. Errore bat gertatu da Errore bat gertatu da. Autentifikazio-errorea Kalkulatzen... Egiaztatu Kontroleko batura Kontroleko baturako fitxategiak Kontroleko batura ez dator bat Aukeratu izen bat zure USB memoriarentzat Deskargatu berriro ISO irudia. Bere kontroleko batura ez dator bat. Denak itxura ona du! FAT32 
  + Erabateko bateragarritasuna.
  -  Ez ditu onartzen 4GB baino handiagoak diren fitxategiak.

exFAT
  + Bateragarritasun zabala.
  + 4GB baino handiagoak diren fitxategiak onartzen ditu.
  -  Ez da FAT32 bezain bateragarria.

NTFS 
  + Windowsekin bateragarria.
  -  Ez da Mac eta hardware gailu gehienekin bateragarria.
  -  Noizean behin bateragarritasun arazoak Linuxekin (NTFS jabeduna da eta atzeranzko ingeniaritza erabili behar da).

EXT4 
  + Modernoa, egonkorra, azkarra, egunkariduna.
  + Linuxeko fitxategi-baimenen euskarria.
  -  Ez da bateragarria Windows, Mac eta hardware gailu gehienekin.
 Fitxategi-sistema: Formateatu Formateatu USB memoria GB GPG sinadurak GPG sinatutako fitxategia GPG sinatutako fitxategia: Joan atzera ISO egiaztapena ISO irudia: ISO irudiak ISO: Sinaduraz fidatzen bazara, ISOaz fida zaitezke. Osotasun-egiaztapenak huts egin du KB Gakoa ez da aurkitu gako-zerbitzarian. Fitxategi lokalak MB Sortu USB memoria abiarazgarri bat Sortu USB memoria abiarazgarri bat Informazio gehiago Ez da aurkitu bolumenaren IDrik Ez dago behar adina leku USB memorian. SHA256 batura SHA256 baturen fitxategia SHA256 baturen fitxategia: SHA256 batura: Hautatu irudia Hautatu USB memoria bat Hautatu irudi bat Sinatzailea: %s Tamaina: TB ISO irudiaren SHA256 batura okerra da. SHA256 baturen fitxategiak ez du ISO irudi honen baturarik. SHA256 baturen fitxategia ez dago sinatua. USB memoria behar bezala formateatu da. Kontroleko batura zuzena da Kontroleko batura zuzena da, baina ez da batuketaren benetakotasuna egiaztatu. Ezin izan da gpg fitxategia egiaztatu. Ezin izan da gpg fitxategia deskargatu. Egiaztatu URLa. Irudia behar bezala idatzi da. Ezin izan da batura fitxategia egiaztatu. Ezin izan da kontroleko baturako fitxategia deskargatu. Egiaztatu URLa. ISO irudi hau sinadura fidagarri baten bidez egiaztatu da. ISO irudi hau fidagarria ez den sinadura baten bidez egiaztatu da. ISO hau Windows irudi bat dela dirudi. Hau ISO irudi ofiziala da. Honek USB memoriako datu guztiak suntsituko ditu, ziur zaude aurrera jarraitu nahi duzula? URLak USB irudi-idazlea USB memoria USB memoria formateatzailea USB memoria: Sinadura ezezaguna Sinadura ez fidagarria Egiaztatu Egiaztatu irudiaren benetakotasuna eta osotasuna Bolumenaren etiketa: Bolumena: Windows-eko irudiek prozesamendu berezia behar dute. Idatzi byte ezezaguna 