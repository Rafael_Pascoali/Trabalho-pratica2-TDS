��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  '   �  +   �     �     �     
     #  
   4     ?     O     h  %   �  D   �     �  @  
     K  
   X     c     �     �     �     �     �     �     �     �     �  8   �  $   1     V     Y     s     �  '   �  '   �     �  )   �  #        8     E     X  
   l     w     �     �     �     �     �  -   �  B   �      <  +   ]     �  F   �      �  7        E  #   d  :   �  4   �  5   �  (   .     W  U   q     �     �     �     �          *     8  
   H  1   S     �     �  7   �     �     �     �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-28 15:29+0000
Last-Translator: Umidjon Almasov <Unknown>
Language-Team: Uzbek <uz@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: uz
 Tasvirni nusxalashda xatolik yuz berdi. %s da boʻlim yaratishda xatolik yuz berdi. Xatolik yuz berdi Xatolik yuz berdi. Autentifikatsiya xatosi. Hisoblanmoqda… Tekshirish Nazorat summasi Nazorat summasi fayllari Nazorat summasi moskelmadi USB flesh-diskingiz uchun nom tanlang ISO tasvirini yana yuklab oling. Uning nazorat summasi mos kelmaydi. Hammasi yaxshi ko‘rinadi! FAT32 
  + Hamma joyda mos keladi.
  -  4 GB dan katta fayllar bilan ishlay olmaydi.

exFAT
  + Deyarli hamma joyda mos keladi.
  + 4 GB dan katta fayllar bilan ishlay oladi.
  -  FAT32 kabi mos emas.

NTFS 
  + Windows bilan mos keladi.
  -  Mac va ko‘pgina apparat qurilmalari bilan mos kelmaydi.
  -  Linux bilan vaqti-vaqti bilan moslik muammolari (NTFS xususiy va teskari muhandislik).

EXT4 
  + Zamonaviy, barqaror, tezkor, jurnallashtirilgan.
  + Linux fayl ruxsatlarini qo‘llab-quvvatlaydi.
  -  Windows, Mac va ko‘pgina apparat qurilmalari bilan mos kelmaydi.
 Fayl tizimi: Formatlash USB flesh-diskini formatlash GB GPG imzolari GPG imzolangan fayl GPG imzolangan fayl: Orqaga qaytish ISO tasdiqlash ISO tasviri: ISO tasvirlari ISO: Agar siz imzoga ishonsangiz, ISOga ishonishingiz mumkin. Butunlikni tekshirish amalga oshmadi KB Kalit serverda topilmadi. Lokal fayllar MB Yuklanadigan USB flesh-diskini yaratish Yuklanadigan USB flesh-diskini yaratish Qo‘shimcha ma'lumot Hech qanday disk identifikatori topilmadi USB flesh-diskida joy yetarli emas. SHA256 summa SHA256 summa fayli SHA256 summa fayli: SHA256sum: Tasvirni tanlash USB flesh-diskini tanlang Tasvirni tanlash Imzolagan: %s Hajmi: TB ISO tasvirining SHA256 summasi noto‘g‘ri. SHA256 summa faylida ushbu ISO tasviri uchun summalar mavjud emas. SHA256 summa fayli imzolanmagan. USB flesh-diski muvaffaqiyatli formatlandi. Nazorat summasi to‘g‘ri Nazorat summasi toʻgʻri, lekin summaning haqiqiyligi tasdiqlanmagan. GPG faylni tekshirib bo‘lmadi. GPG faylni yuklab bo‘lmadi. URL manzilini tekshiring. Tasvir muvaffaqiyatli yozildi. Summa faylini tekshirib bo‘lmadi. Summa faylini yuklab bo‘lmadi. URL manzilini tekshiring. Ushbu ISO tasviri ishonchli imzo bilan tasdiqlangan. Ushbu ISO tasviri ishonchsiz imzo bilan tasdiqlangan. Ushbu ISO Windows tasviriga o‘xshaydi. Bu rasmiy ISO tasviridir. Bu USB flesh-diskidagi barcha ma'lumotlarni yo‘q qiladi. Davom etishni xohlaysizmi? URL manzillari USB tasvir yozuvchisi USB flesh-diski USB flesh-disk formatlagichi USB flesh-diski: Noma'lum imzo Ishonchsiz imzo Tasdiqlash Tasvirning haqiqiyligi va yaxlitligini tekshirish Disk belgisi: Disk qismi: Windows tasvirlari maxsus ishlov berishni talab qiladi. Yozish baytlar noma'lum 