��    (      \  5   �      p     q     �     �     �     �     �     �     �  -   �     �       
        )  
   /  	   :     D  
   J     U     f     l     u     �     �  0   �     �     �     �     �       	        #     ,     F     L     h     w          �     �  �  �     H     `     |     �     �     �     �     �  ;   �  +     	   8     B     [     j     �     �  
   �     �     �     �     �     �     �  5   �  (   	  .   H	     w	     |	     �	     �	     �	     �	  	   �	  5   �	     &
  	   ?
     I
     O
  �   b
     #   &   !                	                        "      
                 '            $                        %                                    (                              Buttons labels: Buttons layout: Compiz Compton Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Linux Mint Mac style (Left) Marco Metacity Mounted Volumes Network Openbox Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Window Manager Windows Xfwm4 root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-03-10 11:34+0000
Last-Translator: GunChleoc <Unknown>
Language-Team: Sgioba na gàidhlig
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: gd
 Leubailean nam putanan: Co-dhealbhachd nam putanan: Compiz Compton Coimpiutair Desktop Roghainnean an desktop Ìomhaigheagan an desktop Na seall susbaint na h-uinneig fhad a thathar 'gan slaodadh Cuir air mion-ghleus roghainnean an desktop Dhachaigh Meud nan ìomhaigheagan: Ìomhaigheagan Ìomhaigheagan a-mhàin Eadar-aghaidh Mòr Linux Mint Stoidhle Mac (Clì) Marco Metacity Draibhean munntaichte Lìonra Openbox Tagh na rudan a bu toigh leat faicinn air an desktop: Seall na h-ìomhaigheagan air na putanan Seall na h-ìomhaigheagan air na clàran-taice Beag Teacsa fo nithean Teacsa ri taobh nithean Teacsa a-mhàin Bàraichean-inneal Stoidhle tradaiseanta (Deas) An sgudal Cleachd cruth-clò an t-siostaim air a' bhàr-tiotail Manaidsear nan uinneagan Uinneagan Xfwm4 root@linuxmint.com Launchpad Contributions:
  Akerbeltz https://launchpad.net/~fios
  GunChleoc https://launchpad.net/~gunchleoc
  alasdair caimbeul https://launchpad.net/~alexd-deactivatedaccount 