��    
      l      �       �   ]   �      O     ]  �   i            �   5     �     �  �    �   �     y     �  2  �  %   �  ;   	  ;  E     �  5   �              
             	                  Click the button below to discover the new features introduced in this version of Linux Mint. Documentation First Steps Linux Mint is a great project. It is open to anyone who wants to participate. There are many ways to help. Click the button below to see how you can get involved. New Features Show this dialog at startup The Linux Mint documentation consists of a collection of guides, available in PDF, ePUB and HTML and available in many languages. Click the button below to see which guides are available. Welcome Welcome to Linux Mint Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-05-16 18:33+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Macedonian <mk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:34+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Кликнете на копчето подолу за да ги откриете новите карактеристики додадени во оваа верзија на Linux Mint. Документација Почетни чекори Linux Mint е одличен проект. Отворен е за секој што сака да придонесе. Постојат многу начини да помогнете. Кликнете на копчето подолу за да видите како можете да се вклучите. Нови Карактеристики Прикажи го овој дијалог на старт Документацијата за Linux Mint се состои од збирка со упатства, достапна во PDF, ePUB, HTML и достапна на повеќе јазици. Кликнете на копчето подолу за да видите кои упатства се достапни. Добредојде Добредојдовте во Линукс Минт 