��    G      T  a   �        	             !     4     8     I     d  K   q  %   �  -   �       "   &  F   I     �     �     �     �     �     �     �       	          	   (     2     >     O     \  -   q     �     �     �     �     �     �     �     �     	     	     	     	     4	     <	     L	     Q	     e	     l	  1   x	  6   �	     �	     �	  $   �	     
     !
     7
     S
     r
     �
     �
     �
     �
  '   �
     �
          &     -     6     =  	   C     M  �  b     �            
   &     1  (   D     m  r   z  =   �  M   +  $   y  3   �  Q   �  
   $     /     H  .   c  '   �     �     �     �     
  
     
   (     3     C     [     r  =   �     �     �     �  3        9     G  	   T     ^     x     }     �     �     �     �     �     �  	   �     �  7     :   =     x       0   �  	   �  #   �  (   �          7     O  (   i  )   �  (   �  B   �     (      G     h     u     �     �     �     �     5           "       A   F   D   .       	      #      +   )             $           3                B                   -         '   E   %   0       :      1          9   ;           <                 >   7   2       8   G   ,   =       /   ?   *       !                 6           4      &   C                               
      @             (       <not set> About Advanced MATE Menu All All applications Applet button in the panel Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Custom Place Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu button Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-09-15 09:39+0000
Last-Translator: MinhTran <Unknown>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <chưa đặt> Giới thiệu Menu MATE nâng cao Tất cả Mọi ứng dụng Nút Applet trong bảng điều khiển Ứng dụng Duyệt  xem mọi ổ đĩa cục bộ và từ xa, cũng như các thư mục truy cập được từ máy này Duyệt xem và cài đặt những phần mềm hiện hữu Duyệt các vị trí mạng cục bộ và các vị trí đã đánh dấu Duyệt xem các tập tin đã xóa Duyệt xem các mục trên màn hình làm việc Nhấp để đặt phím tăng tốc mới để mở và đóng trình đơn.   Máy tính Cấu hình hệ thống Trung tâm Điều khiển Không kích hoạt được  trình bổ sung Không nạp được trình bổ sung: Địa điểm tùy chỉnh Màn hình làm việc Chủ đề màn hình nền Chỉnh sửa Menu Đổ rác Ưa thích Thư mục Nhà Chèn dấu phân cách Chèn khoảng trắng Cài đặt gói '%s' Cài đặt, gỡ bỏ và nâng cấp các gói phần mềm Khởi chạy Khởi chạy khi đăng nhập Khoá màn hình Đăng xuất hoặc chuyển đổi người dùng Đăng xuất Trình đơn Nút menu Tùy chỉnh trình đơn Tên Tên: Mạng Mở thư mục cá nhân Tùy chọn Trình Quản lý Gói Đường dẫn Chọn phím gia tốc Vị trí Tùy chỉnh Nhấn Backspace để xoá tổ hợp phím hiện có Nhấn Escape hoặc nhập lại để huỷ tác vụ.   Thoát Tải lại plugins Ghi nhớ tìm kiếm và thể loại sau cùng Gỡ bỏ Loại khỏi các mục ưa thích Yêu cầu mật khẩu để mở khóa Tìm gói để cài đặt Chọn một thư mục Hiện mọi ứng dụng Hiển thị biểu tượng nút nhấn Hiển thị biểu tượng thể loại Hiển thị trong các mục ưa thích Tắt máy, khởi động lại, tạm ngưng hoặc ngủ đông Trình Quản lý Phần mềm Hoán đổi tên và tên chung Hệ thống Trình dòng lệnh Chủ đề: Thùng rác Gỡ cài đặt Sử dụng dòng lệnh 