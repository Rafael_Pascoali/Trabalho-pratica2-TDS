��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  Z   !  i   |  <   �  H   #	  J   l	  �  �	     �  (   �  P   �  P   )  X   z  -   �  "     !   $  G   F  <   �  �   �  :   �     �  =   �     %     :     Z     j                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-01-16 10:15+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Thai <th@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 มีปัญหาเกิดขึ้นขณะคัดลอกอิมเมจ มีปัญหาเกิดขึ้นขณะสร้างพาร์ทิชันบน %s มีข้อผิดพลาดเกิดขึ้น การยืนยันตัวบุคคลผิดพลาด เลือกชื่อให้สติ๊ก USB ของคุณ FAT32 
  + เข้ากันได้ในทุกที่
  -  ไม่สามารถจัดการไฟล์ที่ใหญ่กว่า 4GB ได้

exFAT
  + เข้ากันได้ในเกือบทุกที่
  + สามารถจัดการไฟล์ที่ใหญ่กว่า 4GB ได้
  -  เข้ากันได้ไม่มากเช่นเดียวกับ FAT32

NTFS 
  + เข้ากันได้กับ Windows
  -  เข้ากันไม่ได้กับ Mac และอุปกรณ์ฮาร์ดแวร์ส่วนมาก
  -  อาจมีปัญหาเรื่องความเข้ากันได้บางอย่างกับ Linux (NTFS เป็นกรรมสิทธิ์และเป็นวิศวกรรมแบบย้อนกลับ)

EXT4 
  + ทันสมัย เสถียร รวดเร็ว และบันทึกได้ดี
  + รองรับสิทธิอนุญาตไฟล์ Linux
  -  เข้ากันไม่ได้กับ Windows, Mac และอุปกรณ์ฮาร์ดแวร์ส่วนมาก
 ฟอร์แมต ฟอร์แมตสติ๊ก USB ทำสติ๊ก USB ที่ใช้เริ่มระบบได้ ทำสติ๊ก USB ที่ใช้เริ่มระบบได้ ไม่มีพื้นที่ว่างเหลือบนสติ๊ก USB เลือกแฟ้มอิมเมจ เลือกสติ๊ก USB เลือกอิมเมจ สติ๊ก USB ฟอร์แมตสำเร็จแล้ว อิมเมจเขียนเสร็จแล้ว การกระทำนี้จะทำลายข้อมูลทุกอย่างบนสติ๊ก USB คุณต้องการทำต่อไปอีกหรือไม่? โปรแกรมเขียนอิมเมจ USB สติ๊ก USB โปรแกรมฟอร์แมตสติ๊ก USB สติ๊ก USB: ชื่อโวลุ่ม: เขียน ไม่รู้จัก 