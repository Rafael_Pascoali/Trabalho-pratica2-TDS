��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  #        A     ^     t     �  J   �  
   �  	   �  .   �     -  4   L  
   �  (   �  _   �                                            	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-06-23 21:18+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <is@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: is
 %s meira diskpláss verður notað. %s af diskplássi mun losna. %s verða alls sótt. Fleiri breytinga er krafist Villa kom upp Get ekki fjarlægt %s pakkann þar sem hans er krafist af öðrum pökkum. Flatpakkar Setja upp Pakkans %s er krafist af eftirfarandi pökkum: Pakkar sem verða fjarlægðir Skoðaðu listann yfir breytingar hér fyrir neðan. Fjarlægja Eftirfarandi pakkar verða fjarlægðir: Þessi valmyndarfærsla er ekki tengd neinum pakka. Viltu samt fjarlægja hana úr valmyndinni? Uppfæra 