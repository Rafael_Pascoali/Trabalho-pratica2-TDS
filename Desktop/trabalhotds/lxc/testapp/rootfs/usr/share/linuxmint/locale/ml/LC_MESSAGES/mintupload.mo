��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  �   D  s     �   �  V   "  �   y  $   *  �   O  $   �            7   1  4   i     �  J   �  x     h   ~  S  �  v   ;  �   �  �   K  �     Y   �               	  �   #     �     �     �     �  0  �  "   �  
   !     ,  �   I  X     %   n  $   �  �   �  �   U  p   �  n   ^  z   �     H  
   b     m  )   �  4   �  7   �  @   $  u   e  �   �  |   h  H   �     .     N      U!     e!  \   v!                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-11-09 23:04+0000
Last-Translator: praveenp <Unknown>
Language-Team: Malayalam <ml@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(2)s അല്ലെങ്കില്‍ %(3)s നു കീഴെ കണ്ടെത്തിയ ക്രമീകരണ ഫയലില്‍ %(1)s ഉറപ്പിച്ചിട്ടില്ല %(number)d പ്രമാണങ്ങളുടെ %(percentage)s- %(service)s എന്നതിലോട്ട് ഒരു ഫയലിന്റെ %(percentage)s - %(service)s എന്നതിലോട്ട് അപ്‌ലോഡ് ചെയ്യുന്നു %(total_size)s എന്നതിൽ %(size_so_far)s പൂർത്തിയായി %(total_size)s എന്നതിൽ %(size_so_far)s പൂർത്തിയായി - %(time_remaining)s അവശേഷിക്കുന്നു (%(speed)s/സെക്.) %s ഗുണഗണങ്ങള്‍ <b>ദയവായി പുതിയ അപ്‌ലോഡ്‌ സര്‍വിസിനു ഒരു നാമം ചേര്‍ക്കുക:</b> സംബന്ധിച്ചു് B റദ്ദാക്കുക അപ്‌ലോഡ് റദ്ദാക്കുക കണൿഷൻ പരിശോധിക്കുക അടയ്ക്കുക ആവശ്യമായ സ്ഥലം ലഭ്യമായില്ല രൂപരേഖാ മാറ്റം രേഖപ്പെടുത്താന്‍ കഴിഞ്ഞില്ല അപ്‌ലോഡ്‌ സെര്‍വിസുകളെ നിര്‍വചിക്കുക അപ്‌ലോഡ്‌ ചെയ്യേണ്ട തട്ട്. <TIMESTAMP> നു പകരമായി നിലവിലെ സമയമുദ്ര ഉപയോഗിക്കും, തന്നിരിക്കുന്ന സമയമുദ്രാ രൂപഘടന പിന്തുടരും. തനതായി: . ഈ അപ്‌ലോഡ് താങ്കൾക്ക് റദ്ദാക്കണമെന്നുണ്ടോ? %s  ലേക്ക് അപ്‌ലോഡ്‌ ചെയ്യുവാനായി ഇവിടേയ്ക്ക് വലിച്ചിടുക ഫയലിന്റെ വലിപ്പം സര്‍വീസില്‍ ലഭ്യമായ സ്ഥലത്തേക്കാള്‍ കൂടുതല്‍ ആണ് ഫയലിന്റെ വലിപ്പം സെര്‍വിസിന്റെ പരിധിയേക്കാള്‍ കൂടുതല്‍ ആണ് ഫയല്‍ വിജയകരമായി അപ്-ലോഡ് ചെയ്തു. GB GiB ഹോസ്റ്റ്: ഹോസ്റ്റ്നെയിം അല്ലെങ്കില്‍ ഐപി വിലാസം, സ്വതവേ ഉള്ളത്(default):  KB KiB MB MiB തനതായ, അടയാളവാക്ക്: അടയാളവാക്ക്-ഇല്ലാത്ത SCP ബന്ധം, ശൂന്യ-നാട FTP ബന്ധം, SFTP ബന്ധങ്ങള്‍ക്കായി ഉപയോഗിക്കുന്ന ~/.ssh ചാവികള്‍ അടയാളവാക്ക്: വഴി: പോര്‍ട്ട്: വിദൂര പോര്‍ട്ട്‌, സ്വതവേ ഉള്ളത്(default) FTP ക്ക് വേണ്ടി 21, SFTP, SCP എന്നിവയ്ക്ക് വേണ്ടി 22 പിന്നാമ്പുറത്ത് പ്രവർത്തിക്കുക സര്‍വിസ് നാമം സര്‍വിസുകള്‍ %(number)d ഫയലുകൾ '%(service)s' എന്നതിലോട്ട് വിജയകരമായി അപ്‌ലോഡ് ചെയ്തു '%(service)s' എന്നതിലേയ്ക്ക് ഒരു ഫയൽ വിജയകരമായി അപ്‌ലോഡ് ചെയ്തു '%(service)s' എന്നതിലോട്ടുള്ള അപ്‌ലോഡ് റദ്ദാക്കി ഈ സെര്‍വിസിനു ഒരു അടയാളവാക്ക് ആവശ്യമാണ്. നാള്‍വഴി രൂപകല്‍പന (strftime). സ്വതവേ ഉള്ളത്(default) ആയി: നാള്‍വഴി: തരം: യുആര്‍എല്‍: അപരിചിത സേവനം: %s അപ്‌ലോഡ്‌ മാനേജര്‍ അപ്‌ലോഡ്‌ മാനേജര്‍... അപ്‌ലോഡ്‌ സര്‍വിസുകള്‍ '%s' എന്നതിലോട്ടുള്ള അപ്‌ലോഡ് പരാജയപ്പെട്ടു:  %(number)d ഫയലുകൾ %(service)s എന്നതിലേയ്ക്ക് അപ്‌ലോഡ് ചെയ്യുന്നു %(service)s എന്നതിലോട്ട് ഒരു ഫയൽ അപ്‌ലോഡ് ചെയ്യുന്നു ഫയല്‍ അപ്-ലോഡ് ചെയ്യുന്നു... ഉപയോക്താവ്: ഉപയോക്തൃനാമം, സ്വതവേ ഉള്ളത്(default) ആയി താങ്കളുടെ പ്രാദേശിക(ലോക്കല്‍) ഉപയോക്തൃനാമം ഉപയോഗിക്കപ്പെടും ഫയല്‍ _സഹായം വിജയകരമായി ബന്ധം സ്ഥാപിക്കപെട്ടു 