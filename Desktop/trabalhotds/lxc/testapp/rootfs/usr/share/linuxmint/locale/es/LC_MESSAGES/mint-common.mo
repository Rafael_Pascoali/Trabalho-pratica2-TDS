��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  '   3  %   [     �      �  ;   �      �  !     %   >  #   d  %   �     �  J   �     	     	     #	     ,	  <   5	     r	  0   �	     �	  
   �	     �	     �	  *   �	  j   
      �
  
   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-04 19:14+0000
Last-Translator: Toni Estevez <toni.estevez@gmail.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Se usarán %s más de espacio de disco. Se liberarán %s de espacio de disco. En total se descargarán %s. Se requieren cambios adicionales Se revertirá el software adicional a una versión anterior Se instalará software adicional Se purgará el software adicional Se reinstalará el software adicional Se eliminará el software adicional Se actualizará el software adicional Se ha producido un error No se puede eliminar el paquete %s porque es requerido por otros paquetes. Revertir Flatpak Flatpaks Instalar El paquete %s es una dependencia de los paquetes siguientes: Paquetes que se desinstalarán Eche un vistazo a la lista de cambios siguiente. Purgar Reinstalar Eliminar Omitir actualización Se desinstalarán los paquetes siguientes: Este elemento del menú no está asociado a ningún paquete. ¿Quiere eliminarlo del menú de todos modos? Se omitirán las actualizaciones Actualizar 