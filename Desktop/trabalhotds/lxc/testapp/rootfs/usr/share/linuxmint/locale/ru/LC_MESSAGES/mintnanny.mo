��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  4   �  Y   *  +   �  %   �     �  �   �  "   �  #   �  )   	  ^   3                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-08-31 16:43+0000
Last-Translator: Dmitriy Kulikov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Недопустимое доменное имя %s. Блокирование доступа к выбранным доменыи именам Заблокированные домены Блокировщик доменов Доменное имя Доменные имена должны начинаться и заканчиваться буквой или цифрой и содержать только буквы, цифры, точки и дефисы. Пример: my.number1domain.com Недопустимый домен Родительский контроль Введите доменное имя, которое хотите заблокировать 