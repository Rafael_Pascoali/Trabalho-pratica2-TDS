��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  $   $      I  !   j     �  >   �  /   �  -     8   G  +   �  0   �     �  K   �     ;	     W	     _	  
   h	  8   s	     �	  )   �	     �	     �	     	
     
  )   $
  b   N
      �
  
   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 16:51+0000
Last-Translator: Marek Adamski <Unknown>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Zostanie użyte %s miejsca na dysku. zwolni się %s miejsca na dysku. za %s zostanie pobrany do końca. Wymagane są dodatkowe zmiany Dodatkowe oprogramowanie zostanie zastąpione starszą wersją Dodatkowe oprogramowanie zostanie zainstalowane Dodatkowe oprogramowanie zostanie oczyszczone Dodatkowe oprogramowanie zostanie zainstalowane ponownie Dodatkowe oprogramowanie zostanie usunięte Dodatkowe oprogramowanie zostanie zaktualizowane Wystąpił błąd Nie można usunąć pakietu %s, ponieważ jest wymagany przez inne pakiety. Przywróć starszą wersję Flatpak Flatpaki Zainstaluj Pakiet %s jest zależnością następujących pakietów: Pakiety do usunięcia Proszę przejrzeć listę zmian poniżej. Oczyść Zainstaluj ponownie Usuń Pomiń aktualizację Następujące pakiety zostaną usunięte: Ten element menu nie jest powiązany z żadnym z pakietów. Czy chcesz mimo to usunąć go z menu? Aktualizacje zostaną pominięte Aktualizuj 