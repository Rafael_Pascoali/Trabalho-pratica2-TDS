��          �      L      �  w   �     9     L     _     f     x     �     �     �     �     �     �       	   "     ,     >     E     b    �  x   �     	          +     4     O     d     p     �     �     �     �     �           	     *     1     G                 	          
                                                                         <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Install Install / Remove Languages Install / Remove Languages... Install language packs Install the selected language Language Settings Language settings Languages No locale defined Remove Remove the selected language Some language packs are missing Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-07-11 11:59+0000
Last-Translator: Almir Zulic <zalmir@yahoo.com>
Language-Team: Bosnian <bs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>Napomena: Instalacija ili nadogradnja jezičkih paketa može pokrenuti instalaciju dodatnih jezika</small></b> Dodaj novi jezik Dodaj novi jezik Dodaj... Primijeni na cijeli sistem Poptpuno instalirano Instalirano Instaliraj / Ukloni jezike Instaliraj / Ukloni jezike... Instaliraj jezičke pakete Instaliraj odabrani jezik Jezične postavke Jezičke postavke Jezici Lokalne postavke nisu definisane Ukloni Ukloni odabrani jezik Nedostaju neki jezički paketi 