��    .      �  =   �      �     �  .         /     C     H     c     j     x     �  :   �     �  	   �     �     �          3     ;     R     c     k     }     �     �     �     �     �     �     �     �  8   �  +   �           =     D  
   \     g     z     �     �     �     �     �     �  '   �  *     �  7     �  3   �     	     0	  1   7	     i	  	   p	     z	  	   �	  3   �	     �	  
   �	     �	     
     
     5
     A
     ^
  	   p
     z
     �
  	   �
     �
     �
  	   �
  
   �
     �
     �
     �
  B   �
  9   %  #   _     �     �     �     �  #   �     �               #     +     9     N     j     ,       -                 "   .      !   &   #             
       $   %                                  	             (                                +                     )                  '      *            More info: %s Adding private PPAs is not supported currently Backported packages Base CD-ROM (Installation Disc) Cancel Cancelling... Cannot add PPA: '%s'. Clear Configure the sources for installable software and updates Country Downgrade Downgrade foreign packages Edit the URL of the PPA Edit the URL of the repository Enabled Fix MergeList problems Foreign packages Install Installed version Key Main Maintenance Mirrors Obsolete Open.. PPA PPAs Package Please enter the name of the repository you want to add: Press Enter to continue or Ctrl+C to cancel Purge residual configuration Remove Remove foreign packages Repository Repository version Restore the default settings Select a mirror Software Sources Sources Speed Unreachable Unstable packages You are about to add the following PPA: You are about to remove the following PPA: Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-02-07 22:42+0000
Last-Translator: tuxmaniack <Unknown>
Language-Team: Latvian <lv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
  Vairāk informācijas: %s Privātu PPA pievienošana šobrīd nav atbalstīta Neatbalstītās pakotnes Bāzes Lasāmatmiņas kompaktdisks (instalācijas disks) Atcelt Atceļ... Nevar pievienot PPA: '%s'. Notīrīt Konfigurē jaunu programmu un atjauninājumu avotus Valsts Pazemināt Pazemināt svešās pakotnes Rediģēt PPA adresi Rediģēt repozitorija adresi Aktivizēts Salabot MergeList problēmas Svešās pakotnes Instalēt Instalētā versija Atslēga Galvenais Uzturēšana Spoguļserveri Novecojis Atvērt... PPA PPA Pakotne Lūdzu, ievadiet repozitorija nosaukumu, kuru vēlaties pievienot: Nospiediet Enter, lai turpinātu vai Ctrl+C, lai atceltu. Izmest pārpalikušo konfigurāciju Izņemt Noņemt svešās pakotnes Repozitorijs Repozitorija versija Atjaunot noklusējuma iestatījumus Izvēlieties spoguļserveri Programmatūras avoti Avoti Ātrums Nesasniedzams Nestabilās pakotnes Tiks pievienoti šādi PPA: Tiks izņemti šādi PPA: 