��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  )   9  '   c  !   �  )   �  &   �  "   �      !  $   B  !   g  #   �     �  R   �     	     "	     *	     3	  6   <	     s	  C   �	     �	  
   �	     �	     �	  &   �	  X    
  "   y
  	   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 21:43+0000
Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Serão utilizados %s de espaço em disco. Serão libertos %s de espaço em disco. No total, serão transferidos %s. São necessárias alterações adicionais Software adicional descerá de versão Software adicional será instalado Software adicional será purgado Software adicional será reinstalado Software adicional será removido Software adicional será atualizado Ocorreu um erro Não é possível remover o pacote %s, pois o mesmo é exigido por outros pacotes. Descer de versão Flatpak Flatpaks Instalar O pacote %s é uma dependência dos seguintes pacotes: Pacotes a remover Por favor dê uma vista de olhos na seguinte lista de alterações. Purgar Reinstalar Remover Ignorar atualização Os pacotes seguintes serão removidos: Este item do menu não está associado a um pacote. Quer removê-lo do menu mesmo assim? As atualizações serão ignoradas Atualizar 