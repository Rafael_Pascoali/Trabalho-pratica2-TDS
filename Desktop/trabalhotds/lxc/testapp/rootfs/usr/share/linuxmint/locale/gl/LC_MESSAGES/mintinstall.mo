��    -      �  =   �      �  ?   �  B   !  !   d  $   �     �     �     �     �     �     �     �  	   �     �  	   �     �                         %  	   >     H     Q     _     m     t     |  +   �     �  
   �     �     �     �     �     �  3     *   @     k     �     �     �     �     �     �  �  �  I   �  J   �  )   3	  *   ]	     �	     �	  
   �	     �	     �	     �	     �	  
   �	     �	  
   �	     �	     �	     
  	   
     
     !
  	   <
     F
     O
     `
  
   o
     z
     �
  '   �
     �
     �
     �
     �
  
   �
  	   �
       7     +   T     �     �     �     �     �     �     �                $              !      (         -   "   ,              #                                        +                          &   	      
                *                    %      '           )          %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-03-13 07:13+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Galician <gl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s que descargar, %(localSize)s de espazo liberado no disco %(downloadSize)s que descargar, %(localSize)s de espazo requirido no disco %(localSize)s de espazo liberado no disco %(localSize)s de espazo requirido no disco 3D Sobre Accesorios Xogos de taboleiro Conversa Detalles Debuxo Educación Correo Emuladores Compartir ficheiros Tipos de letra Xogos Gráficos Instalar Instalar novos aplicativos Instalado Internet Non dispoñíbel Non instalados Ofimática Paquete Fotografía Use apt-get para instalar este paquete. Programación Publicación Estratexia en tempo real Retirar Revisións Escaneado Ciencia e educación Buscar na descrición dos paquetes (aínda máis lento) Buscar no resumo dos paquetes (máis lento) Simulación e carreiras Xestor de software Son e vídeo Ferramentas do sistema Estratexia baseada en quendas Visores Web 