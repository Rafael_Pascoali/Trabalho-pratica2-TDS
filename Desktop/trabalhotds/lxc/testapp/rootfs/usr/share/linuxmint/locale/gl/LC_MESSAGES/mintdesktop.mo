��             +         �     �     �     �     �     �       -        G     b  
   g     r  
   x  	   �     �     �     �     �  0   �     �                    .  	   @     J     S     m     s     �     �     �  �  �     f     }  
   �  
   �     �     �  0   �  )        :     B     V  
   ]  	   h     r     y     �     �  5   �     �     �               4  	   Q     [     q     �  3   �     �     �  �   �                         
                                                                                                     	                  Buttons labels: Buttons layout: Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Mac style (Left) Mounted Volumes Network Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Window Manager Windows translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-09-14 15:06+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Galician <gl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Etiquetas nos botóns: Disposición dos botóns: Computador Escritorio Configuración do escritorio Iconas do escritorio Non mostrar o contido das xanelas ao arrastralas Axuste preciso das opcións do escritorio Persoal Tamaño das iconas: Iconas Só iconas Interface Grande Estilo MAC (esquerda) Volumes montados Rede Escolla os elementos que quere ver no seu escritorio: Mostrar iconas nos botóns Mostrar iconas nos menús Pequeno Texto debaixo dos elementos Texto a carón dos elementos Só texto Barras de ferramentas Estilo tradicional (dereita) Lixo Usar o tipo de letra do sistema na barra de título Xestor de xanelas Xanelas Launchpad Contributions:
  J. M. Castroagudín Silva https://launchpad.net/~chavescesures
  Miguel Anxo Bouzada https://launchpad.net/~mbouzada
  SLR https://launchpad.net/~santiago-lorente-rey 