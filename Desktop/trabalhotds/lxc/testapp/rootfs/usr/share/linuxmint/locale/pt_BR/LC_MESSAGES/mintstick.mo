��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  .   �  7   �               ,     D  	   R     \  !   k     �  *   �  I   �       �  *     �     �     �               ,     A     W     ^     s          �  3   �  %   �     �  ,   �          *  &   -  $   T     y     �  /   �     �     �     �          $     6     R     h     y     �  ,   �  B   �  .   �  ,   $  %   Q  W   w  (   �  6   �  !   /  -   Q  I     <   �  A     -   H     v  W   �     �     �     
          9     J     b  	   }  1   �     �     �  6   �                    K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-28 12:16+0000
Last-Translator: Gilberto vagner <vagner.unix@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Ocorreu um erro enquanto a imagem era copiada. Ocorreu um erro enquanto a partição em %s era criada. Ocorreu um erro Ocorreu um erro. Erro de Autenticação. Calculando... Verificar Verificar soma Soma de verificação do arquivos Checagem incompatível Escolha um nome para o seu dispositivo USB Baixe a imagem ISO novamente. Sua soma de verificação não corresponde. Tudo parece bem! FAT32 
  + Compatível em todos os lugares.
  - Não consegue manipular arquivos maiores que 4 GB.

exFAT
  + Compatível em quase todos os lugares.
  + Consegue lidar com arquivos maiores que 4 GB.
  - Não é tão compatível quanto FAT32.

NTFS 
  + Compatível com o Windows
  - Não é compatível com o Mac e com a maioria dos dispositivos de hardware.
  - As vezes acontecem problemas de compatibilidade com o Linux (o NTFS é patenteado e o suporte do GNU/Linux foi feito usando de engenharia reversa).

EXT4 
  + Moderno, estável, rápido, jornalizado.
  + Suporta permissões de arquivos do Linux.
  - Não é compatível com o Windows, Mac e a maioria dos dispositivos de hardware.
 Sistema de arquivos: Formatar Formate um dispositivo USB GB Assinatura GPG Arquivo assinado GPG Arquivo assinado GPG: Voltar Verificação da ISO Imagem ISO: Imagens ISO ISO: Se você confia na assinatura, pode confiar no ISO. Falha na verificação de integridade KB Chave não encontrada no servidor de chaves. Arquivos locais MB Crie um dispositivo USB inicializável Criar dispositivo USB inicializável Mais informações Nenhum ID de volume encontrado Não há espaço suficiente no dispositivo USB. Soma SHA256 Arquivo de somas SHA256 Arquivo de somas SHA256: somaSHA256: Selecionar Imagem Selecione o dispositivo USB Selecionar uma imagem Assinado por: %s Tamanho: TB A soma SHA256 da imagem ISO está incorreta. O arquivo de somas SHA256 não contém somas para esta imagem ISO. O arquivo de somas SHA256 não está assinado. O dispositivo USB foi formatado com sucesso. A soma de verificação está correta A soma de verificação está correta, mas a autenticidade da soma não foi verificada. O arquivo gpg não pôde ser verificado. O arquivo gpg não pôde ser baixado. Verifique a URL. A imagem foi gravada com sucesso. O arquivo de somas não pôde ser verificado. Baixe a imagem ISO novamente. Sua soma de verificação não corresponde. Esta imagem ISO é verificada por uma assinatura confiável. Esta imagem ISO é verificada por uma assinatura não confiável. Este ISO se parece com uma imagem do Windows. Esta é uma imagem ISO oficial. Todos os dados do dispositivo USB serão apagados, tem certeza de que deseja continuar? URLs Gravador de Imagem USB Dispositivo USB Formatador de dispositivos USB Dispositivo USB: Assinatura desconhecida Assinatura não confiável Verificar Verifique a autenticidade e integridade da imagem Rótulo do volume: Volume: As imagens do Windows requerem processamento especial. Gravar bytes desconhecido 