��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  1  N  Z   �  U   �  8   1  3   j  �   �     h     k     r     �     �  0   �  =   �          -  �   H     �     �  H   
  =   S     �  
   �  !   �     �     �  !   �          ,     ?     [  _   i  I   �                /  
   @  ,   K     x  !   �     �  f   �     "     3  
   <  ;   G     �  5   �  =   �  3   
  �   >     �     �  
   �     �        #     Z   1  E   �     �  
   �     �  .   �  
   "     -  
   L     W  f   o     �  
   �     �       B     5   b  T   �  (   �          6  
   N     Y     u  F   �  e   �  =   :     x     �  
   �         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-05 11:30+0000
Last-Translator: أنس <Unknown>
Language-Team: anwar AL_iskandrany <anwareleskndrany13@gmail.com >
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n % 100 >= 3 && n % 100 <= 10 ? 3 : n % 100 >= 11 && n % 100 <= 99 ? 4 : 5;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ar_EG
 للتنزيل %(downloadSize)s، تم تحرير %(localSize)s من مساحة القرص للتنزيل %(downloadSize)s، مطلوب %(localSize)s من مساحة القرص تم تحرير %(localSize)s من مساحة القرص مطلوب %(localSize)s من مساحة القرص %d مهمة تعمل حاليا %d مهمات تعمل حاليا %d مهمات تعمل حاليا %d مهمات تعمل حاليا %d مهمات تعمل حاليا %d مهمات تعمل حاليا 3D حول الملحقات الكل كل التطبيقات تم الإنتهاء من كل العمليات حدث خطأ أثناء محاولة إضافة Flatpak repo ألعاب الطاولة لا يمكن تثبيته لا يمكن معالجة هذا الملف أثناء وجود عمليات نشطة.
برجاء المحاولة لاحقا بعد انتهائها. لا يمكن إزالته الدردشة اضغط <a href='%s'>هنا</a> لإضافة مراجعتك الخاص. يتم العمل حاليا على الحزم التالية التفاصيل الرسم اختيارات المحررين التعليم الإلكترونيات البريد الإلكتروني المحاكيات الأساسيات مشاركة الملفات أول شخص دعم Flatpak غير متوفر حاليا. حاول تثبيت Flatpak و gir1.2-flatpak-1.0. دعم Flatpak غير متوفر حاليا. حاول تثبيت Flatpak. الخطوط الألعاب الرسومات تثبيت تثبيت التطبيقات الجديدة مثبت التطبيقات المثبتة جاري التثبيت قد يؤدي تثبيت هذه الحزمة إلى تلف لا يمكن إصلاحه في نظامك. الإنترنت جافا إطلاق تحديد  البحث على القائمة الحالية الرياضيات برامج ترميز الوسائط المتعددة برامج ترميز الوسائط المتعددة لـKDE لم يتم العثور على حزم مطابقة لا توجد حزم لعرضها.
قد يدل ذلك على وجود مشكلة - حالو تحديث ذاكرة التخزين المؤقتة. غير متوفر فير مثبت أوفيس PHP الحزمة الصور الفوتوغرافيه يرجى التحلي بالصبر. هذا يمكن أن يستغرق بعض الوقت... الرجاء استخدام apt-get لتثبيت هذه الحزمة. البرمجة النشر Python استراتيجية الوقت الحقيقي نحديث حدّث قائمة الحزم إزالة جاري الإزالة قد يؤدي إزالة هذه الحزمة إلى تلف لا يمكن إصلاحه في نظامك. المراجعات الفحص العلوم العلوم والتعليم البحث في وصف الحزم (حتى البحث الأبطأ) البحث في ملخص الحزم (بحث أبطأ) يبحث في مستودعات البرمجيات، انتظر دقيقة واحدة عرض التطبيقات المثبتة المحاكاة والسباق مدير البرامج الصوت الصوت والفيديو ادوات النظام Flatpak repo الذي تحاول إضافته موجود بالفعل. هناك عمليات نشطة حاليا.
هل أنت متأكد من أنك تريد الخروج؟ الاستراتيجية القائمة علي التشغيل الفيديو المشاهدين الويب 