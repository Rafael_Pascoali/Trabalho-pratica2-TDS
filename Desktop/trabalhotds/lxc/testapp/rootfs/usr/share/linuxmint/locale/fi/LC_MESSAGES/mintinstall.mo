��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  ;   _  <   �      �  !   �       3   6     j     m     u     �     �     �     �  /   �  
   �     �     �  r        �  
   �  ;   �  4   �               $     1     F     M     [     h  
   u     �     �     �  `   �  J        g     n     t  	   �     �     �     �  	   �     �  
   �  V   �     B     K     P  "   \          �     �     �     �  [   �     ;     X     f     s     |     �     �  
   �  2   �  $   �     �                 	   6     @  	   Z     d  
   k  Q   v  
   �     �     �     �  (   �  *     -   H     v     �     �     �  5   �     �               '  :   >  C   y     �     �     �       Q         r     �     �  
   �     �     �     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-01 09:27+0000
Last-Translator: Kimmo Kujansuu <Unknown>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: fi
 %(downloadSize)s ladataan, %(localSize)s levytilaa vapautuu %(downloadSize)s ladataan, %(localSize)s levytilaa tarvitaan %(localSize)s levytilaa vapautuu %(localSize)s levytilaa tarvitaan %d arvostelu %d arvostelua %d tehtävä käynnissä %d tehtävää käynnissä 3D Tietoja Apuohjelmat Lisää Kaikki Kaikki sovellukset Kaikki toimet suoritettu Flatpak varastoa lisättäessä tapahtui virhe. Lautapelit Haara: Ei voitu asentaa Tätä tiedostoa ei voida käsitellä, kun on muita aktiivisia operaatioita.
Yritä uudelleen niiden päätyttyä. Ei voitu poistaa Keskustelu Napsauta <a href='%s'>tähän</a> ja lisää oma arvostelu. Tällä hetkellä käsitellään seuraavia paketteja Tiedot Dokumentointi Piirtäminen Toimittajan valinnat Opetus Elektroniikka Sähköposti Emulaattorit Oleelliset Tiedostojen jakaminen Pelaajan näkökulmasta Flatpak (%s) Flatpak tukea ei ole tällä hetkellä saatavilla. Yritä asentaa flatpak ja gir1.2-flatpak-1.0. Flatpak tukea ei ole tällä hetkellä saatavilla. Yritä asentaa flatpak. Fontit Pelit Luodaan välimuistia, hetki Grafiikka Kotisivu Asenna Asenna uusia ohjelmia Asennettu Asennetut sovellukset Asennetaan Tämän paketin asentaminen voi aiheuttaa peruuttamatonta vahinkoa järjestelmällesi. Internet Java Käynnistä Rajoita haku nykyiseen listaukseen Matematiikka Multimediakoodekit Multimediakoodekit KDE:lle Nimi: Ei löytynyt sopivia paketteja Ei näytettäviä paketteja.
Tämä voi merkitä ongelmaa - yritä virkistää välimuisti. Kaappausta ei ole saatavilla Ei saatavilla Ei asennettu Toimisto Valinnaiset osat PHP Paketti Valokuvaus Ole kärsivällinen. Tämä voi viedä hetkisen... Asenna paketti apt-get -työkalulla. Ohjelmointi Julkaiseminen Python Reaaliaikainen strategia Virkistä Virkistä pakettiluettelo Etäkone: Poista Poistetaan Tämän paketin poisto voi aiheuttaa peruuttamatonta vahinkoa järjestelmällesi. Arvostelut Skannaus Tiede Tiede ja koulutus Etsi pakettien kuvauksista (hitain haku) Etsi pakettien yhteenvedoista (hidas haku) Etsitään ohjelmiston varastoja, pieni hetki Näytä asennetut sovellukset Simulaatio ja kilpa-ajo Koko: Ohjelmistohallinta Jotain meni pieleen. Yritä uudelleen napsauttamalla. Ääni Ääni ja video Järjestelmäpaketti Järjestelmätyökalut Flatpak varasto, jota olit lisäämässä, on jo olemassa. Käynnissä on aktiivisia operaatioita.
Haluatko varmasti lopettaa? Tämä on järjestelmäpaketti Tämä on Flatpak paketti Yritä uudelleen Vuoropohjainen strategia Ei yhteyttä palvelimien kanssa. Tarkista Internet-yhteytesi ja yritä uudelleen. Ei saatavilla Versio: Video Katselimet Netti Järjestelmäsi paketinhallinta 