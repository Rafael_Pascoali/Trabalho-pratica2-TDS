��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  \   Y  ]   �  *     '   ?     g     ~     �     �     �  
   �     �     �     �  3   �       	   0     :  g   P     �     �  ?   �  +        A  	   M     W     _     u     |     �     �     �     �     �     �  r   �  \   L  	   �  
   �     �     �     �     �     �          '     >  Q   N     �     �     �  .   �  
   �     �          ,     2  j   I     �     �     �     �     �          	       +     6   G  
   ~     �     �     �  	   �     �  
   �     �     �  Q        b  
   k     v     z  6   �  0   �  9   �  &   *     Q     g     n  <   �     �     �     �     �  0   �  @   (     i     y     �     �  h   �     )  
   5     @     F     U     Y     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-04 15:35+0000
Last-Translator: Umidjon Almasov <Unknown>
Language-Team: Uzbek <uz@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: uz
 %(downloadSize)s hajmdagi ma'lumotni ko‘chish uchun diskda %(localSize)s joy bo‘shatildi %(downloadSize)s hajmdagi ma'lumotni ko‘chish uchun diskda %(localSize)s joy talab qilinadi Diskning %(localSize)s qismi bo‘shatildi Diskda %(localSize)s joy talab qilinadi %d ta ko‘rib chiqish %d ta vazifa bajarilmoqda 3D Haqida Aksessuarlar Qo‘shish Hammasi Barcha ilovalar Hamma amallar tugatildi Flatpak repo ko‘shishga uringanda xato yuz berdi. Taxta o‘yinlari Bo‘lim: O‘rnatib bo‘lmadi Faol amallar bo‘lganda ushbu fayl bilan ishlab bo‘lmaydi.
Ular tugagandan so‘ng urinib ko‘ring. Olib tashlab bo‘lmadi Chat Yangi sharh qo‘shish uchun <a href='%s'>shu joyga</a> bosing. Hozirda quyidagi paketlar ustida ishlamoqda Tafsilotlar Hujjatlar Chizish Tahririyat tanlovlari Ta'lim Elektronika Elektron pochta Emulyatorlar Asosiy Fayllar ulushish Birinchi shaxs Flatpak (%s) Flatpak qo‘llab-quvvatlanishi hozirda mavjud emas. Flatpak va gir1.2-flatpak-1.0 o‘rnatishga urinib ko‘ring. Flatpak qo‘llab-quvvatlanishi hozirda mavjud emas. Flatpak o‘rnatishga urinib ko‘ring. Shriftlar O‘yinlar Kesh yaratilmoqda, bir daqiqa Grafika Uy sahifasi O‘rnatish Yangi ilovalarni o‘rnatish O‘rnatilgan O‘rnatilgan ilovalar O‘rnatilmoqda Ushbu paketni o‘rnatish tizimga tuzatib bo‘lmaydigan zarar keltirishi mumkin. Internet Java Ishga tushirish Joriy ro‘yxat bo‘yicha qidirishni cheklash Matematika Multimedia kodeklari KDE uchun multimedia kodeklari Nomi: Mos paketlar topilmadi Ko‘rsatiladigan paketlar yo‘q.
Bu muammoni ko‘rsatishi mumkin - keshni yangilashga urinib ko‘ring. Skrinshotlar mavjud emas Mavjud emas O‘rnatilmagan Ofis Ixtiyoriy komponentlar PHP Paket Fotografiya Sabr qiling. Bu biroz vaqt olishi mumkin... Ushbu paketni o'rnatish uchun apt-get dan foydalaning. Dasturlash Nashr etish Python Real-vaqt  rejimida strategiya Yangilash Paketlar ro‘yxatini yangilash Masofaviy: Olib tashlash Olib tashlanmoqda Ushbu paketni o‘chirish tizimga tuzatib bo‘lmaydigan zarar keltirishi mumkin. Sharhlar Skanerlash Fan Fan va ta'lim Paketlar tavsifida qidirish (yanada sekinroq qidirish) Paketlar xulosasida qidirish (sekinroq qidirish) Dasturiy ta'minot repozitoriylari qidirilmoqda, bir lahza O‘rnatilgan dasturlarni ko‘rsatish Simulyatsiya va poyga Hajmi: Dasturiy ta'minot menejeri Nimadir noto‘g‘ri bajarildi. Qayta urinish uchun bosing. Ovoz Ovoz va video Tizim paketi Tizim uskunalari Siz qo‘shmoqchi bo‘lgan Flatpak repo mavjud. Hozirda faol amallar mavjud.
Chiqish uchun ishonchingiz komilmi? Bu tizim paketi Ushbu paket Flatpak Qayta urinib ko‘ring Navbat asosidagi strategiya Serverlar bilan bog‘lanib bo‘lmadi. Internet ulanishingizni tekshiring va qaytadan urinib ko‘ring. Mavjud emas Versiyasi: Video Ko‘ruvchilar Veb Tizim paket menejeri 