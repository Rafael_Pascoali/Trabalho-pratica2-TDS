��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J    N  L   U  N   �  +   �  -     !   K     m     p  
   s     ~     �     �  J   �  	   �     �  g        w     �  F   �  $   �     �               #     +     :     H     O     \     j  f   z  L   �     .     4     ;  	   G     Q     f     n     }  _   �     �     �     �  #   �        '   /  +   W      �  h   �               (     4     8     >  9   J  0   �     �     �     �     �  	   �     �  
     	     a        {     �     �     �  /   �  &   �  ,        .     C     T     j     r  
   �  X   �  J   �     1  
   H  	   S     ]         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: mintinstall_mintinstall-is
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-06-23 21:19+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: is
 %(downloadSize)s til að sækja, losar %(localSize)s rými á hörðum diski %(downloadSize)s til að sækja, krefst %(localSize)s rýmis á hörðum diski Losar %(localSize)s rými á hörðum diski Krefst %(localSize)s rýmis á hörðum diski %d verk keyrandi %d verk keyrandi 3D Um Aukahlutir Allt Öll forrit Öllum aðgerðum lokið Villa kom upp þegar reynt var að bæta við Flatpak-hugbúnaðarsafninu. Borðspil Get ekki sett upp Get ekki unnið með þessa skrá á meðan aðgerðir eru virkar.
Reyndu aftur þegar þeim er lokið. Get ekki fjarlægt Spjall Smelltu <a href='%s'>hér</a> til að bæta við þinni eigin umsögn. Er að vinna með eftirfarandi pakka Nánar Teikning Úrval umsjónarmanna Menntun Rafeindatækni Tölvupóstur Hermar Nytsamlegast Skráamiðlun Fyrstu-persónu Stuðningur við Flatpak er ekki tiltækur-1.0. Prófaðu að setja upp flatpak og gir1.2-flatpak-1.0. Stuðningur við Flatpak er ekki tiltækur. Prófaðu að setja flatpak upp. Letur Leikir Myndvinnsla Setja upp Setja upp ný forrit Uppsett Uppsett forrit Set upp Ef þessi pakki er settur inn getur það valdið alvarlegum skemmdum á uppsetningu kerfisins. Internetið Java Ræsa Takmarka leit við núverandi lista Stærðfræði Kóðunarlyklar fyrir margmiðlunarefni KDE-kóðunarlyklar fyrir margmiðlunarefni Engir samsvarandi pakkar fundust Engir pakkar til að sna.
Þetta gæti gefið til kynna vandamál - prfaðu að endurlesa skyndiminnið. Ekki tiltækt Ekki uppsett Skrifstofan PHP Pakki Ljósmyndun Vertu þolinmóð/ur. Þetta getur tekið nokkra stund... Notaðu apt-get til að setja upp þennan pakka. Forritun Útgáfa Python Rauntíma herkænska Endurlesa Endurlesa pakkalistann Fjarlægja Fjarlægi Ef þessi pakki er fjarlægður getur það valdið alvarlegum skemmdum á uppsetningu kerfisins. Umsagnir Skönnun Vísindi Menntun og vísindi Leita í lýsingum pakka (ennþá hægari leit) Leita í yfirliti pakka (hægari leit) Leita í hugbúnaðarsöfnum, bíddu aðeins Sýna uppsett forrit Hermar og keppni Hugbúnaðarstjórnun Hljóð Hljóð og mynd Kerfistól Flatpak-hugbúnaðarsafnið sem þú ert að reyna að bæta við er þegar til staðar. Það eru virkar aðgerðir í gangi.
Ertu viss um að þú viljir hætta? Til-skiptis herkænska Myndskeið Skoðarar Vefur 