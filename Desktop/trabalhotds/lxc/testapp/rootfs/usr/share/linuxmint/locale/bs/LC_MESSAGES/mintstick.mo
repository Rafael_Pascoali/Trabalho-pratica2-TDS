��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  1   �  6        >     Q  (   l     �  !   �  (   �  (   �  3        J      Z     {  /   �     �  ~   �     W     j  !   �     �     �     �     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-07-11 12:22+0000
Last-Translator: Almir Zulic <zalmir@yahoo.com>
Language-Team: Bosnian <bs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Desila se greška prilikom kopiranja slike USB-a. Desila se greška prilikom pravljenja particije na %s. Desila se greška. Greška pri autentikaciji. Izaberite ime za vaš memorijski uređaj Formatiranje Formatiraj USB memorijski uređaj Napravi butabilni USB memorijski uređaj Napravi butabilni USB memorijski uređaj Nema dovoljno prostora na USB memorijskom uređaju. Odaberite sliku Odaberite USB memorijski uređaj Odaberite sliku USB memorijski uređaj je uspješno formatiran. Slika je uspješno zapisana. Ova operacija će uništiti sve postojeće podatke na vašem USB memorijskom uređaju, da li ste sigurni da želite nastaviti? Pisač slike USB-a USB memorijski uređaj Formater USB memorijskog uređaja USB memorijski uređaj: Natpis na uređaju: Piši 