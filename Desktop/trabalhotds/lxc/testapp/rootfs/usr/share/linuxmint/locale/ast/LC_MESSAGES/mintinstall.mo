��    +      t  ;   �      �  ?   �  B   �  !   <  $   ^     �     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �     �  	              )     7     >     F     R  
   ^     i     |     �     �     �  3   �  *   �     	          0     @     M     a     i  �  m  H   1  H   z  +   �  *   �     	  	   	  
   '	     2	     A	     H	     Q	  
   X	     c	  
   x	     �	     �	     �	  	   �	     �	     �	  	   �	     �	     �	     
     
     
     
     (
     5
  
   N
  
   Y
     d
     m
  8   �
  .   �
     �
               #     <     R     Z            "         !                    (                          +   '                %       *                                #       &                	          $                 )   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-02-26 15:29+0000
Last-Translator: ivarela <ivarela@ubuntu.com>
Language-Team: Asturian <ast@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s a descargar, %(localSize)s d'espaciu de discu lliberáu %(downloadSize)s a descargar, %(localSize)s d'espaciu de discu requeríu %(localSize)s d'espaciu del discu lliberáu %(localSize)s d'espaciu de discu requeríu 3D Tocante a Accesorios Xuegos de mesa Charra Detalles Dibuxu Educación Corréu electrónicu Emuladores Compartición de ficheros Tipografíes Xuegos Gráficos Instalar Instalar nueves aplicaciones Instalada Internet Ensin instalar Oficina Paquete Semeya Programación Espublizando Estratexa en tiempu real Desaniciar Revisiones Escanéu Ciencia y Educación Guetar na descripción de los paquetes (entá mas lento) Guetar nel resume de los paquetes (más lento) Simuladores y carreres Xestor de software Soníu y videu Ferramientes del sistema Estratexia por turnos Visores Web 