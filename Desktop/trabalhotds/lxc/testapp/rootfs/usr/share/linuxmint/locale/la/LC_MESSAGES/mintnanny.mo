��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  (      "   )     L     [     o  �   }          ,     ?     Q                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-06-10 09:51+0000
Last-Translator: Davide Novemila <Unknown>
Language-Team: Latin <la@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: la
 %s non est dominiorum nomen admittendum. Dominiorum nomina selecta claudere Dominia clausa Vigiliae dominiorum Nomen dominii Nomina dominiorum cum littera vel cifra incipienda atque terminanda sunt; litteris, cifris, punctis atque interductis constituenda tantum sunt. Exemplum: my.number1domain.com Dominium corruptum Genitorum Regulae Nomem dominii vetandum scribe 