��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  1   2  !   d  "   �  ,   �  P   �  0   '  -   X  3   �  0   �  1   �     	  J   9	  -   �	     �	     �	  	   �	  6   �	     
  7   
     Q
     X
     e
     m
  (   �
  f   �
  "        :                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 14:45+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s d'espace disque supplémentaire sera utilisé. %s d'espace disque sera libéré. %s seront téléchargés au total. Des changements supplémentaires sont requis Des logiciels supplémentaires seront mis à jour vers des versions inférieures Des logiciels supplémentaires seront installés Des logiciels supplémentaires seront purgés Des logiciels supplémentaires seront réinstallés Des logiciels supplémentaires seront supprimés Des logiciels supplémentaires seront mis à jour Une erreur s’est produite Impossible de retirer le paquet %s car il est requis par d'autres paquets. Mettre à niveau vers une version inférieure Flatpak Flatpaks Installer Le paquet %s est une dépendance des paquets suivant : Paquets à supprimer Veuillez consulter la liste des changements ci-dessous. Purger Réinstaller Retirer Ignorer la mise à niveau Les paquets suivants seront supprimés : Cet élément du menu n'est associé à aucun paquet. Voulez-vous tout de même le supprimer du menu ? Les mises à jour seront ignorées Mettre à jour 