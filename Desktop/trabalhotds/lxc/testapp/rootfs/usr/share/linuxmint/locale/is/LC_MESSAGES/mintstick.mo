��    1      �  C   ,      8  )   9  2   c     �     �     �     �     �     �          "     /     ;     B     U     X  
   g  
   r     }     �     �     �     �     �     �  "   �  
   �               %     5     C     I  )   L     v  #   �  N   �     	     	  	   	     !	  
   5	     @	     R	     Y	     g	     o	     u	     {	  �  �	  *   ]  0   �     �     �     �  	   �     �     	         S  A     �  
   �     �     �     �     �     �     �     �     �               0     O  /   d     �     �     �     �     �     �     �  2   �     !  -   5  [   c     �     �  
   �     �     �       	        )     ?     K     R     X           !                      (          1      )                *   +      0   /                                       #   	           ,   &             
                .   '      %                     -   "      $                      An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Checksum files Checksum mismatch Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures ISO image: ISO images ISO: KB Local files MB Make a bootable USB stick Make bootable USB stick More information Not enough space on the USB stick. SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The USB stick was formatted successfully. The checksum is correct The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Verify Volume label: Volume: Write bytes unknown Project-Id-Version: mintstick_mintstick-is
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-25 17:15+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: is
 Villa kom upp við að afrita diskmyndina. Villa kom upp við að útbúa disksneið á %s. Villa kom upp Villa kom upp. Villa við auðkenningu. Reikna... Gátsummuskrár Gátsumma stemmir ekki Veldu nafn á USB-lykilinn þinn FAT32 
   + Allstaðar samhæft.
   -  Meðhöndlar ekki skrár stærri en 4GB.

exFAT 
   + Allstaðar samhæft.
   + Meðhöndlar skrár stærri en 4GB.
   -  Ekki eins samhæfanlegt og FAT32.

NTFS 
  + Samhæft við Windows.
   - Ekki samhæft við Mac eða megnið af vélbúnaði.
   - Einstaka samhæfingarvandamál við Linux (NTFS er séreignarbúnaður sem 
þarf að afturábakvinna til að virki annarsstaðar).

EXT4 

  + Nútímalegt, stöðugt, hraðvirkt, dagbókarfært.
  + Styður skráaheimildakerfi Linux.
   - Ekki samhæft við Windows, Mac eða megnið af vélbúnaði.
 Skráakerfi: Forsníða Forsníða USB-lykil GB GPG-undirritanir ISO-diskmynd: ISO-diskmyndir ISO: KB Staðværar skrár MB Búa til ræsanlegan USB-lykil Búa til ræsanlegan USB-lykil Nánari upplýsingar Ekki nógu mikið pláss eftir á USB-lyklinum. SHA256summa: Veldu diskmynd Veldu USB-lykil Veldu diskmynd Undirritað af: %s Stærð: TB Það heppnaðist vel að forsníða USB-lykilinn. Gátsumman er rétt Það heppnaðist vel að skrifa diskmyndina. Þetta mun eyða öllum gögnum á USB-lyklinum, ertu viss um að þú viljir halda áfram? Slóðir USB-diskmyndaskrifari USB-lykill USB-forsníðingatól USB-lykill: Óþekkt undirritun Sannreyna Heiti á gagnahirslu: Gagnarými: Skrifa bæti óþekkt 