��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  ;     C   R  '   �     �  E   �  �  !	     �  /   �  A   �  :   <  7   w     �  +   �  '   �  Y   "  >   |  �   �  &   C     j  &   �     �     �  
   �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-05-28 13:52+0000
Last-Translator: Umar Hammad <Unknown>
Language-Team: Urdu <ur@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ایمیج کاپی کرتے وقت مسئلہ درپیش۔ ‏%s پہ پارٹیشن بناتے وقت مسئلہ درپیش۔ کوئی مسئلہ درپیش ہوا۔ توثیق میں مسئلہ اپنی یو.ایس.بی سٹک کے لیے کوئی نام چنیں FAT32 ￼
 + ہر جگہ ہم آہنگ۔￼
 - 4GB سے بڑی فائلوں کو ہینڈل نہیں کر سکتے۔￼
 ￼
 exFAT￼
 + تقریبا ہر جگہ مطابقت رکھتا ہے۔￼
 + 4GB سے بڑی فائلوں کو ہینڈل کر سکتے ہیں۔￼
 - FAT32 کی طرح مطابقت نہیں رکھتا۔￼
 ￼
 NTFS ￼
 + ونڈوز کے ساتھ ہم آہنگ
 - میک اور زیادہ تر ہارڈویئر آلات کے ساتھ مطابقت نہیں رکھتا۔￼
 - لینکس کے ساتھ کبھی کبھار مطابقت کے مسائل (NTFS ملکیتی اور ریورس انجینئرڈ ہے) ￼
 ￼
 EXT4 ￼
 + جدید، مستحکم، تیز، جرنلائزڈ۔￼
 + لی نکس فائل کی اجازتوں کی حمایت کرتا ہے۔
 - ونڈوز، میک اور زیادہ تر ہارڈویئر آلات کے ساتھ مطابقت نہیں رکھتا۔￼
 فارمیٹ کریں کوئی یو.ایس.بی فارمیٹ کریں ایک بوٹ ایبل یوا.یس.بی ڈرائیو بنائیے بوٹ ایبل یوا.یس.بی ڈرائیو بنائیے یو.ایس.بی سٹک پر جگہ ناکافی ہے۔ ایمیج منتخب کریں یو.ایس.بی سٹک منتخب کریں کوئی ایمیج منتخب کریں یوایس بی ڈرائیو کامیابی کے ساتھ فارمیٹ کر دی گئی۔ ایمیج کامیابی کا ساتھ لکھ دیا گیا۔ یہ یو.ایس.بی پہ موجود تمام ڈیٹا حذف کردے گا۔ کیا آپ واقعی یہ کرنا چاہتے ہیں؟ یو.ایس.بی ایمیج رائٹر میموری سٹک(USB) یو.ایس.بی سٹک فارمیٹر یو.ایس.بی سٹک: ڈسک کا لیبل: لکھیں نامعلوم 