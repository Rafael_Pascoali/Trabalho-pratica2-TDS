��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �          ,     <     I  l   P     �     �     �     �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-30 05:41+0000
Last-Translator: 张琦 <Unknown>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s 不是个有效的域名 拦截选中的域名 已拦截域名 域名拦截 域名 域名的开头和结尾必须是字母或者数字，而且只能包含字母、数字、点和连字号。 范例： my.number1domain.com 域名无效 家长控制 请输入您想拦截的域名 