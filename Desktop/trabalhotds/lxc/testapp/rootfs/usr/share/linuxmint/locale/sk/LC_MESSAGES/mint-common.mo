��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  $     %   A     g     �  .   �  $   �  5   �  %   +  !   Q     s     �  O   �     �     	     	     $	  3   1	     e	  (   |	     �	     �	     �	     �	  '   �	  i   
      �
     �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-03 13:36+0000
Last-Translator: menom <Unknown>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s veľkosti disku bude použitých. %s veľkosti disku bude uvoľnených. %s bude celkovo stiahnutých Vyžadujú sa ďalšie zmeny Dodatočný softvér prejde na nižšiu verziu Nainštaluje sa dodatočný softvér Dodatočný softvér sa odstráni aj s konfiguráciou Dodatočný softvér sa preinštaluje Dodatočný softvér sa odstráni Dodatočný softvér sa inovuje Nastala chyba Balík %s nemôže byť odstránený, pretože je vyžadovaný inými balíkmi. Prechod na nižšiu verziu Flatpak Balíky Flatpak Inštalovať Balík %s je závislý na nasledujúcich balíkoch: Balíky k odstráneniu Prosím, skontrolujte zoznam zmien dolu. Odstrániť s konfiguráciou Preinštalovať Odstrániť Preskočiť inováciu Nasledovné balíky budú odstránené: Táto položka ponuky nie je spojená so žiadným balíkom. Chcete ju z ponuky odstrániť napriek tomu? Aktualizácie budú preskočené Aktualizovať 