��    +      t  ;   �      �     �     �     �     �     �     �  !   �               0     O     W  $   o     �     �     �     �     �    �     �     �     �     �     �     �     �     �       8        E     L  
   d     o     �  
   �     �     �  Q   �          .     @     E  �  b     �     �     	     	     	     #	  (   ,	     U	     \	     v	     �	  (   �	  -   �	     �	     

     
     
     ,
  
  1
     <     A     M  
   Z     e     k     s     w     {  ?   �     �     �  	   �     �  .   �     .     >     T  U   ]     �     �     �  "   �     *                              $                (                 '   +      
   "                 &          )                                  #                                	          !       %        More info: %s Apply Base Cancel Cancelling... Clear Click OK to update your APT cache Country Edit the URL of the PPA Edit the URL of the repository Enabled Error: must run as root Error: need a repository as argument Foreign packages GB/s Install Installed version Key Linux Mint uses Romeo to publish packages which are not tested. Once these packages are tested, they are then moved to the official repositories. Unless you are participating in beta-testing, you should not enable this repository. Are you sure you want to enable Romeo? MB/s Main Maintenance OK Open Open.. PPA PPAs Package Please enter the name of the repository you want to add: Remove Remove foreign packages Repository Repository version Restore the default settings Select All Software Sources Speed The content of this PPA is not available. Please refresh the cache and try again. Your configuration changed already installed kB/s version %s already installed Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-10-06 18:01+0000
Last-Translator: amm <Unknown>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: 
  Prus informatziones: %s Àplica Base Annulla Cantzellende... Isbòida Carca AB pro atualizare sa cache APT tua Istadu Modìfica s'URL de su PPA Modìfica s'URL de su depòsitu Ativadu Faddina: si depet esecutare comente root Faddina: serbit unu depòsitu che a argumentu pachetes istràngios GB/s Installa Versione installada Crae Linux Mint uses Romeo to publish packages which are not tested. Once these packages are tested, they are then moved to the official repositories. Unless you are participating in beta-testing, you should not enable this repository. Ses seguru chi boles ativare Romeo? MB/s Printzipale Mantenimentu Andat bene Aberi Aberi.. PPA PPA Pachete Inserta·nche su nùmene de su depòsitu chi boles agiùnghere: Boga Boga pachetes istràngios Depòsitu Versione depòsitu Torra a ponnere is impostatziones predefinidas Seletziona totu Mitzas de su software Lestresa Su cuntenutu de su PPA no est a disponimentu. Atualiza sa cache e torra·bi a proare. Cunfiguratzione modificada giai installada kB/s sa versione %s est giai installada 