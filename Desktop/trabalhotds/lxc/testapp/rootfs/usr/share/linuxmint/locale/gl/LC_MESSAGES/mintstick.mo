��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  %     /   C     s  %   �  "   �  h  �     9     B  "   \  "     %   �     �     �     �  *        <  L   \     �     �     �     �     �                          
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-10-20 13:41+0000
Last-Translator: Alberto Gómez <Unknown>
Language-Team: Galician <gl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Produciuse un erro ao copiar a imaxe. Produciuse un erro ao crear a partición en %s. Produciuse un erro. Produciuse un erro de autenticación. Escolla un nome para a memoria USB FAT32
  +  Compatible en tódalas situacións
  - Non soporta arquivos de máis de 4GB.

exFAT 
  +Compatible en case tódalas situacións.
  - Soporra arquivos de máis de 4 GB.
  - Non é tan compatible coma FAT32.

NTFS 
  + Compatible con Windows.
  - Non compatible con Mac nin coa maioría de dispositivos de hardware.
  - Problemas ocasionais de compatibilidade con Linux (NTFS é propietario e empregado mediante enxeñería inversa)

EXT4
  + Moderno, estable, rápido, periodizado.
  + Soporta permisos de arquivo de Linux.
  - Non compatible con Windows, Mac nin coa maioría de dispositivos de hardware.
 Formatar Formatar unha memoria USB Crear unha memoria USB arrincábel Crear unha memoria USB arrincábel Non hai espazo abondo na memoria USB. Seleccionar unha imaxe Seleccione unha memoria USB Seleccione unha imaxe A memoria USB foi formatada correctamente. A imaxe grabouse correctamente. Isto destruirá todos os datos da memoria USB, confirma que quere continuar? Gravador de imaxes USB Memoria USB Formatador de memorias USB Memoria USB: Etiqueta do volume: Escribir descoñecido 