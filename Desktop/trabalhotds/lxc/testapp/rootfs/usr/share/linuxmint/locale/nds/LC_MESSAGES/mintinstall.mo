��    ,      |  ;   �      �  ?   �  B   	  !   L  $   n     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �       	        &     /     =     K     R     Z  +   f     �  
   �     �     �     �     �     �  3   �  *        I     _     p     �     �     �     �  �  �  D   �  G   �  &   	  0   ;	     l	     o	     u	     ~	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     
     !
     *
     ;
     L
     R
  
   X
  <   c
     �
     �
     �
  	   �
     �
     �
     �
  3     *   9     d     z     �     �     �     �     �            #         "                     )          !               ,   (                &       +                               $       '                 	          %                *   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-01-27 14:53+0000
Last-Translator: Cedrik Heckenbach <cedrik.heckenbach@gmail.com>
Language-Team: German, Low <nds@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s herunterzuladen, %(localSize)s freier Speicherplatz %(downloadSize)s herunterzuladen, %(localSize)s Speicherplatz benötigt %(localSize)s des Speicherplatzes frei %(localSize)s des Speicherplatzes wird benötigt 3D Über Zubehör Brettspiele Chat Details Zeichnen E-Mail Emulatoren Gemeinsame Nutzung von Dateien Schriftarten Spiele Grafik Installieren Neue Programme installieren Installiert Internet Nicht verfügbar Nicht installert Büro Paket Fotografie Bitte verwenden Sie apt-get um dieses Paket zu installieren. Programmieren Bekanntgeben Echtzeit-Strategie Entfernen Bewertungen Scannen Wissenschaft und Bildung In Paketbeschreibung suchen (noch langsamere Suche) In Paketinhalten suchen (langsamere Suche) Simulation und Rennen Software Manager Audio und Video Systemwerkzeuge Rundenbasiertes Strategiespiel Besucher Web 