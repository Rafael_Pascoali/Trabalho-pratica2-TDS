��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  "   (  *   K     v     �     �     �     �
     �
     �
          "     @     O     `     o     �  1   �     �  
   �     �               (  
   /                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-06-22 14:21+0000
Last-Translator: Davide Novemila <Unknown>
Language-Team: Latin <la@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: la
 Error evenit dum imago copiabatur. Error evenit dum partitio creabatur in %s. Error evenit. Error comprobationis. Nomen tuae USB clavis elige FAT32 
  + Ubicumque acceptum.
  - Plicas mariores quam 4GB uti non potest.

exFAT
  + Quasi ubicumque acceptum.
  + Plicas maiores quam 4GB uti potest. 
  - Non est tam acceptum quam FAT32.

NTFS
  + Acceptum in Windows.
  - Not acceptum a Mac atque a parte maiore apparatuum.
  - Nonnumquam in Linux difficultates oriuntur (NTFS est proprietarium atque deductum ingenio inverso)

EXT4 
  + Recens, stabile, celere, cum journaling.
  + Permissionibus plicarum Linucis utitur.
  - Not acceptum a Windows, a Mac atque a parte maiore apparatuum.
 Forma USB clavem forma USB clavem initialem crea USB clavem initialem crea Spatium deficit in USB clave. Imaginem elige USB clavem elige Imaginem elige USB clavis bene formata est. Imago bene servata est. Data delenda sunt ex USB clave, an perficere vis? USB Imaginem Scriptor USB Clavis USB Clavem Formator USB clavis: Nomen voluminis: Scribe incognitum 