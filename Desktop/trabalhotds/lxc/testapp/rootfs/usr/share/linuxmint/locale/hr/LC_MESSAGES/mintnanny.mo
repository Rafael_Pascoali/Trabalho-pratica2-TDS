��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �  #        9     J     Z  q   g     �     �     	  ,                          
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-21 18:23+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nije valjan naziv domene. Blokiraj pristup odabranim domenama Blokirane domene Blokator domena Naziv domene Nazivi domena moraju započeti i završiti sa slovom ili brojem, i mogu samo sadržavati slova, brojeve i crtice. Primjer: moja.broj1domena.com Neispravna domena Roditeljska kontrola Upišite naziv domene koju želite blokirati 