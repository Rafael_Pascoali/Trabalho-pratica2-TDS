��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  &      3   '     [     m     �  �   �     -     J     b  ;   u                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-07-10 01:47+0000
Last-Translator: guwrt <guwrt77@gmail.com>
Language-Team: French (Canada) <fr_CA@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s n'est pas un nom de domaine valide. Bloquer l'accès aux noms de domaine sélectionnés Domaines bloqués Bloqueur de domaine Nom de domaine Les noms de domaine doivent commencer et finir par une lettre ou un chiffre, et ne peuvent contenir que des lettres, des chiffres, des points et des tirets. Exemple: mon.nom2domaine.com Nom de domaine invalide Contrôle parental Veuillez taper le nom de domaine que vous souhaitez bloquer 