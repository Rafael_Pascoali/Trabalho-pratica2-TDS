��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  F     G   _  #   �  !   �  '   �                    *     0     D  -   `     �     �  R   �            7   (  *   `     �     �     �     �     �     �     �  
   �     �     �  U     ?   c     �     �     �     �     �  	   �     �     �  I     	   V     `     e  "   k     �     �     �  %   �  N   �     A     M     \     e  
   i     t  ,   �  5   �     �     �     �                '     C     J  G   S  
   �     �     �     �  =   �  4   
  &   ?      f     �     �     �     �     �  )   �  :     !   J     l     r  	            4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-06-10 10:13+0000
Last-Translator: Davide Novemila <Unknown>
Language-Team: Latin <la@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: la
 %(downloadSize)s ex rete deducenda, %(localSize)s areae disci inanitae %(downloadSize)s ex rete deducenda, %(localSize)s areae disci quaesitae %(localSize)s areae disci liberatae %(localSize)s areae disci petitae %d actio operatur %d actiones operantur 3D Circa Additamenta Omnia Applicationes Omnes Operationes omnes perfectae Error evenit dum Flatpak thesaurus addebatur. Tabularis ludi Institui non potest Dum actiones operantur haec plica elaborari non potest.
Postquam desinent retenta. Removeri non potest Confabulationes Ad tuam relationem addendam <a href='%s'>huc</a> clica. In presenti fasciculi sequentes laborantur Singula Pictura Editoris Selectio Eruditio Electronica Cursus electronicus Aemulatores Necessaria Plicarum communio Prima-persona In presenti Flatpak non toleratur. Flatpak et gir1.2-flatpak-1.0 instituere pertenta. In presenti Flatpak non toleratur. Flatpak instituere pertenta. Typi Ludi Graphica Institue Novas applicationes institue Instituta Applicationes Institutae Instituuntur Hoc fasciculum instituere tuo systemate insanabile vulnus afferre potest. Interrete Java Pelle Quaestio ad indicem currentem fini Mathematica Multimediorum Codeca Multimediorum Codeca pro KDE Fasciculi congruentes non inveniuntur Nullus fasciculus ostenditurus.
Hic esse potest error - cachem renovare proba. Non paratus Non institutum Officina PHP Fasciculus Photographia Tolera quaeso. Hic diu consistere potest ... Ad hoc fasciculum instituendum apt-get utere, quaeso. Programmatura Promulgatio Python Vero-tempore res militaris Renova Indicem fasciculorim renova Remove Removens Hoc fasciculum removere tuo systemate insanabile vulnus afferre potest. Relationes Scansio Scientia Scientia et Eruditio In fasciculoroum descriptionibus quaere (quaestio lentissima) In fasciculoroum summariis quaere (quaestio lentior) Softwaris thesauri inspiciuntur, brevi Applicationes institutas ostende Simulatio atque cursus Administrator Softwaris Sonus Sonus atque visio Instrumenta systematis Flatpak thesaurus a te additurus iam est. In presenti nonnullae actuosae actiones sunt.
Exire visne? Partitis-temporibus res militaris Visio Ostenditores Interrete 