��          t      �              %   0     V     f     u  q   �     �             -   1  �  _       3        Q     g     v  �   �          <     O  1   a                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-05-31 15:23+0000
Last-Translator: kevin <kevin.abdullah.m@gmail.com>
Language-Team: Indonesian <id@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s bukan sebuah nama domain Blokir akses terhadap nama-nama domain yang dipilih Domain yang di blokir Pemblok Domain Nama domain Nama domain harus dimulai dan diakhiri dengan abjad atau angka, dan bisa hanya berisikan abjad, dan bisa berisikan abjad, angka, titik dan tanda penghubung. Contoh: my.number1domain.com Domain tidak valid Kontrol Orang tua Mohon ketikkan nama domain yang ingin Anda blokir 