��    2      �  C   <      H  ?   I  B   �  !   �  $   �                 5   (     ^  _   j     �     �     �  	   �     �  	   �     �  Z     C   a     �     �     �     �     �  	   �     �     �     �     
            +   %     Q  
   ]     h     {     �     �     �  3   �  *   �               /     ?  6   L     �     �     �  �  �  M   Y
  J   �
  6   �
  *   )     T     W     f  1   }     �  �   �  
   e  
   p     {     �     �     �     �  e   �  N   &     u  
   �     �     �     �     �     �  
   �     �     �            4        O     \  "   i  	   �     �     �     �  8   �  2        5     M     l     �  @   �     �  
   �          !      (   
      #       2                           &            1          %           0               "      /   +   '      	                       *                    ,                     )         .   -               $           %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories An error occurred attempting to add the Flatpak repo. Board games Cannot process this file while there are active operations.
Please try again after they finish. Chat Details Drawing Education Email Emulators File sharing Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools The Flatpak repo you are trying to add already exists. Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-09-18 15:27+0000
Last-Translator: MinhTran <Unknown>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Tải xuống %(downloadSize)s, giải phóng %(localSize)s không gian đĩa Tải xuống %(downloadSize)s, yêu cầu %(localSize)s không gian đĩa %(localSize)s không gian đĩa được giải phóng Yêu cầu %(localSize)s không gian đĩa 3D Giới thiệu Tiện ích bổ trợ Đã xảy ra lỗi khi đang thêm repo Flatpak. Những trò cờ Không thể xử lý tệp này trong khi có các hoạt động khác đang hoạt động.
Vui lòng thử lại sau khi các hoạt động ấy hoàn thành. Tán gẫu Chi tiết Vẽ Giáo dục Điện thư Trình giả lập Chia sẻ tập tin Hỗ trợ Flatpak hiện không khả dụng. Hãy thử cài đặt flatpak và gir1.2-flatpak-1.0. Hỗ trợ Flatpak hiện không khả dụng. Hãy thử cài đặt flatpak. Phông chữ Trò chơi Đồ họa Cài đặt Cài đặt ứng dụng mới Đã cài đặt Internet Không có Chưa cài đặt Văn phòng Gói Nhiếp ảnh Vui lòng dùng apt-get để cài đặt gói này. Lập trình Xuất bản Chiến thuật thời gian thực Gỡ bỏ Nhận xét Quét ảnh Khoa học và Giáo dục Tìm trong phần mô tả các gói (lâu hơn nhiều) Tìm trong phần tóm tắt các gói (lâu hơn) Mô phỏng và đua xe Trình Quản lý Phần mềm Âm thanh và phim ảnh Công cụ hệ thống Repo Flatpak mà bạn đang cố gắng thêm đã tồn tại. Chiến thuật theo lược Trình xem Web 