��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  #     $   *     O     l     �  S   �     �  	   �  :        =  #   Z     ~  %   �  d   �                                            	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-07 15:50+0000
Last-Translator: karm <melo@carmu.com>
Language-Team: Interlingua <ia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s plus spatio de disco sera usate. %s de spatio de disco sera liberate. %s sera discargate in total. Altere cambios es necessari Un error occurreva Impossibile remover le pacchetto %s perque illo es necessari pro altere pacchettos. Flatpaks Installar Le pacchetto %s es un dependentia del pacchettos sequente: Le pacchettos a ser removite Perspice le lista de cambios infra. Eliminar Le sequente pacchettos sera removite: Ce commando del menu non es associate a alcun pacchetto. Desira tu remover lo ab le menu comocunque? Promover 