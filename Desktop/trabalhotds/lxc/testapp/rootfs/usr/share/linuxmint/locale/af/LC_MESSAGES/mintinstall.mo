��    +      t  ;   �      �  ?   �  B   �  !   <  $   ^     �     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �     �  	              )     7     >     F     R  
   ^     i     |     �     �     �  3   �  *   �     	          0     @     M     a     i  �  m  C   -  E   q  #   �  %   �     	  	   	  	   	     	     '	     -	     :	  	   @	     J	     O	  
   [	  
   f	  
   q	     |	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     
     
  	   "
  
   ,
     7
  ,   N
  *   {
     �
     �
     �
     �
     �
                      "         !                    (                          +   '                %       *                                #       &                	          $                 )   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-10-24 05:08+0000
Last-Translator: Dawid de Jager <Unknown>
Language-Team: Afrikaans <af@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s om af te laai, %(localSize)s van skyf spasie bevry %(downloadSize)s om af te laai, %(localSize)s van skyf spasie benodig %(localSize)s van skyp spasie bevry %(localSize)s van skyf spasie benodig 3D Aangaande Toebehore Bordspeletjies Klets Besonderhede Teken Opvoeding Epos Emuleerders Leêr deel Skriftipes Speletjies Grafika Installeer Installeer nuwe program Geïnstalleer Internet Nie geinstalleer nie Kantoor Pakket Fotografie Programmering Publisering Intydse strategie Verwyder Resensies Skandering Wetenskap en Opvoeding Soek deur pakkies beskrywing (stadiger soek) Soek deur pakkies opsomming (stadige soek) Simulasie en resies Sagteware Bestuurder Klank en oudio Stelsel gereedskap Beur gebaseerde strategie Vertoonprogramme Web 