��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  /   #  7   S     �     �     �  -  �  	             "     @     [     {     �     �  $   �      �  Q   �     C  
   V     a     w     �     �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-08-27 12:04+0000
Last-Translator: Thomas Olsen <Unknown>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Det oppstod en feil ved kopiering av avtrykket. En feil oppstod ved opprettelse av en partisjon på %s. Det oppstod en feil. Autentiseringsfeil Velg et navn for din minnepinne FAT32 
   + Kompatibel overalt.
   - Kan ikke håndtere filer som er større enn 4 GB.

exFAT
   + Kompatibel nesten overalt.
   + Kan håndtere filer som er større enn 4 GB.
   - Ikke så kompatibel som FAT32.

NTFS
   + Kompatibel med Windows.
   - Ikke kompatibel med Mac og de fleste maskinvareenheter.
   - Av og til kompatibilitetsproblemer med Linux (NTFS er proprietær og omvendt konstruert).

EXT4
   + Moderne, stabil, rask, journalisert.
   + Støtter Linux-filtillatelser.
   - Ikke kompatibel med Windows, Mac og de fleste maskinvareenheter.
 Formatér Formater en minnepinne Lag en oppstartbar minnepinne Lag oppstartbar minnepinne Ikke nok plass på minnepinnen. Velg avtrykk Velg en minnepinne Velg et avtrykk Minnepinnen ble vellykket formatert. Avtrykket ble vellykket skrevet. Dette vil slette alle data på minnepinnen, er du sikker på at du vil fortsette? USB-avtrykkskriver Minnepinne Minnepinne-formaterer Minnepinne: Volum merkelapp Skriv ukjent 