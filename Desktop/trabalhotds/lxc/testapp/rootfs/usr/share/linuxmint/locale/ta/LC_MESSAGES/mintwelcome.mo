��          �      l      �  %   �  ]     l   e  
   �     �     �     �               %     @  �   G     �     �          !  �   2     �     �       �       �  ?  E  m  �     �	     
  ,   .
  "   [
     ~
  J   �
  L   �
     #  �  <  +   :  @   f  j   �  :     !  M     o  %   �  _   �              
                	                                                                    Add all the missing multimedia codecs Click the button below to discover the new features introduced in this version of Linux Mint. Click the button below to read about known bugs, limitations and workarounds for this version of Linux Mint. Contribute Documentation Driver Manager First Steps Help Install Multimedia Codecs Introduction to Linux Mint Launch Linux Mint is a great project. It is open to anyone who wants to participate. There are many ways to help. Click the button below to see how you can get involved. New Features Release Notes Show this dialog at startup System Snapshots The Linux Mint documentation consists of a collection of guides, available in PDF, ePUB and HTML and available in many languages. Click the button below to see which guides are available. Welcome Welcome Screen Welcome to Linux Mint Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-05-16 18:33+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:34+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 அனைத்து விடுபட்ட பல்லூடக கோடெக்குகளையும் சேர் லினக்ஸ் மின்டின் இந்தப் பதிப்பில் அறிமுகப்படுத்தப்பட்ட புதிய அம்சங்களைக் கண்டறிய கீழே உள்ள பொத்தானைக் கிளிக் செய்க. லினக்ஸ் மின்டின் இந்த பதிப்பில் அறியப்பட்ட பிழைகள், வரம்புகள் மற்றும் கடந்தவைகல் பற்றி படிப்பதற்கு கீழே உள்ள பொத்தானைக் கிளிக் செய்க. பங்களிக்க ஆவணமாக்கம் டிரைவர்  மேலாளர் முதல் படிகள் உதவி பல்லூடக கோடெக்குகளை நிறுவு லினக்ஸ் மின்ட்க்குஅறிமுகம் துவக்கம் லினக்ஸ் மின்ட் ஒரு அற்புதமான தயாரிப்பு. பங்கேற்க விரும்புபவர்களுக்கு அது திறந்திருக்கும். உதவ பல வழிகள் உள்ளன. நீங்கள் எப்படி ஈடுபடலாம் என்பதைப் பார்க்க கீழே உள்ள பொத்தானைக் கிளிக் செய்க. புதிய அம்சங்கள் வெளியீட்டு குறிப்புகள் இந்த உரையாடலைத் தொடங்கலின் போது காண்பி சிஸ்டம் ஸ்னாப்ஷாட்ஸ் லினக்ஸ் மின்ட் ஆவணங்கள் PDF ,ePUB மற்றும் HTML இல் கிடைக்கும் வழிகாட்டிகளின் தொகுப்பைக் கொண்டுள்ளன மேலும் பல மொழிகளில் கிடைக்கிறது. எந்த வழிகாட்டிகள் கிடைக்கின்றன என்பதை அறிய கீழுள்ள பொத்தானைக் கிளிக் செய்க. நல்வரவு வரவேற்பு திரை லினக்ஸ் மின்டிற்கு வரவேற்கின்றோம் 