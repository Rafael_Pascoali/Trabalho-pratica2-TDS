��    #      4  /   L        w   	     �     �     �     �     �     �  �   �     �     �     �  ^   �     &     =     [     d     k     t     �     �  	   �     �     �  ,   �     �     �          "     5     U     c     j     o  
   �  �  �  �   N  ;   >	  ;   z	     �	  M   �	  ,   #
  8   P
    �
  3   �  c   �  q   3  �   �  Y   �  J   �     >     Q     j  '   w  '   �     �     �  G   �  #   E  D   i     �     �  G   �  +   +  O   W  7   �     �  	   �  1   �     .                               
         !                            	       "      #                                                                            <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Telugu Thai Traditional Chinese Vietnamese Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-01-25 11:15+0000
Last-Translator: Siddharth Belbase <Unknown>
Language-Team: Nepali <ne@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>द्रष्टव्य: भाषाका प्याकहरू स्थापना वा उन्नयन  अतिरिक्त भाषा को ट्रिगर गर्न सकिनछ ! </small></b> एक नयाँ भाषा थप्नुहोस ! एक नयाँ भाषा थप्नुहोस ! थप्नुहोस्... व्यापक प्रणाली लागू गनुहोस् ! पूरै इन्स्टल भयो प्रविष्टि हुने तरिका इन्पुट मेथोदस बाट चिनिया, जापानि, कोरियन्, थाई अथवा भियत्नामी लेख्न सकिन्छ जुन किबोर्डमा दिएको हुँदैन । स्थापित गर्नुहोस् ! भाषा स्थापित गर्नुहोस वा हटाउनुहोस् ! भाषाहरु स्थापना गर्नुहोस् वा हटाउनुहोस् ... चयन गरिएको भाषाको निम्ति कुनै पनि हराएको भाषा पैक, अनुवाद फाइलहरू, शब्दकोश स्थापना गर्नुहोस भाषा प्याकहरू स्थापना गर्नुहोस् ! चयन भाषा स्थापित गर्नुहोस् ! जापनिज कोरियाली भाषा भाषा सेटिंग्स ! भाषा सेटिंग्स ! भाषा समर्थन भाषाहरु कुनै स्थानीय परिभाषित छैन ! कुनै पनि होइन अंक्, मुद्रा, ठेगाना,नापन... क्षेत्र हटाउनुहोस् चयन गरेको भाषा हटाउनुहोस् ! सरलीकृत चिनियाँ केहि भाषाका प्याकेजहरु छैनन । प्रणालीको स्थानीयता तेलुगु थाइ परम्परागत चिनियाँ भियतनामी 