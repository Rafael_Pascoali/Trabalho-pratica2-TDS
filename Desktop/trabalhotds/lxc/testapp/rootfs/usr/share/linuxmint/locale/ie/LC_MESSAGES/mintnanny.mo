��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  %   �  0        H     X     m  u   �     �          &  1   ;                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-01-04 21:29+0000
Last-Translator: Silvara <Unknown>
Language-Team: Interlingue <ie@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ne es un valid nómine de dominia. Blocar li accesse a selectet nómines de dominia Blocat dominias Blocator de dominias Nómine de dominia Nómines de dominias comense e fini se per un líttere o ciffre, e posse contener solmen lítteres, ciffres e tirés. Exemple: mi.numero1dominia.com Ínvalid dominia Supervision parental Provide li nómine de dominia quel vu vole blocar 