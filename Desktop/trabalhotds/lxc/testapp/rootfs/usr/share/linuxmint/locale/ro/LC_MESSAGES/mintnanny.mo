��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  $   �  )        C     S     c  �   p     �          ,  9   =                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-02-10 21:23+0000
Last-Translator: Flaviu <flaviu@gmx.com>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nu este un nume de domeniu valid. Blochează accesul la domeniile selectate Domenii blocate Blocare domenii Nume domeniu Numele de domenii trebuie să înceapă și să se termine cu o literă sau o cifră și pot conține doar litere, cifre, puncte și cratime. Exemplu: my.number1domain.com Domeniu invalid Control parental Tastează numele domeniului pe care vrei să îl blochezi 