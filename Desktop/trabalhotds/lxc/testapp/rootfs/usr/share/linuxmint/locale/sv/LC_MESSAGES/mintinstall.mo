��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  B   f  @   �  %   �           1  '   M     u     x  
   {  
   �     �     �     �  5   �  	   �            b        ~     �  D   �     �     �     �            
   %  
   0     ;  
   B     M  
   ^     i     y  h   �  Q   �     A     M     R     r  	   y  
   �     �     �     �     �  D   �          !     &  -   -  	   [     e     y     �     �  Y   �          3     C     R     a     u     y       <   �  8   �                    $     :     B     X     e     m  F   v     �     �  	   �     �  ;   �  1   7  2   i     �     �     �     �  4   �          $     3     ?  9   M  X   �     �     �          $  N   ?     �     �     �  
   �     �     �     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-05 17:59+0000
Last-Translator: Jan-Olof Svensson <jan-olof.svensson@abc.se>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s att ladda ner, %(localSize)s diskutrymme frigjort %(downloadSize)s att ladda ner, %(localSize)s diskutrymme krävs %(localSize)s av diskutrymme frigjort %(localSize)s diskutrymme krävs %d Recension %d Recensioner %d uppgift utförs %d uppgifter utförs 3D Om Tillbehör Lägg till Alla Alla program Alla åtgärder har slutförts Ett fel inträffade vid tillägg av Flatpak-förråd. Brädspel Gren: Kan ej installeras Kan inte behandla filen eftersom andra åtgärder utförs.
Vänligen vänta tills dessa är klara. Kan ej tas bort Chat Klicka <a href='%s'>här</a> för att lägga till en egen recension. Arbetar med följande paket Detaljer Dokumentation Teckning Redaktörens val Utbildning Elektronik E-post Emulatorer Nödvändigheter Fildelning Första-persons Flatpak (%s) Det finns just nu inget stöd för Flatpak. Prova att installera paketen flatpak och gir1.2-flatpak-1.0. Det finns just nu inget stöd för Flatpak. Prova att installera paketet flatpak. Teckensnitt Spel Genererar cache, var god vänta Grafik Webbplats Installera Installera nya program Installerade Installerade Program Installerar Att installera detta paket kan orsaka irreparabla skador i systemet. Internet Java Starta Begränsa sökningen till den aktuella listan Matematik Multimedia-tillägg Multimedia-tillägg för KDE Namn: Hittade inga matchande paket Inga paket att visa.
Detta kan tyda på ett problem - prova att läsa in cachen på nytt. Inga skärmbilder tillgängliga Ej tillgänglig Ej installerat Kontorsprogram Valbara komponenter PHP Paket Fotografering Var vänlig och ha lite tålamod. Det här kan ta ett tag... Var god använd apt-get för att installera detta paket. Programmering Publicering Python Realtids strategispel Läs om Hämta om paketlistan Fjärrdator: Ta bort Tar bort Att avinstallera detta paket kan orsaka irreparabla skador i systemet. Recensioner Bildinläsning Vetenskap Vetenskap och utbildning Sök i paketens beskrivningar (ännu långsammare sökning) Sök i paketens summering (långsammare sökning) Söker genom programvaruförråden, ett ögonblick Visa installerade program Simulering och racingspel Storlek: Programhanterare Någonting blev fel. Klicka för att försöka igen. Ljud Ljud och video Systempaket Systemverktyg Flatpak-förrådet du försöker lägga till finns redan. Det finns åtgärder som ännu inte har utförts.
Är du säker på att du vill avbryta? Detta är ett systempaket Detta är ett Flatpak-paket Försök igen Strategi enligt turordning Kan inte nå servrarna. Kontrollera din internetuppkoppling och försök igen. Inte tillgänglig Version: Video Bildvisare Webbläsare Pakethanteraren i ditt system 