��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  �     �   �  J   *  O   u  Q   �          (     B     b  !   o  5   �  f   �     .  *   G  �   r  #   %     I  c   g  4   �             "   (     K     b     y     �  
   �      �     �  �   �  q   h     �     �     �       9        T  +   s     �  �   �     O     `     e  D   x     �  %   �  .   �  8   '  �   `               5     >  
   B     M  f   b  ^   �     (     A     X  >   _     �  8   �     �       �        �     �  
   �  $   �  W   �  Y   N  P   �  <   �  *   6  #   a     �     �  '   �  u   �  a   C   #   �   
   �      �   %   �          4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-21 18:20+0000
Last-Translator: Nikolay Trifonov <trifonov88@gmail.com>
Language-Team: Bulgarian <bg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s ще бъдат свалени, ще бъдат освободени %(localSize)s дисково пространство %(downloadSize)s ще бъдат свалени, необходими са %(localSize)s дисково пространство %(localSize)s дисково пространство освободено Необходими са %(localSize)s дисково пространство %d задача се изпълнява %d задачи се изпълняват 3-измерни За програмата Помощни програми Всичко Всички приложения Всички операции са завършени Настъпи грешка по време на добавяне на Flatpak хранилището. Игри на дъска Не може да се инсталира Файла не може да бъде обработен докато има активни операции.
Моля, опитайте след като те завършат. Не мога да премахна Бързи съобщения Натиснете <a href='%s'>тук</a>, за да добавите собствено реяю. Работи се по следните пакети Подробности Рисуване Избор на редактора Образование Електроника Ел. поща Емулатори Важни Обмяна на файлове От първо лице Поддръжката на Flatpak не е налична. Опитайте да инсталирате flatpak и gir1.2-flatpak-1.0. Поддръжката на Flatpak не е налична. Опитайте да инсталирате flatpak. Шрифтове Игри Графика Инсталиране Инсталиране на нови приложения Инсталиран(а/о/и) Инсталирани приложения Инсталиране Инсталирането на този пакет може да предизвика непоправими щети по вашата система. Интернет Java Стартирай Ограничи търсенето до текущия списък Математика Мултимедийни кодеци Мултимедийни кодеци за KDE Не са открити съвпадащи пакети Няма пакети за показване.
Това може да е признак на проблем - опитайте да опресните кеша. Не е налично Не е инсталиран Офис PHP Пакет Фотография Моля бъдете търпеливи. Това може да отнеме малко време... Моля използвайте apt-get, за да инсталирате този пакет. Програмиране Публикуване Python Стратегически игри в реално време Опресняване Обновяване на списъка с пакети Премахване Премахване Премахването на този пакет ще предизвика непоправими щети по вашата система. Отзиви Сканиране Наука Наука и образование Търсене в описанието на пакетите (дори по-бавно) Търсене в резюмето на пакетите (по-бавно търсене) Търсене на софтуерни източници, един момент Покажи инсталираните приложения Симулации и надпревари Софтуерен мениджър Звук Звук и видео Системни инструменти Flatpak хранилището, което се опитвате да добавите вече съществува. Има активни операции.
Наистина ли искате да излезете? Походови стратегии Видео Визуализатори Мрежови инструменти 