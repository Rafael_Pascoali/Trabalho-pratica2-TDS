��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  -     -   3     a     v     �  �   �     6     U     j  @                          
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-12-09 09:01+0000
Last-Translator: Algent Albrahimi <algent@protonmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nuk është një emër domain i vlefshëm. Blloko hyrjen në erma të zgjedhur domeinesh Domeinet e bllokuara Bllokues i Domeineve Emri i domain-it Emrat e domenit duhet të fillojnë dhe të mbarojnë me një shkronjë ose një shifër dhe mund të përmbajnë vetëm shkronja, shifra, pika dhe viza. Shembull: my.number1domain.com Domain i pavlefshëm Kontrolli Prindëror Ju lutemi shkruani emrin e domain-it që dëshironi të bllokoni 