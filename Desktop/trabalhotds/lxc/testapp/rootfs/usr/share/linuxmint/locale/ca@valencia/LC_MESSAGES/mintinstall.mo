��    ,      |  ;   �      �  ?   �  B   	  !   L  $   n     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �       	        &     /     =     K     R  +   ^     �  
   �     �     �     �     �     �  3   �  *        A     ]     s     �     �     �     �     �  �  �  D   �  C   �  '   $	  &   L	     s	     v	  
   ~	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     

     %
     1
     :
     H
  
   W
  
   b
  6   m
     �
     �
     �
     �
  	   �
     �
     �
  8     +   @  !   l     �     �  
   �     �     �     �     �            "         !                    )                          ,   (                &       +                  %            #       '                  	          $                *   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Show installed applications Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-02-15 19:34+0000
Last-Translator: Juan Hidalgo-Saavedra <Unknown>
Language-Team: Catalan (Valencian) <ca@valencia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s per baixar, %(localSize)s d'espai de disc alliberat %(downloadSize)s per baixar, %(localSize)s d'espai de disc requerit %(localSize)s d'espai de disc alliberat %(localSize)s d'espai de disc requerit 3D Quant a Accessoris Jocs de tauler Xat Detalls Dibuix Correu electrònic Emuladors Compartició de fitxers Tipus de lletra Jocs Gràfics Instal·lar Instal·lar nous programes Instal·lat Internet No disponible No instal·lat Ofimàtica Fotografia Si us plau, useu apt-get per instal·lar aquest paquet Programació Autoedició Estratègia en temps real Elimina Ressenyes Escaneig Ciència i Educació Cercar en la descripció dels paquets (encara més lent) Cercar en el resum dels paquets (més lent) Mostrar aplicacions instal·lades Simulació i curses Gestor de programes So i video Eines del sistema Estratègia per torns Visors Web 