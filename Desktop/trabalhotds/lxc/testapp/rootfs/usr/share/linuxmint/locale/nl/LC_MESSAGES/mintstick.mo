��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  ?     B   �               7     J     a     m     y     �  !   �  i   �     8  W  P     �     �     �     �     �     �          (     /     ?     P     c  >   h     �     �  '   �     �     	  !        .     L     \  &   z     �     �     �  
   �     �                5     J     S  0   V  U   �  5   �  .        B  E   W  *   �  D   �  *     4   8  N   m  P   �  O     0   ]  &   �  W   �               7     C     `     m     �     �  7   �     �     �  9     	   G     Q     W     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-26 19:15+0000
Last-Translator: Pjotr12345 <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Er is een fout opgetreden bij het kopiëren van de schijfkopie. Er is een fout opgetreden bij het aanmaken van een partitie op %s. Er is een fout opgetreden Er is een fout opgetreden. Authenticatiefout. Bezig met berekenen... Controleren Controlesom Controlesombestanden Controlesom komt niet overeen Kies een naam voor uw USB-staafje Haal het ISO-schijfkopiebestand opnieuw binnen. De controlesom van het huidige bestand komt niet overeen. Alles ziet er goed uit! FAT32 
  + Altijd en overal verenigbaar met alles.
  -  Kan niet omgaan met bestanden groter dan 4GB.

exFAT
  + Bijna altijd verenigbaar met alles.
  + Kan omgaan met bestanden groter dan 4GB.
  -  Niet even verenigbaar als FAT32.

NTFS 
  + Verenigbaar met Windows.
  -  Niet verenigbaar met Mac en de meeste apparaten.
  -  Soms verenigbaarheidsproblemen met Linux (NTFS is niet-vrije gesloten code, en achterwaarts geanalyseerd).

EXT4 
  + Modern, stabiel, snel, met bestandendagboek.
  + Ondersteunt het rechtensysteem van Linux.
  -  Niet verenigbaar met Windows, Mac en de meeste apparaten.
 Bestandssysteem: Formatteren Een USB-staafje formatteren GB GPG-handtekeningen GPG-ondertekend bestand GPG-ondertekend bestand: Vorige ISO-verificatie ISO-schijfkopie: ISO-schijfkopieën ISO: Als u de handtekening vertrouwt, dan kunt u de ISO vertrouwen. Integriteitscontrole is mislukt KB Sleutel niet gevonden op sleutelserver. Plaatselijke bestanden MB Een opstartbaar USB-staafje maken Opstartbaar USB-staafje maken Meer informatie Geen opslagmedium-ID gevonden Niet genoeg ruimte op het USB-staafje. SHA256-controlesom SHA256-controlesommenbestand SHA256-controlesommenbestand: SHA256sum: Kies schijfkopie Kies een USB-staafje Kies een schijfkopie Ondertekend door: %s Grootte: TB De SHA256-som van de ISO-schijfkopie is onjuist. Het SHA256-controlesommenbestand bevat geen controlesommen voor deze ISO-schijfkopie. Het SHA256-controlesommenbestand is niet ondertekend. Het USB-staafje werd met succes geformatteerd. De controlesom klopt De controlesom klopt maar de echtheid van de som werd niet nagekeken. Het GPG-bestand kon niet worden nagekeken. Het GPG-bestand kon niet worden binnengehaald. Kijk het webadres na. De schijfkopie werd met succes geschreven. Het controlesommenbestand kon niet worden nagekeken. Het controlesommenbestand kon niet worden binnengehaald. Kijk het webadres na. Deze ISO-schijfkopie is geverifieerd met behulp van een vertrouwde handtekening. Deze ISO-schijfkopie werd "geverifieerd" door een niet-vertrouwde handtekening. Deze ISO ziet eruit als een Windows-schijfkopie. Dit is een officiële ISO-schijfkopie. Dit zal alle gegevens op het USB-staafje vernietigen; weet u zeker dat u wilt doorgaan? Webadressen Schijfkopieschrijver voor USB USB-staafje Formatteur voor USB-staafjes USB-staafje: Onbekende handtekening Niet-vertrouwde handtekening Verifiëren Verifieer de echtheid en integriteit van de schijfkopie Etiket voor opslagmedium: Opslagmedium: Windows-schijfkopieën vereisen een speciale behandeling. Schrijven bytes onbekend 