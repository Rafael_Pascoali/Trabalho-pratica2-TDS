��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  '     E   D  !   �     �     �  �   �     �     �     �  B   �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-10 15:31+0000
Last-Translator: انور الاسكندرانى <Unknown>
Language-Team: anwar AL_iskandrany <anwareleskndrany13@gmail.com >
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ar_EG
 اسم النطاق %s غير صالح. حظر الوصول إلى أسماء النطاقات المحددة النطاقات المحظورة مانع النطاق اسم النطاق يجب أن تبدأ أسماء النطاقات وتنتهي بحرف أو رقم، ويمكن أن تحتوي فقط على أحرف وأرقام ونقاط وواصلات. مثل: my.number1domain.com نطاق غير صالح الرقابة الأبوية يرجى كتابة اسم النطاق الذي تريد حظره 