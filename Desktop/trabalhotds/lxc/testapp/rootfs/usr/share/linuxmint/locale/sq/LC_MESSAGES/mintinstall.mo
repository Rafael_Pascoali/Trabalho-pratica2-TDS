��    -      �  =   �      �  ?   �  B   !  !   d  $   �     �     �     �     �     �     �     �  	   �     �  	   �     �                         %  	   >     H     Q     _     m     t     |  +   �     �  
   �     �     �     �     �     �  3     *   @     k     �     �     �     �     �     �  �  �  a   �  O   	  )   S	  +   }	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     
     
     !
  	   *
     4
     <
     V
  	   b
     l
     
     �
     �
  
   �
  =   �
  
   �
  	   �
     �
               +     3  B   G  9   �     �     �     �     �          &     /                $              !      (         -   "   ,              #                                        +                          &   	      
                *                    %      '           )          %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-07-05 22:26+0000
Last-Translator: Indrit Bashkimi <indrit.bashkimi@gmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s për tu shkarkuar, %(localSize)s hapësirë në disk e liruarof disk space freed %(downloadSize)s për tu shkarkuar, %(localSize)s hapësirë në disk kërkohet %(localSize)s hapësirë në disk u lirua %(localSize)s hapësirë në disk kërkohet 3D Rreth Aksesorët Lojëra board Bisedë Hollësitë Vizatim Edukimi Email Stimuluesit Shpërndarje skedarësh Gërmat Lojërat Grafikët Instalo Instalo programe të reja E instaluar Interneti Jo në dispozicion Nuk është instaluar Zyra Paketa Fotografia Ju lutemi përdorni apt-get që të instaloni këtë paketë. Programimi Publikimi Strategji në kohë reale Hiqe Përshtypjet Skanimi Shkenca dhe Edukimi Kërko në përshkrimin e paketave (kërkim akoma më i ngadaltë) Kërko në përmbledhjen e paketave (kërkim i ngadaltë) Stimulim dhe garë Menaxhuesi i Programeve Zë dhe video Mjetet e sistemit Strategji me ndryshime Shfaqës Web 