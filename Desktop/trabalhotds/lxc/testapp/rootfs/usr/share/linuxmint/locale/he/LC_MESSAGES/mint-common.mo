��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  1   �  &   1  .   X  (   �     �  e   �     ,  
   A  7   L     �  9   �     �  %   �  v     
   |                                       	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-08-04 16:14+0000
Last-Translator: Omer I.S. <Unknown>
Language-Team: Hebrew <he@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s נוספים מנפח הכונן ינוצלו. %s מנפח הכונן ישוחררו. תתבצע הורדה של %s בסך הכול. נדרשים שינויים נוספים אירעה שגיאה לא ניתן להסיר את החבילה %s בגלל שהיא נחוצה לחבילות אחרות. חבילות Flatpak התקנה החבילות הבאות תלויות בחבילה %s: חבילות להסרה נא לעיין ברשימת השינויים שלהלן. הסרה החבילות הבאות יוסרו: פריט תפריט זה לא קשור לחבילה כל שהיא. האם להסירו מהתפריט בכל מקרה? שדרוג 