��    =        S   �      8  >   9  =   x  3   �  !   �  K        X  6   f  4   �     �     �     �     �     �            #   %     I     `  "   �  $     *   (  "   S     v     �     �     �  !   �     �     �     �     �  u   �  	   E	     O	     U	  7   [	     �	     �	  	   �	  7   �	  -   �	  )   '
  !   Q
  (   s
  
   �
     �
     �
     �
     �
     �
     �
     �
  )        9     Y     o  )   u     �     �  #   �  �  �  `   �  M   �  ?   0  $   p  V   �     �  :     8   >     w     �     �     �     �     �  .   �  :   �  $   (  �   M  4   �  +   
  :   6  ?   q  '   �     �     �     �  .   �               "     %  �   )     �     �     �  @   �     (     5     G  :   T  1   �  #   �  )   �  7        G     W     _     d     y     �     �     �  3   �  (         I     g  X   w  
   �     �  *   �                     <      /          0           !                  :   '      1   6   "              )            *                ,                    2       7   $          3      4         #   9              8       5       =   (                ;   %              +   .   &   
   	              -    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> <i>Try to avoid spaces and special characters...</i> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-09-15 02:31+0000
Last-Translator: Saki <Unknown>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s chưa được thiết lập trong tập tin cấu hình ở các mục %(2)s hoặc %(3)s %(percentage)s trong số %(number)d tập tin - Đang tải lên %(service)s %(percentage)s của 1 tập tin - Đang tải lên %(service)s %(size_so_far)s trên %(total_size)s %(size_so_far)s trên %(total_size)s - Còn lại %(time_remaining)s (%(speed)s/giây) Thuộc tính của %s <b>Vui lòng nhập tên dịch vụ tải lên mới:</b> <i>Cố tránh ký tự trống và đặc biệt...</i> Giới thiệu B Hủy Hủy tải lên? Kiểm tra kết nối Đóng Không truy vấn không gian trống được Không lưu lại được những thay đổi cấu hình Chỉ định dịch vụ tải lên Thư mục tải lên. Sẽ thay thế <TIMESTAMP> bằng thời điểm hiện hành theo dạng thức đã định. Mặc định: . Bạn có muốn hủy bản tải lên này không? Kéo & thả vào đây để tải lên %s Tập tin lớn hơn không gian trống của dịch vụ Tập tin lớn hơn kích thước tối đa của dịch vụ Tập tin đã tải lên thành công. GB GiB Máy chủ: Tên máy hay địa chỉ IP, mặc định:  KB KiB MB MiB Mật khẩu, mặc dịnh: kết nối SCP không dùng mật khẩu, kết nối FTP dùng chuỗi rỗng, kết nối SFTP dùng khóa trong ~/.ssh Mật khẩu: Đường dẫn: Cổng: Cổng ở xa, mặc định là 21 cho FTP, 22 cho SFTP và SCP Chạy nền Tên dịch vụ: Dịch vụ: Tải %(number)d tập tin lên '%(service)s' thành công Tải 1 tập tin lên '%(service)s' thành công Đã hủy tải lên '%(service)s' Dịch vụ này yêu cầu mật khẩu. Dạng thức thời điểm (strftime). Mặc định: Thời điểm: Kiểu: URL: Dịch vụ lạ: %s Trình Quản lý Tải lên Trình Quản lí Tải lên... Dịch vụ tải lên Tải lên '%s' thất bại:  Đang tải %(number)d tập tin lên '%(service)s' Đang tải 1 tập tin lên %(service)s Đang tải tập tin lên... Người dùng: Tên người dùng, mặc định là tên người dùng cục bộ trên hệ thống _Tập tin Trợ _giúp đã triển khai kết nối thành công 