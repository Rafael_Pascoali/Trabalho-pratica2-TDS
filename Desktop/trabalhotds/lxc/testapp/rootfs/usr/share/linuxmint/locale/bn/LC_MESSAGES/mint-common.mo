��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �  Y   n  K   �  6     G   K  ?   �     �     �  C   �  d   @  "   �  a   �  �   *  "                               	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-06-23 12:05+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Bengali <bn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s আরো ডিস্কে স্থান ব্যবহার করা হবে ডিস্কে স্থান%s মুক্ত করা হবে। %s মোট ডাউনলোড করা হবে অতিরিক্ত পরিবর্তন প্রয়োজন একটি ত্রুটি দেখা দিয়েছে Flatpaks ইনস্টল করুন যে সব প্যাকেজ অপসারিত হবে নিম্নের পরিবর্তনের দিকে লক্ষ্য করুন। সরিয়ে ফেলুন নিম্নলিখিত প্যাকেজসমুহ অপসারিত হবে: এই মেনু আইটেম কোনো প্যাকেজের সাথে যুক্ত করা হয় না। আপনি কি মেনু থেকে এটিকে সরাতে চান? আপগ্রেড করুন 