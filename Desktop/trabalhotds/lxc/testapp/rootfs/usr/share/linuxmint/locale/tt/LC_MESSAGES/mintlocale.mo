��    "      ,  /   <      �  w   �     q     �     �     �     �     �     �     �  �   �     �     �     �  ^   �     "     9     W     `     g     p     �  	   �  	   �     �  ,   �     �     �     �          %     3     :     ?  �  G  �        �     �     �  ,   �     %	  !   7	  
   Y	     d	  �   z	     r
  )   y
  .   �
  �   �
  "   _  $   �     �     �     �     �     �          
       <        [     h  *   u  +   �     �     �  
   �     �               	      "                 !                       
                                                                                                 <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Cancel Fully installed Help Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language: Languages None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Some language packs are missing System locale Telugu Thai Welcome Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-11 00:51+0000
Last-Translator: Karolina Novosiltseva <Unknown>
Language-Team: Tatar <tt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>Искәрмә: тел пакетларын кую яисә яңарту өстәмә тел пакетларын куюга китерергә мөмкин</small></b> Яңа тел өстәү Яңа тел өстәү Өстәү... Бөтен системага куллану Баш тарту Тулысынча куелган Ярдәм Кертү ысулы Кертү ысуллары төймәсардә булмаган билгеләрне язу өчен кулланыла. Алар кытайча, японча, корейча, тайча, вьетнамча язганда файдалылар... Кую Телләр кую яисә бетерү Телләрне кую һәм бетерү... Сайланган телгә җитмәгән тел пакетларын, тәрҗемә файлларын, сүзлекләрне кую Тел пакетларын кую Сайланган телне кую Японча Корейча Тел Тел көйләүләре Тел көйләүләре Тел: Телләр Буш Саннар, валюта, адреслар, үлчәнеш Регион Бетерү Сайланган телне бетерү Кайбер тел пакетлары юк Система теле Телугча Тайча Рәхим итегез 