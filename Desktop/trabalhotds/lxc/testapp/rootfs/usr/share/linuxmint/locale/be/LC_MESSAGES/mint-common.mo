��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  I   %  D   o  /   �  6   �  h     G   �  C   �  O   	  C   `	  E   �	     �	  z   

     �
     �
     �
     �
  F   �
  &     ;   2     n          �  )   �  >   �  �     6   �     �                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-01 18:48+0000
Last-Translator: Anton Hryb <Unknown>
Language-Team: Belarusian <be@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Будзе дадаткова выкарыстана на дыску: %s. Будзе вызвалена дыскавай прасторы: %s. Усяго будзе спампавана: %s. Патрабуюцца дадатковыя змены Дадатковыя праграмы будуць вернуты да папярэдніх версій Дадатковыя праграмы будуць усталяваны Дадатковыя праграмы будуць ачышчаны Дадатковыя праграмы будуць пераўсталяваны Дадатковыя праграмы будуць выдалены Дадатковыя праграмы будуць абноўлены Адбылася памылка Немагчыма выдаліць пакет %s, таму што ён патрэбны для іншых пакетаў. Адкаціць Flatpak Флэтпакі Усталяваць Пакет %s залежыць ад наступных пакетаў: Пакеты для выдалення Паглядзіце на спіс зменаў ніжэй. Ачысціць Пераўсталяваць Выдаліць Прапусціць абнаўленне Наступныя пакеты будуць выдалены: Гэты элемент меню не звязаны ні з адным пакетам. Усё адно жадаеце выдаліць яго з меню? Абнаўленні будуць прапушчаны Абнавіць 