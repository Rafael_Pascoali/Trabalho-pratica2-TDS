��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �     	   �     �     �     �  1   �     #     ,     5  
   A     L     [     j          �     �  	   �     �     �     �     �  
   �     �     �     �     �                     9     @     ^     y  "   �     �  "   �  1   �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-28 12:57+0000
Last-Translator: Toni Estevez <toni.estevez@gmail.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Acerca de Acciones Permitir rutas Aplicaciones Área donde aparecen los iconos de estado de XApp Examinar Cancelar Categorías Categoría Elija un icono Predeterminada Icono predeterminado Dispositivos Emblemas Emoji Favoritos Icono Tamaño de icono Imagen Cargando... Tipos MIME Abrir Operación no admitida Otros Lugares Buscar Seleccionar Seleccionar un archivo de imagen Estado La categoría predeterminada. El icono usado por defecto Tamaño de icono preferido. La cadena que representa un icono. Si se permiten rutas  o no. Miniaplicación del estado de XApp Fábrica de la miniaplicación del estado de XApp 