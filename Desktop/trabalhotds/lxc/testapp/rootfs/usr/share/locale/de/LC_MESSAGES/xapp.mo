��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  4   �       	   +  
   5  	   @     J     `     h     v     ~     �  	   �     �     �     �     �  
   �     �     �     �     �        
             '     /  .   D     s  '   �     �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-27 17:34+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Über Aktionen Pfade erlauben Anwendungen Bereich, in dem XApp-Statussymbole angezeigt werden. Durchsuchen Abbrechen Kategorien Kategorie Ein Symbol auswählen Vorgabe Vorgabesymbol Geräte Embleme Emoji Favoriten Symbol Symbolgröße Bild Wird geladen … Mime-Typen Öffnen Vorgang wird nicht unterstützt Andere Orte Suchen Auswählen Bilddatei auswählen Zustand Die Vorgabekategorie Das Symbol, welches als Vorgabe verwendet wird Die bevorzugte Symbolgröße. Die Zeichenfolge stellt das Symbol dar. Ob Pfade erlaubt sind. XApp-Status-Applet XApp-Status-Applet-Fabrik 