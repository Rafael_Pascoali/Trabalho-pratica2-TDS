��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  =   �     &     .  	   6  	   @     J     Z     f     x     �     �  	   �     �     �     �     �  	   �     �     �     �     �       	             0     6  !   P  $   r  "   �     �     �      �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-25 17:42+0000
Last-Translator: Dragone2 <Unknown>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Informazioni su... Azioni Consenti percorsi Applicazioni Area in cui vengono visualizzate le icone di stato delle XApp Sfoglia Annulla Categorie Categoria Scegli un'icona Predefinita Icona predefinita Dispositivi Simboli Emoji Preferiti Icona Dimensioni icona Immagine Caricamento... Tipi Mime Apri Operazione non supportata Altro Risorse Cerca Seleziona Seleziona un file di immagine Stato La categoria predefinita. L'icona da usare come predefinita La dimensione preferita per le icone La stringa che rappresenta l'icona Se consentire i percorsi Applet di Stato XApp Fabbrica di Applet di Stato XApp 