��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  /        2     :  
   C  	   N     X     j     r     �     �     �  	   �     �     �     �     �  
   �     �     �             	     
        #     @     G     \     v     �  !   �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-12-10 15:42+0000
Last-Translator: Gilberto vagner <vagner.unix@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Sobre Ações Caminhos Permitidos Aplicativos A área onde o ícone do XApp status aparecerá Navegar Cancelar Categorias Categoria Escolha um ícone Padrão Ícone padrão Dispositivos Emblemas Emoji Favoritos Ícone Tamanho do ícone Imagem Carregando… Tipos MIME Abrir Operação não suportada Outros Locais Pesquisar Selecionar Selecionar arquivo da imagem Status A categoria padrão. O ícone para uso padrão O tamanho de ícone preferido O texto representando o ícone. Se necessário permitir caminhos. Applet XApp Status Fábrica do Applet XApp Status 