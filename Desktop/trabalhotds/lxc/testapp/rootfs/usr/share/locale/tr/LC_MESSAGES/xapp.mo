��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �     	   �     �     �     �  +   �                    '     0     A     M  	   _  	   i     s     z     �     �     �     �     �     �     �     �     �     �     �     �            &   (     O     k  $   �     �  %   �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-26 15:05+0000
Last-Translator: Butterfly <Unknown>
Language-Team: Turkish <tr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Hakkında Eylemler Yollara İzin Ver Uygulamalar XApp durum simgelerinin göründüğü alan Gözat İptal Kategoriler Kategori Bir simge seçin Varsayılan Varsayılan simge Aygıtlar Amblemler İfade Sık kullanılanlar Simge Simge boyutu Resim Yükleniyor... Mime türleri Aç İşlem desteklenmiyor Diğer Yerler Arama Seç Resim dosyası seç Durum Varsayılan kategori. Varsayılan olarak kullanılacak simge Tercih edilen simge boyutu. Simgeyi temsil eden dize. Yollara izin verilip verilmeyeceği. XApp Durum Uygulamacığı XApp Durum Uygulamacığı Üreticisi 