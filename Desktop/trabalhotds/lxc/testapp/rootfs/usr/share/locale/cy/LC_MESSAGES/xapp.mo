��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �  	   �  3   �     &     +     3     ?     H     T     a  	   t     ~     �  
   �     �     �     �     �     �     �  $   �     �                          )     0  (   H     q  "   �     �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-10-28 15:45+0000
Last-Translator: Rhoslyn Prys <Unknown>
Language-Team: Welsh <cy@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
Language: cy
 Ynghylch Gweithredoedd Caniatáu Llwybrau Rhaglenni Yr ardal lle mae'r eiconau statws XApp yn ymddangos Pori Diddymu Categorïau Categori Dewis eicon Rhagosodedig Eicon rhagosodedig Dyfeisiau Arwyddluniau Emoji Ffefrynnau Eicon Maint eicon Delwedd Yn llwytho... Mathau mime Agor Nid yw'r weithred yn cael ei chynnal Arall Mannau Chwilio Dewis Dewis ffeil delwedd Statws Y categori rhagosodedig Yr eicon i'w ddefnyddio drwy rhagosodiad Y maint eicon dewisol. Y llinyn sy'n cynrychioli'r eicon. P'un ai i ganiatáu llwybrau. Apled XApp Status Ffactori Apled XApp Status 