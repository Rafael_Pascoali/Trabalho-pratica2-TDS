��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �  
   �  +   �     +     7  
   @  
   K     V     c     o  
   �     �  	   �     �     �     �     �     �  	   �     �     �               %     .     6     N     U     m     �     �     �     �      �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-02-10 18:36+0000
Last-Translator: Adrián Bíro <biroadrian@protonmail.com>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
Language: sk
 O aplikácii Akcie Povoliť cesty Aplikácie Miesto kde sa objaví ikona pre XApp status Prehliadať Zrušiť Kategórie Kategória Výber ikony Predvolená Predvolená ikona Zariadenia Emblémy Emotikony Obľúbené položky Ikona Veľkosť ikony Obrázok Načítava sa… Mime typy Otvoriť Operácia nie je podporovaná Ostatné Miesta Hľadať Vybrať Vybrať súbor obrázku Status Predvolená kategória. Ikona použitá automaticky Preferovaná veľkosť ikony. Reťazec reprezentujúci ikonu. Či povoliť cesty. Stavový aplet aplikácií XApp Výchozie nastavenie apletu XApp 