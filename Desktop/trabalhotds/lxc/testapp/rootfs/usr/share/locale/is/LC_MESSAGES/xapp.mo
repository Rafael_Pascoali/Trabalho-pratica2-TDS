��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �  
   �     �     �  *   �          $     0     8     @     P     ]     s     y     �  
   �  	   �     �     �     �     �     �     �          	                    -     4  &   J     q  0   �     �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-01-12 17:39+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <is@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Um hugbúnaðinn Aðgerðir Leyfa slóðir Forrit Svæði þar sem XApp stöðutákn birtast Flakka Hætta við Flokkar Flokkur Veldu táknmynd Sjálfgefið Sjálfgefin táknmynd Tæki Merkimiðar Tjáningartákn - emoji Eftirlæti Táknmynd Táknmyndastærð Mynd Hleð inn... MIME tegundir Opna Aðgerðin er ekki studd Annað Staðir Leita Velja Veldu myndskrá Staða Sjálfgefinn flokkur. Táknmyndin sem skal nota sjálfgefið Kjörstærð táknmyndar. Textastrengurinn sem stendur fyrir táknmyndina. Hvort leyfa eigi slóðir. XApp stöðusmáforrit XApp stöðusmáforritasmiðja 