��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  .   �       
     
   '  	   2     <     R     a     v     �     �     �     �     �     �     �  
   �     �     �     �     �     �  
             -     3     P     k  "   �     �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-25 18:47+0000
Last-Translator: Isidro Pisa <isidro@utils.info>
Language-Team: Catalan <ca@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Quant a Accions Permet camins Aplicacions Àrea on es mostren les icones d'estat de XApp Navega Cancel·la Categories Categoria Seleccioneu una icona Predeterminada Icona predeterminada Dispositius Emblemes Emoji Favorits Icona Mida de la icona Imatge S'està carregant... Tipus MIME Obre Operació no admesa Altres Llocs Cerca Selecciona Selecciona el fitxer d'imatge Estat La categoria predeterminada. La icona usada per defecte La mida preferida de la icona. La cadena que representa la icona. Si es permeten camins o no. XApp Status Applet XApp Status Applet Factory 