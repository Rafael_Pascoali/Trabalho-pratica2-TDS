��    H      \  a   �            !     -     ?     R  	   a     k     z  ;   �     �     �     �     �     �       5   (  /   ^     �     �  /   �     �     �       .         =     ^     t     |  	   �     �     �     �     �     �     �  
   	     $	     ;	     D	  
   L	  
   W	     b	     w	     �	     �	     �	     �	     �	  C   �	  	   
     
  
   
     &
  #   3
     W
     f
     x
     �
  /   �
  A   �
                 	   0     :     N     l     |     �     �  <   �     �  �  �  	   �     �     �     �     �     �      �  ?        ]     d     |     �      �     �  2   �  -   �     -     @  2   H     {     �     �  /   �     �     	     %     -     I     V     e     �     �     �     �     �     �     �            
        '     ;     P     Y     e     l     t  \   �     �     �     �       4   !     V     d  	   v     �  4   �  :   �     �            
   2     =     Q     k     }     �     �  F   �     �                                             4      
   &      ?   3       1      $       C   )   2   A   :   ,   8   F         *   B      +   6       /   H   %                        -   7      E                    @           '   "   D   5          (            =   !   >       0       	      G             9             ;   <                         #   .    About Tilix Add terminal down Add terminal right Advanced Paste All Files All JSON Files Badges enabled=%b Be sure you understand what each part of this command does. Cancel Change Session Name Close Close session Convert CRLF and CR to LF Convert spaces to tabs Copying commands from the internet can be dangerous.  Could not load session due to unexpected error. Create a new session Default Disable input synchronization for this terminal Do not show this again Edit Encodings Edit Profile Enable input synchronization for this terminal Enter a new name for the session Error Loading Session Error:  Filename '%s' does not exist Find next Find previous Find text in terminal GTK Version: %d.%d.%d Keyboard Shortcuts Load Session Match as regular expression Match case Match entire word only Maximize Name… New Window New output New output displayed Notifications enabled=%b Open Open… Paste Preferences Quake Mode Not Supported Quake mode is not supported under Wayland, running as normal window Read-Only Save Save As… Save Session Search '%s' is not a valid regex
%s Search Options Synchronize Input Terminal Terminal bell There are multiple sessions open, close anyway? This command is asking for Administrative access to your computer Tilix Tilix Special Features Tilix version: %s Transform Triggers enabled=%b Unexpected exception occurred VTE version: %s Versions View session sidebar Wrap around Your GTK version is too old, you need at least GTK %d.%d.%d! disabled Project-Id-Version: tilix
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-18 01:41+0000
Last-Translator: Augusto Fornitani <augusto.fornitani@outlook.com>
Language-Team: Esperanto <https://hosted.weblate.org/projects/tilix/translations/eo/>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.2-dev
 Pri Tilix Aldoni terminalon suben Aldoni terminalon dekstren Speciala Algluo Ĉiuj Dosieroj Ĉiuj dosieroj JSON Bildsimbola nombrilo ŝaltita=%b Atentu, ke vi devas kompreni plene, kion ĉi tio komando faras. Nuligi Ŝanĝi nomon de seanco Fermi Fermi seancon Konverti CRLF-n kaj CR-n al LF-n Konverti spacojn al tabojn Kopii komandojn el interreto povas esti danĝere.  Ne eblis ŝargi seancon pro neatendita eraro. Krei novan seancon Implica Malŝalti enigan sinkronigon por ĉi tiu terminalo Ne montri ĉi tion denove Ŝanĝi Kodoprezentojn Modifi Profilon Ŝalti enigan sinkronigon por ĉi tiu terminalo Enigu novan nomon por seanco Eraro je Ŝargado de Seanco Eraro:  Dosiernomo '%s' ne ekzistas Trovi sekvan Trovi antaŭan Trovi tekston en terminalo Versio de GTK: %d.%d.%d Fulmoklavoj Ŝargi Seancon Kongrui kiel regula esprimo Atenti usklecon Kongrui nur tutajn vortojn Maksimumigi Nomo… Nova Fenestro Nova eligo Nova eligo montrata Sciigoj ŝaltitaj=%b Malfermi Malfermi… Alglui Agordoj "Quake"-reĝimo Nesubtenatas "Quake"-reĝimo ne funkcias ĉe Vejlando, tial la programo ruliĝas kvazaŭ normala fenestro Nurlega Konservi Konservi Kiel… Konservi Seancon La serĉo '%s' ne egalas validan regulan esprimon
%s Serĉo-opcioj Sinkronigi Enigon Terminalo Terminala pepo Pluraj seancoj estas malfermitaj, ja fermi ĉiukaze? Ĉi tiu komando petas Administran atingon al via komputilo Tilix Specialaj Ebloj de Tilix Versio de Tilix: %s Transformi Baskulo ŝaltita=%b Okazis neatendita escepto Versio de VTE: %s Versioj Vidi seancan flankpanelon Ĉirkaŭflui Via versio de GTK estas malnova, vi bezonas minimume version %d.%d.%d! malŝaltite 