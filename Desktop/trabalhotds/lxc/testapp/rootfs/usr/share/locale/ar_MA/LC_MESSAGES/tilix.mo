��    *      l  ;   �      �  '   �     �     �     �     �            0     	   D     N     \     d     k     �     �     �     �  
   �     �     �     �  
   �     �     �     �     �     �     �                     -     <     N     T     Y     b     r  <   {     �     �  �  �  6   �     	       
   1     <  
   R     ]  R   d     �     �     �     �  !   �     	     	     +	     8	     E	     a	      h	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     '
     ;
  
   W
     b
     o
     �
     �
  c   �
       &                             
      "                                 $      	      #                                     (   &                          '                    *   %              !          )        A VTE based terminal emulator for Linux Add Bookmark Application name Cancel Clear folder Columns Command Could not load bookmarks due to unexpected error Directory Edit Bookmark Error:  Folder GTK Version: %d.%d.%d Host Hostname ID Icon Icon title Name Notifications enabled=%b OK Parameters Path Protocol Remote Root Select Bookmark Select Folder Select Path Select folder Session name Session number Tilix version: %s Title User Username VTE version: %s Versions Your GTK version is too old, you need at least GTK %d.%d.%d! disabled translator-credits Project-Id-Version: tilix
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-04-08 17:39+0000
Last-Translator: كريم أولاد الشلحة <herr.linux88@gmail.com>
Language-Team: Arabic (Morocco) <https://hosted.weblate.org/projects/tilix/translations/ar_MA/>
Language: ar_MA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Weblate 2.13-dev
 محاكي طرفية مبني على VTE للينكس أضف علامة إسم التطبيق إلغاء أفرغ المجلد أعمدة أمر لم نتمكن من تحميل العلامات بسبب خلل غير متوقع مجلد حرر علامة خطأ:  مجلد إصدار جتك: %d 1.%d 2.%d 3 مستضيف إسم المستضيف المعرف أيقونة أيقونة العنوان إسم الإشعارات مفعلة=%d حسناً إعدادات مسار بروتوكول بعيد جذر حدد علامة حدد مجلد حدد مساراً حدد مجلد إسم الجلسة عدد الجلسة إصدار تيليكس: %s عنوان مستخدم إسم المستخدم إصدار VTE: %s إصدارات تملك إصدار جتك قديم, تحتاج على الأقل إلى إصدار %d 1.%d 2.%d 3! معطل Karim Oulad Chalha <karim@karimoc.com> 