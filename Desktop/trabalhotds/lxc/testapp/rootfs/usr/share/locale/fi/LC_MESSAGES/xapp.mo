��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �  	   �     �     �  $   �          
                     /     6     C  	   L     V     ]     f     m     |     �     �     �     �     �  	   �     �     �     �     �     �     �          4     R     h     z            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 16:19+0000
Last-Translator: Kimmo Kujansuu <Unknown>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Tietoja Toiminnot Salli polut Sovellukset Alue, jossa xapp-kuvakkeet näkyvät Selaa Peruuta Luokat Luokka Valitse kuvake Oletus Oletuskuvake Laitteet Tunnukset Hymiö Suosikit Kuvake Kuvakkeen koko Kuva Ladataan... Mime tyypit Avaa Toimenpide ei ole tuettu Muut Sijainnit Haku Valitse Valitse kuvatiedosto Tila Oletus luokka. Oletuksena käytettävä kuvake Haluttu kuvakkeen koko. Kuvaketta kuvaava merkkijono. Tuleeko polut sallia. Toimintatila xapp Sovelma xapp toimintatila 