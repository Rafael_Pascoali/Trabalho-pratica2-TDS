��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �  
   �  -   �     
       	     	   %     /     C     P     g     s     {     �     �     �     �     �     �     �     �     �     �       	             2     8  )   N  %   x      �     �     �  -   �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-12-20 10:45+0000
Last-Translator: Dorian Baciu <Unknown>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Despre Acțiuni Permite căi Aplicații Zona în care apar pictogramele de stare XApp Navigare Anulare Categorii Categorie Alege o pictogramă Implicit(ă) Pictogramă implicită Dispozitive Embleme Emoji Preferințe Pictogramă Mărime pictogramă Imagine Se încarcă... Tipuri MIME Deschide Operația nu este acceptată Alte Locații Căutare Selectare Selectare fişier imagine Stare Categoria implicită. Pictograma pentru a fi folosită implicit Dimensiunea preferată a pictogramei. Șirul reprezentând pictograma. Fie pentru a permite căi. XApp Status Applet Zona în care apar pictogramele de stare XApp 