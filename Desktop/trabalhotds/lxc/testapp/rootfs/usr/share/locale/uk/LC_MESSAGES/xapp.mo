��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  S   �     P     a     t     �  #   �     �  !   �     �     �               +     8     R     g     �     �  &   �     �  
   �  
   �     �  *   �      	      )	  N   J	  )   �	  9   �	  #   �	     !
  /   @
            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-05-15 08:00+0000
Last-Translator: Mykola Tkach <chistomov@gmail.com>
Language-Team: Ukrainian <uk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Про Дії Дозволяти шляхи Додатки Ділянка, де з'являється піктограма статусу XApp Перегляд Скасувати Категорії Категорія Оберіть піктограму Типово Типова піктограма Пристрої Емблеми Емодзі Улюблені Іконка Розмір іконки Зображення Завантаження... Типи MIME Відкрити Дія не підтримується Інше Місця Пошук Обрати Обрати файл зображення Стан Типова категорія. Піктограма, яка використовується усталено Бажаний розмір іконки. Рядок, який представляє іконку. Чи дозволяти шляхи. Аплет статусу XApp Фабрика аплета статусу XApp 