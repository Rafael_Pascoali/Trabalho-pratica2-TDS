��          �            h     i     x     �     �     �     �     �  	   �     �     �     �     �       <     �  R     �     �                    1     L     U     b  %   o     �     �     �  R   �        	   
                                                                     Edit Encodings Edit Profile Encoding Error:  Find previous GTK Version: %d.%d.%d Profiles Read-Only Reset Tilix Special Features Tilix version: %s VTE version: %s Versions Your GTK version is too old, you need at least GTK %d.%d.%d! Project-Id-Version: tilix
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-08-29 22:51+0000
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Vietnamese <https://hosted.weblate.org/projects/tilix/translations/vi/>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 2.17-dev
 Chỉnh sửa mã hóa Chỉnh sửa hồ sơ Mã hóa Lỗi:  Tìm trước đó GTK Phiên bản: %d.%d.%d Hồ sơ Chỉ đọc Đặt lại Tính năng đặc biệt của Tilix Phiên bản Tilix: %s Phiên bản VTE: %s Phiên bản Phiên bản GTK của bạn là quá cũ, bạn cần tối thiểu GTK %d.%d.%d! 