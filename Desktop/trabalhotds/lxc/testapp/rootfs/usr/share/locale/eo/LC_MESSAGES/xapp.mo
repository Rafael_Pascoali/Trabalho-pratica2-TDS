��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �  
   �  ,   �            
     	   '     1  	   E     O     e     n     w          �     �     �     �  
   �     �     �     �     �     �     �                    4      S  (   t     �     �  &   �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-03-04 16:22+0000
Last-Translator: Piet Coppens <Unknown>
Language-Team: Esperanto <eo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Pri Agoj Permesi dosierindikojn Aplikaĵoj Areo, kie aperas bildsimboloj pri xapa stato Foliumi Nuligi Kategorioj Kategorio Elektu bildsimbolon Defaŭlto Defaŭlta bildsimbolo Aparatoj Emblemoj Emoĝio Plej ŝatataj Bildsimbolo Bildsimbola grando Bildo Ŝargado... Mime-tipoj Malfermi Operacio ne subtenata Alia Lokoj Serĉi Elekti Elektu bildodosieron Stato La defaŭlta kategorio. La bildsimbolo uzita defaŭlte La preferita bildsimbola grando. La ĉeno, kiu prezentas la bildsimbolon. Ĉu permesi dosierindikojn. Aplikaĵeto pri xapa stato Fabriko por aplikaĵeto pri xapa stato 