��    #      4  /   L           	               #  #   0     T     [  
   b     m     v     �     �     �     �     �     �  	   �     �  
   �  
   �     �     �     �     �     �     �               *     E  !   ^     �     �     �  �  �     ^     b     i  	   {  $   �     �  
   �     �  	   �     �     �     �  	   �          	               "     (  
   <     G     P     V     ]     e     j     z     �  !   �     �  "   �     �          +                 "       !                                        	         #                                                           
                         About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Icon Icon size Image Loading... Mime types Open Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-06-08 07:08+0000
Last-Translator: Shane Griffiths <Unknown>
Language-Team: Afrikaans <af@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Oor Aksies laat paadjies toe Programme Gebied waar XApp-statusikone verskyn Bespeur Kanselleer Kategorieë Kategorie Kies 'n ikoon Verstek Verstek Ikoon Toestelle Embleme Emoji Ikoon Ikoongrootte Beeld Besig om te laai... Mime tipes Maak Oop Ander Plekke Soektog Kies Kies beeldlêer Status Die standaardkategorie. Die Ikoon om standaard te gebruik Die verkiesde ikoongrootte Die string wat die ikoon voorstel. Of om paaie toe te laat of nie. XApp-statusapplet XApp-statusapplet-fabriek 