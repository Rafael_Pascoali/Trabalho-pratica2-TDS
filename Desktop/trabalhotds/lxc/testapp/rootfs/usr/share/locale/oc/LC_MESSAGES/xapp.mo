��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �     	   �     �     �     �  /   �  
   #     .  
   6  	   A     K  
   ]     h  
   z  	   �     �     �     �     �     �     �  
   �     �     �     �     �                     ?     E  "   [     ~  !   �     �     �  "   �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-12-12 11:26+0000
Last-Translator: Quentin PAGÈS <Unknown>
Language-Team: Occitan (post 1500) <oc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 A prepaus Accions Permetre los camins Aplicacions Zòna ont las icònas d’estat Xapp apareisson Percórrer Anullar Categorias Categoria Causir una icòna Per defaut Icòna per defaut Periferics Emblèmas Emoji Favorits Icòna Talha d'icòna Imatge Cargament... Tipes MIME Dobrir Operacion pas presa en carga Autre Emplaçaments Recercar Seleccionar Seleccionar un fichièr imatge Estat Categoria per defaut. L’icòna d’utilizar per defaut La talha d’icòna preferida. Lo tèxt representant l’icòna. Se cal permetre los camins. Applet d’estat XApp Fabrica d’applets d’estat XApp 