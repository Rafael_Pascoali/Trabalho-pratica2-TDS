��          t      �         1     G   C     �     �     �     �     �          1  "   Q  �  t  5     Q   <  '   �  2   �  '   �          /  +   G  #   s  =   �        
                  	                                  Add a new repository and install packages from it Add a new repository of purchased software and install packages from it Cancel the task of another user Change software configuration Change software repository Install or remove packages Install package file List keys of trusted vendors Remove downloaded package files Set a proxy for software downloads Project-Id-Version: aptdaemon
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-08-28 18:25+0000
Last-Translator: Ali Ismayilov <Unknown>
Language-Team: Azerbaijani <az@li.org>
Language: az
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2013-03-09 23:28+0000
X-Generator: Launchpad (build 16523)
 Yeni repositor əlavə et və ordan paketlər yüklə Alınmış proqramların yeni repositorun əlavə et və ordan paketləri yüklə Başqa istifadəçinin işini dayandır Proqram təminatının konfiqurasiyasını dəyiş Proqram təminatı repositorunu dəyiş Paketləri yüklə və ya sil Paket faylını yüklə İnanılmış vendorların açar siyahısı Yüklənmiş paket fayllarını sil Proqram təminatını yükləmək üçün proxy müəyyən et 